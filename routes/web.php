<?php
Route::get('/','Auth\LoginController@getLogin');
//ADMIN + CUSTOMER LOGOUT:
Route::get('logout',['as'=>'get.logout','uses'=>'Auth\LogoutController@logout']);

Route::group(['middleware'=>['islogin']],function(){
//CUSTOMER + ADMIN LOGIN INTO SYSTEM:
    Route::get('login',['as'=>'get.login','uses'=>'Auth\LoginController@getLogin']);
    Route::post('login',['as'=>'post.login','uses'=>'Auth\LoginController@postLogin']);

//CUSTOMER + ADMIN FOGOT PASSWORD:
    Route::get('forgot',['as'=>'get.forgot','uses'=>'Auth\ForgotPasswordController@getForgot']);
    Route::post('forgot',['as'=>'post.forgot','uses'=>'Auth\ForgotPasswordController@postForgot']);

//CUSTOMER REGISTER:
    Route::get('register',['as'=>'get.register','uses'=>'Auth\RegisterController@getRegister']);
    Route::post('register',['as'=>'post.register','uses'=>'Auth\RegisterController@postRegister']);
    Route::get('active/{code}/{name}',['as'=>'get.active','uses'=>'Auth\RegisterController@getActive']);

    //CUSTOMER + ADMIN RESETPASSWORD:
    Route::get('resetpassword/{code}/{email}',['as'=>'get.resetpassword','uses'=>'Auth\ResetPasswordController@getResetPassword']);

});
//***************************************ROUTE FOR ADMIN:
Route::group(['prefix'=>'admin','middleware'=>['checklogin','getSidebarData','adminInfo']],function (){
    Route::get('dashboard',['as'=>'admin.dashboard','uses'=>'Admin\DashboardController@getHome']);
    Route::post('fillter',['as'=>'admin.dashboard.fillter','uses'=>'Admin\DashboardController@fillter']);
    //MEMBER MANAGE
    Route::group(['prefix'=>'member','middleware'=>['AcceptRoleAdminManager']],function (){
        Route::get('listmember',['as'=>'get.member.list','uses'=>'Admin\AdminController@getList']);

        Route::get('addmember',['as'=>'get.member.add','uses'=>'Admin\AdminController@getAdd']);
        Route::post('addmember',['as'=>'post.member.add','uses'=>'Admin\AdminController@postAdd']);

        Route::get('editmember/{id}',['as'=>'get.member.edit','uses'=>'Admin\AdminController@getEdit']);
        Route::post('editmember/{id}',['as'=>'post.member.edit','uses'=>'Admin\AdminController@postEdit']);

        Route::get('deletemember/{id}',['as'=>'get.member.delete','uses'=>'Admin\AdminController@getDelete']);
        Route::get('detailmember/{id}',['as'=>'get.member.detail','uses'=>'Admin\AdminController@getDetail']);
        Route::get('change-status-user/{id}','Admin\AdminController@changeStatus');

        Route::get('change-password','Admin\AdminController@getFormChangePassword');
        Route::post('change-password','Admin\AdminController@postChangePassword');

        Route::post('admin-change-user-password/{id}','Admin\AdminController@adminChangeUserPassword');
        Route::post('admin-change-admin-password/{id}','Admin\AdminController@adminChangeAdminPassword');


    });
    //CUSTOMER MANAGE:
    Route::group(['prefix'=>'customer','middleware'=>['AcceptRoleCustomerManager']],function (){
        Route::get('listcustomer',['as'=>'get.customer.list','uses'=>'Admin\CustomerController@getList']);

        Route::get('addcustomer',['as'=>'get.customer.add','uses'=>'Admin\CustomerController@getAdd']);
        Route::post('addcustomer',['as'=>'post.customer.add','uses'=>'Admin\CustomerController@postAdd']);

        Route::get('editcustomer/{id}',['as'=>'get.customer.edit','uses'=>'Admin\CustomerController@getEdit']);
        Route::post('editcustomer/{id}',['as'=>'post.customer.edit','uses'=>'Admin\CustomerController@postEdit']);

        Route::get('deletecustomer/{id}',['as'=>'get.customer.delete','uses'=>'Admin\CustomerController@getDelete']);
        Route::get('detailcustomer/{id}',['as'=>'get.customer.detail','uses'=>'Admin\CustomerController@getDetail']);
        Route::get('change-status-user/{id}','Admin\CustomerController@changeStatus');

        Route::get('change-password','Admin\AdminController@getFormChangePassword');
        Route::post('change-password','Admin\AdminController@postChangePassword');

        Route::post('admin-change-user-password/{id}','Admin\CustomerController@adminChangeUserPassword');

    });
    //SERVICE MANAGE:
    Route::group(['prefix'=>'service','middleware'=>['AcceptRoleServiceManager']],function (){
        Route::get('listservice',['as'=>'get.service.list','uses'=>'Admin\ServiceController@getList']);

        Route::get('addservice',['as'=>'get.service.add','uses'=>'Admin\ServiceController@getAdd']);
        Route::post('addservice',['as'=>'post.service.add','uses'=>'Admin\ServiceController@postAdd']);

        Route::get('editservice/{id}',['as'=>'get.service.edit','uses'=>'Admin\ServiceController@getEdit']);
        Route::post('editservice/{id}',['as'=>'post.service.edit','uses'=>'Admin\ServiceController@postEdit']);

        Route::get('deleteservice/{id}',['as'=>'get.service.delete','uses'=>'Admin\ServiceController@getDelete']);
        Route::get('detailservice/{id}',['as'=>'get.service.detail','uses'=>'Admin\ServiceController@getDetail']);

        Route::get('change-status-user/{id}','Admin\ServiceController@changeStatus');

    });
    //ORDER MANAGE:
    Route::group(['prefix'=>'order','middleware'=>['AcceptRoleOrderManager']],function (){
        Route::get('listorder',['as'=>'get.order.list','uses'=>'Admin\OrderController@getList']);

        Route::get('addorder',['as'=>'get.order.add','uses'=>'Admin\OrderController@getAdd']);
        Route::post('addorder',['as'=>'post.order.add','uses'=>'Admin\OrderController@postAdd']);

        Route::get('editorder/{id}',['as'=>'get.order.edit','uses'=>'Admin\OrderController@getEdit']);
        Route::post('editorder/{id}',['as'=>'post.order.edit','uses'=>'Admin\OrderController@postEdit']);

        Route::get('deleteorder/{id}',['as'=>'get.order.delete','uses'=>'Admin\OrderController@delete']);
        Route::get('detailorders/{id}',['as'=>'get.orders.detail','uses'=>'Admin\OrderController@getDetails']);

        Route::get('change-status-user/{id}','Admin\OrderController@changeStatus');
        Route::get('active-order/{id}','Admin\OrderController@active');
    });
    //SUPPORT MANAGE:
    Route::group(['prefix'=>'support','middleware'=>['AcceptRoleSupportManager']],function (){
        Route::get('listsupport',['as'=>'get.support.list','uses'=>'Admin\SupportController@getList']);

        Route::get('addsupport',['as'=>'get.support.add','uses'=>'Admin\SupportController@getAdd']);
        Route::post('addsupport',['as'=>'post.support.add','uses'=>'Admin\SupportController@postAdd']);

        Route::get('editsupport/{id}',['as'=>'get.support.edit','uses'=>'Admin\SupportController@getEdit']);
        Route::post('editsupport/{id}',['as'=>'post.support.edit','uses'=>'Admin\SupportController@postEdit']);

        Route::get('deletesupport/{id}',['as'=>'get.support.delete','uses'=>'Admin\SupportController@getDelete']);
        Route::get('detailsupport/{id}',['as'=>'get.support.detail','uses'=>'Admin\SupportController@getDetail']);
        Route::get('changestatus/{id}',['as'=>'get.support.changest','uses'=>'Admin\SupportController@changeStatus']);
        Route::post('postAddfeedBack',['as'=>'post.support.addFeedBack','uses'=>'Admin\SupportController@postAddfeedBack']);

    });

    /*SEARCH*/
    Route::group(['prefix'=>'search'],function (){
        Route::get('search','Admin\SearchController@searching');
    });

    /*TYPE OF SERVICE*/
    Route::group(['prefix'=>'type','middleware'=>['AcceptRoleServiceManager']],function (){
        Route::get('type','Admin\TypeController@listType');
        Route::get('typeEdit/{id}','Admin\TypeController@editType');
        Route::post('typeUpdate/{id}','Admin\TypeController@updateType');
        Route::get('typeDelete/{id}','Admin\TypeController@deleteType');
        Route::get('typeAdd','Admin\TypeController@addType');
        Route::post('typeStore','Admin\TypeController@storeType');
    });

    /*POST MANAGER*/
    Route::group(['prefix'=>'post','middleware'=>['AcceptRolePostManager']],function (){
        Route::get('post','Admin\PostController@getList');
        Route::get('post-add','Admin\PostController@getViewAdd');
        Route::post('post-store','Admin\PostController@postStore');
        Route::get('post-edit/{id}/{slug}','Admin\PostController@getEditView');
        Route::post('post-update/{id}','Admin\PostController@postUpdate');
        Route::get('post-delete/{id}','Admin\PostController@deletePost');
    });

    /*OPTION*/
    Route::group(['prefix'=>'option','middleware'=>['AcceptRoleOptionManager']],function (){
        Route::get('index','Admin\UserOptionController@index');
        Route::get('insert','Admin\UserOptionController@insert');
        Route::post('store','Admin\UserOptionController@store');
        Route::get('edit/{id}','Admin\UserOptionController@edit');
        Route::post('update/{id}','Admin\UserOptionController@update');
        Route::get('delete/{id}','Admin\UserOptionController@delete');
    });

    /*EMAIL_MARKETING*/
    Route::group(['prefix'=>'email-marketing','middleware'=>['AcceptRoleEmailMarketingManager']],function (){
        Route::group(['prefix'=>'email'],function(){
            Route::get('index','Admin\EmailMarketing\EmailController@index');
            Route::get('insert','Admin\EmailMarketing\EmailController@insert');
            Route::post('store','Admin\EmailMarketing\EmailController@store');
            Route::get('edit/{id}','Admin\EmailMarketing\EmailController@edit');
            Route::get('search','Admin\EmailMarketing\EmailController@search');
            Route::get('filter/{id}','Admin\EmailMarketing\EmailController@filter');
            Route::post('update/{id}','Admin\EmailMarketing\EmailController@update');
            Route::get('delete/{id}','Admin\EmailMarketing\EmailController@delete');
        });
        Route::group(['prefix'=>'group'],function(){
            Route::get('index','Admin\EmailMarketing\GroupController@index');
            Route::get('insert','Admin\EmailMarketing\GroupController@create');
            Route::post('store','Admin\EmailMarketing\GroupController@store');
            Route::get('edit/{id}','Admin\EmailMarketing\GroupController@edit');
            Route::get('search','Admin\EmailMarketing\GroupController@search');
            Route::get('show/{id}','Admin\EmailMarketing\GroupController@show');
            Route::post('update/{id}','Admin\EmailMarketing\GroupController@update');
            Route::get('delete/{id}','Admin\EmailMarketing\GroupController@delete');
        });
        Route::group(['prefix'=>'campaign'],function(){
            Route::get('index','Admin\EmailMarketing\CampaignController@index');
            Route::get('insert','Admin\EmailMarketing\CampaignController@insert');
            Route::post('store','Admin\EmailMarketing\CampaignController@store');
            Route::get('edit/{id}','Admin\EmailMarketing\CampaignController@edit');
            Route::get('search','Admin\EmailMarketing\CampaignController@search');
            Route::get('show/{id}','Admin\EmailMarketing\CampaignController@show');
            Route::get('run/{id}','Admin\EmailMarketing\CampaignController@run');
            Route::post('action/{id}','Admin\EmailMarketing\CampaignController@action');
            Route::post('update/{id}','Admin\EmailMarketing\CampaignController@update');
            Route::get('delete/{id}','Admin\EmailMarketing\CampaignController@delete');
        });
        /*template*/
        /*autorespondse*/
        /*analystic*/
    });
});
//***************************************ROUTE CUSTOMER:
Route::group(['prefix'=>'customer','middleware'=>['checklogin','getSidebarDataUser','userInfo']],function (){
    Route::get('dashboard',['as'=>'customer.dashboard','uses'=>'Customer\DashboardController@getHome']);
    Route::get('profile','Customer\ProfileController@index');
    Route::get('profile-edit/{id}','Customer\ProfileController@edit');
    Route::post('profile/{id}','Customer\ProfileController@update');
    Route::post('changepassword/{id}','Customer\ProfileController@changePassword');

    Route::get('faq','Customer\DashboardController@faq');

    Route::post('agree-term','Customer\DashboardController@agreeTerms');

    Route::group(['prefix'=>'service'],function (){
        Route::get('listservice',['as'=>'get.user.service.list','uses'=>'Customer\ServiceController@getList']);

        Route::get('addservice',['as'=>'get.user.service.add','uses'=>'Customer\ServiceController@getAdd']);
        Route::post('addservice',['as'=>'post.user.service.add','uses'=>'Customer\ServiceController@postAdd']);

        Route::get('editservice/{id}',['as'=>'get.user.service.edit','uses'=>'Customer\ServiceController@getEdit']);
        Route::post('editservice/{id}',['as'=>'post.user.service.edit','uses'=>'Customer\ServiceController@postEdit']);

        Route::get('deleteservice/{id}',['as'=>'get.user.service.delete','uses'=>'Customer\ServiceController@getDelete']);
        Route::get('detailservice/{id}',['as'=>'get.user.service.detail','uses'=>'Customer\ServiceController@getDetail']);

    });
    //ORDER MANAGE:
    Route::group(['prefix'=>'order'],function (){
        Route::get('listorder',['as'=>'get.order.list','uses'=>'Customer\OrderController@getList']);

        Route::get('addorder',['as'=>'get.order.add','uses'=>'Customer\OrderController@getAdd']);
        Route::post('addorder',['as'=>'post.order.add','uses'=>'Customer\OrderController@postAdd']);

        Route::get('detailorder/{id}',['as'=>'get.order.detail','uses'=>'Customer\OrderController@getDetail']);

        Route::get('checkout','Customer\OrderController@getCheckOut');
        Route::post('checkout','Customer\OrderController@postCheckOut');
    });
    //SUPPORT MANAGE:
    Route::group(['prefix'=>'support'],function (){
        Route::get('listsupport',['as'=>'get.support.list','uses'=>'Customer\SupportController@getList']);

        Route::get('addsupport',['as'=>'get.support.add','uses'=>'Customer\SupportController@getAdd']);
        Route::post('addsupport',['as'=>'post.support.add','uses'=>'Customer\SupportController@postAdd']);

        Route::get('editsupport/{id}',['as'=>'get.support.edit','uses'=>'Customer\SupportController@getEdit']);
        Route::post('editsupport/{id}',['as'=>'post.support.edit','uses'=>'Customer\SupportController@postEdit']);

        Route::get('deletesupport/{id}',['as'=>'get.support.delete','uses'=>'Customer\SupportController@getDelete']);
        Route::get('detailsupport/{id}',['as'=>'get.support.detail','uses'=>'Customer\SupportController@getDetail']);
        Route::post('postAddfeedBack',['as'=>'post.support.addFeedBack','uses'=>'Customer\SupportController@postAddfeedBack']);
        Route::get('changestatus/{id}',['as'=>'get.support.changest','uses'=>'Customer\SupportController@changeStatus']);

    });
    Route::get('post/{id}','Customer\DashboardController@getPost');

});
Route::get('testTime','Admin\OrderController@test');