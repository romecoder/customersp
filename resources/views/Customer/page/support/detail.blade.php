@extends('Customer.master')
@section('title','Dashboard')
@section('headlink')
    <link href="{{asset('CS2/plugins/material-preloader/css/materialPreloader.min.css')}}" rel="stylesheet">
    <script src="{{asset('CS2/plugins/vertical-timeline/js/modernizr.js')}}"></script>
    <link href="{{asset('CS2/plugins/vertical-timeline/css/style.css')}}" rel="stylesheet">
@endsection
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title">Lịch sử Hỗ trợ:</div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
            </div>
            <div class="col s12">

                <section id="cd-timeline" class="cd-container">
                    @if($listDetailSupport)
                        @foreach($listDetailSupport as $detailSupport)
                            <div class="cd-timeline-block">
                                @if($detailSupport->author == 1)
                                    <div class="cd-timeline-img cd-picture">
                                        <img src="{{asset('CS2/images/avatar').'/'.$thisSupport->admin_id->avatar}}"
                                             alt="Picture">
                                    </div> <!-- cd-timeline-img -->
                                    <div class="cd-timeline-content">
                                        <p>{{$detailSupport->content}}</p>
                                        <span class="cd-date">{{$detailSupport->create_at}}</span>
                                    </div> <!-- cd-timeline-content -->
                                @else
                                    <div class="cd-timeline-img cd-movie">
                                        <img src="{{asset('CS2/images/avatar').'/'.Auth::user()->avatar}}"
                                             alt="Movie">
                                    </div> <!-- cd-timeline-img -->
                                    <div class="cd-timeline-content">
                                        <span class="cd-date">{{$detailSupport->create_at}}</span>
                                        <p>{{$detailSupport->content}}</p>
                                        @if($detailSupport->seen)
                                            <p><i><span class="pull-right text-info" style="font-size: 10px">Đã xem</span></i></p>
                                        @endif
                                    </div> <!-- cd-timeline-content -->
                                @endif
                            </div> <!-- cd-timeline-block -->
                        @endforeach
                    @else
                        <p><i>Trống rỗng...</i></p>
                    @endif
                </section> <!-- cd-timeline -->
            </div>
        </div>

    </main>
    <div class="page-footer">
        <div class="footer-grid container" style="">
            <form action="{{action('Customer\SupportController@postAddfeedBack')}}" method="post">
                <div class="row">
                    <textarea style=" outline: none; border: 0px;" name="text" id="" cols="32" rows="10" placeholder="Bạn muốn nói gì?"></textarea>
                    <input type="hidden" value="{{$thisSupport->id}}" name="supportId">
                    <input type="submit" value="Trả lời" class="btn btn-primary">
                </div>

                {{csrf_field()}}
            </form>
        </div>
    </div>
@endsection
@section('footlink')

@endsection