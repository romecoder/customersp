@extends('Customer.master')
@section('title','Dashboard')
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title">Add Services</div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <form class="col s12 m12">
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="last" type="text" class="validate">
                                        <label for="last_name">Subject</label>
                                    </div>

                                    <div class="input-field col s6">
                                        <select>
                                            <option value="" disabled selected>Service</option>
                                            <option value="1">Laravel app</option>
                                            <option value="2">Angular app</option>
                                            <option value="3">Wordpress web</option>
                                        </select>
                                        <label>Service</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <select>
                                            <option value="" disabled selected>Customer</option>
                                            <option value="1">Hoàng An</option>
                                            <option value="2">Toàn Nguyễn</option>
                                            <option value="3">Biger</option>
                                        </select>
                                        <label>Custormer</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <select>
                                            <option value="" disabled selected>Supporter</option>
                                            <option value="1">Rome</option>
                                            <option value="2">Cody</option>
                                            <option value="3">Solozon</option>
                                        </select>
                                        <label>Supporter</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <textarea id="textarea1" class="materialize-textarea" length="120"></textarea>
                                        <label for="textarea1" class="">Content</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col s6">
                                        <!-- Switch -->
                                        <h6 for="">Status</h6>
                                        <div class="switch m-b-md">
                                            <label>
                                                Off
                                                <input type="checkbox">
                                                <span class="lever"></span>
                                                On
                                            </label>
                                        </div>
                                    </div>
                                    <div class="input-field col s6">

                                        <label for="birthdate">Date</label>
                                        <input id="birthdate" type="text" class="datepicker">

                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!---->
        </div>
    </main>
@endsection