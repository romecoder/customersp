@extends('Customer.master')
@section('title','Dashboard')
{{--@section('headlink')--}}
{{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--}}
{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>--}}
{{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--}}
{{--@endsection--}}
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="col s6 pull-left">
                    <div class="page-title"><label for="" style="font-size: 14px">Danh sách các hỗ trợ của bạn</label></div>
                </div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
            </div>

            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <table id="example" class="display responsive-table datatable-example striped highlight">
                            <thead>
                            <tr>
                                <th>Tên hỗ trợ</th>
                                <th>Nhân viên hỗ trợ</th>
                                <th>Dịch vụ</th>
                                <th>Trạng thái</th>
                                <th> <a href="{{action('Customer\SupportController@getAdd')}}" class="btn green"><i class=""></i>Cần
                                        Hỗ trợ ngay!</a></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($listSupport)
                                @foreach($listSupport as $support)

                                    <tr>
                                        <td><span style="text-transform: capitalize" class="text-danger">{{$support->name}}</span></td>
                                        <td><img src="{{asset('CS2/images/avatar')."/".$support->admin_id->avatar}}"
                                                        alt="{{$support->admin_id->name}}" class="img-responsive"
                                                        width="50px">
                                            :: <span class="text-warning">{{$support->admin_id->name}}</span>
                                        </td>
                                        <td>
                                            <span style="text-transform: capitalize" class="text-info">{{$support->service_id->name}}</span>
                                        </td>
                                        @if($support->status == 1)
                                            <td><a class="text-success chip"
                                                   href="{{action('Customer\SupportController@changeStatus',['id',$support])}}">Đã
                                                    giải quyết</a></td>
                                        @else
                                            <td><a class="text-danger chip"
                                                   href="{{action('Customer\SupportController@changeStatus',['id',$support])}}">đang
                                                    giải quyết</a></td>
                                        @endif
                                        <td>
                                            <ul class="list-inline">
                                                <li>
                                                    <a href="{{action('Customer\SupportController@getDetail',['id'=>$support->id])}}"
                                                       class="btn yellow" title="Chi tiết"><i
                                                                class="fa fa-search-plus" aria-hidden="true"></i></a>
                                                </li>
                                                <li>
                                                    <a href="{{action('Customer\SupportController@getDelete',['id'=>$support->id])}}"
                                                       class="btn red" title="Xóa"
                                                       onclick="return confirm('Bạn có thực sự muốn xóa Hỗ trợ này?')"><i
                                                                class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <p><i>Chưa có hỗ trợ nào.</i></p>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @if(sizeof($listSupport))
            <div class="col-md-12">
                <div class="paginate">
                    <p class="pull-left">Tổng số trang : {{$listSupport->lastPage()}}</p>
                    <ul class="pagination pull-right no-margin">
                        <li class="">
                            <a href="{{$listSupport->url(1)}}">
                                <i class="ace-icon fa fa-angle-double-left"></i>
                            </a>
                        </li>
                        <li class="prev {{($listSupport->currentPage() == 1) ? 'disabled' : ''}}">
                            <a href="{{$listSupport->url($listSupport->currentPage() - 1)}}">Trước</a>
                        </li>
                        @for($i=1; $i<=$listSupport->lastPage();$i++ )
                            <li class="{{ ($listSupport->currentPage() == $i) ? 'active' : '' }}">
                                <a href="{{$listSupport->url($i)}}">{{$i}}</a>
                            </li>
                        @endfor
                        <li class="next {{($listSupport->currentPage() == $listSupport->lastPage()) ? 'disabled' : ''}}">
                            <a href="{{$listSupport->url($listSupport->currentPage() + 1)}}">Sau</a>
                        </li>
                        <li class="">
                            <a href="{{$listSupport->url($listSupport->lastPage())}}">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        @endif
    </main>

@endsection