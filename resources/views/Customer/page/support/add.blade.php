@extends('Customer.master')
@section('title','Hỗ trợ ngay')
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title">Hỗ trợ ngay</div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
                @if(count($errors)>0)
                    @foreach($errors->all() as $error)
                        <div class="alert alert-block alert-warning message-box">
                            <button type="button" class="close" data-dismiss="alert">
                                <i class="ace-icon fa fa-times"></i>
                            </button>
                            <strong>
                                {{$error}}
                            </strong>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <form class="col s12 m12" action="{{action('Customer\SupportController@postAdd')}}" method="post" >
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="last" type="text" class="validate" name="name" value="{{old('name')}}">
                                        <label for="last_name" class="active">Vấn đề bạn gặp ?</label>
                                    </div>

                                    <div class="input-field col s6">
                                        <select name="service">
                                            @if($listService)
                                                <option value="" selected disabled>Chọn dịch vụ</option>
                                                @foreach($listService as $service)
                                                    <option value="{{$service->id}}">{{$service ->name}}</option>
                                                @endforeach
                                                @else
                                                <option value="" selected>Bạn chưa có dịch vụ nào</option>
                                            @endif
                                        </select>
                                        <label>Dịch vụ gặp vấn đề ?</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <textarea name="text" placeholder="Bạn đang thực sự gặp vấn đề gì?">{{old('content')}}</textarea>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col s12">
                                        <input type="submit" class="btn btn-success" value="Gửi đi">
                                    </div>
                                </div>
                                {{csrf_field()}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!---->
        </div>
    </main>
@endsection
@section('style')

    .mn-inner form {
    padding: 5% !important;
    }
    textarea{
    outline:0px !important;
    border: 1px dotted #dcdcdc;
    height: 150px;
    }
    input{
        border:none;
    }
    input:hover{
        border:none;
    }

@endsection