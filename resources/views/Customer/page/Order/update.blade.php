@extends('Customer.master')
@section('title','Dashboard')
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title">Add Order</div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <form class="col s12 m12">
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="" type="text" class="validate">
                                        <label for="last_name">Customer Name</label>
                                    </div>

                                    <div class="input-field col s6">
                                        <select>
                                            <option value="" disabled selected>Choose your option</option>
                                            <option value="1">Option 1</option>
                                            <option value="2">Option 2</option>
                                            <option value="3">Option 3</option>
                                        </select>
                                        <label>Service</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="" type="email" class="validate">
                                        <label for="last_name">Email</label>
                                    </div>

                                    <div class="input-field col s6">
                                        <input id="" type="text" class="validate">
                                        <label for="last_name">Address</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="phone" type="number" class="validate">
                                        <label for="last_name">Phone</label>
                                    </div>

                                    <div class="input-field col s6">

                                        <label for="birthdate">Date</label>
                                        <input id="birthdate" type="text" class="datepicker">

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col s12">
                                        <textarea id="textarea1" class="materialize-textarea" length="120"></textarea>
                                        <label for="textarea1" class="">Note</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s12">
                                        <!-- Switch -->
                                        <h6 for="">Status</h6>
                                        <div class="switch m-b-md">
                                            <label>
                                                Off
                                                <input type="checkbox">
                                                <span class="lever"></span>
                                                On
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!---->
        </div>
    </main>
@endsection