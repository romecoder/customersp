@extends('Customer.master')
@section('title','Dashboard')
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="col s6 pull-left">
                    <div class="page-title"><label for="" style="font-size: 14px">Quản Lý Đơn Hàng:</label></div>
                </div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <table id="example" class="display responsive-table datatable-example striped highlight">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Tên dịch vụ</th>
                                <th>Ngày</th>
                                <th>Trạng thái</th>
                                <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($listOrder)
                                <?php $count = 0;?>
                                @foreach($listOrder as $order)
                                    <?php $count += 1?>
                                    <tr>
                                        <td>{{$count}}</td>
                                        <td>@if(isset($order->service->name)){{$order->service->name}} @else {{$order->service}}@endif</td>

                                        {{--Phần này xử lý hiển thị Thời gian tự động theo giây, phút, giờ, ngày--}}
                                        <?php

                                        $now = strtotime(date('Y-m-d H:i:s'));
                                        $createTime = strtotime($order->created_at);
                                        $subTime =  $now - $createTime  ;
                                        $subTimeSecond = $subTime;
                                        $subTimeMinute = round($subTime/60);
                                        $subTimeHour =  round($subTime/3600);
                                        $subTimeDay =  round($subTime/86400);
                                        ?>
                                        <td>
                                            <span class="text-warning">
                                                @if($subTime < 60)
                                                    <label for="">{{$subTimeSecond}} Giây trước</label>
                                                @elseif($subTime > 60 && $subTime < 3060)
                                                    <label for="">{{$subTimeMinute}} Phút trước</label>
                                                @elseif($subTime > 3600 && $subTime < 86400)
                                                    <label for="">{{$subTimeHour}} Giờ trước</label>
                                                @elseif($subTime > 84600 && $subTime < 86400*10)
                                                    <label for="">{{$subTimeDay}} Ngày trước</label>
                                                @elseif($subTime > 84600*10)
                                                    {{date('d-m-Y',$createTime)}}
                                                @endif
                                            </span>
                                        </td>
                                        {{--Kết thúc xử lý ngày--}}

                                        @if($order->status)
                                            <td class="text-success"><a href="" class="chip"><small>Đã hoàn thành</small></a></td>
                                        @else
                                            <td class="text-danger"><a href="" class="chip"><samll>Chưa hoàn thành</samll></a></td>
                                        @endif
                                        <td>
                                            <ul class="list-inline">
                                                <li>
                                                    <a href="{{action('Customer\OrderController@getDetail',['id'=>$order->id])}}"
                                                       class="btn red"><i class="fa fa-search-plus"
                                                                                 aria-hidden="true"
                                                                                 title="Xem chi tiết"></i></a>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <p><i>Bạn chưa có hóa đơn nào...</i></p>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @if(sizeof($listOrder))
            <div class="col-md-12">
                <div class="paginate">
                    <p class="pull-left">Tổng số trang : {{$listOrder->lastPage()}}</p>
                    <ul class="pagination pull-right no-margin">
                        <li class="">
                            <a href="{{$listOrder->url(1)}}">
                                <i class="ace-icon fa fa-angle-double-left"></i>
                            </a>
                        </li>
                        <li class="prev {{($listOrder->currentPage() == 1) ? 'disabled' : ''}}">
                            <a href="{{$listOrder->url($listOrder->currentPage() - 1)}}">Trước</a>
                        </li>
                        @for($i=1; $i<=$listOrder->lastPage();$i++ )
                            <li class="{{ ($listOrder->currentPage() == $i) ? 'active' : '' }}">
                                <a href="{{$listOrder->url($i)}}">{{$i}}</a>
                            </li>
                        @endfor
                        <li class="next {{($listOrder->currentPage() == $listOrder->lastPage()) ? 'disabled' : ''}}">
                            <a href="{{$listOrder->url($listOrder->currentPage() + 1)}}">Sau</a>
                        </li>
                        <li class="">
                            <a href="{{$listOrder->url($listOrder->lastPage())}}">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        @endif
    </main>

@endsection