@extends('Customer.master')
@section('title','Dashboard')
@section('content')
    <main class="mn-inner">
        <div class="cyan darken-1">
            <div class="container">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="card card-transparent no-m">
                            <div class="card-content  white-text">
                                <div class="row">
                                    <div class="col s12 m6 l6">
                                        <h4>Pveser</h4>
                                        <address>
                                            Số 1 Nguyễn Chánh, Trung Hòa, Cầu Giấy, Hà Nội<br>
                                            P: (123) 456-7890
                                        </address>
                                    </div>
                                    <div class="col s12 m6 l6 right-align">
                                        <h4>{{number_format($totalPrice,0,',','.')}}đ</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card card-transparent no-m">
                        <div class="card-content invoice-relative-content">
                            <div class="row">
                                <div class="col s12 m6 l6 right-align">
                                    <a class="btn-floating btn-large waves-effect waves-light light-green invoice-edit-btn"><i
                                                class="material-icons">edit</i></a>
                                </div>
                            </div>
                            <form action="{{action('Customer\OrderController@postCheckOut')}}" method="post">
                                <input type="hidden" name="name" value="{{$data['username']}}">
                                <input type="hidden" name="email" value="{{$data['email']}}">
                                <input type="hidden" name="address" value="{{$data['address']}}">
                                <input type="hidden" name="address" value="{{$data['address']}}">
                                <input type="hidden" name="phone" value="{{$data['phone']}}">
                            <div class="row">
                                <div class="col s12 m12 l12">
                                    <table class="table responsive-table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Dịch vụ</th>
                                            <th>Giá</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($listService)
                                            <?php $count = 0?>
                                            @foreach($listService as $service)
                                                <?php $count++?>
                                                <tr>
                                                    <td>{{$count}}</td>
                                                    <td>{{$service->name}}</td>
                                                    <td>{{number_format($service->price,0,',','.')}}đ</td>
                                                </tr>
                                                <input type="hidden" value="{{$service->id}}" name="serviceId[]">
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 m6 l8">
                                    <span class="heading-title">Xin cảm ơn!</span>
                                    <p>Xin cảm ơn đã luôn tin tưởng và sử dụng những dịch vụ của pveser,
                                        chúng tôi luôn cam kết hỗ trợ bạn mọi lúc mọi nơi. Lorem ipsum dolor sit
                                        amet, consectetur adipisicing elit. Aspernatur at atque cumque eius fuga impedit
                                        ipsa laborum maxime.</p>
                                    <img src="{{asset('CS2/images/signature.png')}}" class="invoice-signature m-t-md" alt="">
                                </div>
                                <div class="col s12 m6 l4 right-align">
                                    <div class="text-right">
                                        <h6 class="m-t-md text-success light-blue-text"><b>Tổng cộng</b></h6>
                                        <h4 class="text-success">{{number_format($totalPrice,0,',','.')}} đ</h4>
                                        <div class="divider"></div>
                                        <input type="submit" class="waves-effect waves-light btn teal" value="Chấp nhận thanh toán">
                                    </div>
                                </div>
                            </div>
                                {{csrf_field()}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection