@extends('Customer.master')
@section('title','Dashboard')
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title">Thông tin sử dụng dịch vụ</div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <form class="col s12 m12" method="post" action="{{action('Customer\OrderController@postAdd')}}">
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="" type="text" class="validate" value="{{$userInfo->name}}"
                                               name="name">
                                        <label for="last_name" class="active">Tên</label>
                                    </div>

                                    <div class="input-field col s6">
                                        <select name="service">
                                            <option value="" disabled selected>Chọn dịch vụ</option>
                                            @if($listService)
                                                @foreach($listService as $service)
                                                    <option value="{{$service->id}}" @if(isset($data['service_id'][0]) && ((int)$data['service_id'][0] == $service->id))selected @endif>{{$service->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <label>Dịch vụ </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="" type="email" class="validate" value="{{$userInfo->email}}"
                                               name="email">
                                        <label for="last_name" class="active">Email</label>
                                    </div>

                                    <div class="input-field col s6">
                                        <input id="" type="text" class="validate" value="{{$userInfo->address}}"
                                               name="address">
                                        <label for="last_name" class="active">Địa chỉ</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="phone" type="number" class="validate" value="{{$userInfo->phone}}"
                                               name="phone">
                                        <label for="last_name" class="active">Số điện thoại</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col s12">
                                        <textarea id="textarea1" class="materialize-textarea" length="120"
                                                  name="note"></textarea>
                                        <label for="textarea1" class="active">Ghi chú</label>
                                    </div>
                                </div>
                                <input type="submit" value="Thanh Toán" class="btn btn-primary">
                                @if(isset($data) && $data['service_id'])
                                    @foreach($data['service_id'] as $serviceId)
                                        <input type="hidden" value="{{$serviceId}}" name="serviceId[]">
                                    @endforeach
                                    @foreach($data['price'] as $price)
                                        <input type="hidden" value="{{$price}}" name="price[]">
                                    @endforeach
                                @endif

                                {{csrf_field()}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!---->
        </div>
    </main>
@endsection