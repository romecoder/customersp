@extends('Customer.master')
@section('title','Dashboard')
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title"><label for="" style="font-size: 14px">CHI TIẾT ĐƠN HÀNG</label></div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                           <p><span class="text-info">Người Đăng ký:</span> {{\Illuminate\Support\Facades\Auth::user()->name}}</p>
                            <br>
                           <p><span class="text-info">Email:</span> {{$thisOrder->email}}</p>
                            <br>

                           <p><span class="text-info">Số điện thoại:</span> {{$thisOrder->phone}}</p>
                            <br>

                           <p><span class="text-info">Địa chỉ:</span> {{$thisOrder->address}}</p>
                            <br>

                           <p><span class="text-info">Tên dịch vụ:</span> {{$thisOrder->service_id}}</p>
                            <br>

                           <p><span class="text-info">Trạng thái:</span>@if($thisOrder->service_id) Đã thanh toán @else Chờ thanh toán @endif </p>
                            <br>

                            <p><span class="text-info">Ngày đăng ký:</span> {{$thisOrder->created_at}}</p>
                        </div>
                    </div>
                </div>
            </div>
            <!---->
        </div>
    </main>
@endsection