
@extends('Admin.master')
@section('title','Dashboard')
@section('headlink')
@endsection
@section('content')
    <main class="mn-inner">
        <div class="search-header">
            <div class="card card-transparent no-m">
                <div class="card-content no-s">
                    <div class="z-depth-1 search-tabs">
                        <div class="search-tabs-container">
                            <div class="col s12 m12 l12">
                                <div class="row search-tabs-row search-tabs-header">
                                    <div class="input-field col s12 m12 l2">
                                        <select>
                                            <option value="" disabled selected>Sort by</option>
                                            <option value="1">Newest</option>
                                            <option value="2">Trending</option>
                                            <option value="3">Most Related</option>
                                        </select>
                                    </div>
                                    <div class="col s12 m12 l10 right">
                                        <a class="waves-effect waves-grey btn-flat search-tabs-button right"><i class="material-icons">settings_input_svideo</i>Search Tools</a>
                                        <a class="waves-effect waves-grey btn-flat search-tabs-button right"><i class="material-icons">list</i>Advanced Search</a>
                                    </div>
                                </div>
                                <div class="row search-tabs-row search-tabs-container grey lighten-4">
                                    <div class="col s12 m6 l6">
                                        <ul class="tabs">
                                            <li class="tab col s3"><a href="#web">WEB</a></li>
                                            <li class="tab col s3"><a href="#images">IMAGES</a></li>
                                            <li class="tab col s3"><a href="#videos">VIDEOS</a></li>
                                            <li class="tab col s3"><a href="#news">NEWS</a></li>
                                        </ul>
                                    </div>
                                    <div class="col s12 m6 l6 right-align search-stats">
                                        <span class="m-r-sm">About 138,000,000 results</span><span class="secondary-stats">0.47 seconds</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card card-transparent no-m">
            <div class="card-content invoice-relative-content search-results-container">
                <div class="col s12 m12 l12">
                    <div class="search-page-results">
                        <div id="web" class="col s12 m12 l12">
                            <div class="row">
                                <div class="col s12 m6 l8">
                                    <div class="card">
                                        <div class="card-content">
                                            <div class="search-result">
                                                <a href="#" class="search-result-title">Pantera - Wikipedia, the free encyclopedia</a>
                                                <a href="#" class="search-result-link">https://en.wikipedia.org</a>
                                                <p class="search-result-description">Pantera was an American heavy metal band from Arlington, Texas. The group was formed in 1981 by the Abbott brothers – drummer Vinnie Paul and guitarist ...</p>
                                            </div>
                                            <div class="divider"></div>
                                            <div class="search-result">
                                                <a href="#" class="search-result-title">Pantera</a>
                                                <a href="#" class="search-result-link">pantera.com</a>
                                                <p class="search-result-description">HISTORY OF HOSTILITY, a nine-track Pantera primer that includes some of Pantera's most legendary tracks from all five studio albums is available here ...</p>
                                            </div>
                                            <div class="divider"></div>
                                            <div class="search-result">
                                                <a href="#" class="search-result-title">Pantera - Facebook</a>
                                                <a href="#" class="search-result-link">https://www.facebook.com</a>
                                                <p class="search-result-description">Pantera. 8922172 likes · 116335 talking about this. http://www.pantera.com http://smarturl.it/panterastore http://www.twitter.com/pantera...</p>
                                            </div>
                                            <div class="divider"></div>
                                            <div class="search-result">
                                                <a href="#" class="search-result-title">Pantera — Free listening, videos, concerts, stats and ...</a>
                                                <a href="#" class="search-result-link">www.last.fm</a>
                                                <p class="search-result-description">Watch videos &amp; listen free to Pantera: Cowboys from Hell, Walk &amp; more. Pantera was an extremely popular metal band from Arlington, Texas, formed in 1981.</p>
                                            </div>
                                            <div class="divider"></div>
                                            <div class="search-result">
                                                <a href="#" class="search-result-title">Pantera | New Music And Songs | - MTV.com</a>
                                                <a href="#" class="search-result-link">www.mtv.com</a>
                                                <p class="search-result-description">29 items - Pantera new music, concerts, photos, and official news updates directly from Pantera's Twitter and Facebook.</p>
                                            </div>
                                            <div class="divider"></div>
                                            <div class="search-result">
                                                <a href="#" class="search-result-title">Pantera News - Loudwire</a>
                                                <a href="#" class="search-result-link">loudwire.com/tags</a>
                                                <p class="search-result-description">Pantera was arguably the most important heavy metal band of the 1990s — becoming a major commercial force in spite of a hostile alternative rock climate that ...</p>
                                            </div>
                                            <div class="divider"></div>
                                            <div class="search-result">
                                                <a href="#" class="search-result-title">'Mayhem' Festival Producer Says He Has Approached ...</a>
                                                <a href="#" class="search-result-link">www.blabbermouth.net</a>
                                                <p class="search-result-description">Late PANTERA guitarist "Dimebag" Darrell Abbott's longtime girlfriend Rita Haney in 2011 called on ex-PANTERA drummer Vinnie Paul Abbott ...</p>
                                            </div>
                                            <div class="divider"></div>
                                            <div class="search-result">
                                                <a href="#" class="search-result-title">Pantera | Biography, Albums, &amp; Streaming Radio | AllMusic</a>
                                                <a href="#" class="search-result-link">www.allmusic.com</a>
                                                <p class="search-result-description">Find Pantera bio, music, credits, awards, &amp; streaming radio on AllMusic - One of the best metal bands of the '90s due to...</p>
                                            </div>
                                            <div class="divider"></div>
                                            <div class="search-result">
                                                <a href="#" class="search-result-title">pantera - YouTube</a>
                                                <a href="#" class="search-result-link">https://www.youtube.com</a>
                                                <p class="search-result-description">2008 WMG I'm Broken (Video) - - - - Far Beyond Driven 2CD 20th Anniversary Edition Coming March 25th! 2CD Deluxe: http://smarturl.it/panterafbd - - - -</p>
                                            </div>

                                            <div class="divider"></div>
                                            <ul class="pagination">
                                                <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                                                <li class="active"><a href="#!">1</a></li>
                                                <li class="waves-effect"><a href="#!">2</a></li>
                                                <li class="waves-effect"><a href="#!">3</a></li>
                                                <li class="waves-effect"><a href="#!">4</a></li>
                                                <li class="waves-effect"><a href="#!">5</a></li>
                                                <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12 m6 l4">
                                    <div class="card">
                                        <div class="card-image">
                                            <img src="assets/images/search_image.jpg" alt="">
                                            <span class="card-title">Pantera</span>
                                        </div>
                                        <div class="card-content">
                                            <p>Pantera was an American heavy metal band from Arlington, Texas. The group was formed in 1981 by the Abbott brothers – drummer Vinnie Paul and guitarist Dimebag Darrell – along with vocalist Terry Glaze.</p>
                                        </div>
                                        <div class="card-action">
                                            <a href="#">More about Pantera</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="images" class="col s12 m12 l12">
                            <div class="row search-image-results">
                                <div class="col s12 m6 l4">
                                    <img src="assets/images/search_images/1.jpg" class="materialboxed responsive-img" alt="">
                                    <img src="assets/images/search_images/4.jpg" class="materialboxed responsive-img" alt="">
                                    <img src="assets/images/search_images/7.jpg" class="materialboxed responsive-img" alt="">
                                    <img src="assets/images/search_images/10.jpg" class="materialboxed responsive-img" alt="">
                                </div>
                                <div class="col s12 m6 l4">
                                    <img src="assets/images/search_images/2.jpg" class="materialboxed responsive-img" alt="">
                                    <img src="assets/images/search_images/5.jpg" class="materialboxed responsive-img" alt="">
                                    <img src="assets/images/search_images/8.jpg" class="materialboxed responsive-img" alt="">
                                    <img src="assets/images/search_images/11.jpeg" class="materialboxed responsive-img" alt="">
                                </div>
                                <div class="col s12 m6 l4">
                                    <img src="assets/images/search_images/3.jpg" class="materialboxed responsive-img" alt="">
                                    <img src="assets/images/search_images/6.jpg" class="materialboxed responsive-img" alt="">
                                    <img src="assets/images/search_images/9.jpg" class="materialboxed responsive-img" alt="">
                                    <img src="assets/images/search_images/12.jpg" class="materialboxed responsive-img" alt="">
                                </div>
                            </div>
                            <ul class="pagination">
                                <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                                <li class="active"><a href="#!">1</a></li>
                                <li class="waves-effect"><a href="#!">2</a></li>
                                <li class="waves-effect"><a href="#!">3</a></li>
                                <li class="waves-effect"><a href="#!">4</a></li>
                                <li class="waves-effect"><a href="#!">5</a></li>
                                <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
                            </ul>
                        </div>
                        <div id="videos" class="col s12 m12 l12">
                            <div class="row">
                                <div class="col s12 m8 offset-m2 l10 offset-l1">
                                    <div class="card-panel grey lighten-5 z-depth-1">
                                        <div class="row valign-wrapper">
                                            <div class="col s2">
                                                <a href="#"><img src="assets/images/video_placeholder.png" alt="" class="circle responsive-img"></a>
                                            </div>
                                            <div class="col s10">
                                                <div class="black-text">
                                                    <a href="#" class="heading-title">Pantera - Vulgar Display Of Power ( Full Album ) - YouTube</a>
                                                    <a href="#" class="green-text">https://www.youtube.com/watch?v=Nx08MK_O_TQ</a>
                                                    <p class="blue-grey-text">May 4, 2012 - Uploaded by youtube</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12 m8 offset-m2 l10 offset-l1">
                                    <div class="card-panel grey lighten-5 z-depth-1">
                                        <div class="row valign-wrapper">
                                            <div class="col s2">
                                                <a href="#"><img src="assets/images/video_placeholder.png" alt="" class="circle responsive-img"></a>
                                            </div>
                                            <div class="col s10">
                                                <div class="black-text">
                                                    <a href="#" class="heading-title">Pantera - Walk - YouTube</a>
                                                    <a href="#" class="green-text">https://www.youtube.com/watch?v=5Ju11Q1MW-4</a>
                                                    <p class="blue-grey-text">Sep 27, 2011 - Uploaded by youtube</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12 m8 offset-m2 l10 offset-l1">
                                    <div class="card-panel grey lighten-5 z-depth-1">
                                        <div class="row valign-wrapper">
                                            <div class="col s2">
                                                <a href="#"><img src="assets/images/video_placeholder.png" alt="" class="circle responsive-img"></a>
                                            </div>
                                            <div class="col s10">
                                                <div class="black-text">
                                                    <a href="#" class="heading-title">Cemetery Gates - Pantera (HQ Audio) - YouTube</a>
                                                    <a href="#" class="green-text">https://www.youtube.com/watch?v=1OYw7FPB7CE</a>
                                                    <p class="blue-grey-text">Sep 1, 2011 - Uploaded by youtube</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ul class="pagination">
                                <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                                <li class="active"><a href="#!">1</a></li>
                                <li class="waves-effect"><a href="#!">2</a></li>
                                <li class="waves-effect"><a href="#!">3</a></li>
                                <li class="waves-effect"><a href="#!">4</a></li>
                                <li class="waves-effect"><a href="#!">5</a></li>
                                <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
                            </ul>
                        </div>
                        <div id="news" class="col s12 m12 l12">
                            <div class="row">
                                <div class="col s12 m6 l4">
                                    <div class="card">
                                        <div class="card-image">
                                            <img src="assets/images/search_images/3.jpg" alt="">
                                            <span class="card-title">Pantera</span>
                                        </div>
                                        <div class="card-content">
                                            <p>Within the material environment, virtual lights illuminate the scene. Key lights create directional shadows, while ambient light creates soft shadows from all angles.</p>
                                        </div>
                                        <div class="card-action">
                                            <a href="#">READ MORE</a>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-image">
                                            <img src="assets/images/search_images/7.jpg" alt="">
                                            <span class="card-title">Pantera</span>
                                        </div>
                                        <div class="card-content">
                                            <p>Color in material design is inspired by bold hues juxtaposed with muted environments, deep shadows, and bright highlights. Material takes cues from contemporary architecture, road signs, pavement marking tape, and athletic courts. Color should be unexpected and vibrant.</p>
                                        </div>
                                        <div class="card-action">
                                            <a href="#">READ MORE</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12 m6 l4">
                                    <div class="card">
                                        <div class="card-image">
                                            <img src="assets/images/search_images/5.jpg" alt="">
                                            <span class="card-title">Pantera</span>
                                        </div>
                                        <div class="card-content">
                                            <p>Material design is guided by print-based design elements—such as typography, grids, space, scale, color, and imagery—to create hierarchy, meaning, and focus that immerse the user in the experience. </p>
                                        </div>
                                        <div class="card-action">
                                            <a href="#">READ MORE</a>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-image">
                                            <img src="assets/images/search_images/9.jpg" alt="">
                                            <span class="card-title">Pantera</span>
                                        </div>
                                        <div class="card-content">
                                            <p>Imagery is more than decoration. It’s a powerful tool to help you communicate and differentiate your product. Bold, graphic, and intentional imagery helps to engage the user. </p>
                                        </div>
                                        <div class="card-action">
                                            <a href="#">READ MORE</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12 m6 l4">
                                    <div class="card">
                                        <div class="card-image">
                                            <img src="assets/images/search_images/4.jpg" alt="">
                                            <span class="card-title">Pantera</span>
                                        </div>
                                        <div class="card-content">
                                            <p>Just as the shape of an object indicates how it might behave, watching an object move demonstrates whether it’s light, heavy, flexible, rigid, small or large. In the world of material design, motion describes spatial relationships, functionality, and intention with beauty and fluidity.</p>
                                        </div>
                                        <div class="card-action">
                                            <a href="#">READ MORE</a>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-image">
                                            <img src="assets/images/search_images/12.jpg" alt="">
                                            <span class="card-title">Pantera</span>
                                        </div>
                                        <div class="card-content">
                                            <p>Clear, accurate, and concise text makes interfaces more usable and builds trust. Strive to write text that is understandable by anyone, anywhere, regardless of their culture or language.</p>
                                        </div>
                                        <div class="card-action">
                                            <a href="#">READ MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ul class="pagination">
                                <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                                <li class="active"><a href="#!">1</a></li>
                                <li class="waves-effect"><a href="#!">2</a></li>
                                <li class="waves-effect"><a href="#!">3</a></li>
                                <li class="waves-effect"><a href="#!">4</a></li>
                                <li class="waves-effect"><a href="#!">5</a></li>
                                <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('footlink')
    <script>
        CKEDITOR.replace('textarea1');
    </script>
@endsection
@section('afterJquery')
    <script src="{{asset('CS2/js/ckeditor/ckeditor.js')}}"></script>
@endsection