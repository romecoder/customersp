@extends('Customer.master')
@section('title','Dashboard')
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title"><label for="" style="font-size: 14px">Câu hỏi thường gặp</label></div>
            </div>
            <div class="col s12 m12 l8">
                <div class="card">
                    <div class="card-content">
                        <ul class="collapsible" data-collapsible="accordion">
                            <li>
                                <div class="collapsible-header">1. Hệ thống Pveser là gì?
                                </div>
                                <div class="collapsible-body"><p>Curabitur a lectus at risus porttitor facilisis. Duis
                                        ac fermentum mi, consequat finibus erat. Proin hendrerit, ex et gravida
                                        hendrerit, mauris quam lacinia nunc, et laoreet mi risus quis risus.</p></div>
                            </li>
                            <li>
                                <div class="collapsible-header">2. Làm thế nào để bạn sử dụng Pveser?</div>
                                <div class="collapsible-body"><p>Vestibulum laoreet nisi vel massa rutrum ullamcorper.
                                        Phasellus nec turpis lacus. Suspendisse pulvinar ut ligula ut egestas. Quisque
                                        posuere viverra enim sagittis pellentesque.</p></div>
                            </li>
                            <li>
                                <div class="collapsible-header">3. Làm thế nào để đăng ký sử dụng dịch vụ mới?
                                </div>
                                <div class="collapsible-body"><p>Mauris interdum erat ac sapien fringilla, at
                                        sollicitudin enim finibus.</p></div>
                            </li>
                            <li>
                                <div class="collapsible-header">4. Làm thế nào khi dịch vụ gặp trục trặc?</div>
                                <div class="collapsible-body"><p>Proin non facilisis lorem, a finibus nulla. Donec sed
                                        eleifend nulla, nec dictum enim. Sed tempus non ex id pulvinar. Nam mollis in
                                        dolor quis convallis. Donec vulputate leo nibh, ac ultrices ex auctor eu.
                                        Praesent sit amet velit at dui tristique tincidunt.</p></div>
                            </li>
                            <li>
                                <div class="collapsible-header">5. Quyền lợi của bạn là gì?
                                </div>
                                <div class="collapsible-body"><p>Nunc vel risus quis sem posuere tincidunt. Vestibulum
                                        dignissim orci ut erat ullamcorper, pharetra dictum lorem rutrum.</p></div>
                            </li>
                            <li>
                                <div class="collapsible-header">6. Các điều khoản sử dụng của Pveser là gì?</div>
                                <div class="collapsible-body"><p>Maecenas nulla nunc, vehicula non ultricies in, maximus
                                        sed urna. Proin nibh enim, consequat vel metus et, auctor varius ex. Vestibulum
                                        facilisis leo non tellus commodo, feugiat condimentum urna pretium.</p></div>
                            </li>
                            <li>
                                <div class="collapsible-header">7. Làm thế nào khi muốn chỉnh sửa thông tin cá nhân?</div>
                                <div class="collapsible-body"><p>Maecenas elementum lacinia neque, id mollis lorem
                                        aliquam at. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent
                                        nulla mi, pharetra eu nulla eu, consectetur convallis turpis. Interdum et
                                        malesuada fames ac ante ipsum primis in faucibus. Integer eleifend sollicitudin
                                        enim, eget placerat odio pellentesque ut. Sed vel rhoncus leo, sit amet luctus
                                        lorem. Cras vitae dolor mi.</p></div>
                            </li>
                            <li>
                                <div class="collapsible-header">8. Góp ý cho Pveser?
                                </div>
                                <div class="collapsible-body"><p>Cras rutrum consectetur velit eget porta. Phasellus non
                                        odio id ligula mollis tempor eu et diam.</p></div>
                            </li>
                            <li>
                                <div class="collapsible-header">9. Đối tác cho Pveser?</div>
                                <div class="collapsible-body"><p>Sed tincidunt dapibus lorem, eget iaculis massa
                                        ultrices a. Sed ornare neque sit amet mauris feugiat iaculis. In velit nisi,
                                        egestas id mauris nec, interdum pellentesque sem.</p></div>
                            </li>
                            <li>
                                <div class="collapsible-header">10. Pveser cam kết?
                                </div>
                                <div class="collapsible-body"><p>Phasellus nec turpis lacus. Suspendisse pulvinar ut
                                        ligula ut egestas. Quisque posuere viverra enim sagittis pellentesque.</p></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col s12 m12 l4">
                <div class="card">
                    <div class="card-content">
                        <span class="card-title">Đăng ký Email</span>
                        <p>Hãy đăng ký email để luôn cập nhật những thông tin giảm giá sử dụng dịch vụ mới nhất</p>
                        <form>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="email" type="email" class="validate">
                                    <label for="email">Email</label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <span class="card-title black-text">Chủ đề</span>
                        <div class="chip m-b-xs">
                            Dịch vụ
                            <i class="material-icons close">close</i>
                        </div>
                        <div class="chip m-b-xs">
                            Đơn hàng
                            <i class="material-icons close">close</i>
                        </div>
                        <div class="chip m-b-xs">
                            Hỡ trợ
                            <i class="material-icons close">close</i>
                        </div>
                        <div class="chip m-b-xs">
                            Chỉnh sửa
                            <i class="material-icons close">close</i>
                        </div>
                        <div class="chip m-b-xs">
                            Cá nhân
                            <i class="material-icons close">close</i>
                        </div>
                        <div class="chip m-b-xs">
                            Tiền
                            <i class="material-icons close">close</i>
                        </div>
                        <div class="chip m-b-xs">
                            Phí dịch vụ
                            <i class="material-icons close">close</i>
                        </div>
                        <div class="chip m-b-xs">
                            Tin mới
                            <i class="material-icons close">close</i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection