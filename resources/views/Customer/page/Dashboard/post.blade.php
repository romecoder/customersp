@extends('Customer.master')
@section('title','Post')
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title"><h5>{{$thisPost->tittle}}</h5></div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <div class="row">

                            <div class="col s8">
                                <div class="row">
                                    <center>
                                        <img src="{{asset('CS2/images/post').'/'.$thisPost->image}}" alt="" class="img-responsive">
                                    </center>
                                </div>
                                <div class="row">
                                    {!! $thisPost->content !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!---->
        </div>
    </main>

@endsection