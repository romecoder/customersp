@extends('Customer.master')
@section('title','Service list')
{{--@section('headlink')--}}
{{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
{{--@endsection--}}
@section('content')
    {{--    {{dd($serviceList)}}--}}
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="col s6 pull-left">
                    <div class="page-title"><label for="" style="font-size: 14">Các dịch vụ bạn đang sử dụng</label></div>
                </div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
                @if(count($errors)>0)
                    @foreach($errors->all() as $error)
                        <div class="alert alert-block alert-warning message-box">
                            <button type="button" class="close" data-dismiss="alert">
                                <i class="ace-icon fa fa-times"></i>
                            </button>
                            <strong>
                                {{$error}}
                            </strong>
                        </div>
                    @endforeach
                @endif
            </div>

            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home"><small>Dịch vụ của bạn</small></a></li>
                <li><a data-toggle="tab" href="#menu2"><small>Sử dụng thêm dịch vụ</small></a></li>
            </ul>

            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <div class="col s12 m12 l12">
                        <div class="card">
                            <div class="card-content">
                                <table id="example" class="display responsive-table datatable-example striped">
                                    <thead>
                                    <tr>
                                        <th><small>#</small></th>
                                        <th><small>Tên</small></th>
                                        <th><small>Loại</small></th>
                                        <th><small>Ngày hết hạn</small></th>
                                        <th><small>Trạng thái</small></th>
                                        <th><small>#</small></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($serviceList!= null)
                                        <?php $count = 0?>
                                        @foreach($serviceList as $service)
                                            <?php $count += 1?>
                                            <tr>
                                                <td><small></small>{{$count}}</td>
                                                <td class="text-primary"><small>{{$service->name}}</small></td>
                                                @if($service->type_id == 1)
                                                    <td class="text-danger"><a href="" class="chip"><small>Gia hạn</small></a></td>
                                                @else
                                                    <td class="text-success"><a href="" class="chip"><small>Vĩnh viễn</small></a></td>
                                                @endif
                                                <td class="text-danger"><small>{{date('d-m-Y',strtotime($service->end_date))}}</small></td>
                                                @if($service->status)
                                                    <td><a href=""
                                                           style="color: lightseagreen; font-weight: bold; font-size: 15px" class="chip"><small>Đang
                                                                sử dụng</small></a>
                                                    </td>
                                                @else
                                                    <td><a href=""
                                                           style="color: red; font-weight: bold; font-size: 15px" class="chip"><small>Đã
                                                                dừng</small></a>
                                                    </td>
                                                @endif
                                                <td>
                                                    <ul class="list-inline">
                                                        <li>
                                                            <a href="" class="btn orange"><i
                                                                        class="fa fa-search-plus"
                                                                        aria-hidden="true"
                                                                ></i></a>
                                                        </li>
                                                        <li><a href="" class="btn red"
                                                               onclick="return confirm('Bạn thực sự muốn thôi sử dụng dịch vụ này?')"><i
                                                                        class="fa fa-trash-o"
                                                                        aria-hidden="true"></i></a>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <p><i>Bạn chưa sử dụng dịch vụ nào, Hoặc những dịch vụ của bạn chưa
                                                ở trạng thái kích hoạt.
                                                <a href="">Hãy lựa chọn những dịch vụ tốt nhất...</a></i></p>
                                    @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="menu2" class="tab-pane fade" style="margin-top: 3em">
                    @if($UnusedServiceList)
                        <form action="{{action('Customer\ServiceController@postAdd')}}" method="post">
                            <div class="row">
                                @foreach($UnusedServiceList as $service)
                                    <div class="col-md-3">
                                        <div class="box" style="width: 14em;height: 18em; background-color: #CDCDCD; margin-bottom: 8em">
                                            <a href="{{action('Customer\ServiceController@getDetail',['id'=>$service->id])}}">
                                                <center>
                                                 <span class="p-v-xs">
                                                     <img src="{{asset('CS2/images/service').'/'.$service->thumbnail}}"
                                                          alt="" width="100%" height="100%">
                                                </span>
                                                </center>
                                            </a>
                                            <center>
                                                <span class="text-danger">{{number_format($service->price,0,',','.')}}đ</span>
                                            </center>
                                            <input type="checkbox" id="{{$service->id}}" value="{{$service->id}}"
                                                   name="serviceId[{{$service->price}}]">

                                            <label for="{{$service->id}}"><b><small>{{$service->name}}</small></b></label>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="row">
                                <input type="submit" value="Đăng ký sử dụng" class="btn blue pull-right">
                            </div>
                            {{csrf_field()}}
                        </form>
                    @else
                    @endif
                </div>
            </div>
        </div>
        {{--@if(sizeof($serviceList))--}}
            {{--<div class="col-md-12">--}}
                {{--<div class="paginate">--}}
                    {{--<p class="pull-left">Tổng số trang : {{$serviceList->lastPage()}}</p>--}}
                    {{--<ul class="pagination pull-right no-margin">--}}
                        {{--<li class="">--}}
                            {{--<a href="{{$serviceList->url(1)}}">--}}
                                {{--<i class="ace-icon fa fa-angle-double-left"></i>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li class="prev {{($serviceList->currentPage() == 1) ? 'disabled' : ''}}">--}}
                            {{--<a href="{{$serviceList->url($serviceList->currentPage() - 1)}}">Trước</a>--}}
                        {{--</li>--}}
                        {{--@for($i=1; $i<=$serviceList->lastPage();$i++ )--}}
                            {{--<li class="{{ ($serviceList->currentPage() == $i) ? 'active' : '' }}">--}}
                                {{--<a href="{{$serviceList->url($i)}}">{{$i}}</a>--}}
                            {{--</li>--}}
                        {{--@endfor--}}
                        {{--<li class="next {{($serviceList->currentPage() == $serviceList->lastPage()) ? 'disabled' : ''}}">--}}
                            {{--<a href="{{$serviceList->url($serviceList->currentPage() + 1)}}">Sau</a>--}}
                        {{--</li>--}}
                        {{--<li class="">--}}
                            {{--<a href="{{$serviceList->url($serviceList->lastPage())}}">--}}
                                {{--<i class="ace-icon fa fa-angle-double-right"></i>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--@endif--}}
    </main>

@endsection