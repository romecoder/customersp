@extends('Customer.master')
@section('title','Cá nhân')
@section('headlink')
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
@endsection
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title"><label style="font-size: 14px">Chỉnh sửa thông tin cá nhân</label></div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
                @if(count($errors)>0)
                @foreach($errors->all() as $error)
                <div class="alert alert-block alert-warning message-box">
                <button type="button" class="close" data-dismiss="alert">
                <i class="ace-icon fa fa-times"></i>
                </button>
                <strong>
                {{$error}}
                </strong>
                </div>
                @endforeach
                @endif
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <form class="col s12 m12" method="post"
                                  action="{{action('Customer\ProfileController@update',['id'=>$thisCustomer->id])}}"
                                  enctype="multipart/form-data">
                                <div class="row">

                                    <div class="input-field col s6">
                                        <input id="email" type="email" class=""
                                               value="{{old('email',isset($thisCustomer)? $thisCustomer->email:'')}}"
                                               name="email">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('email') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label class="active" for="email">Email:</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input id="birthday" type="date" class=""
                                               value="{{old('birthday',isset($thisCustomer)? $thisCustomer->birthday:'')}}"
                                               name="birthday">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('birthday') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label class="active" for="birthday">Sinh nhật:</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="phone" type="number" class=""
                                               value="{{old('phone',isset($thisCustomer)? $thisCustomer->phone:'')}}"
                                               name="phone">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('phone') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label class="active" for="phone">Số điện thoại:</label>
                                    </div>

                                    <div class="input-field col s6">
                                        <label class="active" for="address" class="">Địa chỉ:</label>
                                        <input id="address" name="address" type="text"
                                               value="{{old('address',isset($thisCustomer)? $thisCustomer->address:'')}}"
                                               name="address">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('address') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="fullname" type="text" class=""
                                               value="{{old('fullname',isset($thisCustomer)? $thisCustomer->fullname:'')}}"
                                               name="fullname">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('fullname') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label class="active" for="fullname">Họ và tên:</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <div class="file-field input-field">
                                            <button type="button" class="btn teal lighten-1 btn-sm" data-toggle="modal"
                                                    data-target="#changePassword">Đổi mật khẩu
                                            </button>
                                        </div>
                                    </div>
                                    <div class="input-field col s6">
                                        <div class="input-field col s12">
                                            <div class="file-field input-field">
                                                <div class="btn teal lighten-1">
                                                    <span>Avatar</span>
                                                    <input type="file" name="avatar" value="{{old('avatar')}}">
                                                </div>
                                                <div class="file-path-wrapper">
                                                    <input class="file-path validate" type="text"
                                                           value="{{old('avatar')}}"
                                                           placeholder="{{ old('avatar',isset($thisCustomer)? $thisCustomer->avatar:'')}}">
                                                    <input type="hidden" value="{{$thisCustomer->avatar}}"
                                                           name="oldavatar">
                                                    @if(count($errors)>0)
                                                        @foreach($errors->get('avatar') as $error)
                                                            <p style="color: red">{{$error}}</p>
                                                        @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <label class="active" for="address" class="">Giới thiệu</label>
                                        <textarea id="textarea1" class="materialize-textarea" length="120"
                                                  name="contents">{!! old('address',isset($thisCustomer)? $thisCustomer->content:'') !!} </textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s6">
                                        <!-- Switch -->
                                        <h6 for="">Giới tính</h6>
                                        <div class="switch m-b-md">
                                            <label class="active">
                                                Nữ
                                                @if($thisCustomer->gender)
                                                    <input type="checkbox" name="gender" checked>
                                                    <span class="lever"></span>
                                                @else
                                                    <input type="checkbox" name="gender" class="validate">
                                                    <span class="lever"></span>
                                                @endif
                                                Nam
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col cs12 pull-right">
                                        <input type="submit" class="btn btn-block btn-primary btn-lg"
                                               value="Cập nhật">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!---->
        </div>
    </main>

    <!-- Modal -->
    <div id="changePassword" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Đổi mật khẩu:</h4>
                <div class="modal-body">
                    <form action="{{action('Customer\ProfileController@changePassword',['id'=>$thisCustomer->id])}}"
                          method="post">
                        <label for="">Mật khẩu mới:</label>
                        <input type="password" name="pass" placeholder="Nhập vào mật khẩu bạn muốn đổi">
                        <label for="">Nhập lại mật khẩu:</label>
                        <input type="password" name="repass" placeholder="Nhập lại mật khẩu">
                        {{csrf_field()}}

                        <input type="submit" class="btn teal lighten-1 btn-sm" value="Đổi mật khẩu">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footlink')
    <script>
        CKEDITOR.replace('textarea1');
    </script>
@endsection

@section('afterJquery')
    <script src="{{asset('CS2/js/ckeditor/ckeditor.js')}}"></script>
@endsection