<aside id="slide-out" class="side-nav white fixed">
    <div class="side-nav-wrapper">
        <div class="sidebar-profile">
            <div class="sidebar-profile-image">
                @if(Auth::user()->avatar != null)
                    <img src="{{asset('CS2/images/avatar').'/'.Auth::user()->avatar}}" class="circle" alt="">
                @else
                    <img src="{{asset('CS2/images/avatar/avatar.png')}}" class="circle" alt="">
                @endif
            </div>
            <div class="sidebar-profile-info">
                <a href="javascript:void(0);" class="account-settings-link">
                    <p>{{Auth::user()->full_name}}</p>
                    <span>{{Auth::user()->email}}<i class="material-icons right">arrow_drop_down</i></span>
                </a>
            </div>
        </div>
        <div class="sidebar-account-settings">
            <ul>
                <li class="no-padding">
                    <a class="waves-effect waves-grey" href="{{action('Customer\ProfileController@index')}}"><i
                                class="material-icons">history</i>Cá nhân</a>
                </li>
                <li class="no-padding">
                    <a class="waves-effect waves-grey" href="{{action('Customer\ProfileController@edit',['id'=>Auth::user()->id])}}"><i
                                class="material-icons">history</i>Sửa</a>
                </li>
                <li class="no-padding">
                    <a class="waves-effect waves-grey" href="{{route('get.logout')}}"><i class="material-icons">exit_to_app</i>Đăng
                        xuất</a>
                </li>
            </ul>
        </div>
        <ul class="sidebar-menu collapsible collapsible-accordion" data-collapsible="accordion">
            <li class="no-padding active"><a class="waves-effect waves-grey active" href="{{action('Customer\DashboardController@getHome')}}"><i
                            class="material-icons">settings_input_svideo</i>Bảng điều khiển</a></li>
            <li class="no-padding">
                <a class="collapsible-header waves-effect waves-grey" href="{{action('Customer\ServiceController@getList')}}"><i class="material-icons">star_border</i>Dịch vụ
                    của tôi
                    {{--<i class="nav-drop-icon material-icons">keyboard_arrow_right</i>--}}
                </a>
                {{--<div class="collapsible-body">--}}
                    {{--<ul>--}}
                        {{--<li><a href="{{action('Customer\ServiceController@getList')}}">Dịch vụ đang dùng</a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            </li>
            <li class="no-padding">
                <a class="collapsible-header waves-effect waves-grey" href="{{action('Customer\SupportController@getList')}}"><i class="material-icons">tag_faces</i>Hỗ trợ
                    của tôi
                    {{--<i class="nav-drop-icon material-icons">keyboard_arrow_right</i>--}}
                </a>
                {{--<div class="collapsible-body">--}}
                    {{--<ul>--}}
                        {{--<li><a href="{{action('Customer\SupportController@getList')}}">Danh sách Hỗ trợ</a></li>--}}
                        {{--<li><a href="{{action('Customer\SupportController@getAdd')}}">Cần Hỗ trợ ngay!</a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            </li>
            <li class="no-padding">
                <a class="collapsible-header waves-effect waves-grey" href="{{action('Customer\OrderController@getList')}}"><i class="material-icons">mode_edit</i>Hóa đơn
                    của tôi
                    {{--<i class="nav-drop-icon material-icons">keyboard_arrow_right</i>--}}
                </a>
                {{--<div class="collapsible-body">--}}
                    {{--<ul>--}}
                        {{--<li><a href="{{action('Customer\OrderController@getList')}}">Danh sách Hóa Đơn</a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            </li>
            <li class="no-padding">
                <a class="collapsible-header waves-effect waves-grey" href="{{action('Customer\DashboardController@faq')}}"><i class="material-icons">search</i>Câu hỏi
                    thường gặp</a>
            </li>
        </ul>
        <div class="footer">
            <p class="copyright">Romecody.com ©</p>
            <a href="#!">Privacy</a> &amp; <a href="#!">Terms</a>
        </div>
    </div>
</aside>