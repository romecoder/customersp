<?php $link = url()->current();?>
<aside id="slide-out" class="side-nav white fixed">
    <div class="side-nav-wrapper">
        <div class="sidebar-profile">
            @if(isset(Auth::guard('admin')->user()->id))
                <div class="sidebar-profile-image">
                    <img src="@if(Auth::guard('admin')->user()->avatar !== null) {{asset('CS2/images/avatar').'/'.Auth::guard('admin')->user()->avatar}} @else {{asset('CS2/images/avatar/avatar.png')}} @endif " class="circle"
                         alt="">
                </div>
                <div class="sidebar-profile-info">
                    <a href="javascript:void(0);" class="account-settings-link">
                        <p>{{Auth::guard('admin')->user()->full_name}}</p>
                        <span>{{Auth::guard('admin')->user()->email}}<i class="material-icons right">arrow_drop_down</i></span>
                    </a>
                </div>
            @else
                <div class="sidebar-profile-image">
                    <img src="{{asset('CS2/images/avatar').'/'.Auth::user()->avatar}}" class="circle" alt="">
                </div>
                <div class="sidebar-profile-info">
                    <a href="javascript:void(0);" class="account-settings-link">
                        <p>{{Auth::user()->name}}</p>
                        <span>{{Auth::user()->email}}<i class="material-icons right">arrow_drop_down</i></span>
                    </a>
                </div>
            @endif
        </div>
        <div class="sidebar-account-settings">
            <ul>
                {{--<li class="no-padding">--}}
                {{--<a class="waves-effect waves-grey"><i class="material-icons">mail_outline</i>Inbox</a>--}}
                {{--</li>--}}
                {{--<li class="no-padding">--}}
                {{--<a class="waves-effect waves-grey"><i class="material-icons">star_border</i>Starred<span--}}
                {{--class="new badge">18</span></a>--}}
                {{--</li>--}}
                {{--<li class="no-padding">--}}
                {{--<a class="waves-effect waves-grey"><i class="material-icons">done</i>Sent Mail</a>--}}
                {{--</li>--}}
                <li class="no-padding">
                    <a class="waves-effect waves-grey" href="{{action('Admin\AdminController@getFormChangePassword')}}"><i
                                class="material-icons">history</i>Đổi mật khẩu</a>
                </li>
                <li class="divider"></li>
                <li class="no-padding">
                    <a class="waves-effect waves-grey" href="{{route('get.logout')}}"><i class="material-icons">exit_to_app</i>Đăng
                        xuất</a>
                </li>
            </ul>
        </div>
        <ul class="sidebar-menu collapsible collapsible-accordion" data-collapsible="accordion">
            <li class="no-padding"><a class="waves-effect waves-grey active" href="{{route('admin.dashboard')}}"><i
                            class="material-icons">settings_input_svideo</i>Bảng điều khiển</a></li>

            @if(sizeof(session('adminInfo')->role) && in_array('1',session('adminInfo')->role))
                <li class="no-padding">
                    <a href="{{route('get.customer.list')}}" class="collapsible-header waves-effect waves-grey
                    @if($link == action('Admin\CustomerController@getList')
                    || $link == action('Admin\CustomerController@getAdd')) active @endif"><i
                                class="material-icons">apps</i>Khách hàng <span
                                class="label label-danger">{{$countUserUnseen}}</span>
                        {{--<i class="nav-drop-icon material-icons">keyboard_arrow_right</i>--}}
                    </a>
                    {{--<div class="collapsible-body">--}}
                        {{--<ul>--}}
                            {{--<li @if($link == action('Admin\CustomerController@getList')) class="active-tag" @endif>--}}
                                {{--<a href="{{route('get.customer.list')}}">Danh sách Khách hàng</a></li>--}}
                            {{--<li @if($link == action('Admin\CustomerController@getAdd')) class="active-tag" @endif>--}}
                                {{--<a href="{{route('get.customer.add')}}">Thêm Khách hàng</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                </li>
            @else

            @endif
            @if(sizeof(session('adminInfo')->role) && in_array('2',session('adminInfo')->role))
                <li class="no-padding">
                    <a href="{{action('Admin\AdminController@getList')}}" class="collapsible-header waves-effect waves-grey
                        @if($link == action('Admin\AdminController@getList')
                        || $link == action('Admin\AdminController@getAdd')) active @endif
                        ">
                        <i class="material-icons">code</i>Quản trịviên
                        {{--<i class="nav-drop-icon material-icons">keyboard_arrow_right</i>--}}
                    </a>
                    {{--<div class="collapsible-body">--}}
                        {{--<ul>--}}
                            {{--<li @if($link == action('Admin\AdminController@getList')) class="active-tag" @endif>--}}
                                {{--<a href="{{action('Admin\AdminController@getList')}}">Danh sách quản trị viên</a>--}}
                            {{--</li>--}}
                            {{--<li @if($link == action('Admin\AdminController@getAdd')) class="active-tag" @endif>--}}
                                {{--<a href="{{route('get.member.add')}}">Thêm quản trị viên</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                </li>
            @else

            @endif
            @if(sizeof(session('adminInfo')->role) && in_array('8',session('adminInfo')->role))
                <li class="no-padding">
                    <a href="{{action('Admin\PostController@getList')}}" class="collapsible-header waves-effect waves-grey
                    @if($link == action('Admin\PostController@getList')
                        || $link == action('Admin\PostController@getViewAdd')) active @endif
                    "><i class="material-icons">star</i>Bài viết
                        {{--<i class="nav-drop-icon material-icons">keyboard_arrow_right</i>--}}
                    </a>
                    {{--<div class="collapsible-body">--}}
                        {{--<ul>--}}
                            {{--<li @if($link == action('Admin\PostController@getList')) class="active-tag" @endif><a href="{{action('Admin\PostController@getList')}}">Danh sách bài viết</a></li>--}}
                            {{--<li @if($link == action('Admin\PostController@getViewAdd')) class="active-tag" @endif><a href="{{action('Admin\PostController@getViewAdd')}}">Thêm bài viết</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                </li>
            @else

            @endif

            @if(sizeof(session('adminInfo')->role) && in_array('3',session('adminInfo')->role))
                <li class="no-padding">
                    <a class="collapsible-header waves-effect waves-grey
                        @if($link == action('Admin\ServiceController@getList')
                        || $link == action('Admin\ServiceController@getAdd')
                        || $link == action('Admin\TypeController@listType')
                        )
                            active
                        @endif
                            "><i class="material-icons">star_border</i>
                        Dịch vụ<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
                    <div class="collapsible-body">
                        <ul>
                            <li @if($link == action('Admin\ServiceController@getList')) class="active-tag" @endif><a href="{{action('Admin\ServiceController@getList')}}">Danh sách Dịch Vụ</a></li>
                            <li @if($link == action('Admin\TypeController@listType')) class="active-tag" @endif><a href="{{action('Admin\TypeController@listType')}}">Loại dịch vụ</a></li>
                            {{--<li @if($link == action('Admin\ServiceController@getAdd')) class="active-tag" @endif><a href="{{action('Admin\ServiceController@getAdd')}}">Thêm Dịch Vụ</a></li>--}}
                        </ul>
                    </div>
                </li>
            @else

            @endif

            @if(sizeof(session('adminInfo')->role) && in_array('4',session('adminInfo')->role))
                <li class="no-padding">
                    <a href="{{action('Admin\OrderController@getList')}}"class="collapsible-header waves-effect waves-grey
                        @if($link == action('Admin\OrderController@getList')
                        || $link == action('Admin\OrderController@getAdd')
                        )
                            active
                        @endif

                        "><i class="material-icons">desktop_windows</i>Đơn
                        hàng <span class="label label-danger">{{$countOrderUnseen}}</span>
                        {{--<i class="nav-drop-icon material-icons">keyboard_arrow_right</i>--}}
                    </a>
                    {{--<div class="collapsible-body">--}}
                        {{--<ul>--}}
                            {{--<li  @if($link == action('Admin\OrderController@getList')) class="active-tag" @endif><a href="{{action('Admin\OrderController@getList')}}">Danh sách Đơn Hàng</a></li>--}}
                            {{--<li  @if($link == action('Admin\OrderController@getAdd')) class="active-tag" @endif><a href="{{action('Admin\OrderController@getAdd')}}">Thêm Đơn Hàng</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                </li>
            @else

            @endif

            @if(sizeof(session('adminInfo')->role) && in_array('5',session('adminInfo')->role))
                <li class="no-padding">
                    <a href="{{action('Admin\SupportController@getList')}}"class="collapsible-header waves-effect waves-grey

                        @if($link == action('Admin\SupportController@getList')
                        || $link == action('Admin\SupportController@getAdd')
                        )
                            active
                        @endif
                        "><i class="material-icons">mode_edit</i>Hỗ trợ
                        <span class="label label-danger">{{$countMessage}}</span>
                        {{--<i class="nav-drop-icon material-icons">keyboard_arrow_right</i>--}}
                    </a>
                    {{--<div class="collapsible-body">--}}
                        {{--<ul>--}}
                            {{--<li @if($link == action('Admin\SupportController@getList')) class="active-tag" @endif><a href="{{action('Admin\SupportController@getList')}}">Danh sách Hỗ trợ</a></li>--}}
                            {{--<li @if($link == action('Admin\SupportController@getAdd')) class="active-tag" @endif><a href="{{action('Admin\SupportController@getAdd')}}">Thêm Hỗ Trợ</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                </li>
            @else

            @endif

            @if(sizeof(session('adminInfo')->role) && in_array('6',session('adminInfo')->role))
                <li class="no-padding">
                    <a class="collapsible-header waves-effect waves-grey
                        @if($link == action('Admin\EmailMarketing\CampaignController@index')
                        || $link == action('Admin\EmailMarketing\EmailController@index')
                        || $link == action('Admin\EmailMarketing\GroupController@index')
                        )
                            active
                        @endif
                        "><i class="material-icons">mail</i>Email
                        marketing<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
                    <div class="collapsible-body">
                        <ul>
                            <li @if($link == action('Admin\EmailMarketing\EmailController@index')) class="active-tag" @endif><a href="{{action('Admin\EmailMarketing\EmailController@index')}}">Danh sách Email</a>
                            </li>
                            <li @if($link == action('Admin\EmailMarketing\GroupController@index')) class="active-tag" @endif><a href="{{action('Admin\EmailMarketing\GroupController@index')}}">Nhóm Email</a></li>
                            <li @if($link == action('Admin\EmailMarketing\CampaignController@index')) class="active-tag" @endif><a href="{{action('Admin\EmailMarketing\CampaignController@index')}}">Chiến dịch</a>
                            </li>
                            <li><a href="">Template Email</a></li>
                            <li><a href="">AutoResponse</a></li>
                            <li><a href="">Thống kê</a></li>
                        </ul>
                    </div>
                </li>
            @else

            @endif

            @if(sizeof(session('adminInfo')->role) && in_array('7',session('adminInfo')->role))
                <li class="no-padding">
                    <a href="{{action('Admin\UserOptionController@insert')}}" class="collapsible-header waves-effect waves-grey
                        @if($link == action('Admin\UserOptionController@index') || $link == action('Admin\UserOptionController@insert'))
                            active
                        @endif
                        "><i class="material-icons">tag_faces</i>Thêm tùy chỉnh
                        {{--<i class="nav-drop-icon material-icons">keyboard_arrow_right</i>--}}
                    </a>
                    {{--<div class="collapsible-body">--}}
                        {{--<ul>--}}
                            {{--<li @if($link == action('Admin\UserOptionController@insert')) class="active-tag" @endif><a href="{{action('Admin\UserOptionController@insert')}}">Option Khách hàng</a></li>--}}
                            {{--<li><a href="{{action('Admin\UserOptionController@insert')}}">Option Email</a></li>--}}

                        {{--</ul>--}}
                    {{--</div>--}}
                </li>
            @else

            @endif

        </ul>
        <div class="footer">
            <p class="copyright">Romecody.com ©</p>
            <a href="#!">Privacy</a> &amp; <a href="#!">Terms</a>
        </div>
    </div>
</aside>