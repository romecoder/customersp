
<script src="{{asset('CS2/plugins/jquery/jquery-2.2.0.min.js')}}"></script>
@yield('afterJquery')
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="{{asset('CS2/plugins/materialize/js/materialize.min.js')}}"></script>
<script src="{{asset('CS2/plugins/material-preloader/js/materialPreloader.min.js')}}"></script>
<script src="{{asset('CS2/plugins/jquery-blockui/jquery.blockui.js')}}"></script>
<script src="{{asset('CS2/plugins/waypoints/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('CS2/plugins/counter-up-master/jquery.counterup.min.js')}}"></script>
<script src="{{asset('CS2/plugins/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('CS2/plugins/chart.js/chart.min.js')}}"></script>
<script src="{{asset('CS2/plugins/flot/jquery.flot.min.js')}}"></script>
<script src="{{asset('CS2/plugins/flot/jquery.flot.time.min.js')}}"></script>
<script src="{{asset('CS2/plugins/flot/jquery.flot.symbol.min.js')}}"></script>
<script src="{{asset('CS2/plugins/flot/jquery.flot.resize.min.js')}}"></script>
<script src="{{asset('CS2/plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{asset('CS2/plugins/curvedlines/curvedLines.js')}}"></script>
<script src="{{asset('CS2/plugins/peity/jquery.peity.min.js')}}"></script>
<script src="{{asset('CS2/js/alpha.min.js')}}"></script>

<script src="{{asset('CS2/js/pages/dashboard.js')}}"></script>


