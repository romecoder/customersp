<!-- Title -->
<title>@yield('title')</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<meta charset="UTF-8">
<meta name="description" content="Modify Rome Template"/>
<meta name="keywords" content="admin,dashboard"/>
<meta name="author" content="Steelcoders"/>

<!-- Styles -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link type="text/css" rel="stylesheet" href="{{asset('CS2/plugins/materialize/css/materialize.css')}}"/>
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="{{asset('CS2/plugins/metrojs/MetroJs.min.css')}}" rel="stylesheet">
<link href="{{asset('CS2/plugins/weather-icons-master/css/weather-icons.min.css')}}" rel="stylesheet">
<!-- Theme Styles -->
<link href="{{asset('CS2/css/alpha.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('CS2/css/custom.css')}}" rel="stylesheet" type="text/css"/>
{{--<script src="{{asset('CS2/plugins/jquery/jquery-2.2.0.min.js')}}"></script>--}}
<script defer scr="{{asset('CS2/ownjquery.js')}}"></script>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<style>
    li{
        margin-right: -11px;
    }
    li a.btn{
        width: 15px;
        height: 30px
    }
    li a i{
        font-size: 13px !important;
        margin-left:-3px !important;
    }
    li a:hover{
        text-decoration: none;
    }
    li.active-tag a {
        color: #111111 !important;
        background-color: rgb(230, 230, 230);
    }
    input {
        border: none !important;
    }
    input:focus:not([readonly]){
        border: none !important;
    }
    a{
        text-decoration: none !important;
    }
    @yield('style')
</style>

