
<header class="mn-header navbar-fixed">
    <nav class="cyan darken-1">
        <div class="nav-wrapper row">
            <section class="material-design-hamburger navigation-toggle">
                <a href="javascript:void(0)" data-activates="slide-out" class="button-collapse show-on-large material-design-hamburger__icon">
                    <span class="material-design-hamburger__layer"></span>
                </a>
            </section>
            <div class="header-title col s2 m2">
                <span class="chapter-title">Pveser</span>
            </div>
            <form class="left search col s9 hide-on-small-and-down" method="get" action="{{action('Admin\SearchController@searching')}}">
                {{csrf_field()}}
                <div class="row">
                    <div class="input-field col s9">
                        <input id="search" type="search" placeholder="Search" autocomplete="off" name="search">
                        <label for="search"><i class="material-icons search-icon">search</i></label>
                    </div>
                    {{--<div class="col s5 pull-right" style="height: 50px">--}}
                        {{--<span>--}}
                            {{--<input type="checkbox" value="admin" name="choose">--}}
                            {{--<label for="" style="padding-left: 25px;">Admin</label>--}}
                        {{--</span>--}}
                        {{--<span>--}}
                            {{--<input type="checkbox" value="customer" name="choose">--}}
                            {{--<label for=""  style="padding-left: 25px;">Customer</label>--}}
                        {{--</span>--}}
                        {{--<span>--}}
                            {{--<input type="checkbox" value="service" name="choose">--}}
                            {{--<label for=""  style="padding-left: 25px;">Service</label>--}}
                        {{--</span>--}}
                    {{--</div>--}}
                </div>

            </form>
            {{--<ul class="right col s1 m1 nav-right-menu">--}}
                {{--<li class="hide-on-small-and-down"><a href="javascript:void(0)" data-activates="dropdown1" class="dropdown-button dropdown-right show-on-large"><i class="material-icons">notifications_none</i><span class="badge">4</span></a></li>--}}
                {{--<li class="hide-on-med-and-up"><a href="javascript:void(0)" class="search-toggle"><i class="material-icons">search</i></a></li>--}}
            {{--</ul>--}}


        </div>
    </nav>
</header>