@extends('Admin.master')
@section('title','Dashboard')
@section('headlink')
    <link href="{{asset('CS2/plugins/select2/css/select2.css')}}" rel="stylesheet">
@endsection
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title"><h5>Tạo chiến dịch mới</h5></div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <form class="col s12 m12" method="post"
                                  action="{{action('Admin\EmailMarketing\CampaignController@store')}}"
                                  enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="name" type="text" class="" value="{{old('name')}}" name="name">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('name') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="name" class="active">Tên chiến dịch</label>
                                    </div>
                                </div>
                                <div class="row">
                                    @if($groupList)
                                        <div class="input-field col s12">
                                            <select class="js-states browser-default" multiple=""
                                                    tabindex="-1" style="width: 100%" id="multiple" name="group[]">
                                                @foreach($groupList as $group)
                                                    <option value="{{$group->id}}">{{$group->name}}</option>
                                                @endforeach
                                            </select>
                                            <label for="group" class="active">Gửi cho nhóm email</label>
                                        </div>
                                        @if(count($errors)>0)
                                            @foreach($errors->get('group') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                    @else
                                        <p><i>Hệ thống chưa tạo Nhóm email...</i></p>
                                    @endif
                                </div>
                                {{--<div class="row">--}}
                                    {{--@if($contentList)--}}
                                        {{--<div class="input-field col s12">--}}
                                            {{--<select class="js-states browser-default" multiple="multiple"--}}
                                                    {{--tabindex="-1" style="width: 100%" id="multiple" name="group[]">--}}
                                                {{--@foreach($contentList as $contetn)--}}
                                                    {{--<option value="{{$contetn->id}}">{{$contetn->name}}</option>--}}
                                                {{--@endforeach--}}
                                            {{--</select>--}}
                                            {{--<label for="email">Gửi cho nhóm email</label>--}}
                                        {{--</div>--}}
                                        {{--@if(count($errors)>0)--}}
                                            {{--@foreach($errors->get('group') as $error)--}}
                                                {{--<p style="color: red">{{$error}}</p>--}}
                                            {{--@endforeach--}}
                                        {{--@endif--}}
                                    {{--@else--}}
                                        {{--<p><i class="grey-text">Hệ thống chưa tạo nội dung email...</i></p>--}}
                                    {{--@endif--}}
                                {{--</div>--}}

                        <div class="row">
                            <div class="input-field col s12">
                                <label for="name" class="active">Nội dung</label>
                                <br>
                                <textarea id="textarea1" name="contents">{!!old('contents')!!}</textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col s6">
                                <div class="col s6">
                                    <!-- Switch -->
                                    <h6 for="">Trạng thái</h6>
                                    <div class="switch m-b-md">
                                        <label>
                                            Chờ kích hoạt
                                            <input type="checkbox" name="status" value="1" class="validate">
                                            <span class="lever"></span>
                                            kích hoạt
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col cs12 pull-left">
                                <input type="submit" class="btn btn-block btn-primary btn-lg"
                                       value="Tạo chiến dịch">
                            </div>
                        </div>
                    </div>
                    {{csrf_field()}}

                    </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <!---->
        </div>
    </main>
@endsection
@section('footlink')
    <script src="{{asset('CS2/plugins/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('CS2/js/alpha.min.js')}}"></script>
    <script src="{{asset('CS2/js/pages/form-select2.js')}}"></script>
    <script>
        CKEDITOR.replace('textarea1');
    </script>
@endsection

@section('afterJquery')
    <script src="{{asset('CS2/js/pages/form-select2.js')}}"></script>
    <script src="{{asset('CS2/js/ckeditor/ckeditor.js')}}"></script>
@endsection