@extends('Admin.master')
@section('title','Dashboard')
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="col s12 pull-left text-center">
                    <span class="page-title"><label for="" style="font-size: 14px">Danh sách các chiến dịch</label></span>
                    <span class="btn btn-primary pull-right"><a
                                href="{{action('Admin\EmailMarketing\CampaignController@insert')}}"><i
                                    class="fa fa-user-plus" aria-hidden="true"
                                    style="color: #ffffff"></i></a></span>
                </div>
                <div class="col s12 pull-left text-center">
                    @include('General.displayerrors')
                </div>
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <table id="example"
                               class="display responsive-table datatable-example highlight striped bordered">

                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Tên</th>
                                <th>Nhóm Email</th>
                                <th>Ngày</th>
                                <th>Trạng thái</th>
                                <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $count = 0;?>
                            @if(sizeof($campaignList))
                                @foreach($campaignList as $campaign)
                                    <tr>
                                        <td>{{$loop->index+1}}</td>
                                        <td>@if($campaign->status == 1)<span class="grey-text">{{$campaign->name}}</span>@else <span class="red-text">{{$campaign->name}}</span>@endif</td>
                                        <td>
                                            @if(sizeof($campaign->group_id))
                                                @foreach($campaign->group_id as $name)
                                                    <span class="chip">{{$name}}</span>
                                                @endforeach
                                            @else
                                            @endif
                                        </td>
                                        {{--Phần này xử lý hiển thị Thời gian tự động theo giây, phút, giờ, ngày--}}
                                        <?php
                                        date_default_timezone_set("Asia/Bangkok");
                                        $now = strtotime(date('Y-m-d H:i:s'));
                                        $createTime = strtotime($campaign->created_at);
                                        $subTime = $now - $createTime;
                                        $subTimeSecond = $subTime;
                                        $subTimeMinute = round($subTime / 60);
                                        $subTimeHour = round($subTime / 3600);
                                        $subTimeDay = round($subTime / 86400);
                                        ?>
                                        <td>
                                            <span class="text-warning">
                                                @if($subTime < 60)
                                                    <label for="">{{$subTimeSecond}} Giây trước</label>
                                                @elseif($subTime > 60 && $subTime < 3600)
                                                    <label for="">{{$subTimeMinute}} Phút trước</label>
                                                @elseif($subTime > 3600 && $subTime < 86400)
                                                    <label for="">{{$subTimeHour}} Giờ trước</label>
                                                @elseif($subTime > 84600 && $subTime < 86400*10)
                                                    <label for="">{{$subTimeDay}} Ngày trước</label>
                                                @elseif($subTime > 84600*10)
                                                    {{date('d-m-Y',$createTime)}}
                                                @endif
                                            </span>
                                        </td>
                                        {{--Kết thúc xử lý ngày--}}
                                        @if($campaign->status == 1)
                                            <td>
                                                <a href="" class="green-text">Đã chạy</a>
                                            </td>
                                        @else
                                            <td>
                                                <a href="" class="red-text">Chưa chạy</a>
                                            </td>
                                        @endif
                                        <td>
                                            <ul class="list-inline">
                                                @if($campaign->status == 1)
                                                    <li>
                                                        <a href="{{action('Admin\EmailMarketing\CampaignController@show',['id'=>$campaign->id])}}"
                                                           class="btn grey" title="Chi tiết"><i class="fa fa-search"
                                                                                      aria-hidden="true"></i></a>
                                                    </li>
                                                @else
                                                    <li>
                                                    <a href="{{action('Admin\EmailMarketing\CampaignController@edit',['id'=>$campaign->id])}}"
                                                       class="btn orange" title="chỉnh sửa"><i class="fa fa-pencil"
                                                                                  aria-hidden="true"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="{{action('Admin\EmailMarketing\CampaignController@delete',['id'=>$campaign->id])}}"
                                                           class="btn red" title="Xóa chiến dịch" style="width: 15px; height: 30px"
                                                           onclick="return confirm('Bạn có thực sự muốn xóa Chiến dịch này?')">
                                                            <i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="{{action('Admin\EmailMarketing\CampaignController@run',['id'=>$campaign->id])}}"
                                                           class="btn blue" title="Chạy"><i class="fa fa-check"
                                                                                                     aria-hidden="true"></i></a>
                                                    </li>
                                                @endif


                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <span><i>Chưa có Chiến dịch nào...</i></span>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @if(sizeof($campaignList))
            <div class="col-md-12">
                <div class="paginate">
                    <p class="pull-left">Tổng số trang : {{$campaignList->lastPage()}}</p>
                    <ul class="pagination pull-right">
                        <li class="">
                            <a href="{{$campaignList->url(1)}}">
                                <i class="ace-icon fa fa-angle-double-left"></i>
                            </a>
                        </li>
                        <li class="prev {{($campaignList->currentPage() == 1) ? 'disabled' : ''}}">
                            <a href="{{$campaignList->url($campaignList->currentPage() - 1)}}">Trước</a>
                        </li>
                        @for($i=1; $i<=$campaignList->lastPage();$i++ )
                            <li class="{{ ($campaignList->currentPage() == $i) ? 'active' : '' }}">
                                <a href="{{$campaignList->url($i)}}">{{$i}}</a>
                            </li>
                        @endfor
                        <li class="next {{($campaignList->currentPage() == $campaignList->lastPage()) ? 'disabled' : ''}}">
                            <a href="{{$campaignList->url($campaignList->currentPage() + 1)}}">Sau</a>
                        </li>
                        <li class="">
                            <a href="{{$campaignList->url($campaignList->lastPage())}}">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        @else
            <div class="col-md-12">
                <span><i>Chưa có chiến dịch nào...</i></span>
            </div>
        @endif
    </main>

@endsection
