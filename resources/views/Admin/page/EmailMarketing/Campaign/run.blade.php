@extends('Admin.master')
@section('title','Dashboard')
@section('headlink')
    <link href="{{asset('CS2/plugins/select2/css/select2.css')}}" rel="stylesheet">
@endsection
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title"><label class="active" style="font-size: 14px">Chạy chiến dịch</label></div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
            </div>
            <div class="col s12 m6 l6">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <form class="col s12 m12" method="post"
                                  action="{{action('Admin\EmailMarketing\CampaignController@action',['id'=>$thisCampaign->id])}}"
                                  enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input type="hidden" value="{{json_encode($emailList)}}" name="emailList">
                                <input type="hidden" value="{{$thisContent}}" name="content">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <br>
                                        <p><b>{{$thisCampaign->name}}</b></p>
                                        <label for="name" class="active">Tên chiến dịch:</label>
                                        <input type="hidden" value="{{$thisCampaign->name}}" name="title">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <br>
                                        @if(sizeof($thisCampaign->groupId))
                                            @foreach($thisCampaign->groupId as $group)
                                                <span value="" class="chip">{{$group->name}}</span>
                                            @endforeach
                                        @endif

                                        <label for="name" class="active">Các nhóm Email:</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <br>
                                        @if(sizeof($emailList))
                                            @foreach($emailList as $email)
                                                <span value="" class="chip">{{$email}}</span>
                                            @endforeach
                                        @endif

                                        <label for="name" class="active">Danh sách Email cụ thể:</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <br>
                                        {{--@if($thisContent != null)--}}
                                            {{--<input type="text" name="contents" value="{{$thisContent->content}}">--}}
                                        {{--@else--}}
                                        {{--@endif--}}
                                        <p>{!! $thisCampaign->content  !!}</p>
                                        <input type="hidden" value="{{$thisCampaign->content }}" name="contents">

                                        <label for="name" class="active">Nội dung gửi:</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s12">
                                        <p><i><label class="grey-text">Bạn nên gửi thử trước khi gửi thật</label></i>
                                        </p>
                                        <div class="switch m-b-md">
                                            <label>
                                                Chạy thủ
                                                <input type="checkbox" name="test">
                                                <span class="lever"></span>
                                                Chạy thật luôn
                                            </label>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input type="email" value="{{old('email')}}"
                                               placeholder="Nhập vào email cần test thử." name="testMail">
                                        <label for="name" class="active">Email Test:</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col cs6 pull-left">
                                        <input type="submit" class="btn btn-block red btn-lg" value="Gửi">
                                    </div>
                                </div>
                                {{csrf_field()}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!---->
        </div>
    </main>
@endsection
@section('footlink')
    <script src="{{asset('CS2/plugins/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('CS2/js/alpha.min.js')}}"></script>
    <script src="{{asset('CS2/js/pages/form-select2.js')}}"></script>
    <script>
        CKEDITOR.replace('textarea1');
    </script>
@endsection

@section('afterJquery')
    <script src="{{asset('CS2/js/pages/form-select2.js')}}"></script>
    <script src="{{asset('CS2/js/ckeditor/ckeditor.js')}}"></script>
@endsection