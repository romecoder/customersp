@extends('Admin.master')
@section('title','Dashboard')
@section('headlink')
    <link href="{{asset('CS2/plugins/select2/css/select2.css')}}" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
@endsection
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title"><h5>Chỉnh sửa Nhóm Email</h5></div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <form class="col s12 m12" method="post"
                                  action="{{action('Admin\EmailMarketing\GroupController@update',['id'=>$thisGroup->id])}}"
                                  enctype="multipart/form-data">
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="name" type="text" class=""
                                               value="{{old('name',isset($thisGroup)? $thisGroup->name:'')}}"
                                               name="name">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('name') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="name" class="active">Tên:</label>
                                    </div>
                                </div>
                                <div class="row">99
                                    <div class="col s6">
                                        <div class="switch m-b-md">
                                            <label>
                                                Chờ kích hoạt
                                                @if($thisGroup->status)
                                                    <input type="checkbox" name="status" checked>
                                                    <span class="lever"></span>
                                                @else
                                                    <input type="checkbox" name="status" class="validate">
                                                    <span class="lever"></span>
                                                @endif
                                                kích hoạt
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col cs12 pull-left">
                                        <input type="submit" class="btn btn-block btn-primary btn-lg"
                                               value="Cập nhật">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!---->
        </div>
    </main>
    <!-- Modal -->
@endsection
@section('footlink')
    <script src="{{asset('CS2/plugins/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('CS2/js/alpha.min.js')}}"></script>
    <script src="{{asset('CS2/js/pages/form-select2.js')}}"></script>
    <script>
        CKEDITOR.replace('textarea1');
    </script>
@endsection

@section('afterJquery')
    <script src="{{asset('CS2/js/pages/form-select2.js')}}"></script>
    <script src="{{asset('CS2/js/ckeditor/ckeditor.js')}}"></script>
@endsection