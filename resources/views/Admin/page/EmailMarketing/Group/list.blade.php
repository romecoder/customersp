@extends('Admin.master')
@section('title','Dashboard')
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="col s12 pull-left text-center">
                    <span class="page-title"><label for="" style="font-size: 14px">Danh sách các nhóm Email</label></span>
                    <span class="btn btn-primary pull-right"><a href="{{action('Admin\EmailMarketing\GroupController@create')}}"><i
                                    class="fa fa-user-plus" aria-hidden="true"
                                    style="color: #ffffff"></i></a></span>
                </div>
                <div class="col s12 pull-left text-center">
                    @include('General.displayerrors')
                </div>
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <table id="example" class="display responsive-table datatable-example highlight striped bordered">

                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nhóm</th>
                                <th>Trạng thái</th>
                                <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $count = 0;?>
                            @if(isset($groupList) && $groupList)
                                @foreach($groupList as $group)
                                    <tr>
                                        <td>{{$loop->index+1}}</td>
                                        <td>
                                            <a href="">{{$group->name}}</a>
                                        </td>

                                        @if($group->status == 1)
                                            <td>
                                                <a href="" class="green-text">Sẵn sàng!!</a>
                                            </td>
                                        @else
                                            <td>
                                                <a href="" class="red-text">Chờ...</a>
                                            </td>
                                        @endif
                                        <td>
                                            <ul class="list-inline">
                                                <li>
                                                    <a href="{{action('Admin\EmailMarketing\GroupController@edit',['id'=>$group->id])}}"
                                                       class="btn blue"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                </li>
                                                <li>
                                                    <a href="{{action('Admin\EmailMarketing\GroupController@delete',['id'=>$group->id])}}"
                                                       class="btn red" style="width: 15px; height: 30px"
                                                       onclick="return confirm('Bạn có thực sự muốn xóa Nhóm này?')">
                                                        <i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <span><i>Chưa có nhóm Email nào...</i></span>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @if(sizeof($groupList))
            <div class="col-md-12">
                <div class="paginate">
                    <p class="pull-left">Tổng số trang : {{$groupList->lastPage()}}</p>
                    <ul class="pagination pull-right">
                        <li class="">
                            <a href="{{$groupList->url(1)}}">
                                <i class="ace-icon fa fa-angle-double-left"></i>
                            </a>
                        </li>
                        <li class="prev {{($groupList->currentPage() == 1) ? 'disabled' : ''}}">
                            <a href="{{$groupList->url($groupList->currentPage() - 1)}}">Trước</a>
                        </li>
                        @for($i=1; $i<=$groupList->lastPage();$i++ )
                            <li class="{{ ($groupList->currentPage() == $i) ? 'active' : '' }}">
                                <a href="{{$groupList->url($i)}}">{{$i}}</a>
                            </li>
                        @endfor
                        <li class="next {{($groupList->currentPage() == $groupList->lastPage()) ? 'disabled' : ''}}">
                            <a href="{{$groupList->url($groupList->currentPage() + 1)}}">Sau</a>
                        </li>
                        <li class="">
                            <a href="{{$groupList->url($groupList->lastPage())}}">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        @else
            <div class="col-md-12">
                <span><i>Chưa có email nào...</i></span>
            </div>
        @endif
    </main>

@endsection
