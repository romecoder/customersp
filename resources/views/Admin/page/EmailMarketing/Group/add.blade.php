@extends('Admin.master')
@section('title','Dashboard')
@section('headlink')
    <link href="{{asset('CS2/plugins/select2/css/select2.css')}}" rel="stylesheet">
@endsection
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title"><h5>Thêm nhóm Email</h5></div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <form class="col s12 m6" method="post" action="{{action('Admin\EmailMarketing\GroupController@store')}}"
                                  enctype="multipart/form-data">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="name" type="text" class="" value="{{old('name')}}" name="name">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('name') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="name">Tên</label>
                                    </div>
                                </div>
                        </div>
                        <div class="row">
                            <div class="col s6">
                                <h6 for="">Trạng thái</h6>
                                <div class="switch m-b-md">
                                    <label>
                                        Chờ kích hoạt
                                        <input type="checkbox" name="status" value="1" class="validate">
                                        <span class="lever"></span>
                                        kích hoạt
                                    </label>
                                </div>
                            </div>
                        </div>
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col cs12 pull-right">
                                <input type="submit" class="btn btn-block btn-primary btn-lg"
                                       value="Thêm mới">
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!---->
        </div>
    </main>
@endsection
@section('footlink')
    <script src="{{asset('CS2/plugins/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('CS2/js/alpha.min.js')}}"></script>
    <script src="{{asset('CS2/js/pages/form-select2.js')}}"></script>
    <script>
        CKEDITOR.replace('textarea1');
    </script>
@endsection

@section('afterJquery')
    <script src="{{asset('CS2/js/pages/form-select2.js')}}"></script>
    <script src="{{asset('CS2/js/ckeditor/ckeditor.js')}}"></script>
@endsection