@extends('Admin.master')
@section('title','Sửa Email')
@section('headlink')
    <link href="{{asset('CS2/plugins/select2/css/select2.css')}}" rel="stylesheet">
@endsection
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title"><h5>Sửa Email</h5></div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <form class="col s12 m12" method="post"
                              action="{{action('Admin\EmailMarketing\EmailController@update',['id'=>$thisEmail->id])}}"
                              enctype="multipart/form-data">
                            <div class="row">
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="name" type="text" class="" value="{{old('name',$thisEmail->name)}}"
                                               name="name">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('name') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="name" class="active">Name</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="email" type="email" class=""
                                               value="{{old('email',$thisEmail->email)}}" name="email">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('email') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="email" class="active">Email</label>
                                    </div>
                                </div>
                                <div class="row">
                                    @if($groupList)
                                        <div class="input-field col s12">
                                            <select class="js-states browser-default" multiple="multiple"
                                                    tabindex="-1" style="width: 100%" id="multiple" name="group[]">
                                                @foreach($groupList as $group)
                                                    <option value="{{$group->id}}">{{$group->name}}</option>
                                                @endforeach
                                            </select>
                                            <label for="email" class="active">Nhóm (danh sách Emai)</label>
                                        </div>
                                        @if(count($errors)>0)
                                            @foreach($errors->get('group') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                    @else
                                        <p><i>Hệ thống chưa tạo Nhóm email...</i></p>
                                    @endif
                                </div>

                                {{--<div class="input-field col s6">--}}
                                {{--<div class="file-field input-field">--}}
                                {{--<div class="btn teal lighten-1">--}}
                                {{--<span>Nhập vào file CSV</span>--}}
                                {{--<input type="file" name="file" value="{{old('file')}}">--}}
                                {{--</div>--}}
                                {{--<div class="file-path-wrapper">--}}
                                {{--<input class="file-path validate" type="text"--}}
                                {{--value="{{old('file')}}">--}}
                                {{--@if(count($errors)>0)--}}
                                {{--@foreach($errors->get('avatar') as $error)--}}
                                {{--<p style="color: red">{{$error}}</p>--}}
                                {{--@endforeach--}}
                                {{--@endif--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                            </div>
                            <div class="row">
                                <div class="col s6">
                                    <div class="col s6">
                                        <!-- Switch -->
                                        <h6 for="">Loại</h6>
                                        <div class="switch m-b-md">
                                            <label>
                                                Thường
                                                @if($thisEmail->type == 1)
                                                    <input type="checkbox" name="type" value="1" class="validate" checked>
                                                @else
                                                    <input type="checkbox" name="type" value="0" class="validate">
                                                @endif
                                                <span class="lever"></span>
                                                OPT
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col s6">
                                        <!-- Switch -->
                                        <h6 for="">Trạng thái</h6>
                                        <div class="switch m-b-md">
                                            <label>
                                                Chờ kích hoạt
                                                @if($thisEmail->status == 1)
                                                    <input type="checkbox" name="status" value="1" class="validate" checked>
                                                @else
                                                    <input type="checkbox" name="status" value="0" class="validate">
                                                @endif
                                                <span class="lever"></span>
                                                kích hoạt
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col cs12 pull-right">
                            <input type="submit" class="btn btn-block btn-primary btn-lg"
                                   value="Thêm mới">
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        </div>
        <!---->
        </div>
    </main>
@endsection
@section('footlink')
    <script src="{{asset('CS2/plugins/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('CS2/js/alpha.min.js')}}"></script>
    <script src="{{asset('CS2/js/pages/form-select2.js')}}"></script>
    <script>
        CKEDITOR.replace('textarea1');
    </script>
@endsection

@section('afterJquery')
    <script src="{{asset('CS2/js/pages/form-select2.js')}}"></script>
    <script src="{{asset('CS2/js/ckeditor/ckeditor.js')}}"></script>
@endsection