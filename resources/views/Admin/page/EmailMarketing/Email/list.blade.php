@extends('Admin.master')
@section('title','Dashboard')
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="col s12 pull-left text-center">
                    <span class="page-title"><label for="" style="font-size: 14px">Quản Lý Email</label></span>
                    <span class="btn btn-primary pull-right"><a href="{{action('Admin\EmailMarketing\EmailController@insert')}}"><i
                                    class="fa fa-user-plus" aria-hidden="true"
                                    style="color: #ffffff"></i></a></span>
                </div>
                <div class="col s12 pull-left text-center">
                    @include('General.displayerrors')
                </div>
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <table id="example" class="display responsive-table datatable-example highlight striped">

                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Email</th>
                                <th>Loại</th>
                                <th>Trạng thái</th>
                                <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $count = 0;?>
                            @if(isset($emailList) && $emailList)
                                @foreach($emailList as $email)
                                    <tr>
                                        <td>{{$loop->index+1}}</td>
                                        <td>
                                            <a href="">{{$email->email}}</a> ({{$email->name}})
                                        </td>
                                        @if($email->type == 1)
                                            <td class="text-warning"> <span class="chip">OPT</span></td>
                                        @else
                                            <td class="text-info"> <span class="chip">Thường</span></td>
                                        @endif
                                        @if($email->status == 1)
                                            <td>
                                                <a href="" class="green-text">Sẵn sàng!!</a>
                                            </td>
                                        @else
                                            <td>
                                                <a href="" class="red-text">Chờ...</a>
                                            </td>
                                        @endif
                                        <td>
                                            <ul class="list-inline">
                                                <li>
                                                    <a href="{{action('Admin\EmailMarketing\EmailController@edit',['id'=>$email->id])}}"
                                                       class="btn blue"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                </li>
                                                <li>
                                                    <a href="{{action('Admin\EmailMarketing\EmailController@delete',['id'=>$email->id])}}"
                                                       class="btn red" style="width: 15px; height: 30px"
                                                       onclick="return confirm('Bạn có thực sự muốn xóa Email này?')">
                                                        <i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <span><i>Chưa có email nào...</i></span>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @if(sizeof($emailList))
            <div class="col-md-12">
                <div class="paginate">
                    <p class="pull-left">Tổng số trang : {{$emailList->lastPage()}}</p>
                    <ul class="pagination pull-right">
                        <li class="">
                            <a href="{{$emailList->url(1)}}">
                                <i class="ace-icon fa fa-angle-double-left"></i>
                            </a>
                        </li>
                        <li class="prev {{($emailList->currentPage() == 1) ? 'disabled' : ''}}">
                            <a href="{{$emailList->url($emailList->currentPage() - 1)}}">Trước</a>
                        </li>
                        @for($i=1; $i<=$emailList->lastPage();$i++ )
                            <li class="{{ ($emailList->currentPage() == $i) ? 'active' : '' }}">
                                <a href="{{$emailList->url($i)}}">{{$i}}</a>
                            </li>
                        @endfor
                        <li class="next {{($emailList->currentPage() == $emailList->lastPage()) ? 'disabled' : ''}}">
                            <a href="{{$emailList->url($emailList->currentPage() + 1)}}">Sau</a>
                        </li>
                        <li class="">
                            <a href="{{$emailList->url($emailList->lastPage())}}">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        @else
            <div class="col-md-12">
                <span><i>Chưa có email nào...</i></span>
            </div>
        @endif
    </main>

@endsection
