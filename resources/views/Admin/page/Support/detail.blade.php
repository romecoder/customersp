@extends('Admin.master')
@section('title','Dashboard')
@section('headlink')
    <link href="{{asset('CS2/plugins/material-preloader/css/materialPreloader.min.css')}}" rel="stylesheet">
    <script src="{{asset('CS2/plugins/vertical-timeline/js/modernizr.js')}}"></script>
    <link href="{{asset('CS2/plugins/vertical-timeline/css/style.css')}}" rel="stylesheet">
@endsection
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title"><label for="">Lịch sử Hỗ trợ:</label></div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
            </div>
            <div class="col s12">

                <section id="cd-timeline" class="cd-container">
                    @if($listDetailSupport)
                        @foreach($listDetailSupport as $detailSupport)
                            <div class="cd-timeline-block">
                                @if($detailSupport->author == 1)
                                    <div class="cd-timeline-img cd-picture">
                                        <img src="{{asset('CS2/images/avatar').'/'.$thisSupport->admin_id->avatar}}"
                                             alt="Picture" class="img-circle img-thumbnail">
                                    </div>
                                    <div class="cd-timeline-content">
                                        <p>{{$detailSupport->content}}</p>
                                        @if($detailSupport->seen)
                                        <p><i><span class="pull-right text-info" style="font-size: 10px">Đã xem</span></i></p>
                                        @endif

                                        {{--Phần này xử lý hiển thị Thời gian tự động theo giây, phút, giờ, ngày--}}
                                        <?php
                                        date_default_timezone_set("Asia/Bangkok");
                                        $now = strtotime(date('Y-m-d H:i:s'));
                                        $createTime = strtotime($detailSupport->create_at);
                                        $subTime =  $now - $createTime  ;
                                        $subTimeSecond = $subTime;
                                        $subTimeMinute = round($subTime/60);
                                        $subTimeHour =  round($subTime/3600);
                                        $subTimeDay =  round($subTime/86400);
                                        ?>
                                        @if($subTime < 60)
                                            <span class="cd-date">{{$subTimeSecond}} Giây trước</span>
                                        @elseif($subTime > 60 && $subTime < 3060)
                                            <span class="cd-date">{{$subTimeMinute}} Phút trước</span>
                                        @elseif($subTime > 3600 && $subTime < 86400)
                                            <span class="cd-date">{{$subTimeHour}} Giờ trướ</span>
                                        @elseif($subTime > 84600 && $subTime < 86400*10)
                                            <span class="cd-date">{{$subTimeDay}} Ngày trước</span>
                                        @elseif($subTime > 84600*10)
                                            {{date('d-m-Y',$createTime)}}
                                        @endif

                                        {{--Kết thúc xử lý ngày--}}
                                    </div>
                                @elseif($detailSupport->author == 0)
                                    <div class="cd-timeline-img cd-movie">
                                        <img src="{{asset('CS2/images/avatar').'/'.$thisSupport->user_id->avatar}}"
                                             alt="Movie" class="img-circle img-thumbnail">
                                    </div>
                                    <div class="cd-timeline-content">
                                        {{--Phần này xử lý hiển thị Thời gian tự động theo giây, phút, giờ, ngày--}}
                                        <?php
                                        date_default_timezone_set("Asia/Bangkok");
                                        $now = strtotime(date('Y-m-d H:i:s'));
                                        $createTime = strtotime($detailSupport->create_at);
                                        $subTime =  $now - $createTime  ;
                                        $subTimeSecond = $subTime;
                                        $subTimeMinute = round($subTime/60);
                                        $subTimeHour =  round($subTime/3600);
                                        $subTimeDay =  round($subTime/86400);
                                        ?>
                                        @if($subTime < 60)
                                            <span class="cd-date">{{$subTimeSecond}} Giây trước</span>
                                        @elseif($subTime > 60 && $subTime < 3600)
                                            <span class="cd-date">{{$subTimeMinute}} Phút trước</span>
                                        @elseif($subTime > 3600 && $subTime < 86400)
                                            <span class="cd-date">{{$subTimeHour}} Giờ trước</span>
                                        @elseif($subTime > 84600 && $subTime < 86400*10)
                                            <span class="cd-date">{{$subTimeDay}} Ngày trước</span>
                                        @elseif($subTime > 84600*10)
                                            {{date('d-m-Y',$createTime)}}
                                        @endif

                                        {{--Kết thúc xử lý ngày--}}
                                        <p>{{$detailSupport->content}}</p>
                                        {{--<p><i><span class="pull-right text-info" style="font-size: 10px">Đã xem</span></i></p>--}}
                                    </div>
                                @endif
                            </div>
                        @endforeach
                    @else
                        <p><i>Trống rỗng...</i></p>
                    @endif
                </section>
            </div>
        </div>

    </main>
    <div class="page-footer">
        <div class="footer-grid container" style="">
            <form action="{{action('Admin\SupportController@postAddfeedBack')}}" method="post">
                <div class="row">
                    <textarea style=" outline: none; border: 0px;" name="text" id="content" cols="32" rows="10" placeholder="Bạn muốn nói gì?"></textarea>
                    <input type="hidden" value="{{$thisSupport->id}}" name="supportId">
                    <input type="submit" value="Trả lời" class="btn btn-primary">
                </div>

                {{csrf_field()}}
            </form>
        </div>
    </div>
@endsection
@section('footlink')

@endsection
@section('footlink')
    <script>
        CKEDITOR.replace('content');
    </script>
@endsection
@section('afterJquery')
    <script src="{{asset('CS2/js/ckeditor/ckeditor.js')}}"></script>
@endsection