@extends('Admin.master')
@section('title','Hỗ trợ khách hàng cho khách hàng mới')
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title">Hỗ trợ cho khách hàng mới</div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <form class="col s12 m12" action="{{action('Admin\SupportController@postAdd')}}" method="post">
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="last" type="text" class="validate" name="name"
                                               value="{{old('name')}}">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('name') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label class="active" for="last_name">Chủ đề:</label>
                                    </div>

                                    <div class="input-field col s6">
                                        <select name="service">
                                            <option value="0" disabled selected>Chọn dịch vụ:</option>
                                            @if(isset($listService) && $listService)
                                                @foreach($listService as $service)
                                                    <option <?php old('service') == $service->id?'checked':'' ?>value="{{$service->id}}">{{$service->name}}</option>
                                                @endforeach
                                            @else
                                            @endif
                                        </select>
                                        @if(count($errors)>0)
                                            @foreach($errors->get('service') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label>Dịch vụ:</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <select name="user">
                                            <option value="" disabled selected>Chọn khách hàng:</option>
                                            @if(isset($listUser) && $listUser)
                                                @foreach($listUser as $user)
                                                    <option <?php old('user') == $user->id?'checked':'' ?>value="{{$user->id}}">{{$user->name}}</option>
                                                @endforeach
                                            @else
                                            @endif
                                        </select>
                                        @if(count($errors)>0)
                                            @foreach($errors->get('user') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label>Khách hàng:</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <select name="admin">
                                            <option value="" disabled selected>Hỗ trợ viên:</option>
                                            @if(isset($listAdmin) && $listAdmin)
                                                @foreach($listAdmin as $admin)
                                                    <option <?php old('user') == $admin->id?'checked':'' ?>value="{{$admin->id}}">{{$admin->name}}</option>
                                                @endforeach
                                            @else
                                            @endif
                                        </select>
                                        @if(count($errors)>0)
                                            @foreach($errors->get('admin') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label>Người hỗ trợ:</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        {{--<textarea name="contents" id="textarea1" class="materialize-textarea" length="120">{{old('content')}}</textarea>--}}
                                        <textarea name="contents">{{old('content')}}</textarea>
                                        <label for="textarea1" class="active">Vấn đề:</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col s6">
                                        <!-- Switch -->
                                        <label for="" class="active">Trạng thái:</label>
                                        <div class="switch m-b-md">
                                            <label>
                                                Đang xử lý
                                                <input type="checkbox" name="status">
                                                <span class="lever"></span>
                                                xử lý xong
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col cs12 pull-right">
                                        <input type="submit" class="btn btn-block btn-primary btn-lg" value="Thêm mới">
                                    </div>
                                </div>
                                {{csrf_field()}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!---->
        </div>
    </main>
@endsection
@section('style')
    .mn-inner form {
    padding:6% !important;
    }
    textarea{
    outline:0px !important;
    border: 1px dotted #dcdcdc;
    height: 150px;
    }
@endsection
@section('footlink')
    <script>
        CKEDITOR.replace('textarea1');
    </script>
@endsection
@section('afterJquery')
    <script src="{{asset('CS2/js/ckeditor/ckeditor.js')}}"></script>
@endsection