@extends('Admin.master')
@section('title','Dashboard')
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title">Cập nhật hỗ trợ:</div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <form class="col s12 m12" action="{{action('Admin\SupportController@postEdit',['id'=>$thisSupport->id])}}" method="post">
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="last" type="text" class="validate" name="name"
                                               value="{{old('name',isset($thisSupport)? $thisSupport->name:'')}}">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('name') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="last_name" class="active">Vấn đề:</label>
                                    </div>

                                    <div class="input-field col s6">
                                        <select name="service">
                                            <option value="0" disabled selected>Chọn dịch vụ:</option>
                                            @if(isset($listService) && $listService)
                                                @foreach($listService as $service)
                                                    <option value="{{$service->id}}" {{old('service',isset($thisSupport)? $thisSupport->service_id:'') == $service->id?'selected':''}}>{{$service->name}}</option>
                                                @endforeach
                                            @else
                                            @endif
                                        </select>
                                        @if(count($errors)>0)
                                            @foreach($errors->get('service') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label class="active">Dịch vụ:</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <select name="user">
                                            <option value="" disabled selected>Chọn Khách hàng:</option>
                                            @if(isset($listUser) && $listUser)
                                                @foreach($listUser as $user)
                                                    <option {{old('user',isset($thisSupport)? $thisSupport->user_id:'') == $user->id?'selected':''}} value="{{$user->id}}">{{$user->name}}</option>
                                                @endforeach
                                            @else
                                            @endif
                                        </select>
                                        @if(count($errors)>0)
                                            @foreach($errors->get('user') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label  class="active">Khách hàng:</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <select name="admin">
                                            <option value="" disabled selected>Hỗ trợ viên:</option>
                                            @if(isset($listAdmin) && $listAdmin)
                                                @foreach($listAdmin as $admin)
                                                    <option {{old('admin',isset($thisSupport)? $thisSupport->admin_id:'') == $admin->id?'selected':''}} value="{{$admin->id}}">{{$admin->name}}</option>
                                                @endforeach
                                            @else
                                            @endif
                                        </select>
                                        @if(count($errors)>0)
                                            @foreach($errors->get('admin') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label  class="active">Người hỗ trợ:</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <textarea name="contents">{{old('content',isset($thisSupport)? $thisSupport->content:'')}}</textarea>
                                        <label for="textarea1" class="active">Nội dung:</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col s6">
                                        <!-- Switch -->
                                        <label for="">Trạng thái:</label>
                                        <div class="switch m-b-md">
                                            <label>
                                                Đang xử lý
                                                @if($thisSupport->status)
                                                    <input type="checkbox" name="status"  checked>
                                                    <span class="lever"></span>
                                                @else
                                                    <input type="checkbox" name="status" class="validate">
                                                    <span class="lever"></span>
                                                @endif
                                                Xử lý xong
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col cs12 pull-right">
                                        <input type="submit" class="btn btn-block btn-primary btn-lg" value="Cập nhật">
                                    </div>
                                </div>
                                {{csrf_field()}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!---->
        </div>
    </main>
@endsection
@section('style')
    .mn-inner form {
    padding:6% !important;
    }
    textarea{
    outline:0px !important;
    border: 1px dotted #dcdcdc;
    height: 150px;
    }
@endsection