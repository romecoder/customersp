@extends('Admin.master')
@section('title','Supports')
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="col s12 pull-left text-center">
                    <span class="page-title"><label for="" style="font-size: 14px">Quản lý hỗ trợ khách hàng:</label></span>
                </div>
                <div class="col s12 pull-left text-center">
                    @include('General.displayerrors')
                </div>
            </div>

            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <table id="example" class="display responsive-table datatable-example striped bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th><small>Khách hàng</small></th>
                                <th><small>Dịch vụ gặp vấn đề</small></th>
                                <th><small>Người hỗ trợ</small></th>
                                <th><small>trạng thái</small></th>
                                <th>
                                    <a href="{{action('Admin\SupportController@getAdd')}}"><span style="width: 80%;" class="btn green"><i
                                                     class="fa fa-user-plus" aria-hidden="true"
                                                     style="color: #ffffff;"></i></span></a>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($supportList) && $supportList)
                                <?php $stt = 0 ?>
                                @foreach($supportList as $support)
                                    <tr>
                                        <td><small>#{{$support->id}}</small></td>
                                        <td>
                                            <img src="@if($support->user->avatar == null) {{asset('CS2/images/avatar')."/avatar.png"}} @else {{asset('CS2/images/avatar/'.$support->user->avatar)}} @endif" alt=""
                                                 class="img-circle img-thumbnail" width="50em"> ::
                                            <span class="blue-text"><small>{{$support->user->name}}</small></span>
                                        </td>
                                        <td>
                                            <small>{{$support->service['name']}} :: <span class="red-text">{{$support->name}}</span></small>
                                        </td>
                                        <td>
                                            <img src="@if($support->admin->avatar == null) {{asset('CS2/images/avatar')."/avatar.png"}} @else {{asset('CS2/images/avatar/'.$support->admin->avatar)}} @endif" alt=""
                                                 class="img-circle img-thumbnail" width="50em"> ::
                                            <span class="red-text">{{$support->admin->name}}</span>
                                        </td>
                                        @if($support->status == 1)
                                            <td>
                                                <a href="{{action('Admin\SupportController@changeStatus',['id'=>$support->id])}}"
                                                   style="color: limegreen; font-weight: bold; font-size: 15px" class="chip"> <small>Xử lý xong</small></a>
                                            </td>
                                        @else
                                            <td>
                                                <a href="{{action('Admin\SupportController@changeStatus',['id'=>$support->id])}}"
                                                   style="color: red; font-weight: bold; font-size: 15px" class="chip"><small>Đang xử lý...</small></a>
                                            </td>
                                        @endif
                                        <td>
                                            <ul class="list-inline">
                                                <li style="margin-right: -11px;">
                                                    <a href="{{action('Admin\SupportController@getDetail',['id'=>$support->id])}}"
                                                       class="btn blue"
                                                       style="width: 15px; height: 30px "><i
                                                                class="fa fa-search-plus" aria-hidden="true"
                                                                style="font-size: 12px; margin-left:-4px;"></i></a>
                                                </li>
                                                <li style="margin-right: -11px;">
                                                    <a href="{{action('Admin\SupportController@getEdit',['id'=>$support->id])}}"
                                                       class="btn orange"
                                                       style="width: 15px; height: 30px"><i
                                                                class="fa fa-pencil" aria-hidden="true"
                                                                style="font-size: 12px; margin-left:-4px;"></i></a>
                                                </li>
                                                <li style="margin-right: -11px;">
                                                    <a href="{{action('Admin\SupportController@getDelete',['id'=>$support->id])}}"
                                                       class="btn red" style="width: 15px; height: 30px"
                                                       onclick="return confirm('Bạn có chắc chắn muốn xóa Hỗ trợ này?')">
                                                        <i class="fa fa-trash-o" aria-hidden="true"
                                                           style="font-size: 12px; margin-left:-4px;"></i></a>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                            @else

                            @endif

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @if(sizeof($supportList))
            <div class="col-md-12">
                <div class="paginate">
                    <p class="pull-left">Tổng số trang : {{$supportList->lastPage()}}</p>
                    <ul class="pagination pull-right">
                        <li class="">
                            <a href="{{$supportList->url(1)}}">
                                <i class="ace-icon fa fa-angle-double-left"></i>
                            </a>
                        </li>
                        <li class="prev {{($supportList->currentPage() == 1) ? 'disabled' : ''}}">
                            <a href="{{$supportList->url($supportList->currentPage() - 1)}}">Trước</a>
                        </li>
                        @for($i=1; $i<=$supportList->lastPage();$i++ )
                            <li class="{{ ($supportList->currentPage() == $i) ? 'active' : '' }}">
                                <a href="{{$supportList->url($i)}}">{{$i}}</a>
                            </li>
                        @endfor
                        <li class="next {{($supportList->currentPage() == $supportList->lastPage()) ? 'disabled' : ''}}">
                            <a href="{{$supportList->url($supportList->currentPage() + 1)}}">Sau</a>
                        </li>
                        <li class="">
                            <a href="{{$supportList->url($supportList->lastPage())}}">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        @endif
    </main>

@endsection