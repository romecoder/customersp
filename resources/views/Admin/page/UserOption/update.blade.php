@extends('Admin.master')
@section('title','Sửa trường ghi nhớ')
@section('headlink')
@endsection
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title">Sửa trường ghi nhớ</div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
            </div>
            <div class="col s2"></div>
            <div class="col s8 m8 l8">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <form class="col s12 m12" action="{{action('Admin\UserOptionController@update',['id'=>$thisUserOption->id])}}" method="post"
                                  enctype="multipart/form-data">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <select name="user" id="user">
                                            <option value="" disabled selected>Khách hàng:</option>
                                            @if($listUser)
                                                @foreach($listUser as $user)
                                                    <option value="{{$user->id}}" @if($thisUserOption->element_id == $user->id) selected @endif>{{$user->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @if(count($errors)>0)
                                            @foreach($errors->get('type') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                    </div>

                                    <div class="input-field col s12">
                                        <input id="last" type="text" class="validate" name="key"
                                               value="{{old('key',$thisUserOption->key)}}">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('key') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="last_name" class="active">Tên trường:</label>
                                    </div>
                                    <div class="input-field col s12">
                                        <input id="value" type="text" class="validate" name="value"
                                               value="{{old('value',$thisUserOption->value)}}">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('value') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="last_name" class="active">Giá trị:</label>
                                    </div>

                                </div>
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col cs12 pull-right">
                                        <input type="submit" class="btn blue" value="Cập nhật">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s2"></div>

            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <table id="example" class="display responsive-table datatable-example striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Khách hàng</th>
                                <th>Trường :: Giá trị</th>
                                <th>
                                    <a href="" data-toggle="modal"
                                       data-target="#insertOption">
                                         <span class="btn green" style="width: 42%;">
                                        <i class="fa fa-user-plus" aria-hidden="true"
                                           style="color: #ffffff"></i>
                                         </span>
                                    </a>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($listUserOption) && $listUserOption)
                                <?php $count = 0?>
                                @foreach($listUserOption as $userOption)
                                    <?php $count++ ?>
                                    <tr>
                                        <td>{{$count++}}</td>
                                        <td>
                                            @if(isset($userOption->user))
                                                <a href="{{route('get.customer.detail',['id'=>$userOption->user->id])}}">
                                                    <img src="{{asset('CS2/images/avatar/'.$userOption->user->avatar)}}" alt=""
                                                         class="img-circle" width="50em">
                                                    <span><small><i>({{$userOption->user->name}})</i></small></span>
                                                </a>
                                            @endif
                                        </td>
                                        <td>{{$userOption->key}} ::::> <span class="text-info">{{$userOption->value}}</span></td>
                                        <td>
                                            <ul class="list-inline">
                                                <li style="margin-right: -11px;">
                                                    <a href="{{action('Admin\UserOptionController@edit',['id'=>$userOption->id])}}"
                                                       class="btn yellow"><i
                                                                class="fa fa-pencil" aria-hidden="true"></i></a>
                                                </li>
                                                <li style="margin-right: -11px;">
                                                    <a href="{{action('Admin\UserOptionController@delete',['id'=>$userOption->id])}}"
                                                       class="btn red"
                                                       onclick="confirm('̀Bạn có muốn xóa trường này')">
                                                        <i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <p><i>Chưa có dữ liệu...</i></p>
                            @endif

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @if($listUserOption)
            <div class="col-md-12">
                <div class="paginate">
                    <p class="pull-left">Tổng số trang : {{$listUserOption->lastPage()}}</p>
                    <ul class="pagination pull-right">
                        <li class="">
                            <a href="{{$listUserOption->url(1)}}">
                                <i class="ace-icon fa fa-angle-double-left"></i>
                            </a>
                        </li>
                        <li class="prev {{($listUserOption->currentPage() == 1) ? 'disabled' : ''}}">
                            <a href="{{$listUserOption->url($listUserOption->currentPage() - 1)}}">Trước</a>
                        </li>
                        @for($i=1; $i<=$listUserOption->lastPage();$i++ )
                            <li class="{{ ($listUserOption->currentPage() == $i) ? 'active' : '' }}">
                                <a href="{{$listUserOption->url($i)}}">{{$i}}</a>
                            </li>
                        @endfor
                        <li class="next {{($listUserOption->currentPage() == $listUserOption->lastPage()) ? 'disabled' : ''}}">
                            <a href="{{$listUserOption->url($listUserOption->currentPage() + 1)}}">Sau</a>
                        </li>
                        <li class="">
                            <a href="{{$listUserOption->url($listUserOption->lastPage())}}">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        @endif
    </main>
@endsection
@section('footlink')
    <script>
        CKEDITOR.replace('textarea1');
    </script>
@endsection
@section('afterJquery')
    <script src="{{asset('CS2/js/ckeditor/ckeditor.js')}}"></script>
@endsection
@section('style')
    .mn-inner form {
        padding:6% !important;
    }
@endsection