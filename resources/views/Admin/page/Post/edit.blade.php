@extends('Admin.master')
@section('title','Sửa bài viết')
@section('headlink')
@endsection
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title">Sửa bài viết</div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <form class="col s12 m12" action="{{action('Admin\PostController@postUpdate',['id'=>$thisPost->id])}}" method="post"
                                  enctype="multipart/form-data">
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="last" type="text" class="validate" name="name"
                                               value="{{old('name',$thisPost->tittle)}}">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('name') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="last_name" class="active">Tiêu đề</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <select name="category" id="type">
                                            <option value="" disabled selected>Chuyên mục</option>
                                            <option value="1">Chuyên mục 1</option>
                                            <option value="2">Chuyên mục 2</option>
                                            <option value="3">Chuyên mục 3</option>
                                            <option value="4">Chuyên mục 4</option>
                                        </select>
                                        @if(count($errors)>0)
                                            @foreach($errors->get('type') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="file-field input-field col s6">
                                        <div class="waves-effect waves-blue btn-flat m-b-xs">
                                            <span>Ảnh</span>
                                            <input type="file" name="image" value="{{old('image')}}">
                                            @if(count($errors)>0)
                                                @foreach($errors->get('image') as $error)
                                                    <p style="color: red">{{$error}}</p>
                                                @endforeach
                                            @endif
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" value="{{old('image',$thisPost->image)}}">
                                            <input type="hidden" value="{{$thisPost->image}}" name="oldImage">
                                        </div>
                                    </div>

                                    <div class="col s6">
                                        <!-- Switch -->
                                        <h6 for="">Trạng thái</h6>
                                        <div class="switch m-b-md">
                                            <label>
                                                Chưa kích hoạt
                                                @if($thisPost->status)
                                                    <input type="checkbox" name="status"  checked>
                                                    <span class="lever"></span>
                                                @else
                                                    <input type="checkbox" name="status" class="validate">
                                                    <span class="lever"></span>
                                                @endif
                                                Kích hoạt
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <textarea id="textarea1" name="contents">{!!old('contents',$thisPost->content)!!}</textarea>
                                    </div>
                                </div>

                                <div class="row">

                                </div>
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col cs12 pull-right">
                                        <input type="submit" class="btn btn-block btn-primary btn-lg" value="Cập nhật">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!---->
        </div>
    </main>
@endsection
@section('footlink')
    <script>
        CKEDITOR.replace('textarea1');
    </script>
@endsection
@section('afterJquery')
    <script src="{{asset('CS2/js/ckeditor/ckeditor.js')}}"></script>
@endsection
@section('style')
    .mn-inner form {
    padding: 5% !important;
    }
    .btn-flat{
    border: 1px dashed;
    }
    .btn-flat:hover{
    background-color: #fffffe;
    }
@endsection