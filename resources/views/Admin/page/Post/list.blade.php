@extends('Admin.master')
@section('title','Bài viết')
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="col s6 pull-left">
                    <div class="page-title"><h6>Quản Lý Bài viết:</h6></div>
                </div>
                <div class="col s12 pull-left text-center">
                    @include('General.displayerrors')
                </div>
            </div>

            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <table id="example" class="display responsive-table datatable-example striped bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th><small>Bài viết</small></th>
                                <th><small>Thời gian</small></th>
                                <th><small>Trạng thái</small></th>
                                <th>
                                    <a style="width: 62%" class="btn blue" href="{{action('Admin\PostController@getViewAdd')}}">
                                        <span><i class="fa fa-user-plus" aria-hidden="true" style="color: "></i></span>
                                    </a>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($listPost) && $listPost)
                                <?php $count = 0?>
                                @foreach($listPost as $post)
                                    <?php $count++ ?>
                                    <tr>
                                        <td>{{$count++}}</td>
                                        @if(isset($post->image) && $post->image)
                                            <td><img src="{{asset('CS2/images/post/'.$post->image )}}" alt="ảnh đại diện"
                                                     class="img-thumbnail" width="70em"> :: <a href="">{{$post->tittle}}</a>
                                            </td>
                                        @else
                                            <td><img src="{{asset('CS2/images/post/post.png')}}" alt="ảnh đại diện"
                                                     class="img-thumbnail" width="70em"> :: <a href="">{{$post->tittle}}</a>
                                            </td>
                                        @endif
                                        {{--Phần này xử lý hiển thị Thời gian tự động theo giây, phút, giờ, ngày--}}
                                        <?php
                                           /* date_default_timezone_set("Asia/Bangkok");*/
                                            $now = strtotime(date('Y-m-d H:i:s'));
                                            $createTime = strtotime($post->created_at);
                                            $subTime =  $now - $createTime  ;
                                            $subTimeSecond = $subTime;
                                            $subTimeMinute = round($subTime/60);
                                            $subTimeHour =  round($subTime/3600);
                                            $subTimeDay =  round($subTime/86400);
                                        ?>
                                        <td>
                                            <span class="text-warning">
                                                @if($subTime < 60)
                                                     <label for="">{{$subTimeSecond}} Giây trước</label>
                                                @elseif($subTime > 60 && $subTime < 3600)
                                                     <label for="">{{$subTimeMinute}} Phút trước</label>
                                                @elseif($subTime > 3600 && $subTime < 86400)
                                                     <label for="">{{$subTimeHour}} Giờ trước</label>
                                                @elseif($subTime > 84600 && $subTime < 86400*10)
                                                     <label for="">{{$subTimeDay}} Ngày trước</label>
                                                @elseif($subTime > 84600*10)
                                                    {{date('d-m-Y',$createTime)}}
                                                @endif
                                            </span>
                                        </td>
                                        {{--Kết thúc xử lý ngày--}}
                                        @if($post->status == 1)
                                            <td>
                                                <a href="" class="green-text chip"><small>Công khai</small></a>
                                            </td>
                                        @else
                                            <td>
                                                <a href=""
                                                   class="red-text chip"><small>Không công khai</small></a>
                                            </td>
                                        @endif
                                        <td>
                                            <ul class="list-inline">
                                                <li style="margin-right: -11px;">
                                                    <a href="{{action('Admin\PostController@getEditView',['id'=>$post->id,'slug'=>$post->slug])}}"
                                                       class="btn yellow"><i
                                                                class="fa fa-pencil" aria-hidden="true"></i></a>
                                                </li>
                                                <li style="margin-right: -11px;">
                                                    <a href="{{action('Admin\PostController@deletePost',['id'=>$post->id])}}"
                                                       class="btn red"
                                                       onclick="return confirm('Bạn có muốn xóa đi bài viết này')">
                                                        <i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <p><i>Chưa có bài viết nào...</i></p>
                            @endif

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @if($listPost)
            <div class="col-md-12">
                <div class="paginate">
                    <p class="pull-left">Tổng số trang : {{$listPost->lastPage()}}</p>
                    <ul class="pagination pull-right">
                        <li class="">
                            <a href="{{$listPost->url(1)}}">
                                <i class="ace-icon fa fa-angle-double-left"></i>
                            </a>
                        </li>
                        <li class="prev {{($listPost->currentPage() == 1) ? 'disabled' : ''}}">
                            <a href="{{$listPost->url($listPost->currentPage() - 1)}}">Trước</a>
                        </li>
                        @for($i=1; $i<=$listPost->lastPage();$i++ )
                            <li class="{{ ($listPost->currentPage() == $i) ? 'active' : '' }}">
                                <a href="{{$listPost->url($i)}}">{{$i}}</a>
                            </li>
                        @endfor
                        <li class="next {{($listPost->currentPage() == $listPost->lastPage()) ? 'disabled' : ''}}">
                            <a href="{{$listPost->url($listPost->currentPage() + 1)}}">Sau</a>
                        </li>
                        <li class="">
                            <a href="{{$listPost->url($listPost->lastPage())}}">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        @endif
    </main>

@endsection
@section('style')
@endsection