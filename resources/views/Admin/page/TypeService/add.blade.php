@extends('Admin.master')
@section('title','Thêm loại dịch vụ')
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title">Thêm loại dịch vụ </div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <form class="col s12 m12" action="{{action('Admin\TypeController@storeType')}}" method="post">
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input  type="text" class="validate" name="name" value="{{old('name')}}" placeholder="Thêm tên loại dịch vụ mới" >
                                        @if(count($errors)>0)
                                            @foreach($errors->get('name') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="last_name" class="active">Tên loại</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input  type="text" class="validate" name="time" value="{{old('time')}}" placeholder="Thêm số ngày hỗ trợ" >
                                        @if(count($errors)>0)
                                            @foreach($errors->get('time') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="time" class="active">Thời gian</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s12">
                                        <!-- Switch -->
                                        <h6 for="">Trạng thái</h6>
                                        <div class="switch m-b-md">
                                            <label>
                                                Chờ kích hoạt
                                                    <input type="checkbox" name="status" class="validate">
                                                    <span class="lever"></span>
                                                kích hoạt
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col cs12 pull-right">
                                        <input type="submit" class="btn btn-block btn-primary btn-lg" value="Thêm mới">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!---->
        </div>
    </main>
@endsection