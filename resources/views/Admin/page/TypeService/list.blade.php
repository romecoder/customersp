@extends('Admin.master')
@section('title','Service type')
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="col s12 pull-left text-center">
                    <span class="page-title"><label for="">Quản lý loại dịch vụ</label></span>
                </div>
                <div class="col s12 pull-left text-center">
                    @include('General.displayerrors')
                </div>
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <table id="example" class="display responsive-table datatable-example striped">
                            <thead>
                            <tr>
                                <th></th>
                                <th><small>Tên Loại</small></th>
                                <th><small>Thời gian (ngày)</small></th>
                                <th><small>Trạng thái</small></th>
                                <th>
                                    <a href=""data-toggle="modal" data-target="#insertType">
                                        <span class="btn blue" style="width: 40%;">
                                            <i class="fa fa-user-plus" aria-hidden="true" style="color: #ffffff"></i>
                                        </span>
                                    </a>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($listType)
                                @foreach($listType as $type)
                                    <tr class="text-center">
                                        <td></td>
                                        <td><label class="text-info">{{$type->name}}</label></td>
                                        <td class="text-info">{{$type->time}}</td>
                                        <td>
                                            @if($type->status == 1)<a href="" class="text-success chip"><small>Kích
                                                    hoạt</small></a>
                                            @else
                                                <a href=""
                                                   style="color: red; font-weight: bold; font-size: 15px" class="chip"><small>Chờ kích
                                                        hoạt</small></a>
                                            @endif
                                        </td>
                                        <td>
                                            <ul class="list-inline">
                                                <li style="margin-right: -11px;">
                                                    <a href="{{action('Admin\TypeController@editType',['id' => $type->id])}}"
                                                       class="btn yellow" title="Sửa"><i
                                                                class="fa fa-pencil" aria-hidden="true"
                                                                style="font-size: 12px; margin-left:-4px;"></i></a>
                                                </li>
                                                <li style="margin-right: -11px;">
                                                    <a href="{{action('Admin\TypeController@deleteType',['id' => $type->id])}}"
                                                       class="btn red"
                                                       onclick="return confirm('Bạn có muốn xóa loại dịch vụ này?')">
                                                        <i class="fa fa-trash-o" aria-hidden="true"
                                                           title="Xóa"></i></a>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                        <h3>Chưa có loại dịch vụ nào</h3>
                                    @endif
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        @if(sizeof($listType))
            <div class="col-md-12">
                <div class="paginate">
                    <p class="pull-left">Tổng số trang : {{$listType->lastPage()}}</p>
                    <ul class="pagination pull-right no-margin">
                        <li class="">
                            <a href="{{$listType->url(1)}}">
                                <i class="ace-icon fa fa-angle-double-left"></i>
                            </a>
                        </li>
                        <li class="prev {{($listType->currentPage() == 1) ? 'disabled' : ''}}">
                            <a href="{{$listType->url($listType->currentPage() - 1)}}">Trước</a>
                        </li>
                        @for($i=1; $i<=$listType->lastPage();$i++ )
                            <li class="{{ ($listType->currentPage() == $i) ? 'active' : '' }}">
                                <a href="{{$listType->url($i)}}">{{$i}}</a>
                            </li>
                        @endfor
                        <li class="next {{($listType->currentPage() == $listType->lastPage()) ? 'disabled' : ''}}">
                            <a href="{{$listType->url($listType->currentPage() + 1)}}">Sau</a>
                        </li>
                        <li class="">
                            <a href="{{$listType->url($listType->lastPage())}}">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        @endif

        <div id="insertType" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Thêm loại dịch vụ:</h5>
                <form class="col s12 m12" action="{{action('Admin\TypeController@storeType')}}" method="post">
                    <div class="row">
                        <div class="input-field col s6">
                            <input  type="text" class="validate" name="name" value="{{old('name')}}" placeholder="Thêm tên loại dịch vụ mới" >
                            @if(count($errors)>0)
                                @foreach($errors->get('name') as $error)
                                    <p style="color: red">{{$error}}</p>
                                @endforeach
                            @endif
                            <label for="last_name" class="active">Tên loại</label>
                        </div>
                        <div class="input-field col s6">
                            <input  type="text" class="validate" name="time" value="{{old('time')}}" placeholder="Thêm số ngày hỗ trợ" >
                            @if(count($errors)>0)
                                @foreach($errors->get('time') as $error)
                                    <p style="color: red">{{$error}}</p>
                                @endforeach
                            @endif
                            <label for="time" class="active">Thời gian</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12">
                            <!-- Switch -->
                            <h6 for="">Trạng thái</h6>
                            <div class="switch m-b-md">
                                <label>
                                    Chờ kích hoạt
                                    <input type="checkbox" name="status" class="validate">
                                    <span class="lever"></span>
                                    kích hoạt
                                </label>
                            </div>
                        </div>
                    </div>
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col cs12 pull-right">
                            <input type="submit" class="btn btn-block btn-primary btn-lg" value="Thêm mới">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        </div>
    </main>
@endsection