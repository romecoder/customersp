<?php
$now = \Carbon\Carbon::now();
$this->month = $now->month;
?>
@extends('Admin.master')
@section('title','Dashboard')
@section('content')
    <main class="mn-inner inner-active-sidebar">
        <div class="middle-content">
            <div class="row no-m-t no-m-b">
                <a href="{{action('Admin\OrderController@getList')}}">
                    <div class="col s12 m12 l4">
                        <div class="card stats-card">
                            <div class="card-content">
                                <div class="card-options">
                                    <ul>
                                        <li class="red-text"><span class="badge cyan lighten-1">Tổng</span></li>
                                    </ul>
                                </div>
                                <span class="card-title">Tổng doanh thu</span>
                                <span class="stats-counter"><span class="counter">{{$totalMoney}}</span> đ<small>Tháng {{$now->month}}</small></span>
                            </div>
                            <div id="sparkline-bar"></div>
                        </div>
                    </div>
                </a>
                <a href="{{action('Admin\SupportController@getList')}}">
                    <div class="col s12 m12 l4" style="cursor: pointer;" onclick="window.location='{{action('Admin\SupportController@getList')}}';">
                        <div class="card stats-card">
                            <div class="card-content">
                                <div class="card-options">
                                    <ul>
                                        <li><a href="javascript:void(0)"><i class="material-icons">more_vert</i></a></li>
                                    </ul>
                                </div>
                                <span class="card-title">Tổng thực thu</span>
                                <span class="stats-counter"><span class="counter">{{$totalPaidMoney}}</span> đ<small>Tháng {{$now->month}}</small></span>
                            </div>
                            <div id="sparkline-line"></div>
                        </div>
                    </div>
                </a>
                <a href="{{action('Admin\CustomerController@getList')}}">
                    <div class="col s12 m12 l4">
                        <div class="card stats-card">
                            <div class="card-content">
                                <span class="card-title">Khách hàng mới</span>
                                <span class="stats-counter"><span class="counter">{{$numberCustomerInMonth}}</span> <small>(khách hàng)</small><small>Tháng {{$now->month}}</small></span>
                                <div class="percent-info green-text">8% <i class="material-icons">trending_up</i></div>
                            </div>
                            <div class="progress stats-card-progress">
                                <div class="determinate" style="width: 70%"></div>
                            </div>
                        </div>
                    </div>
                </a>

            </div>

            <div class="row no-m-t no-m-b">
                <div class="col s12 m12 l12">
                    <div class="card invoices-card">
                        <div class="card-content">
                            <div class="row">
                                <form action="{{action('Admin\DashboardController@fillter')}}" method="post">
                                    <div class="col-md-5">
                                        <label for="">Từ ngày:</label>
                                        <input type="date" name="startDate" value="@if(isset($startDate)){{$startDate}}@endif">
                                    </div>
                                    <div class="col-md-5">
                                        <label for="">Từ ngày:</label>
                                        <input type="date" name="endDate" value="@if(isset($endDate)){{$endDate}}@endif">
                                    </div>
                                    <div class="col-md-2">
                                        <input type="submit" value="Lọc" class="btn grey">
                                    </div>
                                    {{csrf_field()}}
                                </form>
                            </div>
                            <hr>
                            <table class="responsive-table bordered striped highlight">
                                <thead class="">
                                <tr>
                                    <th data-field="number"><small>Dịch vụ</small></th>
                                    <th data-field="total"><small>Tổng doanh thu</small></th>
                                    <th data-field="total"><small>Tổng thực thu</small></th>
                                    <th data-field="total"><small>Tổng thiếu nợ</small></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(sizeof($listService))
                                    @foreach($listService as $service)
                                        <tr>
                                            @if($service->thumbnail)
                                                <td><img src="{{asset('CS2/images/service')}}/{{$service->thumbnail}}"
                                                         alt="Ảnh đại diện" width="70px">
                                                    <label for="">{{$service->name}}</label>
                                                </td>
                                            @else
                                                <td><img src="{{asset('CS2/images/service/service.png')}}"
                                                         alt="Ảnh đại diện" width="50px">
                                                    <label for="">{{$service->name}}</label>
                                                </td>
                                            @endif

                                            <td style="color: #5ab738;">{{number_format($service->totalMoneyInMonth,'0',',','.')}}
                                                đ
                                            </td>
                                            <td style="color: #fbb82c;">{{number_format($service->totalPaidMoney,'0',',','.')}}
                                                đ
                                            </td>
                                            <td style="color: #ff191c;">{{number_format($service->totalMissingMoney,'0',',','.')}}
                                                đ
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            {{--<div class="row no-m-t no-m-b">--}}
                {{--<div class="col s12 m12 l12">--}}
                    {{--<div class="card visitors-card">--}}
                        {{--<div class="card-content">--}}
                            {{--<div class="card-options">--}}
                                {{--<ul>--}}
                                    {{--<li><a href="javascript:void(0)" class="card-refresh"><i class="material-icons">refresh</i></a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                            {{--<span class="card-title">Visitors<span class="secondary-title">Showing stats from the last week</span></span>--}}
                            {{--<div id="flotchart1"></div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
        <div class="inner-sidebar">
            <span class="inner-sidebar-title">Tin nhắn mới</span>
            <div class="message-list">
                <div class="info-item message-item"><img class="circle"
                                                         src="{{asset('CS2/images/profile-image-2.png')}}" alt="">
                    <div class="message-info">
                        <div class="message-author">Rome</div>
                        <small>3 hours ago</small>
                    </div>
                </div>
                <div class="info-item message-item"><img class="circle" src="{{asset('CS2/images/profile-image.png')}}"
                                                         alt="">
                    <div class="message-info">
                        <div class="message-author">Quang Tuấn</div>
                        <small>4 hours ago</small>
                    </div>
                </div>
                <div class="info-item message-item"><img class="circle"
                                                         src="{{asset('CS2/images/profile-image-1.png')}}" alt="">
                    <div class="message-info">
                        <div class="message-author">Kiên Trung</div>
                        <small>2 days ago</small>
                    </div>
                </div>
            </div>

            <div class="sidebar-radar-chart">

            </div>
        </div>
    </main>
@endsection