@extends('Admin.master')
@section('title','Danh sách đơn hàng')
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="col s12 pull-left text-center">
                    <span class="page-title"><label for="" style="font-size: 14px">Quản Lý đơn hàng</label></span>
                </div>
                <div class="col s12 pull-left text-center">
                    @include('General.displayerrors')
                </div>
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <table id="example" class="display responsive-table datatable-example striped bordered">

                            <thead>
                            <tr>
                                <th>#</th>
                                <th><small>Khách hàng</small></th>
                                <th><small>Dịch vụ</small></th>
                                <th><small>Thanh toán</small></th>
                                <th> <span class="btn blue" style="width: 76%"><a href="{{action('Admin\OrderController@getAdd')}}"><i
                                                    class="fa fa-user-plus" aria-hidden="true"
                                                    style="color: #ffffff"></i></a></span></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $count = 0;?>
                            @if(isset($listOrder) && $listOrder)
                                @foreach($listOrder as $order)
                                    <?php $count++?>
                                    <tr>
                                        <td><small>#{{$order->id}}</small></td>
                                        <td>@if($order->seen == 0)
                                                <i class="fa fa-star-o" aria-hidden="true" ></i>
                                            @else
                                            @endif
                                             <img src="@if($order->user->avatar) {{asset('CS2/images/avatar')."/".$order->user->avatar}} @else {{asset('CS2/images/avatar')."/avatar.png"}} @endif"
                                                 alt="{{$order->user->name}}" width="60px" class="img-circle img-thumbnail"> ::
                                            <span class="text-warning"><small>{{$order->user->name}}</small></span>
                                        </td>
                                        <td>
                                            @if(is_array($order->service_id) && count($order->service_id) == 1)
                                                <img src="@if($order->service->thumbnail) {{asset('CS2/images/service')."/".$order->service->thumbnail}} @else {{asset('CS2/images/service')."/service.png"}} @endif"
                                                     alt="{{$order->service->name}}" width="60px" class="img-circle img-thumbnail"> ::
                                                <a href="" class="text-capitalize text-info"><small>{{$order->service->name}}</small></a>
                                            @else
                                                <i><small>(Nhiều dịch vụ)</small></i>
                                            @endif

                                        </td>
{{--                                        <td><span class="blue-text">{{date('d-m-Y', strtotime($order->start_date))}}</span></td>--}}

                                        @if($order->status == 1)
                                            <td>
                                                <a href="{{action('Admin\OrderController@changeStatus',['id'=>$order])}}" class="green-text chip ">
                                                    <small>Đã hoàn thành</small>
                                                </a>
                                            </td>
                                        @else
                                            <td>
                                                <a href="{{action('Admin\OrderController@changeStatus',['id'=>$order])}}"
                                                   style="color: red; font-size: 15px" class="chip">
                                                    <small>Thiếu: <b>{{number_format($order->missing_money)}} đ</b></small>
                                                </a>
                                            </td>
                                        @endif
                                        <td>
                                            <ul class="list-inline">
                                                <li>
                                                    <a href="{{action('Admin\OrderController@getEdit',['id'=>$order->id])}}"
                                                       class="btn yellow"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                </li>
                                                <li>
                                                    <a href="{{action('Admin\OrderController@delete',['id'=>$order->id])}}"
                                                       class="btn red" style="width: 15px; height: 30px"
                                                       onclick="return confirm('Bạn có thực sự muốn xóa đơn hàng này?')">
                                                        <i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                </li>
                                                {{--@if(!$order->status)--}}
                                                    {{--<li style="">--}}
                                                        {{--<a href="{{action('Admin\OrderController@active',['id'=>$order->id])}}"--}}
                                                           {{--class="btn blue"--}}
                                                           {{--style="width: 15px; height: 30px "><i--}}
                                                                    {{--class="fa fa-thumbs-o-up" aria-hidden="true"--}}
                                                                    {{--style="font-size: 12px; margin-left:-4px;"></i></a>--}}
                                                    {{--</li>--}}
                                                {{--@endif--}}

                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <h3><i>Chưa có đơn hàng nào...</i></h3>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @if(sizeof($listOrder))
            <div class="col-md-12">
                <div class="paginate">
                    <p class="pull-left">Tổng số trang : {{$listOrder->lastPage()}}</p>
                    <ul class="pagination pull-right">
                        <li class="">
                            <a href="{{$listOrder->url(1)}}">
                                <i class="ace-icon fa fa-angle-double-left"></i>
                            </a>
                        </li>
                        <li class="prev {{($listOrder->currentPage() == 1) ? 'disabled' : ''}}">
                            <a href="{{$listOrder->url($listOrder->currentPage() - 1)}}">Trước</a>
                        </li>
                        @for($i=1; $i<=$listOrder->lastPage();$i++ )
                            <li class="{{ ($listOrder->currentPage() == $i) ? 'active' : '' }}">
                                <a href="{{$listOrder->url($i)}}">{{$i}}</a>
                            </li>
                        @endfor
                        <li class="next {{($listOrder->currentPage() == $listOrder->lastPage()) ? 'disabled' : ''}}">
                            <a href="{{$listOrder->url($listOrder->currentPage() + 1)}}">Sau</a>
                        </li>
                        <li class="">
                            <a href="{{$listOrder->url($listOrder->lastPage())}}">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        @else
            <div class="col-md-12">
                <p><i>Chưa có đơn hàng nào...</i></p>
            </div>
        @endif
    </main>

@endsection