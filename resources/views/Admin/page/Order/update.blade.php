@extends('Admin.master')
@section('title','Cập nhật đơn hàng')
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title">Cập nhật đơn hàng:</div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <br>
                            <br>
                            <form class="col s12 m12" action="{{action('Admin\OrderController@postEdit',['id'=>$thisOrder->id])}}" method="post">
                                <div class="row">
                                    @if(isset($listUser) && $listUser)
                                        <div class="input-field col s6">
                                            <select name="user">
                                                <option value="0" disabled selected>Chọn khách hàng:</option>
                                                @foreach($listUser as $user)
                                                    <option value="{{$user->id}}" {{old('user',isset($thisOrder)? $thisOrder->user_id:'') == $user->id?'selected':''}}>{{$user->name}}</option>
                                                @endforeach
                                            </select>
                                            @if(count($errors)>0)
                                                @foreach($errors->get('user') as $error)
                                                    <p style="color: red">{{$error}}</p>
                                                @endforeach
                                            @endif
                                            <label class="active">Khách hàng:</label>
                                        </div>
                                    @else
                                        <p>Chưa có khách hàng nào.</p>
                                    @endif

                                    <div class="input-field col s6">
                                        <input id="" type="email" class="validate" name="email" value="{{old('email',isset($thisOrder)? $thisOrder->email:'')}}">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('email') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="last_name" class="active">Email:</label>
                                    </div>

                                    <div class="input-field col s6">
                                        <input id="" type="text" class="validate" name="address" value="{{old('address',isset($thisOrder)? $thisOrder->address:'')}}" >
                                        @if(count($errors)>0)
                                            @foreach($errors->get('address') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="last_name" class="active">Địa chỉ:</label>
                                    </div>

                                    <div class="input-field col s6">
                                        <input id="phone" type="number" class="validate" name="phone" value="{{old('phone',isset($thisOrder)? $thisOrder->phone:'')}}" >
                                        @if(count($errors)>0)
                                            @foreach($errors->get('phone') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="last_name" class="active">Điện thoại:</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input id="totalPrice" type="number" class="validate" name="totalPrice" value="{{old('totalPrice',isset($thisOrder)? $thisOrder->total_price:'')}}" >
                                        @if(count($errors)>0)
                                            @foreach($errors->get('totalPrice') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="totalPrice" class="active">Tổng giá trị đơn hàng (vnđ):</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input id="paidMoney" type="number" class="validate" name="paidMoney" value="{{old('paidMoney',isset($thisOrder)? $thisOrder->paid_money:'')}}" >
                                        @if(count($errors)>0)
                                            @foreach($errors->get('paidMoney') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="paidMoney" class="active">Số tiền đã thanh toán (vnđ):</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input id="missingMoney" step="any" type="number" class="validate" name="missingMoney" value="{{old('missingMoney',isset($thisOrder)? $thisOrder->missing_money:'')}}" >
                                        @if(count($errors)>0)
                                            @foreach($errors->get('missingMoney') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="missingMoney" class="active">Số tiền còn thiếu (vnđ):</label>
                                    </div>
                                    <div class="col s6">
                                        <!-- Switch -->
                                        <label for="">Trạng thái:</label>
                                        <div class="switch m-b-md">
                                            <label>
                                                Chưa hoàn thành
                                                @if(old('note',isset($thisOrder)? $thisOrder->status:''))
                                                    <input type="checkbox" name="status"  checked>
                                                    <span class="lever"></span>
                                                @else
                                                    <input type="checkbox" name="status" class="validate">
                                                    <span class="lever"></span>
                                                @endif
                                                Đã hoàn thành
                                            </label>
                                        </div>
                                    </div>
                                    <div class="input-field col s12">
                                        @if(isset($listService) && $listService)
                                            <div class="input-field col s12">
                                                <label for="group" class="active">Dịch vụ</label>
                                                <br>
                                                <select class="js-states browser-default" multiple="" tabindex="-1" style="width: 100%" id="multiple" name="service[]">
                                                    @foreach($listService as $service)
                                                        <option value="{{$service->id}}" @if(in_array($service->id,$thisOrder->service_id)) selected @endif>{{$service->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        @else
                                            <p><i>Chưa có dịch vụ nào</i></p>
                                        @endif
                                    </div>


                                    <div class="col s12">
                                        <label for="textarea1" class="active">Thông tin thêm:</label>
                                        <br>
                                        <textarea name="note">{{old('note',isset($thisOrder)? $thisOrder->note:'')}}</textarea>
                                        @if(count($errors)>0)
                                            @foreach($errors->get('note') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col cs12 pull-right">
                                        <input type="submit" class="btn btn-block btn-primary btn-lg" value="Cập nhật">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!---->
        </div>
    </main>
@endsection
@section('footlink')
    <script src="{{asset('CS2/plugins/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('CS2/js/alpha.min.js')}}"></script>
    <script src="{{asset('CS2/js/pages/form-select2.js')}}"></script>
    <script>
        CKEDITOR.replace('textarea1');
    </script>
@endsection

@section('afterJquery')
    <script src="{{asset('CS2/js/pages/form-select2.js')}}"></script>
    <script src="{{asset('CS2/js/ckeditor/ckeditor.js')}}"></script>
@endsection
@section('headlink')
    <link href="{{asset('CS2/plugins/select2/css/select2.css')}}" rel="stylesheet">
@endsection
@section('style')

    .mn-inner form {
        padding: 5% !important;
    }
    textarea{
        outline:0px !important;
        border: 1px dotted #dcdcdc;
        height: 150px;
    }

@endsection