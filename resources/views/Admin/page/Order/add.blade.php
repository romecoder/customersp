@extends('Admin.master')
@section('title','Thêm đơn hàng')
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title">Thêm đơn hàng:</div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <br>
                            <br>
                            <form class="col s12 m12" action="{{action('Admin\OrderController@postAdd')}}" method="post">
                                <div class="row">
                                    @if(isset($listUser) && $listUser)
                                        <div class="input-field col s6">
                                            <select name="user">
                                                <option value="0" disabled selected>Chọn khách hàng:</option>
                                                @foreach($listUser as $user)
                                                    <option value="{{$user->id}}" {{old('user')==$user->id?'selected':''}}>{{$user->name}}</option>
                                                @endforeach
                                            </select>
                                            <label class="active">Khách hàng:</label>
                                        </div>
                                    @else
                                        <p><i>Chưa có khách hàng nào...</i></p>
                                    @endif

                                    <div class="input-field col s6">
                                        <input id="" type="email" class="validate" name="email" value="{{old('email')}}" placeholder="Nếu không nhập sẽ tự động lấy từ thông tin khách hàng">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('email') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="last_name" class="active">Email:</label>
                                    </div>

                                    <div class="input-field col s6">
                                        <input id="" type="text" class="validate" name="address" value="{{old('address')}}" placeholder="Nếu không nhập sẽ tự động lấy từ thông tin khách hàng">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('address') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="last_name" class="active" >Địa chỉ:</label>
                                    </div>

                                    <div class="input-field col s6">
                                        <input id="phone" type="number" class="validate" name="phone" value="{{old('phone')}}" placeholder="Nếu không nhập sẽ tự động lấy từ thông tin khách hàng">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('phone') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="last_name" class="active">Điện thoại:</label>
                                    </div>

                                    <div class="input-field col s6">
                                        <input id="totalPrice" type="number" class="validate" name="totalPrice" value="{{old('totalPrice')}}" placeholder="Số tiền phải trả của đơn hàng này">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('totalPrice') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="totalPrice" class="active">Tổng giá trị đơn hàng (vnđ):</label>
                                    </div>

                                    <div class="input-field col s6">
                                        <input id="paidMoney" type="number" class="validate" name="paidMoney" value="{{old('paidMoney')}}" placeholder="Số tiền khách hàng đã thanh toán" >
                                        @if(count($errors)>0)
                                            @foreach($errors->get('paidMoney') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="paidMoney" class="active">Số tiền đã thanh toán (vnđ):</label>
                                    </div>

                                    <div class="input-field col s6">
                                        <input id="missingMoney" type="number" class="validate" name="missingMoney" value="{{old('missingMoney')}}" placeholder="Số tiền khách còn thiếu nợ(tự động tính)">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('missingMoney') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="missingMoney" class="active">Số tiền còn thiếu (vnđ):</label>
                                    </div>

                                    <div class="col s6">
                                        <!-- Switch -->
                                        <label for="group" class="active">Trạng thái:</label>
                                        <div class="switch m-b-md">
                                            <label>
                                                Chưa hoàn thành
                                                <input type="checkbox" name="status">
                                                <span class="lever"></span>
                                                Đã hoàn thành
                                            </label>
                                        </div>
                                    </div>


                                    <div class="input-field col s12">
                                        @if(isset($listService) && $listService)
                                            <label for="group" class="active">Dịch vụ:</label>
                                            <br>
                                            <select class="js-states browser-default" multiple=""
                                                    tabindex="-1" style="width: 100%" id="multiple" name="service[]">
                                                @foreach($listService as $service)
                                                    <option value="{{$service->id}}" {{old('service')==$service->id?'selected':''}}>{{$service->name}}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            <p><i>Chưa có dịch vụ nào</i></p>
                                        @endif
                                    </div>

                                    <div class="input-field col s12">
                                        <label for="group" class="active">Thông tin bổ sung:</label>
                                        <br>
                                        <textarea  name="note" placeholder="Ghi chú những thông tin thêm về đơn hàng">{{old('note')}}</textarea>
                                        @if(count($errors)>0)
                                            @foreach($errors->get('note') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        {{--<label for="textarea1" class="active">Ghi chú thêm thông tin</label>--}}
                                    </div>
                                </div>

                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col cs12 pull-right">
                                        <input type="submit" class="btn blue" value="Thêm đơn hàng">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!---->
        </div>
    </main>
@endsection
@section('footlink')
    <script src="{{asset('CS2/plugins/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('CS2/js/alpha.min.js')}}"></script>
    <script src="{{asset('CS2/js/pages/form-select2.js')}}"></script>
    <script>
        CKEDITOR.replace('textarea1');
    </script>
@endsection

@section('afterJquery')
    <script src="{{asset('CS2/js/pages/form-select2.js')}}"></script>
    <script src="{{asset('CS2/js/ckeditor/ckeditor.js')}}"></script>
@endsection
@section('headlink')
    <link href="{{asset('CS2/plugins/select2/css/select2.css')}}" rel="stylesheet">
@endsection
@section('style')

    .mn-inner form {
    padding: 5% !important;
    }
    textarea{
    outline:0px !important;
    border: 1px dotted #dcdcdc;
    height: 150px;
    }

@endsection