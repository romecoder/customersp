@extends('Admin.master')
@section('title','Thêm dịch vụ')
@section('headlink')
@endsection
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title">Thêm dịch vụ</div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <form class="col s12 m12" action="{{route('post.service.add')}}" method="post"
                                  enctype="multipart/form-data">
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="last" type="text" class="validate" name="name"
                                               value="{{old('name')}}">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('name') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="last_name">Tên dịch vụ:</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <select name="type" id="type">
                                            <option value="" disabled selected>Loại dịch vụ:</option>
                                            @if($listType)
                                                @foreach($listType as $type)
                                                    <option value="{{$type->id}}">{{$type->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @if(count($errors)>0)
                                            @foreach($errors->get('type') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="file-field input-field col s6">
                                        <div class="btn blue">
                                            <span>Ảnh</span>
                                            <input type="file" name="thumbnail" value="{{old('thumbnail')}}">
                                            @if(count($errors)>0)
                                                @foreach($errors->get('thumbnail') as $error)
                                                    <p style="color: red">{{$error}}</p>
                                                @endforeach
                                            @endif
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" value="{{old('thumbnail')}}">
                                        </div>
                                    </div>
                               
                                    <div class="file-field input-field col s6">
                                        <div class="input-field col s12">
                                            <input id="last" type="text" class="validate" name="price"
                                                   value="{{old('price')}}">
                                            @if(count($errors)>0)
                                                @foreach($errors->get('price') as $error)
                                                    <p style="color: red">{{$error}}</p>
                                                @endforeach
                                            @endif
                                            <label for="last_name">Giá dịch vụ</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <textarea id="textarea1" name="contents">{!!old('contents')!!}</textarea>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col s12">
                                        <!-- Switch -->
                                        <h6 for="">Trạng thái</h6>
                                        <div class="switch m-b-md">
                                            <label>
                                                Chưa kích hoạt
                                                <input type="checkbox" name="status">
                                                <span class="lever"></span>
                                                Kích hoạt
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col cs12 pull-right">
                                        <input type="submit" class="btn blue" value="Thêm">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!---->
        </div>
    </main>
@endsection
@section('footlink')
    <script>
        CKEDITOR.replace('textarea1');
    </script>
@endsection
@section('afterJquery')
    <script src="{{asset('CS2/js/ckeditor/ckeditor.js')}}"></script>
@endsection
@section('style')
    .mn-inner form {
    padding:6% !important;
    }
@endsection