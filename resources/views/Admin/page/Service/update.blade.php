@extends('Admin.master')
@section('title','Cập nhật thông tin dịch vụ')
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title">Cập nhật Dịch vụ: </div>
            </div>
            <div class="col s12 pull-left text-center">
                @include('General.displayerrors')
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <form class="col s12 m12" action="{{action('Admin\ServiceController@postEdit',['id'=>$thisService->id])}}"
                                  method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="last" type="text" class="validate" name="name"
                                               value="{{old('name',isset($thisService)? $thisService->name:'')}}">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('name') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="last_name" class="active">Tên dịch vụ:</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <select name="type" id="type">
                                            <option value="" disabled selected>Loại dịch vụ:</option>
                                            @if($listType)
                                                @foreach($listType as $type)
                                                    <option value="{{$type->id}}" @if($type->id == $thisService->type_id) selected @endif >{{$type->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @if(count($errors)>0)
                                            @foreach($errors->get('type') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        {{--<label for="type" class="active">Loại dịch vụ</label>--}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="file-field input-field col s6">
                                        <div class="btn blue">
                                            <span>Thumbnail:</span>
                                            <input type="file" name="thumbnail" value="{{old('thumbnail')}}">
                                            @if(count($errors)>0)
                                                @foreach($errors->get('thumbnail') as $error)
                                                    <p style="color: red">{{$error}}</p>
                                                @endforeach
                                            @endif
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text"
                                                   value="{{old('thumbnail')}}" placeholder="{{ old('thumbnail',isset($thisService)? $thisService->thumbnail:'')}}">
                                            <input type="hidden" value="{{$thisService->avatar}}" name="oldthumbnail">
                                        </div>
                                    </div>
                                    <div class="file-field input-field col s6">
                                        <div class="input-field col s12">
                                            <input id="last" type="text" class="validate" name="price"
                                                   value="{{ old('price',isset($thisService)? $thisService->price:'')}}">
                                            @if(count($errors)>0)
                                                @foreach($errors->get('price') as $error)
                                                    <p style="color: red">{{$error}}</p>
                                                @endforeach
                                            @endif
                                            <label for="last_name" class="active">Giá dịch vụ (vnđ):</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <textarea id="textarea1" class="materialize-textarea" length="120"
                                                  name="contents">{!!  old('address',isset($thisService)? $thisService->content:'')!!} </textarea>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col s6">
                                        <!-- Switch -->
                                        <h6 for="">Trạng thái</h6>
                                        <div class="switch m-b-md">
                                            <label>
                                                Chưa kích hoạt
                                                @if($thisService->status)
                                                    <input type="checkbox" name="status"  checked>
                                                    <span class="lever"></span>
                                                @else
                                                    <input type="checkbox" name="status" class="validate">
                                                    <span class="lever"></span>
                                                @endif
                                                Kích hoạt
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col cs12 pull-right">
                                        <input type="submit" class="btn blue" value="Cập nhật">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!---->
        </div>
    </main>
@endsection
@section('footlink')
    <script>
        CKEDITOR.replace('textarea1');
    </script>
@endsection
@section('afterJquery')
    <script src="{{asset('CS2/js/ckeditor/ckeditor.js')}}"></script>
@endsection
@section('style')
    .mn-inner form {
    padding:6% !important;
    }
@endsection