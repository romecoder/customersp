@extends('Admin.master')
@section('title','Danh sách dịch vụ')
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="col s12 pull-left text-center">
                    <span class="page-title"><label for="" style="font-size: 14px">Quản Lý Dịch Vụ</label></span>

                </div>
                <div class="col s12 pull-left text-center">
                    @include('General.displayerrors')
                </div>
            </div>

            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <table id="example" class="display responsive-table datatable-example striped bordered">
                            <thead>
                            <tr>
                                <th></th>
                                <th><small>Dịch vụ</small></th>
                                <th></th>
                                <th><small>Loại</small></th>
                                <th><small>Trạng thái</small></th>
                                <th> <span class="btn blue" style="width: 64%;"><a href="{{route('get.service.add')}}"><i
                                                    class="fa fa-user-plus" aria-hidden="true"
                                                    style="color: #ffffff"></i></a></span></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($listService) && $listService)
                                <?php $count = 0?>
                                @foreach($listService as $service)
                                    <?php $count++ ?>
                                    <tr>
                                        <td></td>
                                        @if(isset($service->thumbnail) && $service->thumbnail )
                                            <td><img src="{{asset('CS2/images/service/'.$service->thumbnail )}}" alt=""
                                                     class="img-circle img-thumbnail" width="50em">
                                            </td>
                                        @else
                                            <td><img src="{{asset('CS2/images/service/service.png')}}" alt=""
                                                     class="img-reponsive" width="50em">
                                            </td>
                                        @endif
                                        <td>::<b><a class="purple-text" href="{{route('get.service.detail',['id'=>$service->id])}}"><small>{{$service->name}}</small></a></b></td>
                                        <td><span class="chip"><small>{{$service->type_id}}</small></span></td>
                                        @if($service->status == 1)
                                            <td>
                                                <a href="{{action('Admin\ServiceController@changeStatus',['id'=>$service->id])}}" class="green-text chip"><small>Chạy</small></a>
                                            </td>
                                        @else
                                            <td>
                                                <a href="{{action('Admin\ServiceController@changeStatus',['id'=>$service->id])}}"
                                                   class="red-text chip"><small>Chưa chạy</small></a>
                                            </td>
                                        @endif
                                        <td>
                                            <ul class="list-inline">
                                                <li style="margin-right: -11px;">
                                                    <a href="{{route('get.service.detail',['id'=>$service->id])}}"
                                                       class="btn green"><i
                                                                class="fa fa-search-plus" aria-hidden="true"></i></a>
                                                </li>
                                                <li style="margin-right: -11px;">
                                                    <a href="{{route('get.service.edit',['id'=>$service->id])}}"
                                                       class="btn yellow"><i
                                                                class="fa fa-pencil" aria-hidden="true"></i></a>
                                                </li>
                                                <li style="margin-right: -11px;">
                                                    <a href="{{route('get.service.delete',['id'=>$service->id])}}"
                                                       class="btn red"
                                                       onclick="alert('Không xóa Dịch vụ khi chưa chắc chắn');return confirm('Dịch vụ này sẽ biến mất khỏi hệ thống, việc này sẽ ảnh hưởng tới các khách hàng đang sử dụng dịch vụ này')">
                                                        <i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <h3><i>Chưa có dịch vụ nào...</i></h3>
                            @endif

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @if($listService)
            <div class="col-md-12">
                <div class="paginate">
                    <p class="pull-left">Tổng số trang : {{$listService->lastPage()}}</p>
                    <ul class="pagination pull-right">
                        <li class="">
                            <a href="{{$listService->url(1)}}">
                                <i class="ace-icon fa fa-angle-double-left"></i>
                            </a>
                        </li>
                        <li class="prev {{($listService->currentPage() == 1) ? 'disabled' : ''}}">
                            <a href="{{$listService->url($listService->currentPage() - 1)}}">Trước</a>
                        </li>
                        @for($i=1; $i<=$listService->lastPage();$i++ )
                            <li class="{{ ($listService->currentPage() == $i) ? 'active' : '' }}">
                                <a href="{{$listService->url($i)}}">{{$i}}</a>
                            </li>
                        @endfor
                        <li class="next {{($listService->currentPage() == $listService->lastPage()) ? 'disabled' : ''}}">
                            <a href="{{$listService->url($listService->currentPage() + 1)}}">Sau</a>
                        </li>
                        <li class="">
                            <a href="{{$listService->url($listService->lastPage())}}">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        @endif
    </main>

@endsection