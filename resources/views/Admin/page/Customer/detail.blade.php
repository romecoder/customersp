@extends('Admin.master')
@section('title','Detail')
@section('headlink')
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
@endsection
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title"><h5>Chi tiết về Khách hàng</h5></div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <div class="row">

                            <div class="col s8">
                                <div class="row">
                                    <div class="input-field col s12"><p><span>Thông tin chung:</span></p>
                                        <hr>
                                        <p><span>Tên người dùng:</span> {{ $thisCustomer->name }}</p>
                                        <p><span>Email:</span> {{ $thisCustomer->email}}</p>
                                        <p><span>Số điện thoại :</span> {{$thisCustomer->phone}}</p>
                                        <p><span>Địa chỉ :</span> {{$thisCustomer->address}}</p>
                                        <p><span>Ghi chú :</span>  {{$thisCustomer->content}}.</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s12">
                                        <!-- Switch -->
                                        <h6 for="">Giới tính:
                                            <label class="active">
                                                @if($thisCustomer->gender)
                                                    Nam
                                                @else
                                                    Nữ
                                                @endif
                                            </label>
                                        </h6>

                                    </div>
                                    <div class="col s12">
                                        <!-- Switch -->
                                        <h6 for="">Trạng thái:
                                            <label class="active">
                                                @if($thisCustomer->status)
                                                    kích hoạt
                                                @else
                                                    chưa kích hoạt
                                                @endif
                                            </label>
                                        </h6>
                                    </div>
                                    <div class="col s12">
                                        <!-- Switch -->
                                        <p>
                                            <span>Đăng ký ngày:</span> {{$thisCustomer->created_at}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col s4">
                                <img src="{{asset('CS2/images/avatar').'/'.$thisCustomer->avatar}}" alt="" width="150">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <div class="col s6">
                                <div class="row">
                                    <p><span>Dịch vụ sử dụng:</span></p>
                                    @if($listService)
                                        @foreach($listService as $service)
                                            <div class="media">
                                                <div class="media-left">
                                                    <img src="{{asset('CS2/images/service'.'/'.$service->thumbnail)}}" alt="" width="100" class="circle-wrapper">
                                                </div>
                                                <div class="media-body">
                                                    <span class="text-success">{{$service->name}}</span>
                                                    <br>
                                                    <span class="label label-success"> <a href="#thaydoitrangthaidichvu"></a>Hoạt động</span>
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                        <p><i>Chưa sử dụng dịch vụ nào được sử dụng</i></p>
                                    @endif
                                </div>
                            </div>
                            <div class="col s6">
                                <div class="row">
                                    <p><span>Dịch vụ chờ kích hoạt:</span></p>
                                    @if($listUnactive)
                                        @foreach($listUnactive as $service)
                                            <div>
                                                <img src="{{asset('CS2/images/service'.'/'.$service->thumbnail)}}" alt="" width="100" class="circle-wrapper">
                                                <span class="text-danger">{{$service->name}}</span>
                                            </div>
                                        @endforeach
                                    @else
                                        <p><i><label for="">Chưa có dịch vụ nào cần kích hoạt</label></i></p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!---->
        </div>
    </main>
    <!-- Modal -->
@endsection