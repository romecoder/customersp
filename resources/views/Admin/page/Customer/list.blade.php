@extends('Admin.master')
@section('title','Danh sách khách hàng')
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="col s12 pull-left text-center">
                    <span class="page-title"><label for="" style="font-size: 18px">Danh sách khách hàng:</label></span>
                </div>
                <div class="col s12 pull-left text-center">
                    @include('General.displayerrors')
                </div>
            </div>

            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <table id="example"
                               class="display responsive-table datatable-example striped bordered">
                            <thead>
                            <tr>
                                <th></th>
                                <th><small>Thông tin</small></th>
                                <th><small>Địa chỉ</small></th>
                                <th><small>Trạng thái</small></th>
                                <th> <span class="btn blue" style="width: 73%;"><a href="{{route('get.customer.add')}}"><i
                                                    class="fa fa-user-plus" aria-hidden="true"
                                                    style="color: #ffffff"></i></a></span></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($listCustomer) && $listCustomer)
                                @foreach($listCustomer as $Customer)
                                    <tr class="text-center">
                                        @if(isset($Customer->avatar) && $Customer->avatar)
                                            @if($Customer->seen == 0)
                                                {{--<td><i class="fa fa-star-o" aria-hidden="true" ></i>  <img--}}
                                                            {{--src="{{asset('CS2/images/avatar/'.$Customer->avatar)}}"--}}
                                                            {{--alt=""--}}
                                                            {{--class="img-reponsive" width="50em">--}}
                                                {{--</td>--}}
                                                <td><img src="{{asset('CS2/images/avatar/'.$Customer->avatar)}}" alt=""
                                                         class="img-circle img-thumbnail" width="50em">
                                                </td>
                                            @else
                                                <td><img src="{{asset('CS2/images/avatar/'.$Customer->avatar)}}" alt=""
                                                         class="img-circle img-thumbnail" width="50em">
                                                </td>
                                            @endif
                                        @else
                                            @if($Customer->seen == 1)
                                                <td>
                                                    <img src="{{asset('CS2/images/avatar/avatar.png')}}" alt=""
                                                            class="img-circle img-thumbnail" width="50em">
                                                </td>
                                            @else
                                                <td><img src="{{asset('CS2/images/avatar/avatar.png')}}" alt=""
                                                         class="img-circle img-thumbnail" width="50em">
                                                </td>
                                            @endif
                                        @endif
                                        <td><a href="{{route('get.customer.detail',['id'=>$Customer->id])}}">{{$Customer->name}}</a> <br><small>({{$Customer->email}})</small></td>
                                        <td width="30%">@if(strlen($Customer->address) >= 25)
                                                {{--<marquee scrolldelay="3" scrollamount="2" witdh="30" behavior="scroll " direction="left">{{$Customer->address}}</marquee>--}}
                                                <small>{{substr($Customer->address,0,25)}}...</small>
                                            @else
                                                        <small>{{$Customer->address}}</small>
                                            @endif
                                        </td>
                                        @if($Customer->status == 1)
                                            <td>
                                                <a href="{{action('Admin\CustomerController@changeStatus',['id'=>$Customer->id])}}"
                                                   class="chip text-success"><small>Kích hoạt</small></a>
                                            </td>
                                        @else
                                            <td>
                                                <a href="{{action('Admin\CustomerController@changeStatus',['id'=>$Customer->id])}}"
                                                   class="chip text-danger"><small>Chờ kích hoạt</small></a>
                                            </td>
                                        @endif
                                        <td>
                                            <ul class="list-inline">
                                                <li style="">
                                                    <a href="{{route('get.customer.detail',['id'=>$Customer->id])}}"
                                                       class="btn green" title="Chi tiết"><i
                                                                class="fa fa-search-plus" aria-hidden="true"
                                                                style="font-size: 12px; margin-left:-4px;"></i></a>
                                                </li>
                                                <li style="">
                                                    <a href="{{route('get.customer.edit',['id'=>$Customer->id])}}"
                                                       class="btn yellow" title="Sửa"><i
                                                                class="fa fa-pencil" aria-hidden="true"
                                                                style="font-size: 12px; margin-left:-4px;"></i></a>
                                                </li>
                                                <li style="">
                                                    <a href="{{route('get.customer.delete',['id'=>$Customer->id])}}"
                                                       class="btn red" title="Xóa"
                                                       onclick="return confirm('Bạn có muốn xóa người dùng này?')">
                                                        <i class="fa fa-trash-o" aria-hidden="true"
                                                           style=""></i></a>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <h3><i>Chưa có thành viên nào...</i></h3>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @if(sizeof($listCustomer))
                <div class="col-md-12">
                    <div class="paginate">
                        <p class="pull-left">Tổng số trang : {{$listCustomer->lastPage()}}</p>
                        <ul class="pagination pull-right no-margin">
                            <li class="">
                                <a href="{{$listCustomer->url(1)}}">
                                    <i class="ace-icon fa fa-angle-double-left"></i>
                                </a>
                            </li>
                            <li class="prev {{($listCustomer->currentPage() == 1) ? 'disabled' : ''}}">
                                <a href="{{$listCustomer->url($listCustomer->currentPage() - 1)}}">Trước</a>
                            </li>
                            @for($i=1; $i<=$listCustomer->lastPage();$i++ )
                                <li class="{{ ($listCustomer->currentPage() == $i) ? 'active' : '' }}">
                                    <a href="{{$listCustomer->url($i)}}">{{$i}}</a>
                                </li>
                            @endfor
                            <li class="next {{($listCustomer->currentPage() == $listCustomer->lastPage()) ? 'disabled' : ''}}">
                                <a href="{{$listCustomer->url($listCustomer->currentPage() + 1)}}">Sau</a>
                            </li>
                            <li class="">
                                <a href="{{$listCustomer->url($listCustomer->lastPage())}}">
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            @endif
        </div>
    </main>

    @include('Admin.block.footer')
@endsection