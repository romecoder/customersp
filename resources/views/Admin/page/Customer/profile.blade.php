@extends('Admin.master')
@section('title','Cá nhân')
@section('headlink')
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
@endsection
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title">Chi tiết khách hàng</div>
            </div>
            <div class="col s12 m4 l3">
                <div class="card">
                    <div class="card-content center-align">
                        <img src="@if($thisCustomer->avatar){{asset('CS2/images/avatar').'/'.$thisCustomer->avatar}} @else {{asset('CS2/images/avatar').'/avatar.png'}} @endif"
                             class="responsive-img circle" width="128px" alt="">
                        <p><span class="m-t-lg flow-text">{{ $thisCustomer->name}}</span>  <span><i>(#{{$thisCustomer->id}})</i></span></p>
                        <div class="chip m-t-sm blue-grey white-text">@if($thisCustomer->gender) Nam @else
                                Nữ @endif</div>
                        <div class="chip m-t-sm blue-grey white-text">@if($thisCustomer->status) Hoạt động @else
                                Chờ @endif</div>
                    </div>
                </div>
            </div>
            <div class="col s12 m4 l4">
                <div class="card">
                    <div class="card-content ">
                        <span class="card-title">Thông tin chung</span>
                        <p>Họ Tên : <span class="chip m-t-sm">{{$thisCustomer->name}}</span></p>
                        <p>Email : <span class="chip m-t-sm">{{$thisCustomer->email}}</span></p>
                        <p>Địa chỉ: <span class="chip m-t-sm">{{$thisCustomer->address}}</span></p>
                        <p>Ngày sinh:  <span class="chip m-t-sm">{{date('Y-m-d',strtotime($thisCustomer->birthday))}}</span></p>
                        <p>Ngày tham gia: <span class="chip m-t-sm">{{date('Y-m-d',strtotime($thisCustomer->created_at))}}</span></p>
                    </div>
                </div>
            </div>
            <div class="col s12 m4 l5">
                <div class="card">
                    <div class="card-content">
                        <span class="card-title">Thông tin thêm</span>
                        @if($listUserOption)
                            @foreach($listUserOption as $Option)
                                <p>{{$Option->key}}: <span class="chip m-t-sm">{{$Option->value}}</span></p>
                            @endforeach
                        @else
                        @endif
                    </div>
                </div>
                <div class="card">
                    <div class="card-content ">
                        <span class="card-title">Giới thiệu:</span>
                        <p>{!! $thisCustomer->content !!}</p>
                    </div>
                </div>
            </div>

            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <div class="col s6">
                                <div class="row">
                                    <p><span><label for="">Dịch vụ sử dụng:</label></span></p>
                                    @if($listService)
                                        @foreach($listService as $service)
                                            <div class="media">
                                                <div class="media-left">
                                                    <img src="@if($service->thumbnail){{asset('CS2/images/service'.'/'.$service->thumbnail)}}@else {{asset('CS2/images/service/service.png')}}@endif"
                                                         alt="" width="60em" class="circle-wrapper">
                                                </div>
                                                <div class="media-body">
                                                    <br>
                                                    <span class="text-success">{{$service->name}}</span>
                                                    <br>
                                                    <span class="label label-success"> <a
                                                                href="#thaydoitrangthaidichvu"></a>Hoạt động</span>
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                        <p><i><label for="">Chưa sử dụng dịch vụ nào được sử dụng</label></i></p>
                                    @endif
                                </div>
                            </div>
                            <div class="col s6">
                                <div class="row">
                                    <p><span><label for="">Dịch vụ chờ kích hoạt:</label></span></p>
                                    @if($listUnactive)
                                        @foreach($listUnactive as $service)
                                            <div class="media">
                                                <div class="media-left">
                                                    <img src="@if($service->thumbnail){{asset('CS2/images/service'.'/'.$service->thumbnail)}}@else {{asset('CS2/images/service/service.png')}}@endif"
                                                         alt="" width="60em" class="circle-wrapper">
                                                </div>
                                                <div class="media-body">
                                                    <br>
                                                    <span class="text-danger">{{$service->name}}</span>
                                                    <br>
                                                    <span class="label label-danger"> <a
                                                                href="#thaydoitrangthaidichvu"></a>Đóng băng</span>
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                        <p><i><label for="">Chưa có dịch vụ nào cần kích hoạt</label></i></p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- Modal -->
    <div id="changePassword" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Đổi mật khẩu:</h4>
                <div class="modal-body">
                    <form action="{{action('Customer\ProfileController@changePassword',['id'=>$thisCustomer->id])}}"
                          method="post">
                        <label for="">Mật khẩu mới:</label>
                        <input type="password" name="pass" placeholder="Nhập vào mật khẩu bạn muốn đổi">
                        <label for="">Nhập lại mật khẩu:</label>
                        <input type="password" name="repass" placeholder="Nhập lại mật khẩu">
                        {{csrf_field()}}

                        <input type="submit" class="btn teal lighten-1 btn-sm" value="Đổi mật khẩu">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <a class="waves-effect waves-light btn modal-trigger" href="#modal1">Modal</a>

    <!-- Modal Structure -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <h4>Modal Header</h4>
            <p>A bunch of text</p>
        </div>
        <div class="modal-footer">
            <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
        </div>
    </div>
@endsection
@section('footlink')
    <script>
        CKEDITOR.replace('textarea1');
    </script>
@endsection

@section('afterJquery')
    <script src="{{asset('CS2/js/ckeditor/ckeditor.js')}}"></script>
@endsection