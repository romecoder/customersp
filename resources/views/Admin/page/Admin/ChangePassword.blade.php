@extends('Admin.master')
@section('title','Đổi mật khẩu')
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title"><h5>Thay đổi mật khẩu của bạn</h5></div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <form class="col s12 m12" method="post" action="{{action('Admin\AdminController@postChangePassword')}}" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="password" type="password" class="" name="oldPassword">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('password') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="last_name">Mật khẩu Cũ</label>
                                    </div>
                                    <div class="input-field col s12">
                                        <input id="password" type="password" class="" name="password">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('password') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="last_name">Mật khẩu mới</label>
                                    </div>
                                    <div class="input-field col s12">
                                        <input id="repassword" type="password" class="" name="repassword">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('repassword') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="repassword">Nhập lại mật khẩu mới</label>
                                    </div>
                                </div>

                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col cs12 pull-right">
                                        <input type="submit" class="btn btn-block btn-primary btn-lg" value="Lưu">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!---->
        </div>
    </main>
@endsection
