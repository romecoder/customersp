@extends('Admin.master')
@section('title','Thêm Admin')
@section('headlink')
    <link href="{{asset('CS2/plugins/select2/css/select2.css')}}" rel="stylesheet">
@endsection
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title"><h5>Thêm Quản trị viên</h5></div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <form class="col s12 m12" method="post" action="{{route('post.member.add')}}"
                                  enctype="multipart/form-data">
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="name" type="text" class="" value="{{old('name')}}" name="name">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('name') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="name" class="active">Tên đăng nhập:</label>
                                    </div>

                                    <div class="input-field col s6">
                                        <input id="email" type="email" class="" value="{{old('email')}}" name="email">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('email') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="email" class="active">Email:</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="phone" type="number" class="" value="{{old('phone')}}" name="phone">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('phone') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="phone" class="active">Điện thoại:</label>
                                    </div>

                                    <div class="input-field col s6">
                                        <label for="address" class="active">Địa chỉ:</label>
                                        <input id="address" name="address" type="text" value="{{old('address')}}"
                                               name="address">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('address') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="password" type="password" class="" name="password">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('password') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="last_name" class="active">Mật khẩu:</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input id="repassword" type="password" class="" name="repassword">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('repassword') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="repassword" class="active">Nhập lại mật khẩu:</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="fullname" type="text" class="" name="fullname">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('fullname') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="fullname" class="active">Họ và tên:</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s6">
                                        <!-- Switch -->
                                        <h6 for="">Giới tính:</h6>
                                        <div class="switch m-b-md">
                                            <label>
                                                Nữ
                                                <input type="checkbox" name="gender" value="1" class="validate">
                                                <span class="lever"></span>
                                                Nam
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col s6">
                                        <!-- Switch -->
                                        <h6 for="">Trạng thái:</h6>
                                        <div class="switch m-b-md">
                                            <label>
                                                Chờ kích hoạt
                                                <input type="checkbox" name="status" value="1" class="validate">
                                                <span class="lever"></span>
                                                kích hoạt
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    @if($listRole)
                                        <div class="input-field col s12">
                                            <select class="js-states browser-default" multiple="multiple"
                                                    tabindex="-1" style="width: 100%" id="multiple" name="role[]" >
                                                @foreach($listRole as $role)
                                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @if(count($errors)>0)
                                            @foreach($errors->get('role') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                    @else
                                        <p><i>Hệ thống chưa tạo quyền...</i></p>
                                    @endif
                                </div>

                                <div class="input-field col s6">
                                    <div class="input-field col s12">
                                        <div class="file-field input-field">
                                            <div class="btn teal lighten-1">
                                                <span>Avatar</span>
                                                <input type="file" name="avatar" value="{{old('avatar')}}">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text"
                                                       value="{{old('avatar')}}">
                                                @if(count($errors)>0)
                                                    @foreach($errors->get('avatar') as $error)
                                                        <p style="color: red">{{$error}}</p>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col s12">
                                        <textarea id="textarea1" class="materialize-textarea" length="120" name="contents">{!! old('content') !!} </textarea>
                                    </div>
                                </div>

                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col cs12 pull-right">
                                        <input type="submit" class="btn btn-block btn-primary btn-lg"
                                               value="Thêm mới">
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!---->
        </div>
    </main>
@endsection
@section('footlink')
    <script src="{{asset('CS2/plugins/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('CS2/js/alpha.min.js')}}"></script>
    <script src="{{asset('CS2/js/pages/form-select2.js')}}"></script>
    <script>
        CKEDITOR.replace('textarea1');
    </script>
@endsection

@section('afterJquery')
    <script src="{{asset('CS2/js/pages/form-select2.js')}}"></script>
    <script src="{{asset('CS2/js/ckeditor/ckeditor.js')}}"></script>
@endsection

@section('style')

    .mn-inner form {
    padding: 5% !important;
    }
    .btn-flat{
    border: 1px dashed;
    }
    .btn-flat:hover{
    background-color: #fffffe;
    }

@endsection