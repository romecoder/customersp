@extends('Admin.master')
@section('title','Danh sách Admin')
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="col s12 pull-left text-center">
                    <h3 class="page-title"><label for="" style="font-size: 18px">Quản Lý Quản trị viên:</label></h3>
                </div>
                <div class="col s12 pull-left text-center">
                    @include('General.displayerrors')
                </div>
            </div>

            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <table id="example" class="display responsive-table datatable-example highlight">
                            <thead>
                            <tr>
                                <th></th>
                                <th><small>Thông tin Admin</small></th>
                                <th><small>Địa chỉ</small></th>
                                <th><small>Trạng thái</small></th>
                                <th><span class="btn blue" style="width: 58%"><a href="{{route('get.member.add')}}"><i
                                                    class="fa fa-user-plus" aria-hidden="true"
                                                    style="color: #ffffff"></i></a></span></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($listMembers) && $listMembers)
                                @foreach($listMembers as $listMember)
                                    <tr class="text-center">
                                        @if(isset($listMember->avatar) && $listMember->avatar)
                                            <td><img src="{{asset('CS2/images/avatar/'.$listMember->avatar)}}" alt=""
                                                     class="img-circle img-thumbnail" width="50em">
                                            </td>
                                        @else
                                            <td><img src="{{asset('CS2/images/avatar/avatar.png')}}" alt=""
                                                     class="img-circle img-thumbnail" width="50em">
                                            </td>
                                        @endif
                                        <td><a href="{{route('get.member.detail',['id'=>$listMember->id])}}">{{$listMember->name}}</a><br><small>({{$listMember->email}})</small>  </td>
                                        <td>@if(strlen($listMember->address) >= 30)
                                                <small>{{substr($listMember->address,0,30)}}...</small>
                                            @else
                                                <small>{{$listMember->address}}</small>
                                            @endif
                                        </td>
                                        @if($listMember->status == 1)
                                            <td>
                                                <a href="{{action('Admin\AdminController@changeStatus',['id'=>$listMember->id])}}"
                                                   class="green-text chip"><small>Kích hoạt</small></a>
                                            </td>
                                        @else
                                            <td>
                                                <a href="{{action('Admin\AdminController@changeStatus',['id'=>$listMember->id])}}"
                                                   class="text-danger chip"><small>Chờ kích hoạt</small></a>
                                            </td>
                                        @endif
                                        <td>
                                            <ul class="list-inline">
                                                <li style="margin-right: -11px;">
                                                    <a href="{{route('get.member.detail',['id'=>$listMember->id])}}"
                                                       class="btn green" title="Chi tiết"><i
                                                                class="fa fa-search-plus" aria-hidden="true"></i></a>
                                                </li>
                                                <li style="margin-right: -11px;">
                                                    <a href="{{route('get.member.edit',['id'=>$listMember->id])}}"
                                                       class="btn yellow" title="Sửa"><i
                                                                class="fa fa-pencil" aria-hidden="true"
                                                                style="font-size: 12px; margin-left:-4px;"></i></a>
                                                </li>
                                                <li style="margin-right: -11px;">
                                                    <a href="{{route('get.member.delete',['id'=>$listMember->id])}}"
                                                       class="btn red"title="Xóa"
                                                       onclick="return confirm('Bạn có muốn xóa quản trị viên này?')">
                                                        <i class="fa fa-trash-o" aria-hidden="true"
                                                           ></i></a>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <h3>Chưa có quản trị viên nào</h3>
                            @endif
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        @if(sizeof($listMembers))
            <div class="col-md-12">
                <div class="paginate">
                    <p class="pull-left">Tổng số trang : {{$listMembers->lastPage()}}</p>
                    <ul class="pagination pull-right no-margin">
                        <li class="">
                            <a href="{{$listMembers->url(1)}}">
                                <i class="ace-icon fa fa-angle-double-left"></i>
                            </a>
                        </li>
                        <li class="prev {{($listMembers->currentPage() == 1) ? 'disabled' : ''}}">
                            <a href="{{$listMembers->url($listMembers->currentPage() - 1)}}">Trước</a>
                        </li>
                        @for($i=1; $i<=$listMembers->lastPage();$i++ )
                            <li class="{{ ($listMembers->currentPage() == $i) ? 'active' : '' }}">
                                <a href="{{$listMembers->url($i)}}">{{$i}}</a>
                            </li>
                        @endfor
                        <li class="next {{($listMembers->currentPage() == $listMembers->lastPage()) ? 'disabled' : ''}}">
                            <a href="{{$listMembers->url($listMembers->currentPage() + 1)}}">Sau</a>
                        </li>
                        <li class="">
                            <a href="{{$listMembers->url($listMembers->lastPage())}}">
                                <i class="ace-icon fa fa-angle-double-right"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        @endif
    </main>
@endsection