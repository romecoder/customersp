@extends('Admin.master')
@section('title','Cập nhật Admin')
@section('headlink')
    <link href="{{asset('CS2/plugins/select2/css/select2.css')}}" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
@endsection
@section('content')
    <main class="mn-inner">
        <div class="row">
            <div class="col s12">
                <div class="page-title"><h5>Cập nhật thông tin quản trị viên</h5></div>
            </div>
            <div class="col s12">
                @include('General.displayerrors')
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <form class="col s12 m12" method="post"
                                  action="{{route('post.member.edit',['id'=>$thisAdmin->id])}}"
                                  enctype="multipart/form-data">
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="name" type="text" class=""
                                               value="{{old('name',isset($thisAdmin)? $thisAdmin->name:'')}}"
                                               name="name">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('name') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="name" class="active">Tên:</label>
                                    </div>

                                    <div class="input-field col s6">
                                        <input id="email" type="email" class=""
                                               value="{{old('email',isset($thisAdmin)? $thisAdmin->email:'')}}"
                                               name="email">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('email') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="email" class="active">Email:</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="phone" type="number" class=""
                                               value="{{old('phone',isset($thisAdmin)? $thisAdmin->phone:'')}}"
                                               name="phone">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('phone') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="phone" class="active">Điện thoại:</label>
                                    </div>

                                    <div class="input-field col s6">
                                        <label for="address" class="active">Địa chỉ:</label>
                                        <input id="address" name="address" type="text"
                                               value="{{old('address',isset($thisAdmin)? $thisAdmin->address:'')}}"
                                               name="address">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('address') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input id="fullname" type="text" class="" name="fullname"
                                               value="{{old('fullname',isset($thisAdmin)? $thisAdmin->full_name:'')}}">
                                        @if(count($errors)>0)
                                            @foreach($errors->get('fullname') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                        <label for="fullname" class="active">Họ và tên</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s6">
                                        <!-- Switch -->
                                        <h6 for="">Giới tính</h6>
                                        <div class="switch m-b-md">
                                            <label>
                                                Nữ
                                                @if($thisAdmin->gender)
                                                    <input type="checkbox" name="gender" checked>
                                                    <span class="lever"></span>
                                                @else
                                                    <input type="checkbox" name="gender" class="validate">
                                                    <span class="lever"></span>
                                                @endif
                                                Nam
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col s6">
                                        <!-- Switch -->
                                        <h6 for="">Status</h6>
                                        <div class="switch m-b-md">
                                            <label>
                                                Chờ kích hoạt
                                                @if($thisAdmin->status)
                                                    <input type="checkbox" name="status" checked>
                                                    <span class="lever"></span>
                                                @else
                                                    <input type="checkbox" name="status" class="validate">
                                                    <span class="lever"></span>
                                                @endif
                                                kích hoạt
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    @if($listRole)
                                        <div class="input-field col s12">
                                            <select class="js-states browser-default" multiple="multiple"
                                                    tabindex="-1" style="width: 100%" id="multiple" name="role[]">
                                                @foreach($listRole as $role)
                                                    <option value="{{$role->id}}"
                                                            @if(in_array($role->id,$listAdminRole))selected @endif>{{$role->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @if(count($errors)>0)
                                            @foreach($errors->get('role') as $error)
                                                <p style="color: red">{{$error}}</p>
                                            @endforeach
                                        @endif
                                    @else
                                        <p><i>Hệ thống chưa tạo quyền...</i></p>
                                    @endif
                                </div>

                                <div class="row">
                                    <div class="input-field col s12">
                                        <textarea id="textarea1" class="materialize-textarea" length="120"
                                                  name="contents">{!! old('address',isset($thisAdmin)? $thisAdmin->content:'') !!} </textarea>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col s6">
                                        <div class="file-field input-field">
                                            <a type="" class="" data-toggle="modal"
                                                    data-target="#changePassword"><i>Đổi mật khẩu</i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="input-field col s6">
                                        <div class="file-field input-field">
                                            <div class="btn teal ">
                                                <span>Avatar</span>
                                                <input type="file" name="avatar" value="{{old('avatar')}}">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text"
                                                       value="{{old('avatar')}}"
                                                       placeholder="{{ old('avatar',isset($thisAdmin)? $thisAdmin->avatar:'')}}">
                                                <input type="hidden" value="{{$thisAdmin->avatar}}"
                                                       name="oldavatar">
                                                @if(count($errors)>0)
                                                    @foreach($errors->get('avatar') as $error)
                                                        <p style="color: red">{{$error}}</p>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col cs12 pull-right">
                                        <input type="submit" class="btn btn-block btn-primary btn-lg"
                                               value="Cập nhật">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!---->
        </div>
    </main>
    <!-- Modal -->
    <div id="changePassword" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Đổi mật khẩu:</h4>
                <div class="modal-body">
                    <form action="{{action('Admin\AdminController@adminChangeAdminPassword',['id'=>$thisAdmin->id])}}"
                          method="post">
                        <label for="">Mật khẩu mới:</label>
                        <input type="password" name="pass" placeholder="Nhập vào mật khẩu bạn muốn đổi">
                        <label for="">Nhập lại mật khẩu mới:</label>
                        <input type="password" name="repass" placeholder="Nhập lại mật khẩu">
                        {{csrf_field()}}

                        <input type="submit" class="btn teal lighten-1 btn-sm" value="Đổi mật khẩu">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footlink')
    <script src="{{asset('CS2/plugins/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('CS2/js/alpha.min.js')}}"></script>
    <script src="{{asset('CS2/js/pages/form-select2.js')}}"></script>
    <script>
        CKEDITOR.replace('textarea1');
    </script>
@endsection

@section('afterJquery')
    <script src="{{asset('CS2/js/pages/form-select2.js')}}"></script>
    <script src="{{asset('CS2/js/ckeditor/ckeditor.js')}}"></script>
@endsection
@section('style')

        .mn-inner form {
            padding: 5% !important;
        }

@endsection