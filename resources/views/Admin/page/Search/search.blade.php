
@extends('Admin.master')
@section('title','Dashboard')
@section('headlink')
@endsection
@section('content')
    <main class="mn-inner">
        <div class="card card-transparent no-m">
            <div class="card-content invoice-relative-content search-results-container">
                <div class="col s12 m12 l12">
                    <div class="search-page-results">
                        <div id="videos" class="col s12 m12 l12">
                            <div class="row">
                                <div class="col s12 m8 offset-m2 l10 offset-l1">
                                    <div class="card-panel grey lighten-5 z-depth-1">
                                        <div class="row valign-wrapper">
                                            <div class="col s2">
                                                <a href="#"><img src="assets/images/video_placeholder.png" alt="" class="circle responsive-img"></a>
                                            </div>
                                            <div class="col s10">
                                                <div class="black-text">
                                                    <a href="#" class="heading-title">Pantera - Vulgar Display Of Power ( Full Album ) - YouTube</a>
                                                    <a href="#" class="green-text">https://www.youtube.com/watch?v=Nx08MK_O_TQ</a>
                                                    <p class="blue-grey-text">May 4, 2012 - Uploaded by youtube</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12 m8 offset-m2 l10 offset-l1">
                                    <div class="card-panel grey lighten-5 z-depth-1">
                                        <div class="row valign-wrapper">
                                            <div class="col s2">
                                                <a href="#"><img src="assets/images/video_placeholder.png" alt="" class="circle responsive-img"></a>
                                            </div>
                                            <div class="col s10">
                                                <div class="black-text">
                                                    <a href="#" class="heading-title">Pantera - Walk - YouTube</a>
                                                    <a href="#" class="green-text">https://www.youtube.com/watch?v=5Ju11Q1MW-4</a>
                                                    <p class="blue-grey-text">Sep 27, 2011 - Uploaded by youtube</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12 m8 offset-m2 l10 offset-l1">
                                    <div class="card-panel grey lighten-5 z-depth-1">
                                        <div class="row valign-wrapper">
                                            <div class="col s2">
                                                <a href="#"><img src="assets/images/video_placeholder.png" alt="" class="circle responsive-img"></a>
                                            </div>
                                            <div class="col s10">
                                                <div class="black-text">
                                                    <a href="#" class="heading-title">Cemetery Gates - Pantera (HQ Audio) - YouTube</a>
                                                    <a href="#" class="green-text">https://www.youtube.com/watch?v=1OYw7FPB7CE</a>
                                                    <p class="blue-grey-text">Sep 1, 2011 - Uploaded by youtube</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('footlink')
    <script>
        CKEDITOR.replace('textarea1');
    </script>
@endsection
@section('afterJquery')
    <script src="{{asset('CS2/js/ckeditor/ckeditor.js')}}"></script>
@endsection