{{--Display zone for: Insert messages --}}
@if(Session::has('message') && Session::has('level'))
    <div class="alert alert-block alert-{!! Session::get('level') !!} message-box">
        <button type="button" class="close" data-dismiss="alert">
        </button>
        @if(Session::get('level')=='success')

            <strong class="">
                {{Session::get('message')}}.
            </strong>
        @elseif(Session::get('level')=='info')
            <strong class="">
                {{Session::get('message')}}.
            </strong>
        @else
            <strong class="">
                {{Session::get('message')}}.
            </strong>
        @endif
    </div>
@endif

{{--Display zone for: validate errors --}}
{{--@if(count($errors)>0)--}}
    {{--@foreach($errors->all() as $error)--}}
        {{--<div class="alert alert-block alert-warning message-box">--}}
            {{--<button type="button" class="close" data-dismiss="alert">--}}
                {{--<i class="ace-icon fa fa-times"></i>--}}
            {{--</button>--}}
            {{--<strong>--}}
                {{--{{$error}}--}}
            {{--</strong>--}}
        {{--</div>--}}
    {{--@endforeach--}}
{{--@endif--}}