<!DOCTYPE html>
<html lang="en">

    <head>
        
        <!-- Title -->
        <title>Pveser Hỗ trợ khách hàng</title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template"/>
        <meta name="keywords" content="admin,dashboard"/>
        <meta name="author" content="Steelcoders"/>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script defer src="{{asset('CS2/ownjquery.js')}}" language="JavaScript"></script>
        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="{{asset('CS2/plugins/materialize/css/materialize.min.css')}}"/>
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="{{asset('CS2/plugins/material-preloader/css/materialPreloader.min.css')}}" rel="stylesheet">


        <!-- Theme Styles -->
        <link href="{{asset('CS2/css/alpha.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('CS2/css/custom.css')}}" rel="stylesheet" type="text/css"/>


    </head>
    <body class="signin-page">
        <div class="loader-bg"></div>
        <div class="loader">
            <div class="preloader-wrapper big active">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mn-content valign-wrapper">
            <main class="mn-inner container ">
                <div class="valign">
                      <div class="row">
                          <div class="col s12 m6 l4 offset-l4 offset-m3">
                              <div class="card white darken-1">
                                  <div class="card-content ">
                                      @include('General.displayerrors')
                                      <span class="card-title">Quên mật khẩu</span>
                                       <div class="row">
                                           <form class="col s12" action="{{route('post.forgot')}}" method="post">
                                               <div class="input-field col s12">
                                                   <input id="email" type="email" class="validate" name="email">
                                                   <label for="email">Email</label>
                                               </div>
                                               <div class="col s12 right-align m-t-sm">
                                                   <a href="{{route('get.login')}}" class="waves-effect waves-grey btn-flat">Trở lại</a>
                                                   <button class="waves-effect waves-light btn teal">reset</button>
                                               </div>
                                               {{csrf_field()}}
                                           </form>
                                      </div>
                                  </div>
                              </div>
                          </div>
                    </div>
                </div>
            </main>
        </div>
        
        <!-- Javascripts -->
        <script src="{{asset('CS2/plugins/jquery/jquery-2.2.0.min.js')}}"></script>
        <script src="{{asset('CS2/plugins/materialize/js/materialize.min.js')}}"></script>
        <script src="{{asset('CS2/plugins/material-preloader/js/materialPreloader.min.js')}}"></script>
        <script src="{{asset('CS2/plugins/jquery-blockui/jquery.blockui.js')}}"></script>
        <script src="{{asset('CS2/js/alpha.min.js')}}"></script>
        
    </body>
</html>