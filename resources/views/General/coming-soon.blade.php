<!DOCTYPE html>
<html lang="en">
@if(env('HOST') == 'offline')
    <?php $link = 'http://localhost/vovan/customerSP/public/CS2'?>
@else
    <?php $link = 'http://pveser.romecody.com/CS2'?>
@endif

    <head>
        
        <!-- Title -->
        <title>Pveser Customer support system</title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />
        
        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="{{$link}}/plugins/materialize/css/materialize.min.css"/>
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="{{$link}}/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">        

        	
        <!-- Theme Styles -->
        <link href="{{$link}}/css/alpha.min.css" rel="stylesheet" type="text/css"/>
        <link href="{{$link}}/css/custom.css" rel="stylesheet" type="text/css"/>
        
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body class="error-page page-coming-soon">
        <div class="loader-bg"></div>
        <div class="loader">
            <div class="preloader-wrapper big active">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mn-content">
            <main class="mn-inner container">
                <div class="center">
                    <h1>
                        <span id="countdown"></span>
                    </h1>
                    <span class="text-white f-s-32">Hệ thống đang trong quá trình phát triển, chúng tôi sẽ trở lại nhanh chóng.</span>
                </div>
            </main>
        </div>
        
        <!-- Javascripts -->
        <script src="{{$link}}/plugins/jquery/jquery-2.2.0.min.js"></script>
        <script src="{{$link}}/plugins/materialize/js/materialize.min.js"></script>
        <script src="{{$link}}/plugins/material-preloader/js/materialPreloader.min.js"></script>
        <script src="{{$link}}/plugins/jquery-blockui/jquery.blockui.js"></script>
        <script src="{{$link}}/plugins/jquery.countdown/jquery.countdown.min.js"></script>
        <script src="{{$link}}/js/alpha.min.js"></script>
        <script src="{{$link}}/js/pages/coming-soon.js"></script>
        
    </body>
</html>