<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'checklogin'=>\App\Http\Middleware\CheckLoginMiddleware::class,
        'islogin'=>\App\Http\Middleware\CheckIsLoginMiddleware::class,
        'getSidebarData'=>\App\Http\Middleware\getSidebarData::class,
        'getSidebarDataUser'=>\App\Http\Middleware\getSidebarDataUser::class,
        'adminInfo'=>\App\Http\Middleware\adminInfo::class,
        'userInfo'=>\App\Http\Middleware\userInfo::class,
        'AcceptRoleAdminManager'=>\App\Http\Middleware\AcceptRole\AcceptRoleAdminManager::class,
        'AcceptRoleCustomerManager'=>\App\Http\Middleware\AcceptRole\AcceptRoleCustomerManager::class,
        'AcceptRoleEmailManager'=>\App\Http\Middleware\AcceptRole\AcceptRoleEmailManager::class,
        'AcceptRoleOptionManager'=>\App\Http\Middleware\AcceptRole\AcceptRoleOptionManager::class,
        'AcceptRoleOrderManager'=>\App\Http\Middleware\AcceptRole\AcceptRoleOrderManager::class,
        'AcceptRolePostManager'=>\App\Http\Middleware\AcceptRole\AcceptRolePostManager::class,
        'AcceptRoleServiceManager'=>\App\Http\Middleware\AcceptRole\AcceptRoleServiceManager::class,
        'AcceptRoleSupportManager'=>\App\Http\Middleware\AcceptRole\AcceptRoleSupportManager::class,
        'AcceptRoleEmailMarketingManager'=>\App\Http\Middleware\AcceptRole\AcceptRoleEmailMarketingManager::class,
    ];
}
