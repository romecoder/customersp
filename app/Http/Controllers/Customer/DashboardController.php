<?php

namespace App\Http\Controllers\Customer;

use App\Objects\CustomerObject;
use App\Repositories\PostRepositories;
use App\Repositories\UserRepositories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class DashboardController extends Controller
{
    private $postRepo;
    private $userRepo;

    public function __construct(
        UserRepositories $userRepositories,
        PostRepositories $postRepositories
    ) {
        $this->postRepo = $postRepositories;
        $this->userRepo = $userRepositories;
    }

    public function faq()
    {
        return view('Customer.page.Faq.faq');
    }

    public function agreeTerms()
    {
        $userObj = new CustomerObject();
        $userObj->id = session('userInfo')->id;
        $userObj->agreeTerm = 1;
        $doAgreeTerms = $this->userRepo->update($userObj);
        if ($doAgreeTerms->messageCode == 1) {
            $level = 'success';
            $message = 'Chào mừng bạn đến với Hệ thống Pveser';
        } else {
            $level = 'warning';
            $message = 'Bạn chưa chấp nhận các điều khoản của Pserve';
        }

        return redirect()->action('Customer\DashboardController@getHome')
            ->with(['level' => $level, 'message' => $message]);
    }

    public function getHome()
    {
        /*lấy ra danh sách bài viết:*/
        $doGetListPost = $this->postRepo->getListPost(0, 1);
        if ($doGetListPost->messageCode == 1) {
            $listPost = $doGetListPost->result;
        } else {
            $listPost = [];
        }

        return view('Customer.page.Dashboard.dashboard',
            ['listPost' => $listPost]);
    }
    public function getPost($id)
    {
        $doGetPost = $this->postRepo->getOne($id);
        if($doGetPost->messageCode == 1){
            $thisPost = $doGetPost->result;
        } else {
            $thisPost = null;
        }
        return view('Customer.page.Dashboard.post',
            ['thisPost' => $thisPost]);
    }
}
