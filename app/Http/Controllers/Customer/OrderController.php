<?php

namespace App\Http\Controllers\Customer;

use App\Mail\ConfirmAddNewOrder;
use App\Mail\ConfirmAddNewOrderToAdmin;
use App\Objects\OrderObject;
use App\Repositories\OrderRepositories;
use App\Repositories\ServiceRepositories;
use App\Repositories\UserRepositories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;

class OrderController extends Controller
{
    private $orderRepo;
    private $serviceRepo;
    private $userRepo;

    public function __construct(
        OrderRepositories $orderRepositories,
        ServiceRepositories $serviceRepositories,
        UserRepositories $userRepositories
    ) {
        $this->orderRepo = $orderRepositories;
        $this->serviceRepo = $serviceRepositories;
        $this->userRepo = $userRepositories;
        date_default_timezone_set("Asia/Bangkok");

    }

    /**
     * Ham: lấy ra danh sách các đơn hàng.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getList()
    {
        $userId = Auth::user()->id;
        $getListOrderOfThisUser = $this->orderRepo->getAllOrderOfUser($userId,
            10);
        if ($getListOrderOfThisUser->messageCode == 1) {
            $listOrder = $getListOrderOfThisUser->result;

            foreach ($listOrder as $order) {
                $serviceIdList = [];
                foreach (json_decode($order->service_id) as $serviceId){
                    $serviceIdList[] = $serviceId;
                }
                $serviceIdList = array_unique($serviceIdList);

                if(count($serviceIdList) > 1){
                    $order->service = 'Nhiều dịch vụ';
                } else {
                    $getServiceInfo = $this->serviceRepo->getOneById($serviceIdList[0]);
                    $order->service = $getServiceInfo->result;
                }
            }
        } else {
            $listOrder = [];
        }

        return view('Customer.page.Order.list', ['listOrder' => $listOrder]);
    }

    /**
     * Hàm: lấy ra giao diện trang hiển thị form thêm order
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\
     */
    public function getAdd(Request $request)
    {
        $data = $request->data;
        $doGetListService = $this->serviceRepo->getList('status');
        if ($doGetListService->messageCode == 1) {
            $listService = $doGetListService->result;
        } else {
            $listService = [];
        }
        $listChoosedServices = [];
        foreach ($data['service_id'] as $item) {
            $doGetService = $this->serviceRepo->getOneById($item);
            if ($doGetService->messageCode == 1) {
                $listChoosedServices[] = $doGetService->result;
            }
        }

        $userId = Auth::user()->id;
        $doGetUserInfo = $this->userRepo->getOneById($userId);
        if ($doGetUserInfo->messageCode == 1) {
            $userInfo = $doGetUserInfo->result;
        } else {
            $userInfo = [];
        }

        return view('Customer.page.Order.add',
            [
                'listService' => $listService,
                'listChoosedServices' => $listChoosedServices,
                'userInfo' => $userInfo,
                'data' => $data
            ]);
    }

    /**
     * Hàm: Sử lý thêm đơn hàng
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAdd(Request $request)
    {
        /*lưu vào bảng order*/
        /*todo: validate dữ liệu đầu vào:*/
        $orderObj = new OrderObject();
        if (isset($request->email) && $request->email) {
            $orderObj->email = $request->email;
        } else {
            $orderObj->email = Auth::user()->email;
        }
        if (isset($request->username) && $request->name) {
            $orderObj->username = $request->name;
        } else {
            $orderObj->username = Auth::user()->name;
        }
        if (isset($request->address) && $request->address) {
            $orderObj->address = $request->address;
        } else {
            $orderObj->address = Auth::user()->address;
        }
        if (isset($request->phone) && $request->phone) {
            $orderObj->phone = $request->phone;
        } else {
            $orderObj->phone = Auth::user()->phone;
        }
        $orderObj->user_id = Auth::user()->id;

        $orderObj->service_id = $request->serviceId;

        $orderObj->price = $request->price;

        /*Chuyển qua trang thanh toán:*/

        return redirect()->action('Customer\OrderController@getCheckOut',
            ['data' => $orderObj])->with([
            'level' => 'success',
            'message' => 'Bạn hãy chọn thanh toán để hoàn tất việc mua dịch vụ nhé'
        ]);
    }

    /**
     * Hàm hiển thị chi tiết đơn hàng:
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDetail($id)
    {
        /*get this order*/
        $doGetOrderDetail = $this->orderRepo->getOneById($id);
        if ($doGetOrderDetail->messageCode == 1) {
            $thisOrder = $doGetOrderDetail->result;
            $doGetNameOfService
                = $this->serviceRepo->getOneById($thisOrder->service_id);
            if ($doGetNameOfService->messageCode == 1) {
                $thisOrder->service_id = $doGetNameOfService->result->name;
            } else {
                $thisOrder->service_id = $thisOrder->service_id;
            }
        } else {
            $thisOrder = null;
        }

        return view('Customer.page.Order.detail', ['thisOrder' => $thisOrder]);
    }

    /**
     * Hàm: hiển thi view checkout
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCheckOut(Request $request)
    {
        $data = $request->data;
        $listService = [];
        foreach ($data['service_id'] as $id) {
            $doGetListService = $this->serviceRepo->getOneById($id);
            if ($doGetListService->messageCode == 1) {
                $listService[] = $doGetListService->result;
            } else {
                $listService[] = null;
            }
        }
        $totalPrice = array_sum($data['price']);

        return view('Customer.page.Order.checkout',
            [
                'totalPrice' => $totalPrice,
                'listService' => $listService,
                'data' => $data
            ]);
    }

    /**
     * Hàm: sử lý thanh toán
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCheckOut(Request $request)
    {
        /*lưu vào bảng order*/
        /*todo: validate dữ liệu đầu vào:*/
        $orderObj = new OrderObject();
        if (isset($request->email) && $request->email) {
            $orderObj->email = $request->email;
        } else {
            $orderObj->email = Auth::user()->email;
        }
        if (isset($request->name) && $request->name) {
            $orderObj->username = $request->name;
        } else {
            $orderObj->username = Auth::user()->name;
        }
        if (isset($request->address) && $request->address) {
            $orderObj->address = $request->address;
        } else {
            $orderObj->address = Auth::user()->address;
        }
        if (isset($request->phone) && $request->phone) {
            $orderObj->phone = $request->phone;
        } else {
            $orderObj->phone = Auth::user()->phone;
        }
        $orderObj->user_id = Auth::user()->id;

        $orderObj->service_id = $request->serviceId;

        $orderObj->status = 0;

        $orderObj->updated_at = date('y-m-d H:i:s');

        $orderObj->created_at = date('y-m-d H:i:s');

        $orderObj->startDate = date('y-m-d H:i:s');

        $orderObj->endDate = date('y-m-d H:i:s');

        foreach ($orderObj->service_id as $item) {
            $orderObj->service_id = $item;
            $doAddNewOrder = $this->orderRepo->add($orderObj);
        }
        if ($doAddNewOrder->messageCode == 1) {
            /*Gửi mail xác nhận lại cho user*/
            /*nhớ tắt AV trước khi gửi mail*/
            try {

                /*GỬI MAIL THÔNG BÁO:*/
                $dataToAdmin = [
                    'name' => $orderObj->username,
                    'service' => $orderObj->service_id
                ];
                $mail = Mail::send('Email.addServiceSendToAdmin', $dataToAdmin,
                    function ($msg) {
                        $msg->from('notification@mailgun.yournews.romecody.com',
                            'Thông báo: có khách hàng đăng ký dịch vụ mới.');
                        $msg->to('romecoder@gmail.com', 'Thông báo')
                            ->subject('Đơn hàng mới');
                    });
                /*----------------------------------------------------*/
                $dataToUser = [
                    'name' => $orderObj->username,
                    'service' =>$orderObj->service_id
                ];
                $mail = Mail::send('Email.addService', $dataToUser,
                    function ($msg) {
                        $msg->from('notification@mailgun.yournews.romecody.com',
                            'Pveser:Đăng ký dịch vụ thành công');
                        $msg->to(Input::get('email'), 'Pveser')
                            ->subject('Pveser thông báo');
                    });

                return redirect()->action('Customer\ServiceController@getList')
                    ->with([
                        'level' => 'success',
                        'message' => 'Chúc mừng bạn đã thêm thành công dịch vụ mới, yêu cầu của bạn đang được chúng tôi xử lý.'
                    ]);
            } catch (\Exception $exception) {
                return redirect()->action('Customer\ServiceController@getList')
                    ->with([
                        'level' => 'warning',
                        'message' => 'Đăng ký dịch vụ thành công, chúng tôi cần thời gian 01 ngày làm việc để xác nhận đơn hàng này.'
                    ]);
            }
        } else {
            return redirect()->back()
                ->with([
                    'level' => 'warning',
                    'message' => 'Xin lỗi, Bạn chưa thể thêm dịch vụ này!'
                ]);
        }
    }
}
