<?php

namespace App\Http\Controllers\Customer;

use App\Http\Requests\validateServiceAdd;
use App\Models\Service;
use App\Objects\OrderObject;
use App\Objects\User_service;
use App\Repositories\ServiceRepositories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ServiceController extends Controller
{
    private $serviceRepo;

    public function __construct(ServiceRepositories $serviceRepositories)
    {
        $this->serviceRepo = $serviceRepositories;
        date_default_timezone_set("Asia/Bangkok");
    }

    public function getList()
    {
        //get all service, that customer is using:
        $userId = Auth::user()->id;
        $status = 1;
        $limit = 10;
        $doOrderGetList = $this->serviceRepo->getAllSerViceIsUsingByUserId($userId,$status,$limit);

        if ($doOrderGetList->messageCode == 1) {
            $orderList = $doOrderGetList->result;
            $serviceIdList = [];
            foreach ($orderList as $order){
                foreach (json_decode($order->service_id) as $serviceId){
                    $serviceIdList[] = $serviceId;
                }
            }
            $serviceIdList = array_unique($serviceIdList);
        } else {
            $serviceIdList = [];
        }

        $serviceList = [];
        foreach ($serviceIdList as $id){
            $getServiceInfo = $this->serviceRepo->getOneById($id);
            if($getServiceInfo->messageCode == 1){
                $serviceList[] = $getServiceInfo->result;
            }
        }


        //get all service, that customer unused:
        $doUnusedGetList = $this->serviceRepo->getAllServiceNotUsingByUserId($userId,$serviceIdList);
        $UnusedServiceList = [];
        if ($doUnusedGetList->messageCode == 1) {
            $UnusedServiceList = $doUnusedGetList->result;
        }
        return view('Customer.page.service.list',
            compact('serviceList', 'UnusedServiceList'));
    }

    /**
     * User add more service:
     */
    public function getAdd()
    {

    }

    public function postAdd(validateServiceAdd $request)
    {
        $orderObj = new OrderObject();
        $orderObj->service_id = $request->serviceId;
        $orderObj->user_id = Auth::user()->id;
        $orderObj->created_at = date('Y-m-d H:i:s');
        $orderObj->status = 0;
        $orderObj->price = [];
        foreach ($request->serviceId as $key => $value){
            $orderObj->price[] = $key;
        }
        $level = 'success';
        /*Khơi tạo 1 đơn hàng mới*/
        //chuyển hướng sang form điền thông tin:(thêm bên Order)
        return redirect()->action('Customer\OrderController@getAdd',
            ['data' => $orderObj])->with([
            'level' => 'success',
            'message' => 'Bạn hãy điền đầy đủ thông tin nhé!',
        ]);
    }

    public function getDetail($id)
    {
        $getDetail = $this->serviceRepo->getOneById($id);
        if ($getDetail->messageCode == 1) {
            $thisService = $getDetail->result;
        } else {
            $thisService = null;
        }

        return view('Customer.page.service.service-detail',
            ['thisService' => $thisService]);
    }
}
