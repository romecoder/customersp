<?php

namespace App\Http\Controllers\Customer;

use App\Http\Requests\validateAddSupport;
use App\Objects\AdminObject;
use App\Objects\DetailSupportObject;
use App\Objects\ServiceObject;
use App\Objects\SupportObject;
use App\Repositories\AdminRepositories;
use App\Repositories\DetailSupportRepositries;
use App\Repositories\ServiceRepositories;
use App\Repositories\SupportRepositories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SupportController extends Controller
{
    private $supportRepo;
    private $detailSupportRepo;
    private $adminRepo;
    private $serviceRepo;

    public function __construct(
        DetailSupportRepositries $detailSupportRepositries,
        ServiceRepositories $serviceRepositories,
        SupportRepositories $supportRepositories,
        AdminRepositories $adminRepositories
    ) {
        $this->supportRepo = $supportRepositories;
        $this->adminRepo = $adminRepositories;
        $this->serviceRepo = $serviceRepositories;
        $this->detailSupportRepo = $detailSupportRepositries;
    }

    public function getList()
    {
        $limit = 10;
        $supportObj = new SupportObject();
        $supportObj->userId = Auth::user()->id;
        //get all support of this User:
        $doGetListSupport = $this->supportRepo->getAllSupportOfUser($supportObj,$limit);
        if ($doGetListSupport->messageCode == 1) {
            foreach ($doGetListSupport->result as $item) {
                $doGetAdminById = $this->adminRepo->getOneById($item->admin_id);
                if ($doGetAdminById->messageCode == 1) {
                    $admin = $doGetAdminById->result;
                } else {
                    $admin = new AdminObject();
                }
                $item->admin_id = $admin;
            }
            foreach ($doGetListSupport->result as $item) {
                $doGetServiceById
                    = $this->serviceRepo->getOneById($item->service_id);
                if ($doGetServiceById->messageCode == 1) {
                    $service = $doGetServiceById->result;
                } else {
                    $service = new ServiceObject();
                }
                $item->service_id = $service;
            }
            $listSupport = $doGetListSupport->result;
        }

        return view('Customer.page.support.list',
            ['listSupport' => $listSupport]);
    }

    public function getAdd()
    {
        $userId = Auth::user()->id;
        $status = 1;
        $limit = 10;
        $doOrderGetList = $this->serviceRepo->getAllSerViceIsUsingByUserId($userId,$status,$limit);

        if ($doOrderGetList->messageCode == 1) {
            $orderList = $doOrderGetList->result;
            $serviceIdList = [];
            foreach ($orderList as $order){
                foreach (json_decode($order->service_id) as $serviceId){
                    $serviceIdList[] = $serviceId;
                }
            }
            $serviceIdList = array_unique($serviceIdList);
        } else {
            $serviceIdList = [];
        }

        $serviceList = [];
        foreach ($serviceIdList as $id){
            $getServiceInfo = $this->serviceRepo->getOneById($id);
            if($getServiceInfo->messageCode == 1){
                $serviceList[] = $getServiceInfo->result;
            }
        }

        return view('Customer.page.support.add',
            ['listService' => $serviceList]);
    }

    public function postAdd(validateAddSupport $request)
    {
        /*todo:validate input data*/
        $supportObj = new SupportObject();
        $supportObj->name = $request->name;
        $supportObj->serviceId = $request->service;
        $supportObj->createdAt = date('y-m-d H:i:s');
        $supportObj->updatedAt = date('y-m-d H:i:s');
        $supportObj->userId = Auth::user()->id;
        $supportObj->display = 1;
        $doAddSupport = $this->supportRepo->add($supportObj);
        if ($doAddSupport->messageCode == 1) {

            $supportObj->content = 1;
            $detaiSupportObj = new DetailSupportObject();
            $detaiSupportObj->supportId = $doAddSupport->result->id;
            $detaiSupportObj->content = $request->text;
            $detaiSupportObj->author = 0;
            $detaiSupportObj->createAt = date('y-m-d H:i:s');

            $doAddNewRecord = $this->detailSupportRepo->addNewRecord($detaiSupportObj);
            if ($doAddNewRecord->messageCode == 1) {
                return redirect()->action('Customer\SupportController@getDetail',['id'=>$doAddSupport->result->id]);
            } else {
                $message = $doAddNewRecord->message;
                return redirect()->action('Customer\SupportController@getDetail',['id'=>$doAddSupport->result->id])->with([
                    'level' => 'warning',
                    'message' => $message
                ]);
            }
        } else {
            return redirect()->back()
                ->with([
                    'level' => 'warning',
                    'message' => $doAddSupport->message
                ]);
        }
    }

    public function getDelete($id)
    {
        $supportObj = new SupportObject();
        $supportObj->id = $id;
        $supportObj->display = 0;
        $doGetDetail = $this->supportRepo->remove($supportObj);
        if ($doGetDetail->messageCode == 1) {
            return redirect()->action('Customer\SupportController@getList')
                ->with([
                    'level' => 'success',
                    'message' => $doGetDetail->message,
                ]);
        } else {
            return redirect()->back()
                ->with([
                    'level' => 'warning',
                    'message' => $doGetDetail->message,
                ]);
        }
    }

    public function getDetail($id)
    {
        $supportObj = new SupportObject();
        $supportObj->id = $id;
        //lấy ra chi tiết hộ trợ:
        $doGetDetail = $this->supportRepo->getOneById($supportObj);
        if ($doGetDetail->messageCode == 1) {
            /*cập nhật tất cả các tin nhắn của Admin về "Đã xem"*/
            $updateDetailSupportToSeen = $this->detailSupportRepo->updateDetailSupportToSeen($supportObj->id,1);
            $thisSupport = $doGetDetail->result;
            /*todo: validate trường hợp không có thuộc tính admin_id*/
            /*todo: validate trường hợp không có thuộc tính service_id*/
            /*Get Admin info:*/
            $doGetAdmin = $this->adminRepo->getOneById($thisSupport->admin_id);
            if ($doGetAdmin->messageCode == 1) {
                $adminObj = $doGetAdmin->result;
            } else {
                $adminObj = null;
            }

            /*Get Service info:*/
            $doGetSevice
                = $this->serviceRepo->getOneById($thisSupport->service_id);
            if ($doGetSevice->messageCode == 1) {
                $serviceObj = $doGetSevice->result;
            } else {
                $serviceObj = null;
            }
            /*fill dữ liệu, tạo đối tượng mới:*/
            $thisSupport->admin_id = $adminObj;
            $thisSupport->service_id = $serviceObj;
        } else {
            $thisSupport = null;
        }

        /*Lấy ra tất cả các detail_support của support đó:*/
        $getAllDetailSupport = $this->detailSupportRepo->getAllBySupportId($id);

        if ($getAllDetailSupport->messageCode == 1) {
            $listDetailSupport = $getAllDetailSupport->result;
        } else {
            $listDetailSupport = [];
        }

        return view('Customer.page.support.detail',
            [
                'thisSupport' => $thisSupport,
                'adminObj' => $adminObj,
                'listDetailSupport' => $listDetailSupport
            ]);
    }

    public function postAddfeedBack(Request $request)
    {
        /*todo:validate input data*/
        $detaiSupportObj = new DetailSupportObject();
        $detaiSupportObj->supportId = $request->supportId;
        $detaiSupportObj->content = $request->text;
        $detaiSupportObj->author = 0; /*đánh dấu là trả lời của KH*/
        $detaiSupportObj->seen = 0; /*đánh dấu là tin nhắm mới*/
        $detaiSupportObj->createAt = date('Y-m-d H:i:s');

        $doAddNewRecord
            = $this->detailSupportRepo->addNewRecord($detaiSupportObj);
        if ($doAddNewRecord->messageCode == 1) {
            return redirect()->back();
        } else {
            $message = $doAddNewRecord->message;
            return redirect()->back()->with([
                'level' => 'warning',
                'message' => $message
            ]);
        }
    }
}
