<?php

namespace App\Http\Controllers\Customer;

use App\Http\Requests\AdminChangeUserPassword;
use App\Objects\CustomerObject;
use App\Repositories\UserRepositories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use File, Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    private $serviceRepo;
    private $typeRepo;
    private $userRepo;
    private $optionRepo;

    public function __construct(
        UserRepositories $userRepositories
    ) {
        $this->userRepo = $userRepositories;
        date_default_timezone_set("Asia/Bangkok");
    }

    public function index()
    {
        $userId = Auth::user()['id'];
        $customer = $this->userRepo->getOneById($userId);
        if ($customer->messageCode) {
            $thisCustomer = $customer->result;
        } else {
            $thisCustomer = null;
        }

        return view('Customer.page.Profile.profile2',['thisCustomer'=>$thisCustomer]);
    }

    public function edit($id)
    {
        $doGetThisUserById = $this->userRepo->getOneById($id);
        if($doGetThisUserById->messageCode == 1){
            $thisCustomer = $doGetThisUserById->result;
        } else {
            $thisCustomer = null;
        }

        return view('Customer.page.Profile.profile',['thisCustomer'=>$thisCustomer]);
    }

    public function update(Request $request, $id)
    {
        $thisAdmin = new CustomerObject();

        $thisAdmin->id = $id;
        $thisAdmin->address = $request->address;
        $thisAdmin->updated_at = date('y-m-d H:i:s');
        $thisAdmin->email = $request->email;
        $thisAdmin->name = $request->name;
        $thisAdmin->birthday = $request->birthday;
        $thisAdmin->status = 1;
        $thisAdmin->phone = $request->phone;
        $thisAdmin->fullName = $request->fullname;
        $thisAdmin->content = $request->contents;
//        if (isset($request->status) && $request->status) {
//            $thisAdmin->status = 1;
//        }
        if (isset($request->gender) && $request->gender) {
            $thisAdmin->gender = 1;
        }
        if (isset($request->avatar) && $request->avatar) {
            $avatar = imageHandle($request->avatar, 'CS2\images\avatar');
            $thisAdmin->avatar = $avatar;

            $oldAvatar = 'CS2/images/avatar/'.$request->oldavatar;
            if (File::exists($oldAvatar)) {
                File::delete($oldAvatar);
            }
        }
        //Handle to save:
        $updateCustomer = $this->userRepo->update($thisAdmin);
        if ($updateCustomer->messageCode) {
            return redirect()->action('Customer\ProfileController@index')
                ->with([
                    'level' => 'success',
                    'message' => 'Cập nhật thông tin cá nhân thành công'
                ]);
        } else {
            return redirect()->back()->with([
                'level' => 'danger',
                'message' => "Cập nhật thông tin cá nhân không thành công"
            ]);
        }
    }

    public function changePassword(AdminChangeUserPassword $request, $id)
    {
        $customerObj = new CustomerObject();
        $customerObj->id = $id;
        $customerObj->status = 1;
        $customerObj->password = Hash::make($request->pass);
        /*dd($customerObj);*/

        $doChangePassword = $this->userRepo->update($customerObj);
        if ($doChangePassword->messageCode == 1) {
            return redirect()->back()->with([
                'level' => 'success',
                'message' => 'Thay đổi mật khẩu thành công'
            ]);
        } else {
            return redirect()->back()->with([
                'level' => 'warning',
                'message' => 'Chưa chuyển thay đổi được mật khẩu'
            ]);
        }
    }


}
