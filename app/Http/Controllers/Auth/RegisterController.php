<?php

namespace App\Http\Controllers\Auth;


use App\Http\Requests\RegisterRequest;
use App\Objects\E_EmailObject;
use App\Repositories\E_EmailRepositories;
use App\Repositories\UserRepositories;
use App\User;
use Hash, DB, Mail;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Objects\CustomerObject;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $emailRepo;

    public function __construct()
    {
        $this->emailRepo = new E_EmailRepositories();
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     *
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function getRegister()
    {
        return view('General.sign-up');
    }

    public function postRegister(RegisterRequest $request)
    {
        $user = new CustomerObject;
        $user->created_at = date('y-m-d H:m:i');
        $user->email = $request->email;
        $user->name = $request->name;
        $user->fullname = $request->fullname;
        $user->password = Hash::make($request->password);

        $model = new UserRepositories();
        $newUser = $model->add($user);
        if ($newUser->messageCode) {
            /*Lưu Email vào cơ sở dữ liệu: E_email*/
            $email = new E_EmailObject();
            $email->email = $request->email;
            $email->name = $request->name;
            $email->status = 0;
            $email->createdAt = date('y-m-d H:m:i');

            $doAddNewMail = $this->emailRepo->addNewEmail($email);
            if ($doAddNewMail->messageCode == 1) {
                $emailId = $doAddNewMail->result->id;

                //send confirm email:
                $activeCode = str_random(32);

                //save $activeCode to database:
                $sql = DB::table('user')->where('name', $request->name)
                    ->update(['active' => $activeCode]);

                $data = [
                    'name' => $request->name,
                    'code' => $activeCode,
                    'emailId' => $emailId
                ];
                try {
                    $mail = Mail::send('email.register', $data,
                        function ($msg) {
                            $msg->from('notification@mailgun.yournews.romecody.com',
                                'Pveser kích hoạt tài khoản');
                            $msg->to(Input::get('email'), 'New user')
                                ->subject('Kích hoạt tài khoản');
                        });

                    return redirect()->action('Auth\LoginController@getLogin')
                        ->with([
                            'level' => 'success',
                            'message' => 'Hãy kiểm tra lại email của bạn để kích hoạt tài khoản'
                        ]);
                } catch (\Exception $ex) {
                    echo "<script>alert('Xin lỗi , chúng tôi hiện chưa thể gửi mail cho bạn'); window.location='"
                        .url('login')."';</script>";
                }
            } else {

            }
        }

        return redirect()->route('get.register')->with([
            'level' => 'warning',
            'massage' => $newUser->message
        ]);
    }

    public function getActive(Request $request)
    {
        $name = $request->name;
        $active = $request->code;
        $emailId = $request->emailId;
        /*cập nhật cho trạng thái mail trong bảng Email*/
        $email = new E_EmailObject();
        $email->id = $emailId;
        $email->status = 1;
        $doUpdateEmailStatus = $this->emailRepo->updateEmail($email);

        $sql = DB::table('user')->where('name', $name)
            ->where('active', $active)
            ->update(['status' => 1]);
        if ($sql) {
            return redirect()->route('get.login')->with([
                'level' => 'success',
                'message' => 'Tài khoản của bạn hiện đã được kích hoạt'
            ]);
        } else {
            return redirect()->route('get.login')->with([
                'level' => 'warning',
                'message' => 'Tài khoản của bạn hiện chưa được kích hoạt'
            ]);
        }
    }
}
