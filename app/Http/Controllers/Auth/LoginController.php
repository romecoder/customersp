<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function getLogin()
    {
        return view('General.sign-in');
    }

    public function postLogin(LoginRequest $request)
    {
        $login = [
            'name' => $request->name,
            'password' =>$request->password,
            'status' => 1,
            'role' => [1, 2,3],
        ];
        if (Auth::guard('admin')->attempt($login)) {
            return redirect()->route('admin.dashboard')->with([
                'level' => 'success',
                'message' => 'Well come back Admin'
            ]);
        } else {
            $login = [
                'name' => $request->name,
                'password' => $request->password,
                'status' => 1,
            ];
            if (Auth::attempt($login)) {
                return redirect()->route('customer.dashboard')->with([
                    'level' => 'success',
                    'message' => 'Well come back'
                ]);
            }else{
                return redirect()->back()->with([
                    'level' => 'danger',
                    'message' => 'Username or Password do not match, try again!'
                ]);
            }
        }
    }
}
