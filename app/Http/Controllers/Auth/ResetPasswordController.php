<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Hash;
use DB;
use Mail;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function getResetPassword(Request $request)
    {
        $email = $request->email;
        $confirm = $request->code;

        //creating a new password:
        $newUserPassword = str_random(32);
        $newDBPassword = Hash::make($newUserPassword);

        //save new password:
        $sql = DB::table('admin')->where('email', $email)
            ->where('active', $confirm)
            ->update(['password' => $newDBPassword]);
        $data = [
            'newpass' => $newUserPassword
        ];


        if ($sql) {
            try {
                $mail = Mail::send('email.getpassword', $data,
                    function ($msg) use ($email) {
                        $msg->from('thetung.pdca@gmail.com',
                            'Pveser Customer Support System');
                        $msg->to($email)->subject('Mật khẩu');
                    });

                echo "<script>alert('Mật khẩu của bạn đã được đổi, hãy kiểm tra mail đế lấy mật khẩu mới'); window.location='"
                    .url('login')."';</script>";
            } catch (\Exception $ex) {
                echo "<script>alert('Chúng tôi chưa thể gửi email cho bạn.'); window.location='"
                    .url('forgot')."';</script>";
            }

        } else {
            $sql = DB::table('user')->where('email', $email)
                ->where('active', $confirm)
                ->update(['password' => $newDBPassword]);
            if ($sql) {
                try {
                    $mail = Mail::send('email.getpassword', $data,
                        function ($msg) use ($email) {
                            $msg->from('thetung.pdca@gmail.com',
                                'Pveser Customer Support System');
                            $msg->to($email)->subject('Mật khẩu');
                        });

                    echo "<script>alert('Mật khẩu của bạn đã được đổi, hãy kiểm tra mail đế lấy mật khẩu mới'); window.location='"
                        .url('login')."';</script>";
                } catch (\Exception $ex) {
                    echo "<script>alert('Chúng tôi chưa thể gửi email cho bạn'); window.location='"
                        .url('forgot')."';</script>";
                }
            }else{
                echo "<script>alert('Xin lỗi, hiện tại bạn chưa thể đổi mật khẩu'); window.location='"
                    .url('login')."';</script>";
            }
        }

    }
}
