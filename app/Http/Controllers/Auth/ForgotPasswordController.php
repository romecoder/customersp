<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ForgotRequest;
use App\Repositories\AdminRepositories;
use App\Repositories\UserRepositories;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Input;
use Auth,DB,Mail;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function getForgot()
    {
        return view('General.forgot');
    }

    public function postForgot(ForgotRequest $request)
    {
        $email = $request->email;

        $userRepo = new UserRepositories();
        $adminRepo = new AdminRepositories();

        if ($adminRepo->checkEmail($email)->messageCode) {
            //gửi yêu cầu đổi pass cho mail admin:

            $confirmCode = str_random(32);
            $sql = DB::table('admin')->where('email', $email)->update(['active' => $confirmCode]);
            $data = [
                'email' => $request->email,
                'code' => $confirmCode
            ];
            try {
                $mail = Mail::send('Email.forgotPassword', $data,
                    function ($msg) {
                        $msg->from('thetung.pdca@gmail.com',
                            'Pveser Customer  Support System');
                        $msg->to(Input::get('email'), 'admin')
                            ->subject('Confirm password forgeting request');
                    });
                echo "<script>alert('We just sent to you a message, confirm now'); window.location='"
                    .url('forgot')."';</script>";
            } catch (\Exception $ex) {
                echo "<script>alert('Sorry, but you could not send this Email!'); window.location='"
                    .url('forgot')."';</script>";
            }


        } elseif ($userRepo->checkEmail($email)->messageCode) {
            //gửi yêu cầu đổi pass cho mail user khách hàng:

            $confirmCode = str_random(32);
            $sql = DB::table('user')->where('email', $email)->update(['active' => $confirmCode]);
            $data = [
                'email' => $request->email,
                'code' => $confirmCode
            ];
            try {
                $mail = Mail::send('Email.forgotPassword', $data,
                    function ($msg) {
                        $msg->from('thetung.pdca@gmail.com',
                            'Pveser Customer  Support System');
                        $msg->to(Input::get('email'), 'admin')
                            ->subject('Confirm password forgeting request');
                    });
                echo "<script>alert('We just sent to you a message, confirm now'); window.location='"
                    .url('forgot')."';</script>";
            } catch (\Exception $ex) {
                echo "<script>alert('Sorry, but you could not send this Email!'); window.location='"
                    .url('forgot')."';</script>";
            }

        } else {
            return redirect()->back()->with([
                'level' => 'danger',
                'message' => 'Email not exits'
            ]);
        }
    }
}
