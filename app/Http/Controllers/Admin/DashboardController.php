<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Repositories\OrderRepositories;
use App\Repositories\ServiceRepositories;
use App\Repositories\SupportRepositories;
use App\Repositories\UserRepositories;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
    private $userRepo;
    private $supportRepo;
    private $orderRepo;
    private $serviceRepo;
    private $month;

    public function __construct(
        UserRepositories $userRepositories,
        SupportRepositories $supportRepositories,
        ServiceRepositories $serviceRepositories,
        OrderRepositories $orderRepositories
    ) {
        $this->userRepo = $userRepositories;
        $this->serviceRepo = $serviceRepositories;
        $this->orderRepo = $orderRepositories;
        $this->supportRepo = $supportRepositories;
        $now = Carbon::now();
        $this->month = $now->month;
    }

    public function getHome()
    {
        /*Lấy ra số lượng khách hàng đăng ký mới trong tháng.*/
        $doGetNewCustomerInMonth
            = $this->userRepo->getNewCustomerInMonth($this->month);
        if ($doGetNewCustomerInMonth->messageCode == 1) {
            $numberCustomerInMonth = $doGetNewCustomerInMonth->numberOfResult;
        } else {
            $numberCustomerInMonth = 0;
        }

        /*Lấy ra số lượng support khách hàng trong tháng.*/
        $doGetNewSupportInMonth
            = $this->supportRepo->getNewSupportInMonth($this->month);
        if ($doGetNewSupportInMonth->messageCode == 1) {
            $numberSupportInMonth = $doGetNewSupportInMonth->numberOfResult;
        } else {
            $numberSupportInMonth = 0;
        }

        /*Tổng số tiền bán sản phẩm trong 1 tháng (doanh thu)*/
        $listPrice = [];
        $doGetAllOrderInMonth
            = $this->orderRepo->getAllOrderInMonth($this->month);
        if ($doGetAllOrderInMonth->messageCode == 1) {
            foreach ($doGetAllOrderInMonth->result as $order) {
                $listPrice[] = $order->total_price;
            }
        }
        $totalMoney = array_sum($listPrice);

        /*Tổng số tiền bán sản phẩm trong 1 tháng (thực thu)*/
        $listPaid = [];
        $doGetAllOrderInMonth = $this->orderRepo->getAllOrderInMonth($this->month);
        if ($doGetAllOrderInMonth->messageCode == 1) {
            foreach ($doGetAllOrderInMonth->result as $order) {
                $listPaid[] = $order->paid_money;
            }
        }
        $totalPaidMoney = array_sum($listPaid);
        /*Danh sách thông tin các dịch vụ, và doanh số:*/
        $doGetListService = $this->serviceRepo->getList();
        if ($doGetListService->messageCode == 1) {
            /*gét doanh thu của sản phẩm:*/
            $serviceList = $doGetListService->result ;
            foreach ($serviceList as $service) {
                /*Lấy tất cả: order của 1 dịch vụ này trong 1 tháng.*/
                $getDoAllOrderOfService = $this->orderRepo->getAllOrderOfService($service->id,$this->month);
                if($getDoAllOrderOfService->messageCode == 1){
                    $orderListOfThisService = $getDoAllOrderOfService->result;
                    $timeBuy = 0;
                    /*Tinh ra tổng số order của dịch vụ này trong tháng này*/
                    $arrayTotalMoney = [];
                    $arrayPaidMoney = [];
                    $arrayMissingMoney = [];
                   foreach ($orderListOfThisService as $order){
                       $timeBuy += 1;
                       $arrayTotalMoney[] = $order->total_price;
                       $arrayPaidMoney[] = $order->paid_money;
                       $arrayMissingMoney[] = $order->missing_money;
                   }
                   /*$service->totalMoneyInMonth = $service->price * $timeBuy;*/
                   $service->totalMoneyInMonth = array_sum($arrayTotalMoney);
                   $service->totalPaidMoney = array_sum($arrayPaidMoney);
                   $service->totalMissingMoney = array_sum($arrayMissingMoney);
                }else {
                    $service->totalMoneyInMonth = 0;
                    $service->totalPaidMoney = 0;
                    $service->totalMissingMoney = 0;
                }
            }
            $listService = $doGetListService->result;
        } else {
            $listService = [];
        }

        return view('Admin.page.Dashboard.dashboard',
            [
                'numberSupportInMonth' => $numberSupportInMonth,
                'numberCustomerInMonth' => $numberCustomerInMonth,
                'listService' => $listService,
                'totalMoney' => $totalMoney,
                'totalPaidMoney' => $totalPaidMoney
            ]);
    }
    public function fillter(Request $request){
        $startDate = date('Y-m-d',strtotime($request->startDate));
        $endDate = date('Y-m-d',strtotime($request->endDate));

        /*Lấy ra số lượng khách hàng đăng ký mới trong tháng.*/
        $doGetNewCustomerInMonth = $this->userRepo->getNewCustomerInMonth($startDate,$endDate);
        if ($doGetNewCustomerInMonth->messageCode == 1) {
            $numberCustomerInMonth = $doGetNewCustomerInMonth->numberOfResult;
        } else {
            $numberCustomerInMonth = 0;
        }

        /*Lấy ra số lượng support khách hàng trong tháng.*/
        $doGetNewSupportInMonth
            = $this->supportRepo->getNewSupportInMonth($startDate,$endDate);
        if ($doGetNewSupportInMonth->messageCode == 1) {
            $numberSupportInMonth = $doGetNewSupportInMonth->numberOfResult;
        } else {
            $numberSupportInMonth = 0;
        }

        /*Tổng số tiền bán sản phẩm trong 1 tháng (doanh thu)*/
        $listPrice = [];
        $doGetAllOrderInMonth
            = $this->orderRepo->getAllOrderInMonth($startDate,$endDate);
        if ($doGetAllOrderInMonth->messageCode == 1) {
            foreach ($doGetAllOrderInMonth->result as $order) {
                $listPrice[] = $order->total_price;
            }
        }
        $totalMoney = array_sum($listPrice);

        /*Tổng số tiền bán sản phẩm trong 1 tháng (thực thu)*/
        $listPaid = [];
        $doGetAllOrderInMonth = $this->orderRepo->getAllOrderInMonth($startDate,$endDate);
        if ($doGetAllOrderInMonth->messageCode == 1) {
            foreach ($doGetAllOrderInMonth->result as $order) {
                $listPaid[] = $order->paid_money;
            }
        }
        $totalPaidMoney = array_sum($listPaid);
        /*Danh sách thông tin các dịch vụ, và doanh số:*/
        $doGetListService = $this->serviceRepo->getList();
        if ($doGetListService->messageCode == 1) {
            /*gét doanh thu của sản phẩm:*/
            $serviceList = $doGetListService->result ;
            foreach ($serviceList as $service) {
                /*Lấy tất cả: order của 1 dịch vụ này trong 1 tháng.*/
                $getDoAllOrderOfService = $this->orderRepo->getAllOrderOfService($service->id,$startDate,$endDate);
                if($getDoAllOrderOfService->messageCode == 1){
                    $orderListOfThisService = $getDoAllOrderOfService->result;
                    $timeBuy = 0;
                    /*Tinh ra tổng số order của dịch vụ này trong tháng này*/
                    $arrayTotalMoney = [];
                    $arrayPaidMoney = [];
                    $arrayMissingMoney = [];
                    foreach ($orderListOfThisService as $order){
                        $timeBuy += 1;
                        $arrayTotalMoney[] = $order->total_price;
                        $arrayPaidMoney[] = $order->paid_money;
                        $arrayMissingMoney[] = $order->missing_money;
                    }
                    /*$service->totalMoneyInMonth = $service->price * $timeBuy;*/
                    $service->totalMoneyInMonth = array_sum($arrayTotalMoney);
                    $service->totalPaidMoney = array_sum($arrayPaidMoney);
                    $service->totalMissingMoney = array_sum($arrayMissingMoney);
                }else {
                    $service->totalMoneyInMonth = 0;
                    $service->totalPaidMoney = 0;
                    $service->totalMissingMoney = 0;
                }
            }
            $listService = $doGetListService->result;
        } else {
            $listService = [];
        }

        return view('Admin.page.Dashboard.dashboard',
            [
                'numberSupportInMonth' => $numberSupportInMonth,
                'numberCustomerInMonth' => $numberCustomerInMonth,
                'listService' => $listService,
                'totalMoney' => $totalMoney,
                'totalPaidMoney' => $totalPaidMoney,
                'endDate' => $request->endDate,
                'startDate' => $request->startDate
            ]);
    }
}
