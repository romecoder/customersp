<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ValidatePostAdd;
use App\Objects\PostObject;
use App\Repositories\PostRepositories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use File;

class PostController extends Controller
{
    private $postRepo;

    public function __construct(PostRepositories $postRepo)
    {
        $this->postRepo = $postRepo;
    }

    public function getList()
    {
        $limit = 10;
        $doGetList = $this->postRepo->getListPost($limit);
        if ($doGetList->messageCode == 1) {
            $listPost = $doGetList->result;
        } else {
            $listPost = [];
        }

        return view('Admin.page.Post.list', ['listPost' => $listPost]);
    }

    public function getViewAdd()
    {
        return view('Admin.page.Post.add');
    }

    public function getEditView($id)
    {
        $doGetThisPost = $this->postRepo->getOne($id);
        if ($doGetThisPost->messageCode == 1) {
            $thisPost = $doGetThisPost->result;
        } else {
            $thisPost = null;
        }

        return view('Admin.page.Post.edit', ['thisPost' => $thisPost]);
    }

    public function postStore(ValidatePostAdd $request)
    {
        $post = new PostObject();
        $post->authorId = Auth::guard('admin')->user()->id;
        $post->categoryId = $request->category;
        $post->content = $request->contents;
        $post->createdAt = date('Y-m-d H:i:s');
        $post->updatedAt = date('Y-m-d H:i:s');
        $post->tittle = $request->name;
        $post->slug = str_slug($request->name);
        if (isset($request->status) && $request->status) {
            $post->status = 1;
        } else {
            $post->status = 0;
        }
        if (isset($request->image) && $request->image) {
            $thumbnail = imageHandle($request->image, 'CS2\images\post');
            $post->image = $thumbnail;
        }

        $doAddPost = $this->postRepo->addPost($post);
        if ($doAddPost->messageCode == 1) {
            $level = 'success';
            $message = 'Thêm bài viết thành công';
        } else {
            $level = 'warning';
            $message = 'Thêm bài viết không thành công';
        }

        return redirect()->action('Admin\PostController@getList')
            ->with(['level' => $level, 'message' => $message]);
    }

    public function postUpdate(ValidatePostAdd $request, $id)
    {
        $doGetthisPost = $this->postRepo->getOne($id);
        if ($doGetthisPost->messageCode == 1) {
            $thisPost = $doGetthisPost->result;
            $thisPost->id = $id;
            if (isset($request->category) && $request->category) {
                $thisPost->categoryId = $request->category;
            }
            if (isset($request->contents) && $request->contents) {
                $thisPost->content = $request->contents;
            }
            if (isset($request->category) && $request->category) {
                $thisPost->categoryId = $request->category;
            }
            if (isset($request->name) && $request->name) {
                $thisPost->tittle = $request->name;
                $thisPost->slug = str_slug($request->name);
            }
            if (isset($request->status) && $request->status) {
                $thisPost->status = 1;
            } else {
                $thisPost->status = 0;
            }

            $thisPost->updatedAt = date('Y-m-d H:i:s');
            $thisPost->authorId = Auth::guard('admin')->user()->id;

            if (isset($request->image) && $request->image) {
                $thumbnail = imageHandle($request->image, 'CS2\images\post');
                $thisPost->image = $thumbnail;

                $oldthumbnail = 'CS2/images/service/'.$request->oldthumbnail;
                if (File::exists($oldthumbnail)) {
                    File::delete($oldthumbnail);
                }
            }

            $doUpdatePost = $this->postRepo->updatePost($thisPost);
            if ($doUpdatePost->messageCode == 1) {
                $level = 'success';
                $message = 'Cập nhật bài viết thành công';
            } else {
                $level = 'warning';
                $message = 'Cập nhật bài viết không thành công';
            }

            return redirect()->action('Admin\PostController@getList')
                ->with(['level' => $level, 'message' => $message]);

        } else {
            return redirect()->action('Admin\PostController@getList')
                ->with([
                    'level' => 'warning',
                    'message' => 'Không tìm thấy dữ liệu'
                ]);
        }
    }

    public function deletePost($id)
    {
        $doDeletePost = $this->postRepo->deletePost($id);
        if ($doDeletePost->messageCode == 1) {
            return redirect()->action('Admin\PostController@getList')
                ->with(['level' => 'success', 'message' => 'Xóa thành công']);
        } else {
            return redirect()->action('Admin\PostController@getList')->with([
                'level' => 'warning',
                'message' => 'Xóa không thành công'
            ]);
        }
    }
}
