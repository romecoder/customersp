<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ServiceEditRequest;
use App\Http\Requests\ServiceRequest;
use App\Objects\ServiceObject;
use App\Objects\ServiceTypeObject;
use App\Repositories\ServiceRepositories;
use App\Repositories\TypeRepositories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use File;

class ServiceController extends Controller
{
    private $serviceRepo;
    private $typeServiceRepo;

    public function __construct(
        ServiceRepositories $serviceRepositories,
        TypeRepositories $typeRepositories
    ) {
        $this->serviceRepo = $serviceRepositories;
        $this->typeServiceRepo = $typeRepositories;
        date_default_timezone_set("Asia/Bangkok");
    }

    public function getList()
    {
        /*Lấy ra tất cả service để quản lý*/
        $repo = new ServiceRepositories();
        $listServices = $repo->getList(null, 10);
        if ($listServices->messageCode == 1) {
            $listService = $listServices->result;
            foreach ($listService as $service) {
                $typeServiceObj = new ServiceTypeObject();
                $typeServiceObj->id = $service->type_id;
                $doGetServiceName
                    = $this->typeServiceRepo->findById($typeServiceObj);
                if ($doGetServiceName->messageCode == 1) {
                    $service->type_id = $doGetServiceName->result->name;
                } else {
                    $service->type_id = null;
                }
            }

            return view('Admin.page.Service.list', compact('listService'));
        } else {
            return redirect()->route('admin.dashboard')->with([
                'level' => 'warning',
                'message' => 'Chưa có Dịch vụ nào hãy thêm mới.'
            ]);
        }
    }

    public function getAdd()
    {
        /*Lấy danh sách các "loại dịch vụ"*/
        $doGetListType = $this->typeServiceRepo->listServiceType();
        if ($doGetListType->messageCode == 1) {
            $listType = $doGetListType->result;
        } else {
            $listType = [];
        }

        return view('Admin.page.Service.add', ['listType' => $listType]);
    }

    public function getEdit($id)
    {
        $repo = new ServiceRepositories();

        $doGetListType = $this->typeServiceRepo->listServiceType();
        if ($doGetListType->messageCode == 1) {
            $listType = $doGetListType->result;
        } else {
            $listType = [];
        }

        $Service = $repo->getOneById($id);
        if ($Service->messageCode) {
            $thisService = $Service->result;

            return view('Admin.page.Service.update', compact('thisService','listType'));
        } else {
            return redirect()->route('get.member.list')->with([
                'level' => 'danger',
                'message' => $Service->message
            ]);
        }
    }

    public function postAdd(ServiceRequest $request)
    {
        $newService = new ServiceObject();

        $newService->name = $request->name;
        $newService->typeId = $request->type;
        $newService->price = $request->price;
        $newService->created_at = date('Y-m-d H:i:s');
        $newService->updated_at = date('Y-m-d H:i:s');
        $newService->content = $request->contents;
        if (isset($request->status) && $request->status) {
            $newService->status = 1;
        } else {
            $newService->status = 0;
        }

        if (isset($request->thumbnail) && $request->thumbnail) {
            $thumbnail = imageHandle($request->thumbnail, 'CS2\images\service');
            $newService->thumbnail = $thumbnail;
        }
        //Handle to save:
        $repo = new ServiceRepositories();
        $addService = $repo->add($newService);

        if ($addService->messageCode) {
            return redirect()->route('get.service.list')->with([
                'level' => 'success',
                'message' => $addService->message
            ]);
        } else {
            return redirect()->back()->with([
                'level' => 'danger',
                'message' => $addService->message
            ]);
        }

    }

    public function postEdit(ServiceEditRequest $request, $id)
    {
        $thisService = new ServiceObject();

        $thisService->typeId = $request->type;
        $thisService->name = $request->name;
        $thisService->price = $request->price;
        $thisService->id = $id;
        $thisService->updated_at = date('Y-m-d H:i:s');
        $thisService->content = $request->contents;
        if (isset($request->status) && $request->status) {
            $thisService->status = 1;
        } else {
            $thisService->status = 0;
        }

        if (isset($request->thumbnail) && $request->thumbnail) {
            $thumbnail = imageHandle($request->thumbnail, 'CS2\images\service');
            $thisService->thumbnail = $thumbnail;

            $oldImage = 'CS2/images/post/'.$request->oldImage;
            if (File::exists($oldImage)) {
                File::delete($oldImage);
            }
        }
        //Handle to save:
        $repo = new ServiceRepositories();
        $updateService = $repo->update($thisService);
        if ($updateService->messageCode) {
            return redirect()->route('get.service.list')->with([
                'level' => 'success',
                'message' => $updateService->message
            ]);
        } else {
            return redirect()->action('Admin\ServiceController@getList')->with([
                'level' => 'danger',
                'message' => $updateService->message
            ]);
        }
    }

    public function getDetail($id)
    {
        $Service = $this->serviceRepo->getOneById($id);
        if ($Service->messageCode) {
            $thisService = $Service->result;

            return view('Admin.page.Service.detail', compact('thisService'));
        } else {
            return redirect()->route('get.member.list')->with([
                'level' => 'warning',
                'message' => 'Không tìm thấy dịch vụ nào'
            ]);
        }
    }

    public function getDelete($id)
    {
        $thisService = new ServiceObject();
        $thisService->id = $id;
        $repo = new ServiceRepositories();

        //delete image:
        $getService = $repo->getOneById($id);
        $oldAvatar = 'CS2/images/service/'.$getService->result->thumbnail;
        if (File::exists($oldAvatar)) {
            File::delete($oldAvatar);
        }

        $deleteService = $repo->delete($thisService);
        if ($deleteService->messageCode) {
            return redirect()->route('get.service.list')->with([
                'level' => 'success',
                'message' => $deleteService->message
            ]);
        } else {
            return redirect()->back()->with([
                'level' => 'danger',
                'message' => $deleteService->message
            ]);
        }
    }

    public function changeStatus($id)
    {
        $serviceObj = new ServiceObject();
        $serviceObj->id = $id;

        $serviceRepo = new ServiceRepositories();

        $doChangeStatus = $serviceRepo->changeStatus($serviceObj);
        if ($doChangeStatus->messageCode == 1) {
            return redirect()->back()->with([
                'level' => 'success',
                'message' => 'chuyển trạng thái thành công'
            ]);
        } else {
            return redirect()->back()->with([
                'level' => 'warning',
                'message' => 'Chưa chuyển đc trạng thái'
            ]);
        }
    }
}
