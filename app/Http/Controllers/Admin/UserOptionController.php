<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ValidateUserOption;
use App\Objects\OptionObject;
use App\Repositories\OptionRepositories;
use App\Repositories\UserRepositories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserOptionController extends Controller
{
    private $optionRepo;
    private $userRepo;

    public function __construct(
        OptionRepositories $optionRepositories,
        UserRepositories $userRepositories
    ) {
        $this->optionRepo = $optionRepositories;
        $this->userRepo = $userRepositories;
    }

    public function index()
    {
    }

    public function insert()
    {
        $doGetListUser = $this->userRepo->getList();
        if ($doGetListUser->messageCode == 1) {
            $listUser = $doGetListUser->result;
        } else {
            $listUser = [];
        }

        $doGetListUserOption = $this->optionRepo->getList(10);
        if ($doGetListUserOption->messageCode == 1) {
            $listUserOption = $doGetListUserOption->result;
            foreach ($listUserOption as $userOption){
                $getUserInfo = $this->userRepo->getOneById($userOption->element_id);
                if($getUserInfo->messageCode == 1){
                    $userOption->user = $getUserInfo->result;
                }
            }
        } else {
            $listUserOption = [];
        }

        return view('Admin.page.UserOption.add', [
            'listUser' => $listUser,
            'listUserOption' => $listUserOption
        ]);
    }

    public function edit($id)
    {
        $doGetListUser = $this->userRepo->getList();
        if ($doGetListUser->messageCode == 1) {
            $listUser = $doGetListUser->result;
        } else {
            $listUser = [];
        }

        $doGetThisOption = $this->optionRepo->getOne($id);
        if ($doGetThisOption->messageCode == 1) {
            $thisUserOption = $doGetThisOption->result;
        } else {
            $thisUserOption = null;
        }

        $doGetListUserOption = $this->optionRepo->getList(10);
        if ($doGetListUserOption->messageCode == 1) {
            $listUserOption = $doGetListUserOption->result;
            foreach ($listUserOption as $userOption){
                $getUserInfo = $this->userRepo->getOneById($userOption->id);
                if($getUserInfo->messageCode == 1){
                    $userOption->user = $getUserInfo->result;
                }
            }
        } else {
            $listUserOption = [];
        }
        return view('Admin.page.UserOption.update',
            [
                'thisUserOption' => $thisUserOption,
                'listUser' => $listUser,
                'listUserOption' => $listUserOption
            ]);
    }

    public function store(ValidateUserOption $request)
    {
        $optionObj = new OptionObject();
        $optionObj->key = $request->key;
        $optionObj->value = $request->value;
        $optionObj->type = 'user';
        $optionObj->elementId = $request->user;
        $optionObj->createdAt = date('Y-m-d H:i:s');
        $doAddOption = $this->optionRepo->store($optionObj);
        if ($doAddOption->messageCode == 1) {
            return redirect()->action('Admin\UserOptionController@insert')
                ->with([
                    'level' => 'success',
                    'message' => 'Thêm trường mới thành công'
                ]);
        } else {
            return redirect()->back()
                ->with([
                    'level' => 'warning',
                    'message' => $doAddOption->message
                ]);
        }
    }

    public function update(ValidateUserOption $request, $id)
    {
        $thisOptionObj = new OptionObject();
        if ($request->key) {
            $thisOptionObj->key = $request->key;
        }
        $thisOptionObj->id = $id;
        if ($request->value) {
            $thisOptionObj->value = $request->value;
        }
        if ($request->user) {
            $thisOptionObj->elementId = $request->user;
        }
        $thisOptionObj->id = $id;
        $thisOptionObj->type = 'user';

        $doUpdate = $this->optionRepo->update($thisOptionObj);
        if ($doUpdate->messageCode == 1) {
            return redirect()->action('Admin\UserOptionController@insert')

                ->with([
                    'level' => 'success',
                    'message' => 'Cập nhật thành công'
                ]);
        } else {
            return redirect()->back()
                ->with([
                    'level' => 'warning',
                    'message' => $doUpdate->message
                ]);
        }
    }

    public function delete($id)
    {
        $doGetThisOption = $this->optionRepo->getOne($id);
        if ($doGetThisOption->messageCode == 1) {
           $deleteThisOption = $this->optionRepo->delete($id);
            if($deleteThisOption->messageCode == 1){
                $level = 'success';
                $message = 'Đã xóa tùy chọn';
            } else {
                $level = 'danger';
                $message = 'Chưa xóa tùy chọn';
            }
        } else {
            $level = 'danger';
            $message = 'Dữ liệu không tồn tại';
        }

        return redirect()->action('Admin\UserOptionController@insert')
            ->with([
                'level' => $level,
                'message' => $message
            ]);
    }


}
