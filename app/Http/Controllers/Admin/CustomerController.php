<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AdminChangeUserPassword;
use App\Http\Requests\CustomerEditRequest;
use App\Http\Requests\CustomerRequest;
use App\Mail\ConfirmCreateAccountSuccess;
use App\Objects\CustomerObject;
use App\Objects\ResultObject;
use App\Repositories\OptionRepositories;
use App\Repositories\ServiceRepositories;
use App\Repositories\TypeRepositories;
use App\Repositories\UserRepositories;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use File;
use Illuminate\Support\Facades\Mail;

class CustomerController extends Controller
{
    private $serviceRepo;
    private $typeRepo;
    private $userRepo;
    private $optionRepo;

    public function __construct(
        ServiceRepositories $serviceRepositories,
        OptionRepositories $optionRepositories,
        TypeRepositories $typeRepositories,
        UserRepositories $userRepositories
    ) {
        $this->serviceRepo = $serviceRepositories;
        $this->typeRepo = $typeRepositories;
        $this->userRepo = $userRepositories;
        $this->optionRepo = $optionRepositories;
        date_default_timezone_set("Asia/Bangkok");

    }

    public function getList()
    {
        $repo = new UserRepositories();
        $listCustomers = $repo->getList(10);
        if ($listCustomers->messageCode) {
            $listCustomer = $listCustomers->result;

            return view('Admin.page.Customer.list', compact('listCustomer'));
        } else {
            return redirect()->route('admin.dashboard')->with([
                'level' => 'warning',
                'message' => 'không có khách hàng nào'
            ]);
        }
    }

    public function getAdd()
    {
        return view('Admin.page.Customer.add');
    }

    public function getEdit($id)
    {
        /*lay tat cả optionUser của user này:*/
        $type = 'user';
        $elementId = $id;
        $doGetUserOption = $this->optionRepo->getListByKeyAndId($type,
            $elementId);
        if ($doGetUserOption->messageCode == 1) {
            $listUserOption = $doGetUserOption->result;
        } else {
            $listUserOption = [];
        }
        /*--------------------------------------------------------------------*/
        $repo = new UserRepositories();
        $customer = $repo->getOneById($id);
        if ($customer->messageCode) {
            $thisCustomer = $customer->result;
        } else {
            $thisCustomer = null;
        }

        return view('Admin.page.Customer.update', compact('thisCustomer','listUserOption'));
    }

    public function postAdd(CustomerRequest $request)
    {
        $newCustomer = new CustomerObject();

        $newCustomer->address = $request->address;
        if (isset($request->avatar) && $request->avatar) {
            $avatar = imageHandle($request->avatar, 'CS2\images\avatar');
            $newCustomer->avatar = $avatar;
        }
        $newCustomer->created_at = date('y-m-d H:i:s');
        $newCustomer->email = $request->email;
        $newCustomer->name = $request->name;
        $newCustomer->birthday = $request->birthday;
        $newCustomer->phone = $request->phone;
        $newCustomer->content = $request->contents;
        $newCustomer->fullName = $request->fullname;
        if (isset($request->status) && $request->status) {
            $newCustomer->status = $request->status;
        }
        if (isset($request->gender) && $request->gender) {
            $newCustomer->gender = $request->gender;
        }
        $newCustomer->password = Hash::make($request->password);

        //Handle to save:
        $repo = new UserRepositories();
        $addCustomer = $repo->add($newCustomer);
        if ($addCustomer->messageCode) {
            /*if save success: send Email*/
            try {
                Mail::to($request->email)
                    ->send(new ConfirmCreateAccountSuccess($request->name));
            } catch (\Exception $exception) {
                return redirect()->back()
                    ->with([
                        'level' => 'warning',
                        'message' => 'không gửi được mail'
                    ]);
            }

            return redirect()->route('get.customer.list')->with([
                'level' => 'success',
                'message' => $addCustomer->message
            ]);
        } else {
            return redirect()->back()->with([
                'level' => 'danger',
                'message' => $addCustomer->message
            ]);
        }
    }

    public function postEdit(CustomerEditRequest $request, $id)
    {
        $thisAdmin = new CustomerObject();

        $thisAdmin->id = $id;
        $thisAdmin->address = $request->address;
        $thisAdmin->updated_at = date('y-m-d H:i:s');
        $thisAdmin->email = $request->email;
        $thisAdmin->name = $request->name;
        $thisAdmin->phone = $request->phone;
        $thisAdmin->content = $request->contents;
        $thisAdmin->fullName= $request->fullname;
        $thisAdmin->birthday = $request->birthday;
        if (isset($request->status) && $request->status) {
            $thisAdmin->status = 1;
        }
        if (isset($request->gender) && $request->gender) {
            $thisAdmin->gender = 1;
        }
        if (isset($request->avatar) && $request->avatar) {
            $avatar = imageHandle($request->avatar, 'CS2\images\avatar');
            $thisAdmin->avatar = $avatar;

            $oldAvatar = 'CS2/images/avatar/'.$request->oldavatar;
            if (File::exists($oldAvatar)) {
                File::delete($oldAvatar);
            }
        }
        if (isset($request->option)  && sizeof($request->option)){
            foreach ($request->option as $key => $value){
                $doUpdateUserOptionByKey = $this->optionRepo->updateByKey($key,$value);
            }
        }
        //Handle to save:
        $repo = new UserRepositories();
        $updateCustomer = $repo->update($thisAdmin);
        if ($updateCustomer->messageCode) {
            return redirect()->route('get.customer.list')->with([
                'level' => 'success',
                'message' => $updateCustomer->message
            ]);
        } else {
            return redirect()->back()->with([
                'level' => 'danger',
                'message' => $updateCustomer->message
            ]);
        }
    }

    public function getDetail($id)
    {
        $repo = new UserRepositories();

        /*Lấy thông tin Thêm của user này*/
        $type = 'user';
        $elementId = $id;
        $doGetUserOption = $this->optionRepo->getListByKeyAndId($type,
            $elementId);
        if ($doGetUserOption->messageCode == 1) {
            $listUserOption = $doGetUserOption->result;
        } else {
            $listUserOption = [];
        }

        /*Lấy thông tin cơ bản của user này*/
        $customer = $repo->getOneById($id);
        if ($customer->messageCode) {
            $thisCustomer = $customer->result;
            /*Xóa ngôi sao trắng*/
            if($thisCustomer->seen == 0){
                $newCustomer = new CustomerObject();
                $newCustomer->seen = 1;
                $newCustomer->id = $id;
                $doUpdateCustomer = $this->userRepo->update($newCustomer);
            }
        } else {
            $thisCustomer = new CustomerObject();
        }

        /*Lấy danh sách các dịch vụ đang sử dụng của user này*/
        $doGetServiceUsedByCustomer
            = $this->serviceRepo->getAllSerViceIsUsingByUserId($thisCustomer->id,
            1);
        if ($doGetServiceUsedByCustomer->messageCode == 1) {
            $listService = $doGetServiceUsedByCustomer->result;
        } else {
            $listService = [];
        }
        /*Lấy danh sách các dịch vụ chưa được kích hoạt của user này*/
        $doGetUnactiveServiceUsedByCustomer
            = $this->serviceRepo->getAllSerViceIsUsingByUserId($thisCustomer->id,
            0);
        if ($doGetUnactiveServiceUsedByCustomer->messageCode == 1) {
            $listUnactive = $doGetUnactiveServiceUsedByCustomer->result;
        } else {
            $listUnactive = [];
        }

        return view('Admin.page.Customer.profile',
            compact('thisCustomer', 'listService', 'listUnactive','listUserOption'));
    }

    public function getDelete($id)
    {
        $thisCustomer = new CustomerObject();
        $thisCustomer->id = $id;
        $repo = new UserRepositories();

        //delete image:
        $getCustomer = $repo->getOneById($id);
        $oldAvatar = 'CS2/images/avatar/'.$getCustomer->result->avatar;
        if (File::exists($oldAvatar)) {
            File::delete($oldAvatar);
        }

        $deleteCustomer = $repo->delete($thisCustomer);
        if ($deleteCustomer->messageCode) {
            return redirect()->route('get.customer.list')->with([
                'level' => 'success',
                'message' => $deleteCustomer->message
            ]);
        } else {
            return redirect()->back()->with([
                'level' => 'danger',
                'message' => $deleteCustomer->message
            ]);
        }
    }

    public function changeStatus($id)
    {
        $customerObj = new CustomerObject();
        $customerObj->id = $id;

        $customerRepo = new UserRepositories();

        $doChangeStatus = $customerRepo->changeStatus($customerObj);
        if ($doChangeStatus->messageCode == 1) {
            return redirect()->back()->with([
                'level' => 'success',
                'message' => 'chuyển trạng thái thành công'
            ]);
        } else {
            return redirect()->back()->with([
                'level' => 'warning',
                'message' => 'Chưa chuyển đc trạng thái'
            ]);
        }
    }

    public function adminChangeUserPassword(
        AdminChangeUserPassword $request,
        $id
    ) {
        $customerObj = new CustomerObject();
        $customerObj->id = $id;
        $customerObj->status = 1;
        $customerObj->password = Hash::make($request->pass);

        $customerRepo = new UserRepositories();

        $doChangePassword = $customerRepo->update($customerObj);
        if ($doChangePassword->messageCode == 1) {
            return redirect()->back()->with([
                'level' => 'success',
                'message' => 'Thay đổi mật khẩu thành công'
            ]);
        } else {
            return redirect()->back()->with([
                'level' => 'warning',
                'message' => 'Chưa chuyển thay đổi được mật khẩu'
            ]);
        }
    }
}
