<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreTypeRequest;
use App\Objects\TypeObject;
use App\Repositories\TypeRepositories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TypeController extends Controller
{
    private $typeRepo;

    public function __construct(TypeRepositories $typeRepositories)
    {
        $this->typeRepo = $typeRepositories;
        date_default_timezone_set("Asia/Bangkok");

    }

    public function listType()
    {
        $doGetList = $this->typeRepo->listServiceType(10);
        if ($doGetList->messageCode == 1) {
            $listType = $doGetList->result;
        } else {
            $listType = [];
        }

        return view('Admin.page.TypeService.list', ['listType' => $listType]);
    }


    public function addType()
    {
        return view('Admin.page.TypeService.add');
    }


    public function storeType(StoreTypeRequest  $request)
    {
        $typeObj = new TypeObject();
        $typeObj->name = $request->name;
        $typeObj->status = $request->status;
        $typeObj->createAt = date('Y-m-d H:i:s');
        $typeObj->updatedAt = date('Y-m-d H:i:s');
        $typeObj->time = $request->time;


        $doAddType = $this->typeRepo->addServiceType($typeObj);
        if ($doAddType->messageCode == 1) {
            return redirect()->action('Admin\TypeController@listType')->with([
                'level' => 'success',
                'message' => 'Thêm mới loại dịch vụ thành công'
            ]);
        } else {
            return redirect()->back()->with([
                'level' => 'warning',
                'message' => 'Thêm mới loại dịch vụ không thành công'
            ]);
        }
    }


    public function editType($id)
    {
        $typeObj = new TypeObject();
        $typeObj->id = $id;

        $doGetThisType = $this->typeRepo->findById($typeObj);
        if($doGetThisType->messageCode == 1 ){
            $thisType = $doGetThisType->result;
        } else {
            $thisType = null;
        }
        $doGetList = $this->typeRepo->listServiceType(10);
        if ($doGetList->messageCode == 1) {
            $listType = $doGetList->result;
        } else {
            $listType = [];
        }

        return view('Admin.page.TypeService.update',['thisType'=>$thisType,'listType'=>$listType]);
    }


    public function updateType(StoreTypeRequest $request, $id)
    {
        $typeObj = new TypeObject();
        $typeObj->name = $request->name;
        $typeObj->time = $request->time;
        $typeObj->id = $id;
        $typeObj->status = $request->status;
        $typeObj->updatedAt = date('Y-m-d H:i:s');
        $doUpdateType = $this->typeRepo->updateServiceType($typeObj);
        if ($doUpdateType->messageCode == 1) {
            return redirect()->action('Admin\TypeController@listType')->with([
                'level' => 'success',
                'message' => 'Sửa loại dịch vụ thành công'
            ]);
        } else {
            return redirect()->back()->with([
                'level' => 'warning',
                'message' => 'sửa loại dịch vụ không thành công'
            ]);
        }
    }


    public function deleteType($id)
    {
        $typeObj = new TypeObject();
        $typeObj->id = $id;

        $doGetThisType = $this->typeRepo->findById($typeObj);
        if($doGetThisType->messageCode == 1 ){
           $doDelete = $this->typeRepo->deleteServiceType($typeObj);
            if($doDelete->messageCode == 1){
                $level = 'success';
                $message = 'Xóa thành công loại dịch vụ';
            } else {
                $level = 'warning';
                $message = 'Xóa không thành công loại dịch vụ';
            }
        } else {
            $level = 'warning';
            $message = 'Không tìm thấy dữ liệu';
        }

        return redirect()->action('Admin\TypeController@listType')->with([
            'level' => $level,
            'message' => $message
        ]);
    }
}
