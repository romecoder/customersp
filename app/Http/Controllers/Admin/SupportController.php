<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AddSupportRequest;
use App\Objects\DetailSupportObject;
use App\Objects\SupportObject;
use App\Repositories\AdminRepositories;
use App\Repositories\DetailSupportRepositries;
use App\Repositories\ServiceRepositories;
use App\Repositories\SupportRepositories;
use App\Repositories\UserRepositories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SupportController extends Controller
{
    private $supportRepo;
    private $serviceRepo;
    private $detailSupportRepo;
    private $adminRepo;
    private $userRepo;

    public function __construct(
        DetailSupportRepositries $detailSupportRepositries,
        SupportRepositories $supportRepositories,
        ServiceRepositories $serviceRepositories,
        AdminRepositories $adminRepositories,
        UserRepositories $userRepositories
    ) {
        $this->supportRepo = $supportRepositories;
        $this->serviceRepo = $serviceRepositories;
        $this->adminRepo = $adminRepositories;
        $this->userRepo = $userRepositories;
        $this->detailSupportRepo = $detailSupportRepositries;
        date_default_timezone_set("Asia/Bangkok");

    }

    public function getList()
    {
        $doGetSupportList = $this->supportRepo->getList(10);
        if ($doGetSupportList->messageCode == 1) {
            $supportList = $doGetSupportList->result;

            return view('Admin.page.Support.list', compact('supportList'));
        } else {
            return redirect()->back()->with([
                'level' => 'warning',
                'message' => 'Chưa có Cuộc hỗ trợ khách hàng nào'
            ]);
        }
    }

    public function getAdd()
    {
        //get list admin:
        $doGetAdminList = $this->adminRepo->getList();

        if($doGetAdminList->messageCode == 1){
            $listAdmin = $doGetAdminList->result;
        } else {
            $listAdmin = [];
        }
        //get list service:
        $doGetUserList = $this->userRepo->getList();
        if($doGetUserList->messageCode == 1){
            $listUser = $doGetUserList->result;
        } else {
            $listUser = [];
        }
        //get list user:
        $doGetServiceList = $this->serviceRepo->getList();
        if($doGetServiceList->messageCode == 1){
            $listService = $doGetServiceList->result;
        } else {
            $listService = [];
        }

        return view('Admin.page.Support.add', compact('listAdmin', 'listUser', 'listService'));

    }

    public function getEdit($id)
    {
        //get list admin:
        $doGetAdminList = $this->adminRepo->getList();
        if($doGetAdminList->messageCode = 1){
            $listAdmin = $doGetAdminList->result;
        } else {
            $listAdmin = [];
        }

        //get list service:
        $doGetUserList = $this->userRepo->getList();
        if($doGetUserList->messageCode == 1){
            $listUser = $doGetUserList->result;
        } else {
            $listUser = [];
        }
        //get list user:
        $doGetServiceList = $this->serviceRepo->getList();
        if($doGetServiceList->messageCode == 1){
            $listService = $doGetServiceList->result;
        } else {
            $listService = [];
        }

        //get this support:
        $support = new SupportObject();
        $support->id = $id;
        $doGetThisSupport = $this->supportRepo->getOneById($support);
        if($doGetThisSupport->messageCode == 1){
            $thisSupport = $doGetThisSupport->result;
        } else {
            $thisSupport = new SupportObject();
        }

        return view('Admin.page.Support.update',
            compact('listAdmin', 'listUser', 'listService', 'thisSupport'));



    }

    public function postAdd(AddSupportRequest $request)
    {
        $support = new SupportObject();

        $support->name = $request->name;
        $support->userId = $request->user;
        $support->adminId = $request->admin;
        $support->serviceId = $request->service;
        $support->content = $request->contents;
        $support->createdAt = $request->date ? $request->date
            : date('Y-m-d H:i:s');
        $support->status = $request->status;

        $doAddSupport = $this->supportRepo->add($support);
        if ($doAddSupport->messageCode == 1) {
            $level = 'success';
        } else {
            $level = 'warning';
        }

        return redirect()->action('Admin\SupportController@getList')->with([
            'level' => $level,
            'message' => $doAddSupport->message
        ]);
    }

    public function postEdit(AddSupportRequest $request, $id)
    {
        $support = new SupportObject();

        $support->id = $id;
        $support->name = $request->name;
        $support->userId = $request->user;
        $support->adminId = $request->admin;
        $support->serviceId = $request->service;
        $support->content = $request->contents;
        $support->updatedAt = $request->date ? $request->date
            : date('Y-m-d H:i:s');
        $support->status = $request->status;
        $doUpdateSupport = $this->supportRepo->update($support);
        if ($doUpdateSupport->messageCode == 1) {
            $level = 'success';
        } else {
            $level = 'warning';
        }

        return redirect()->action('Admin\SupportController@getList')->with([
            'level' => $level,
            'message' => $doUpdateSupport->message
        ]);


    }

    public function changeStatus($id)
    {
        $thisSupport = new SupportObject();
        $thisSupport->id = $id;

        $doChangeStt = $this->supportRepo->changeStatus($thisSupport);
        if ($doChangeStt->messageCode) {
            $level = 'success';
        } else {
            $level = 'danger';
        }

        return redirect()->action('Admin\SupportController@getList')->with([
            'level' => $level,
            'message' => $doChangeStt->message
        ]);
    }

    public function getDelete($id)
    {
        $thisSupport = new SupportObject();
        $thisSupport->id = $id;

        $doDeleteSupport = $this->supportRepo->delete($thisSupport);
        if ($doDeleteSupport->messageCode) {
            $level = 'success';
        } else {
            $level = 'danger';
        }

        return redirect()->action('Admin\SupportController@getList')->with([
            'level' => $level,
            'message' => $doDeleteSupport->message
        ]);
    }
    public function getDetail($id)
    {
        $supportObj = new SupportObject();
        $supportObj->id = $id;
        //lấy ra chi tiết hộ trợ:
        $doGetDetail = $this->supportRepo->getOneById($supportObj);
        if ($doGetDetail->messageCode == 1) {
            /*cập nhật tất cả các tin nhắn của Khách hàng về "Đã xem"*/
            $updateDetailSupportToSeen = $this->detailSupportRepo->updateDetailSupportToSeen($supportObj->id,0);

            /*Lấy thêm thông tin của Admin, và của dịch vụ*/
            $thisSupport = $doGetDetail->result;
            /*todo: validate trường hợp không có thuộc tính admin_id*/
            /*todo: validate trường hợp không có thuộc tính service_id*/
            /*Get Admin info:*/
            $doGetAdmin = $this->adminRepo->getOneById($thisSupport->admin_id);
            if ($doGetAdmin->messageCode == 1) {
                $adminObj = $doGetAdmin->result;
            } else {
                $adminObj = null;
            }

            /*Get Service info:*/
            $doGetSevice
                = $this->serviceRepo->getOneById($thisSupport->service_id);
            if ($doGetSevice->messageCode == 1) {
                $serviceObj = $doGetSevice->result;
            } else {
                $serviceObj = null;
            }

            /*Lấy thông tin về User:*/
            $doGetUserInfo
                = $this->userRepo->getOneById($thisSupport->user_id);
            if ($doGetUserInfo->messageCode == 1) {
                $userObj = $doGetUserInfo->result;
            } else {
                $userObj = null;
            }
            /*fill dữ liệu, tạo đối tượng mới:*/
            $thisSupport->admin_id = $adminObj;
            $thisSupport->service_id = $serviceObj;
            $thisSupport->user_id = $userObj;
        } else {
            $thisSupport = null;
        }

        /*Lấy ra tất cả các detail_support của support đó:*/
        $getAllDetailSupport = $this->detailSupportRepo->getAllBySupportId($id);

        if ($getAllDetailSupport->messageCode == 1) {
            $listDetailSupport = $getAllDetailSupport->result;
        } else {
            $listDetailSupport = [];
        }

        return view('Admin.page.Support.detail',
            [
                'thisSupport' => $thisSupport,
                'listDetailSupport' => $listDetailSupport
            ]);
    }

    public function postAddfeedBack(Request $request)
    {
        /*todo:validate input data*/
        $detaiSupportObj = new DetailSupportObject();
        $detaiSupportObj->supportId = $request->supportId;
        $detaiSupportObj->content = $request->text;
        $detaiSupportObj->author = 1;
        $detaiSupportObj->seen = 0;
        $detaiSupportObj->createAt = date('y-m-d H:i:s');

        $doAddNewRecord
            = $this->detailSupportRepo->addNewRecord($detaiSupportObj);
        if ($doAddNewRecord->messageCode == 1) {
            return redirect()->back();
        } else {
            $message = $doAddNewRecord->message;
            return redirect()->back()->with([
                'level' => 'warning',
                'message' => $message
            ]);
        }
    }

   }
