<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AddOrderRequest;
use App\Models\Order;
use App\Objects\CustomerObject;
use App\Objects\OrderObject;
use App\Objects\ResultObject;
use App\Objects\TypeObject;
use App\Objects\User_service;
use App\Repositories\OrderRepositories;
use App\Repositories\ServiceRepositories;
use App\Repositories\TypeRepositories;
use App\Repositories\UserRepositories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\ConfirmAddNewOrder;
use App\Mail\ConfirmAddNewOrderToAdmin;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;


class OrderController extends Controller
{
    //
    private $orderRepo;
    private $serviceRepo;
    private $userRepo;
    private $serviceType;

    public function __construct(
        TypeRepositories $typeRepositories,
        OrderRepositories $order,
        ServiceRepositories $serviceRepositories,
        UserRepositories $userRepositories
    ) {
        $this->orderRepo = $order;
        $this->serviceRepo = $serviceRepositories;
        $this->userRepo = $userRepositories;
        $this->serviceType = $typeRepositories;
        date_default_timezone_set("Asia/Bangkok");

    }

    public function test()
    {
        dd($this->orderRepo->getIsAboutToExpireOrderList());
    }

    public function getList()
    {
        $doGetListOrder = $this->orderRepo->getList(10);

        if ($doGetListOrder->messageCode == 1) {
            $listOrder = $doGetListOrder->result;
            foreach ($listOrder as $order){
                $order->service_id = json_decode($order->service_id);
            }
            return view('Admin.page.Order.list', compact('listOrder'));
        } else {
            return redirect()->route('admin.dashboard')->with([
                'level' => 'warning',
                'message' => $doGetListOrder->message
            ]);
        }
    }

    public function getAdd()
    {
        //get list service:
        $doGetListService = $this->serviceRepo->getList();
        $doGetListUser = $this->userRepo->getList();
        if ($doGetListService->messageCode == 1
            && $doGetListUser->messageCode == 1
        ) {
            $listService = $doGetListService->result;
            $listUser = $doGetListUser->result;
        } else {
            $listService = [];
            $listUser = [];
        }

        return view('Admin.page.Order.add',
            ['listService' => $listService, 'listUser' => $listUser]);
    }

    public function getEdit($id)
    {
        //get list service:
        $doGetListService = $this->serviceRepo->getList();
        $doGetListUser = $this->userRepo->getList();
        $doGetThisOrder = $this->orderRepo->getOneById($id);
        if ($doGetListService->messageCode == 1
            && $doGetListUser->messageCode == 1
            && $doGetThisOrder->messageCode == 1
        ) {
            $thisOrder = $doGetThisOrder->result;
            if ($thisOrder->seen == 0) {
                $thisOrder->seen = 1;
                $doUpdateOrder = $this->orderRepo->update($thisOrder);
            }

            $thisOrder->service_id = json_decode($thisOrder->service_id);

            $listService = $doGetListService->result;
            $listUser = $doGetListUser->result;
            $thisOrder = $doGetThisOrder->result;

            return view('Admin.page.Order.update',
                compact('listService', 'listUser', 'thisOrder'));
        } else {
            echo "thiếu thông số";
        }

    }

    public function postAdd(AddOrderRequest $request)
    {
        $order = new OrderObject();
        /*Get Customer Infomations:*/
        $getCustomerInfo = $this->userRepo->getOneById($request->user);
        if($getCustomerInfo->messageCode == 1){
            $thisCustomer = $getCustomerInfo->result;
        } else {
            $thisCustomer = new CustomerObject();
        }

        $order->address = ($request->address != null) ? $request->address : $thisCustomer->address;
        $order->created_at = isset($request->date) && $request->date ? $request->date : date('Y-m-d H:i:s');
        $order->updated_at = date('Y-m-d H:i:s');
        $order->email = ($request->email != null) ? $request->email : $thisCustomer->email;
        $order->phone = ($request->phone != null) ? $request->phone : $thisCustomer->phone;
        $order->user_id = $request->user;
        $order->username = ($request->name != null) ? $request->name : $thisCustomer->name;
        $order->note = $request->note;
        $order->status = $request->status;
        $order->totalPrice = $request->totalPrice;
        $order->paidMoney = $request->paidMoney;
        $order->missingMoney =  $order->totalPrice - $order->paidMoney;
        $order->service_id = json_encode($request->service);
        $order->seen = 1;

        $doAddOrder = $this->orderRepo->add($order);
     
        if ($doAddOrder->messageCode == 1) {
            /*todo: tìm ra tên của user, tên của service*/
            /*GỬI MAIL THÔNG BÁO:*/
            $dataToAdmin = [
                'name' => $order->username,
                'service' => $order->service_id
            ];
            /*
            $mail = Mail::send('Email.addServiceSendToAdmin', $dataToAdmin,
                function ($msg) {
                    $msg->from('notification@mailgun.yournews.romecody.com',
                        'Thông báo: có khách hàng đăng ký dịch vụ mới.');
                    $msg->to('romecoder@gmail.com', 'Thông báo')
                        ->subject('Đơn hàng mới');
                });
            /*----------------------------------------------------*/
            $dataToUser = [
                'name' => $order->username,
                'service' =>$order->service_id
            ];
            /*
            $mail = Mail::send('Email.addService', $dataToUser,
                function ($msg) {
                    $msg->from('notification@mailgun.yournews.romecody.com',
                        'Pveser:Đăng ký dịch vụ thành công');
                    $msg->to(Input::get('email'), 'Pveser')
                        ->subject('Pveser thông báo');
                });
            */
            return redirect()->action('Admin\OrderController@getList')
                ->with([
                    'level' => 'success',
                    'message' => 'Đăng ký sử dụng dịch vụ thành công'
                ]);
        } else {
            return redirect()->action('Admin\OrderController@getList')
                ->with([
                    'level' => 'warning',
                    'message' => ' Đăng ký sử dụng dịch vụ không thành công'
                ]);
        }
    }

    public function postEdit(AddOrderRequest $request, $id)
    {
        $order = new OrderObject();
        $order->id = $id;
        $order->address = $request->address;
        $order->created_at = isset($request->date) && $request->date ? $request->date : date('Y-m-d H:i:s');
        $order->updated_at = date('Y-m-d H:i:s');
        $order->email = $request->email;
        $order->phone = $request->phone;
        $order->user_id = $request->user;
        $order->note = $request->note;
        $order->status = $request->status;
        $order->totalPrice = $request->totalPrice;
        $order->service_id = json_encode($request->service);
        $order->paidMoney = $request->paidMoney;
        $order->missingMoney =  $order->totalPrice - $order->paidMoney;
        $order->seen = 1;


        $doUpdateOrder = $this->orderRepo->update($order);
        if ($doUpdateOrder->messageCode == 1) {
            return redirect()->action('Admin\OrderController@getList')
                ->with([
                    'level' => 'success',
                    'message' => ' Cập nhật thông tin đơn hàng thành công'
                ]);
        } else {
            return redirect()->action('Admin\OrderController@getList')
                ->with([
                    'level' => 'warning',
                    'message' => ' Cập nhật thông tin đơn hàng thất bại'
                ]);
        }

    }

    public function getDetails($id)
    {
        $doGetThisOrder = $this->orderRepo->getOneById($id);
        if ($doGetThisOrder->messageCode == 1) {
            $thisOrder = $doGetThisOrder->result;
        } else {
            $thisOrder = null;
        }

        return view('Admin.page.Order.detail', ['thisOrder' => $thisOrder]);
    }

    public function delete($id)
    {
        $res = new ResultObject();

        $findId = $this->orderRepo->getOneById($id);
        if ($findId->messageCode == 1) {
            $order = new OrderObject();
            $order->id = $id;
            $doDelete = $this->orderRepo->delete($order);
            if ($doDelete->messageCode == 1) {
                $level = 'success';
            } else {
                $level = 'warning';
            }
        } else {
            $level = 'warning';
            $message = $findId->message;
        }

        return redirect()->route('get.order.list')->with([
            'level' => 'success',
            'message' => $doDelete->message
        ]);

    }

    public function changeStatus($id)
    {
        $orderObj = new OrderObject();
        $orderObj->id = $id;
        $orderRepo = new OrderRepositories();
        $doChangeStatus = $orderRepo->changeStatus($orderObj);
        if ($doChangeStatus->messageCode == 1) {
            /*Thay đổi trạng thái của user_service*/
            return redirect()->action('Admin\OrderController@getList')->with([
                'level' => 'success',
                'message' => 'chuyển trạng thái thành công'
            ]);
        } else {
            return redirect()->action('Admin\OrderController@getList')->with([
                'level' => 'warning',
                'message' => 'Chưa chuyển đc trạng thái'
            ]);
        }
    }

    public function active($id)
    {
        $orderObj = new OrderObject();
        $orderObj->id = $id;

        /*Tính số ngày hết hạn của đơn hàng*/
        $doGetThisOrder = $this->orderRepo->getOneById($id);
        $time = 0;
        if ($doGetThisOrder->messageCode == 1) {
            $thisOrder = $doGetThisOrder->result;
            $doGetThisService
                = $this->serviceRepo->getOneById($thisOrder->service_id);
            if ($doGetThisService->messageCode == 1) {
                $thisService = $doGetThisService->result;
                $typeObj = new TypeObject();
                $typeObj->id = $thisService->type_id;
                $doGetThisServiceType = $this->serviceType->findById($typeObj);
                if ($doGetThisServiceType->messageCode == 1) {
                    $thisType = $doGetThisServiceType->result;
                    $time = $thisType->time
                        * 86400; /*số ngày * số giây/ngày*/
                }
            }
        }
        $orderObj->updated_at = date('Y-m-d H:i:s');
        $orderObj->status = 1;
        $orderObj->startDate = date('Y-m-d H:i:s');
        $orderObj->endDate = date('Y-m-d H:i:s',
            strtotime($orderObj->startDate) + $time);
        $doChangeStatus = $this->orderRepo->update($orderObj);
        if ($doChangeStatus->messageCode == 1) {
            /*Thay đổi trạng thái của user_service*/
            return redirect()->back()->with([
                'level' => 'success',
                'message' => 'Kích hoạt đơn hàng thành công'
            ]);
        } else {
            return redirect()->back()->with([
                'level' => 'warning',
                'message' => 'Đơn hàng chưa được kích hoạt'
            ]);
        }
    }
}
