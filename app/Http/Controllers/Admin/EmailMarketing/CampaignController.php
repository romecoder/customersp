<?php

namespace App\Http\Controllers\Admin\EmailMarketing;

use App\Mail\CampaignMail;
use App\Models\E_Email;
use App\Objects\E_CampaignObject;
use App\Repositories\E_CampaignRepositories;
use App\Repositories\E_ContentRepositories;
use App\Repositories\E_EmailRepositories;
use App\Repositories\E_GroupRepositories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;

class CampaignController extends Controller
{
    private $campaignRepo;
    private $contentRepo;
    private $groupRepo;
    private $emailRepo;

    public function __construct()
    {
        $this->campaignRepo = new E_CampaignRepositories();
        $this->emailRepo = new E_EmailRepositories();
        $this->groupRepo = new E_GroupRepositories();
        $this->contentRepo = new E_ContentRepositories();
        date_default_timezone_set("Asia/Bangkok");
    }

    public function index()
    {
        $doGetCampaignList = $this->campaignRepo->getListCampaign(
            $order = 'updated_at',
            $by = 'DESC',
            $limit = 10,
            $offset = null
        );
        if ($doGetCampaignList->messageCode == 1) {
            $campaignList = $doGetCampaignList->result;
            foreach ($campaignList as $campaign) {
                if ($campaign->group_id) {
                    $campaign->group_id = json_decode($campaign->group_id);
                    if (sizeof($campaign->group_id)) {
                        $arrayCampaignName = [];
                        foreach ($campaign->group_id as $groupId) {
                            $doGetGroup
                                = $this->groupRepo->getGroupById($groupId);
                            if ($doGetGroup->messageCode == 1) {
                                $arrayCampaignName[]
                                    = $doGetGroup->result->name;
                            } else {
                                $arrayCampaignName[] = null;
                            }
                        }
                    } else {
                        $arrayCampaignName[] = null;
                    }
                } else {
                    $arrayCampaignName[] = null;
                }
                $campaign->group_id = $arrayCampaignName;
            }
        } else {
            $campaignList = [];
        }

        return view('Admin.page.EmailMarketing.Campaign.list',
            ['campaignList' => $campaignList]);
    }

    public function insert()
    {
        $doGetGroupList = $this->groupRepo->getListGroup();
        if ($doGetGroupList->messageCode == 1) {
            $groupList = $doGetGroupList->result;
        } else {
            $groupList = [];
        }
        $doGetContentList = $this->contentRepo->getListContent();
        if ($doGetContentList->messageCode == 1) {
            $contentList = $doGetContentList->result;
        } else {
            $contentList = [];
        }

        return view('Admin.page.EmailMarketing.Campaign.add',
            ['groupList' => $groupList, 'contentList' => $contentList]);
    }

    public function store(Request $request)
    {
        $newCampaign = new E_CampaignObject();
        if ($request->group) {
            $newCampaign->groupId = json_encode($request->group);
        }
        $newCampaign->content = $request->contents;
        $newCampaign->createdAt = date('Y-m-d H:i:s');
        $newCampaign->updatedAt = date('Y-m-d H:i:s');
        $newCampaign->name = $request->name;
        $newCampaign->status = $request->status;


        $doAddNewCampaign = $this->campaignRepo->addNewCampaign($newCampaign);
        if ($doAddNewCampaign->messageCode == 1) {
            $level = 'success';
            $message = 'Thanh cong';
        } else {
            $level = 'warning';
            $message = 'Khong thanh cong'.$doAddNewCampaign->message;
        }

        return redirect()
            ->action('Admin\EmailMarketing\CampaignController@index')
            ->with(['level' => $level, 'message' => $message]);
    }

    public function edit($id)
    {
        $doGetThisCampaign = $this->campaignRepo->getCampaignById($id);
        if ($doGetThisCampaign->messageCode == 1) {
            $thisCampaign = $doGetThisCampaign->result;
            if (sizeof($thisCampaign->groupId)) {
                $thisCampaign->groupId = json_decode($thisCampaign->groupId);
            } else {
                $thisCampaign->groupId = null;
            }
        } else {
            $thisCampaign = null;
        }
        /*get list group to import select input*/
        $doGetGroupList = $this->groupRepo->getListGroup();
        if ($doGetGroupList->messageCode == 1) {
            $groupList = $doGetGroupList->result;
        } else {
            $groupList = [];
        }
        /*get content list to import select input*/
        $doGetContentList = $this->contentRepo->getListContent();
        if ($doGetContentList->messageCode == 1) {
            $contentList = $doGetContentList->result;
        } else {
            $contentList = [];
        }

        return view('Admin.page.EmailMarketing.Campaign.update',
            [
                'thisCampaign' => $thisCampaign,
                'groupList' => $groupList,
                'contentList' => $contentList
            ]);
    }

    public function search()
    {

    }

    /**
     * Hàm lấy thông tin chi tiết , hienr thị qua view run
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function run($id)
    {
        /*Get Fullinfo*/
        $doGetThisCampaign = $this->campaignRepo->getCampaignById($id);
        if ($doGetThisCampaign->messageCode == 1) {
            $thisCampaign = $doGetThisCampaign->result;
            /*get this group*/
            if (sizeof($thisCampaign->groupId)) {
                $thisCampaign->groupId = json_decode($thisCampaign->groupId);
                $arrayGroupObj = [];
                foreach ($thisCampaign->groupId as $groupId) {
                    $doGetThisGroup = $this->groupRepo->getGroupById($groupId);
                    if ($doGetThisGroup->messageCode == 1) {
                        $thisGroup = $doGetThisGroup->result;
                        /*do Get Email of group*/
                        $doGetEmailOfGroup
                            = $this->emailRepo->getEmailOfGroup($groupId);
                        if ($doGetEmailOfGroup->messageCode == 1) {
                            $thisGroup->numberEmail
                                = $doGetEmailOfGroup->numberOfResult;
                            if (sizeof($doGetEmailOfGroup->result)) {
                                foreach (
                                    $doGetEmailOfGroup->result as $emailGroup
                                ) {
                                    $doGetThisEmail
                                        = $this->emailRepo->getEmailById($emailGroup->emailId);
                                    if ($doGetThisEmail->messageCode == 1) {
                                        $thisGroup->emailList[]
                                            = $doGetThisEmail->result;
                                    }
                                }
                            } else {
                                $thisGroup->numberEmail = null;
                                $thisGroup->emailList = null;
                            }
                        } else {
                            $thisGroup->numberEmail = null;
                            $thisGroup->emailList = null;
                        }
                        $arrayGroupObj[] = $thisGroup;
                    } else {
                        $arrayGroupObj[] = null;
                    }
                }
                $thisCampaign->groupId = $arrayGroupObj;
            } else {
                $thisCampaign->groupId = null;
            }
            $emailList = [];
            if (sizeof($thisCampaign->groupId)) {
                foreach ($thisCampaign->groupId as $group) {
                    if (sizeof($group->emailList)) {
                        foreach ($group->emailList as $email) {
                            $emailList[] = $email->email;
                        }
                    }
                }
            }

            /*Get this content*/
            if ($thisCampaign->content != null) {
                $doGetThisContent
                    = $this->contentRepo->getContentById($thisCampaign->content);
                if ($doGetThisContent->messageCode == 1) {
                    $thisContent = $doGetThisContent->result;
                } else {
                    $thisContent = null;
                }
            } else {
                $thisContent = null;
            }
        } else {
            $thisCampaign = null;
        }

        /*get this group*/


        return view('Admin.page.EmailMarketing.Campaign.run',
            [
                'thisCampaign' => $thisCampaign,
                'emailList' => $emailList,
                'thisContent' => $thisContent
            ]);
    }

    /**
     * Hàm: sau khi Admin quyết định gửi loạt email.
     * @param Request $request
     * @param         $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function action(Request $request, $id)
    {
        $content = $request->contents;
        if ($request->testMail != null) {
            /*Gửi nội dung cho mail đã nhập*/
            $testMail = $request->testMail;
            $data = [
                'email' => $testMail,
                'content' => $content
            ];

            try {
                /*Mail::to($testMail)->send(new CampaignMail($testMail,
                    $content));*/
                $mail = Mail::send('Email.addServiceSendToAdmin', $data,
                    function ($msg) {
                        $msg->from('notification@mailgun.yournews.romecody.com',
                            'Test Chiến dịch');
                        $msg->to(Input::get('testMail'), 'test chiến dịch')
                            ->subject('Test Chiến dịch');
                    });
            } catch (\Exception $exception) {

                return redirect()->back()->with([
                    'level' => 'warning',
                    'message' => 'Hệ thống chưa thể gửi được mail, bạn hãy kiểm tra lại'
                ]);
            }

            return redirect()
                ->action('Admin\EmailMarketing\CampaignController@index')
                ->with([
                    'level' => 'success',
                    'message' => 'Mail của bạn hiện đã được gửi tới địa chỉ cần kiểm tra.'
                ]);

        } else {
            $emailList = json_decode($request->emailList);
            $sendMailProcess = [];
            $campaign = new E_CampaignObject();
            $campaign->id = $id;
            $campaign->status = 1;
            foreach ($emailList as $email) {
                try {
                    /* Mail::to($email)->send(new CampaignMail($email, $content));*/
                    $data = [
                        'email' => $email,
                        'content' => $content
                    ];

                    Mail::send('Email.campaign', $data, function($msg) use ($emailList)
                    {
                        $msg->from('notification@mailgun.yournews.romecody.com',
                            'Hệ thống Pveser');
                        $msg->to($emailList)->subject('Pveser');
                    });
                    $sendMailProcess[] = 1;
                } catch (\Exception $exception) {
                    $sendMailProcess[] = 0;
                }
            }
            /*thay đổi status của campaign*/
            $doUpdateCampaignStatus = $this->campaignRepo->updateCampaign($campaign);
            if ($doUpdateCampaignStatus->messageCode == 1) {
                return redirect()
                    ->action('Admin\EmailMarketing\CampaignController@index')
                    ->with([
                        'level' => 'success',
                        'message' => 'Chiến dịch gửi mail thành công.'
                    ]);
            } else {
                return redirect()
                    ->action('Admin\EmailMarketing\CampaignController@index')
                    ->with([
                        'level' => 'success',
                        'message' => 'Chiến dịch gửi mail thành công,nhưng chưa cập nhật trạng thái của campaign này.'
                    ]);
            }
        }
    }

    public function show($id)
    {
        $doGetThisCampaign = $this->campaignRepo->getCampaignById($id);
        if ($doGetThisCampaign->messageCode == 1) {
            $thisCampaign = $doGetThisCampaign->result;
            if (sizeof($thisCampaign->groupId)) {
                $thisCampaign->groupId = json_decode($thisCampaign->groupId);
            } else {
                $thisCampaign->groupId = null;
            }
        } else {
            $thisCampaign = null;
        }
        /*get list group to import select input*/
        $doGetGroupList = $this->groupRepo->getListGroup();
        if ($doGetGroupList->messageCode == 1) {
            $groupList = $doGetGroupList->result;
        } else {
            $groupList = [];
        }
        /*get content list to import select input*/
        $doGetContentList = $this->contentRepo->getListContent();
        if ($doGetContentList->messageCode == 1) {
            $contentList = $doGetContentList->result;
        } else {
            $contentList = [];
        }

        return view('Admin.page.EmailMarketing.Campaign.update',
            [
                'thisCampaign' => $thisCampaign,
                'groupList' => $groupList,
                'contentList' => $contentList
            ]);
    }

    public function update(Request $request, $id)
    {
        $campaignObj = new E_CampaignObject();
        $campaignObj->id = $id;
        $doGetThisCampaign = $this->campaignRepo->getCampaignById($id);
        if ($doGetThisCampaign->messageCode == 1) {
            if ($request->group) {
                $campaignObj->groupId = json_encode($request->group);
            }
            $campaignObj->id = $id;
            $campaignObj->content = $request->contents;
            $campaignObj->updatedAt = date('Y-m-d H:i:s');
            $campaignObj->name = $request->name;
            if ($request->status == 'on') {
                $campaignObj->status = 1;
            } else {
                $campaignObj->status = 0;
            }
            $doUpdateCampaign
                = $this->campaignRepo->updateCampaign($campaignObj);
            if ($doUpdateCampaign->messageCode == 1) {
                $level = 'success';
                $message = 'Thanh cong';
            } else {
                $level = 'warning';
                $message = 'Khong thanh cong'.$doUpdateCampaign->message;
            }

        } else {
            $level = 'warning';
            $message = 'khong tim thay du lieu';
        }

        return redirect()
            ->action('Admin\EmailMarketing\CampaignController@index')
            ->with(['level' => $level, 'message' => $message]);
    }

    public function delete($id)
    {
        $doDeleteCampaign = $this->campaignRepo->deleteCampaign($id);
        if ($doDeleteCampaign->messageCode == 1) {
            $level = 'success';
            $message = 'xóa thành công';
        } else {
            $level = 'warning';
            $message = 'Chưa thể xóa';
        }

        return redirect()
            ->action('Admin\EmailMarketing\CampaignController@index')
            ->with(['level' => $level, 'message' => $message]);
    }
}
