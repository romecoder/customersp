<?php

namespace App\Http\Controllers\Admin\EmailMarketing;

use App\Objects\E_EmailObject;
use App\Objects\E_GroupObject;
use App\Repositories\E_GroupRepositories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupController extends Controller
{
    private $groupRepo;

    public function __construct()
    {
        $this->groupRepo = new E_GroupRepositories();
    }

    public function index()
    {
        $doGetList = $this->groupRepo->getListGroup();
        if ($doGetList->messageCode == 1) {
            $groupList = $doGetList->result;
        } else {
            $groupList = [];
        }

        return view('Admin.page.EmailMarketing.Group.list',
            ['groupList' => $groupList]);
    }

    public function create()
    {
        return view('Admin.page.EmailMarketing.Group.add');
    }

    public function store(Request $request)
    {
        $groupObj = new E_GroupObject();
        $groupObj->name = $request->name;
        if ($request->status) {
            $groupObj->status = 1;
        } else {
            $groupObj->status = 0;
        }
        $groupObj->createdAt = date('Y-m-d H:i:s');
        $groupObj->updateAt = date('Y-m-d H:i:s');
        $doCreate = $this->groupRepo->addNewGroup($groupObj);
        if ($doCreate->messageCode == 1) {
            $level = 'success';
            $message = 'Thêm Nhóm email mới thành công';
        } else {
            $level = 'warning';
            $message = 'Chưa thể thêm Nhóm email'.$doCreate->message;
        }

        return redirect()->action('Admin\EmailMarketing\GroupController@index')
            ->with(['level' => $level, 'message' => $message]);
    }

    public function edit($id)
    {
        $doGetThisGroup = $this->groupRepo->getGroupById($id);
        if ($doGetThisGroup->messageCode == 1) {
            $thisGroup = $doGetThisGroup->result;
        } else {
            $thisGroup = null;
        }

        return view('Admin.page.EmailMarketing.Group.update',
            ['thisGroup' => $thisGroup]);
    }

    public function update(Request $request,$id)
    {
        $groupObj = new E_GroupObject();
        $doGetThisGroup = $this->groupRepo->getGroupById($id);
        if($doGetThisGroup->messageCode == 1){
            $groupObj->name = $request->name;
            if ($request->status) {
                $groupObj->status = 1;
            } else {
                $groupObj->status = 0;
            }
            $groupObj->id = $id;
            $groupObj->createdAt = date('Y-m-d H:i:s');
            $groupObj->updateAt = date('Y-m-d H:i:s');
            $doCreate = $this->groupRepo->updateGroup($groupObj);
            if ($doCreate->messageCode == 1) {
                $level = 'success';
                $message = 'Cập nhật email mới thành công';
            } else {
                $level = 'warning';
                $message = 'Chưa thể cập nhậtNhóm email'.$doCreate->message;
            }

        } else {
            $level = 'waring';
            $message = 'Không tìm thấy nhóm này';
        }
        return redirect()->action('Admin\EmailMarketing\GroupController@index')
            ->with(['level' => $level, 'message' => $message]);
    }

    public function search()
    {

    }

    public function delete($id)
    {
        $doGetThisGroup = $this->groupRepo->getGroupById($id);
        if ($doGetThisGroup->messageCode == 1) {
            /*xóa hết email liên quan trogn bản email-group*/
            $doDeleteGroup = $this->groupRepo->deleteGroup($id);
            if ($doDeleteGroup->messageCode == 1) {
                $level = 'success';
                $message = 'Xóa Thành công Group';
            } else {
                $level = 'warning';
                $message = 'Xóa không Thành công Group';
            }
        } else {
            $level = 'warning';
            $message = 'không tìm thấy dữ liệu';
        }

        return redirect()->action('Admin\EmailMarketing\GroupController@index')
            ->with(['level' => $level, 'message' => $message]);
    }

    public function show($id)
    {
        $doGetThisGroup = $this->groupRepo->getGroupById($id);
        if ($doGetThisGroup->messageCode == 1) {
            $thisGroup = $doGetThisGroup->result;
        } else {
            $thisGroup = null;
        }

        return view('Admin.page.EmailMarketing.Group.update',
            ['thisGroup' => $thisGroup]);
    }

}
