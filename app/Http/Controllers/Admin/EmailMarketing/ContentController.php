<?php

namespace App\Http\Controllers\Admin\EmailMarketing;

use App\Models\E_Content;
use App\Repositories\E_ContentRepositories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContentController extends Controller
{
    private $contentRepo;

    public function __construct()
    {
        $this->contentRepo = new E_ContentRepositories();
    }

    public function index()
    {
        $doGetList = $this->contentRepo->getListContent();
        if ($doGetList->messageCode == 1) {
            $listGroup = $doGetList->result;
        } else {
            $listGroup = [];
        }

        return view('Admin.page.EmailMarketing.Content.list',
            ['listGroup' => $listGroup]);
    }

    public function create()
    {
        return view('Admin.page.EmailMarketing.Content.add');
    }

    public function store()
    {

    }

    public function edit($id)
    {
        $doGetThisGroup = $this->contentRepo->getContentById($id);
        if ($doGetThisGroup->messageCode == 1) {
            $thisGroup = $doGetThisGroup->result;
        } else {
            $thisGroup = null;
        }

        return view('Admin.page.EmailMarketing.Content.update',
            ['thisGroup' => $thisGroup]);
    }

    public function update()
    {

    }

    public function search()
    {

    }
    public function deletele($id)
    {
        $doGetThisGroup = $this->contentRepo->getContentById($id);
        if ($doGetThisGroup->messageCode == 1) {
            $doDeleteGroup = $this->contentRepo->deleteContent($id);
            if ($doDeleteGroup->messageCode == 1) {
                $level = 'success';
                $message = 'Xóa Thành công Group';
            } else {
                $level = 'warning';
                $message = 'Xóa không Thành công Group';
            }
        } else {
            $level = 'warning';
            $message = 'không tìm thấy dữ liệu';
        }

        return redirect()->action('Admin\EmailMarketing\GroupController@index')
            ->with(['level' => $level, 'message' => $message]);
    }

    public function show($id)
    {
        $doGetThisGroup = $this->contentRepo->getContentById($id);
        if ($doGetThisGroup->messageCode == 1) {
            $thisGroup = $doGetThisGroup->result;
        } else {
            $thisGroup = null;
        }

        return view('Admin.page.EmailMarketing.Group.update',
            ['thisGroup' => $thisGroup]);
    }
}
