<?php

namespace App\Http\Controllers\Admin\EmailMarketing;

use App\Objects\E_EmailObject;
use App\Repositories\E_EmailRepositories;
use App\Repositories\E_GroupRepositories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmailController extends Controller
{
    private $emailRepo;
    private $groupRepo;

    public function __construct()
    {
        $this->emailRepo = new E_EmailRepositories();
        $this->groupRepo = new E_GroupRepositories();
    }

    public function index()
    {
        $doGetEmailList = $this->emailRepo->getListEmail(
            $order = 'updated_at',
            $by = 'DESC',
            $limit = 10,
            $offset = null
        );
        if ($doGetEmailList->messageCode == 1) {
            $emailList = $doGetEmailList->result;
        } else {
            $emailList = [];
        }

        return view('Admin.page.EmailMarketing.Email.list',
            ['emailList' => $emailList]);
    }

    public function insert()
    {
        $doGetGroupList = $this->groupRepo->getListGroup();
        if ($doGetGroupList->messageCode == 1) {
            $groupList = $doGetGroupList->result;
        } else {
            $groupList = [];
        }

        return view('Admin.page.EmailMarketing.Email.add',
            ['groupList' => $groupList]);
    }

    public function store(Request $request)
    {
        $newEmail = new E_EmailObject();
        $newEmail->email = $request->email;
        $newEmail->name = $request->name;
        $newEmail->createdAt = date('Y-m-d H:i:s');
        $newEmail->updateAt = date('Y-m-d H:i:s');
        $newEmail->groupId = $request->group;
        $newEmail->status = $request->status;
        $newEmail->type = $request->type;

        $doAddNewEmail = $this->emailRepo->addNewEmail($newEmail);
        if ($doAddNewEmail->messageCode == 1) {
            $level = 'success';
            $message = 'Thêm thành công';
        } else {
            $level = 'warning';
            $message = 'chưa thêm được';
        }

        return redirect()->action('Admin\EmailMarketing\EmailController@index')
            ->with(['level' => $level, 'message' => $message]);
    }

    public function edit($id)
    {
        $doGetThisEmail = $this->emailRepo->getEmailById($id);
        if ($doGetThisEmail->messageCode == 1) {
            $thisEmail = $doGetThisEmail->result;
        } else {
            $thisEmail = null;
        }

        $doGetGroupList = $this->groupRepo->getListGroup();
        if ($doGetGroupList->messageCode == 1) {
            $groupList = $doGetGroupList->result;
        } else {
            $groupList = [];
        }

        return view('Admin.page.EmailMarketing.Email.update',
            ['thisEmail' => $thisEmail,'groupList'=>$groupList]);
    }

    public function search()
    {

    }

    public function filter()
    {

    }

    public function update(Request $request,$id)
    {
        $doGetThisEmail = $this->emailRepo->getEmailById($id);
        if($doGetThisEmail->messageCode == 1){
            $thisEmail = $doGetThisEmail->result;
            $thisEmail->email = $request->email;
            $thisEmail->name = $request->name;
            $thisEmail->createdAt = date('Y-m-d H:i:s');
            $thisEmail->updateAt = date('Y-m-d H:i:s');
            $thisEmail->groupId = $request->group;
            $thisEmail->status = $request->status;
            $thisEmail->type = $request->type;

            $doUpdateEmail = $this->emailRepo->updateEmail($thisEmail);
            if($doUpdateEmail->messageCode == 1){
                $level = 'success';
                $message = 'Thêm thành công';
            } else {
                $level = 'warning';
                $message = 'chưa thêm được';
            }
        } else {
            $level = 'warning';
            $message = 'không tìm thấy dữ liệu';
        }

        return redirect()->action('Admin\EmailMarketing\EmailController@index')
            ->with(['level' => $level, 'message' => $message]);
    }

    public function delete($id)
    {
        $doDeleteEmail = $this->emailRepo->deleteEmail($id);
        if ($doDeleteEmail->messageCode == 1) {
            $level = 'success';
            $message = 'xóa thành công';
        } else {
            $level = 'warning';
            $message = 'Chưa thể xóa';
        }

        return redirect()->action('Admin\EmailMarketing\EmailController@index')
            ->with(['level' => $level, 'message' => $message]);
    }
}
