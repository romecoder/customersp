<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AdminChangeUserPassword;
use App\Http\Requests\AdminEditRequest;
use App\Http\Requests\AdminRequest;
use App\Http\Requests\ChangePasswordRequest;
use App\Objects\AdminObject;
use App\Objects\AdminRoleObject;
use App\Repositories\AdminRepositories;
use App\Repositories\AdminRoleRepositories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use File;

class AdminController extends Controller
{

    private $aminRepo;
    private $adminRoleRepo;

    public function __construct(
        AdminRepositories $adminRepositories,
        AdminRoleRepositories $adminRoleRepositories
    ) {
        $this->aminRepo = $adminRepositories;
        $this->adminRoleRepo = $adminRoleRepositories;
        date_default_timezone_set("Asia/Bangkok");

    }

    public function getList()
    {
        $repo = new AdminRepositories();
        $listMember = $repo->getList(10);
        if ($listMember->messageCode) {
            $listMembers = $listMember->result;

            return view('Admin.page.Admin.list', compact('listMembers'));
        } else {
            return redirect()->route('admin.dashboard')->with([
                'level' => 'warning',
                'message' => 'Don\'t have any record to show'
            ]);
        }
    }

    public function getAdd()
    {
        /*Lấy ra danh sách các quyền*/
        $doGetListRole = $this->aminRepo->getAllRole();

        if ($doGetListRole->messageCode == 1) {
            $listRole = $doGetListRole->result;
        } else {
            $listRole = [];
        }

        return view('Admin.page.Admin.add', ['listRole' => $listRole]);
    }

    public function postAdd(AdminRequest $request)
    {
        $newAdmin = new AdminObject();

        $newAdmin->address = $request->address;
        if (isset($request->avatar) && $request->avatar) {
            $avatar = imageHandle($request->avatar, 'CS2\images\avatar');
            $newAdmin->avatar = $avatar;
        }
        $newAdmin->created_at = date('y-m-d H:i:s');
        $newAdmin->updated_at = date('y-m-d H:i:s');
        $newAdmin->email = $request->email;
        $newAdmin->name = $request->name;
        $newAdmin->phone = $request->phone;
        $newAdmin->role = $request->role;
        $newAdmin->content = $request->contents;
        $newAdmin->fullName = $request->fullname;
        if (isset($request->status) && $request->status) {
            $newAdmin->status = $request->status;
        }
        if (isset($request->gender) && $request->gender) {
            $newAdmin->gender = $request->gender;
        }
        $newAdmin->password = Hash::make($request->password);

        //Handle to save:
        $repo = new AdminRepositories();
        $addAdmin = $repo->add($newAdmin);
        if ($addAdmin->messageCode) {
            return redirect()->route('get.member.list')->with([
                'level' => 'success',
                'message' => $addAdmin->message
            ]);
        } else {
            return redirect()->back()->with([
                'level' => 'danger',
                'message' => $addAdmin->message
            ]);
        }
    }

    public function getEdit($id)
    {
        /*Lấy ra danh sách các quyền dưới dạng mảng các Id*/
        $doGetListRole = $this->aminRepo->getAllRole();
        $listRole = [];
        if ($doGetListRole->messageCode == 1) {
            $listRole = $doGetListRole->result;
        }
        /*Lấy ra thông tin về Admin*/
        $repo = new AdminRepositories();

        $Admin = $repo->getOneById($id);
        if ($Admin->messageCode) {
            $thisAdmin = $Admin->result;
            /*lấy tất cả role của Admin này, dưới dạng mảng các role Id*/
            $doGetListAdminRole
                = $this->adminRoleRepo->getAllRoleByAdminId($id);
            $listAdminRole = [];
            if ($doGetListAdminRole->messageCode == 1) {
                foreach ($doGetListAdminRole->result as $role) {
                    $listAdminRole[] = $role->role_id;
                }
            }

            return view('Admin.page.Admin.update',
                compact('thisAdmin', 'listAdminRole', 'listRole'));
        } else {
            return redirect()->route('get.member.list')->with([
                'level' => 'danger',
                'message' => $Admin->message
            ]);
        }
    }

    public function postEdit(AdminEditRequest $request, $id)
    {
        $thisAdmin = new AdminObject();

        $thisAdmin->id = $id;
        $thisAdmin->address = $request->address;
        $thisAdmin->updated_at = date('y-m-d H:i:s');
        $thisAdmin->email = $request->email;
        $thisAdmin->name = $request->name;
        $thisAdmin->phone = $request->phone;
        $thisAdmin->role = $request->role;
        $thisAdmin->content = $request->contents;
        $thisAdmin->fullName = $request->fullname;


        if (isset($request->status) && $request->status) {
            $thisAdmin->status = 1;
        }
        if (isset($request->gender) && $request->gender) {
            $thisAdmin->gender = 1;
        }
        /*Sử lý ảnh avatar*/
        if (isset($request->avatar) && $request->avatar) {
            $avatar = imageHandle($request->avatar, 'CS2\images\avatar');
            $thisAdmin->avatar = $avatar;

            $oldAvatar = 'CS2/images/avatar/'.$request->oldavatar;
            if (File::exists($oldAvatar)) {
                File::delete($oldAvatar);
            }
        }

        //Handle to save:
        $repo = new AdminRepositories();
        $updateAdmin = $repo->update($thisAdmin);
        if ($updateAdmin->messageCode) {
            return redirect()->route('get.member.list')->with([
                'level' => 'success',
                'message' => $updateAdmin->message
            ]);
        } else {
            return redirect()->back()->with([
                'level' => 'danger',
                'message' => $updateAdmin->message
            ]);
        }
    }

    public function getDetail($id)
    {
        $repo = new AdminRepositories();

        $Admin = $repo->getOneById($id);
        if ($Admin->messageCode) {
            $thisAdmin = $Admin->result;

            return view('Admin.page.Admin.detail', compact('thisAdmin'));
        } else {
            return redirect()->route('get.member.list')->with([
                'level' => 'danger',
                'message' => $Admin->message
            ]);
        }
    }

    public function getDelete($id)
    {
        $thisAdmin = new AdminObject();
        $thisAdmin->id = $id;
        $repo = new AdminRepositories();

        //delete image:
        $getAdmin = $repo->getOneById($id);
        $oldAvatar = 'CS2/images/avatar/'.$getAdmin->result->avatar;
        if (File::exists($oldAvatar)) {
            File::delete($oldAvatar);
        }
        /*Xóa các bản ghi có admin_id ở bảng admin_role*/
        $listRole = $this->adminRoleRepo->getAllRoleByAdminId($id);
        $arrayResult = [];
        if($listRole->messageCode == 1){
            foreach ($listRole->result as $record){
                $adminRoleObj = new AdminRoleObject();
                $adminRoleObj->adminId = $record->admin_id;
                $doDeleteRecord = $this->adminRoleRepo->deleteAllAdminRole($adminRoleObj);
                if($doDeleteRecord->messageCode == 1){
                    $arrayResult[] = 1;
                } else {
                    $arrayResult[] = 0;
                }
            }
        }
        /*Xóa các bản ghi có admin_id ở bảng support*/

        /*xóa bản ghi Admin (chính thức)*/
        $deleteAdmin = $repo->delete($thisAdmin);
        if ($deleteAdmin->messageCode) {
            return redirect()->route('get.member.list')->with([
                'level' => 'success',
                'message' => $deleteAdmin->message
            ]);
        } else {
            return redirect()->back()->with([
                'level' => 'danger',
                'message' => $deleteAdmin->message
            ]);
        }
    }

    public function changeStatus($id)
    {
        $adminObj = new AdminObject();
        $adminObj->id = $id;

        $adminRepo = new AdminRepositories();

        $doChangeStatus = $adminRepo->changeStatus($adminObj);
        if ($doChangeStatus->messageCode == 1) {
            return redirect()->back()->with([
                'level' => 'success',
                'message' => 'chuyển trạng thái thành công'
            ]);
        } else {
            return redirect()->back()->with([
                'level' => 'warning',
                'message' => 'Chưa chuyển đc trạng thái'
            ]);
        }
    }

    public function getFormChangePassword()
    {
        return view('Admin.page.Admin.ChangePassword');
    }

    public function postChangePassword(ChangePasswordRequest $request)
    {
        $oldPassword = $request->oldPassword;
        $newPassword = $request->password;
        $userId = Auth::guard('admin')->user()->id;

        $thisAdmin = $this->aminRepo->getOneById($userId);

        if ($thisAdmin->messageCode == 1) {
            if (Hash::check($request->oldPassword,
                $thisAdmin->result->password)
            ) {
                /*Mật khẩu khớp->cho phép đổi mật khẩu mới*/
                $hashNewPass = Hash::make($newPassword);
                $adminObj = new AdminObject();
                $adminObj->id = $userId;
                $adminObj->password = $hashNewPass;
                $adminObj->status = 1;
                $doUpdateAdmin = $this->aminRepo->update($adminObj);
                if ($doUpdateAdmin->messageCode == 1) {
                    return redirect()
                        ->action('Admin\DashboardController@getHome')->with(
                            [
                                'level' => 'success',
                                'message' => 'Mật khẩu đã được cập nhật thành công'
                            ]
                        );
                } else {
                    return redirect()->back()->with(
                        [
                            'level' => 'warning',
                            'message' => 'Chưa thể thay đổi mật khẩu'
                        ]
                    );
                }
            } else {
                /*Mật khẩu cũ không khớp*/
                return redirect()->back()->with(
                    [
                        'level' => 'warning',
                        'message' => 'Mật khẩu cũ không chính xác'
                    ]
                );
            }
        } else {
            return redirect()->back()->with(
                [
                    'level' => 'warning',
                    'message' => 'Không tìm thấy Admin'
                ]
            );
        }
    }

    public function adminChangeUserPassword(
        AdminChangeUserPassword $request,
        $id
    ) {
        $adminObj = new AdminObject();
        $adminObj->id = $id;
        $adminObj->status = 1;
        $adminObj->password = Hash::make($request->password);

        $adminRepo = new AdminRepositories();

        $doChangePassword = $adminRepo->update($adminObj);
        if ($doChangePassword->messageCode == 1) {
            return redirect()->back()->with([
                'level' => 'success',
                'message' => 'Thay đổi mật khẩu thành công'
            ]);
        } else {
            return redirect()->back()->with([
                'level' => 'warning',
                'message' => 'Chưa thay đổi được mật khẩu'
            ]);
        }
    }
    public function adminChangeAdminPassword(
        AdminChangeUserPassword $request,
        $id
    ) {
        $adminObj = new AdminObject();
        $adminObj->id = $id;
        $adminObj->status = 1;
        $adminObj->password = Hash::make($request->pass);

        $adminRepo = new AdminRepositories();

        $doChangePassword = $adminRepo->update($adminObj);

        if ($doChangePassword->messageCode == 1) {
            return redirect()->back()->with([
                'level' => 'success',
                'message' => 'Thay đổi mật khẩu thành công'
            ]);
        } else {
            return redirect()->back()->with([
                'level' => 'warning',
                'message' => 'Chưa thay đổi được mật khẩu'
            ]);
        }
    }

}
