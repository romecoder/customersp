<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|unique:user,name|unique:admin,name,'.$this->id,
            'email'=>'required|unique:user,email|unique:admin,email,'.$this->id,
            'phone'=>'numeric',
            'address'=>'required',
            'role'=>'required',
            'avatar'=>'image|mimes:jpeg,jpg,png,gif|max:10240',
        ];
    }
}
