<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'user'=>'required|not_in:0',
           'service'=>'required|not_in:0',
           'totalPrice'=>'required'
        ];
    }
    public function messages()
    {
        return [
            'user.not_in' => 'Lựa chọn khách hàng cần thêm đơn hàng',
            'service.not_in' => 'Lựa chọn dịch vụ',
            'totalPrice.required' => 'Nhập tổng giá trị đơn hàng'
        ];
    }
}
