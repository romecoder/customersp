<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminChangeUserPassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pass'=>'required',
            'pass'=>'min:6',
            'repass'=>'same:pass',
        ];
    }
    public function messages()
    {
        return [
            'pass.required'=>'Cần nhập vào mật khẩu',
            'pass.min'=>'mật khẩu phải dài hơn 6 ký tự',
            'repass.same'=>'Mật khẩu không khớp',
        ];
    }
}
