<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class validateAddSupport extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'service'=>'required'
        ];
    }
    public function messages()
    {
        return [
            'name.required'=>'Điền vào vấn đề cần hỗ trợ',
            'service.required'=>'Chọn dịch vụ'
        ];
    }
}
