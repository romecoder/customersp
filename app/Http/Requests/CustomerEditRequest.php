<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|unique:admin,name|unique:user,name,'.$this->id,
            'email'=>'required|unique:admin,email|unique:user,email,'.$this->id,
            'phone'=>'numeric',
            'address'=>'required',
            'avatar'=>'image|mimes:jpeg,jpg,png,gif|max:1024',
        ];
    }
}
