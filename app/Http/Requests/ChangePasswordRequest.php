<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required',
            'password' => 'min:4',
            'oldPassword' => 'required',
            'repassword' => 'same:password',
        ];
    }
    public function messages()
    {
        return [
            'password.required' => 'Nhập vào mật khẩu',
            'oldPassword.required' => 'Nhập vào mật khẩu cũ',
            'password.min' => 'Mật khẩu phải có nhiều hờn 4 ký tự',
            'repassword.same' => 'Mật khẩu không khớp',
        ];
    }
}
