<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|unique:user,name|unique:admin,name',
            'email'=>'required|unique:user,email|unique:admin,email',
            'fullname'=>'required',
            'password'=>'required|min:4',
            'repassword'=>'required|same:password',
        ];
    }
}
