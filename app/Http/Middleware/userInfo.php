<?php

namespace App\Http\Middleware;

use App\Repositories\AdminRepositories;
use App\Repositories\AdminRoleRepositories;
use App\Repositories\UserRepositories;
use Closure;
use Illuminate\Support\Facades\Auth;
use Session;

class userInfo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    private $userRepo ;

    public function __construct(UserRepositories $userRepositories)
    {
       $this->userRepo = $userRepositories;
    }

    public function handle($request, Closure $next)
    {
        $doGetUserInfo = $this->userRepo->getOneById(Auth::user()->id);
        if ($doGetUserInfo->messageCode == 1) {
            $thisUser = $doGetUserInfo->result ;
        } else {
            $thisUser = null ;
        }

        Session::put('userInfo',$thisUser);
        return $next($request);
    }
}
