<?php

namespace App\Http\Middleware;

use App\Repositories\DetailSupportRepositries;
use Closure;
use Illuminate\Support\Facades\Auth;

class getSidebarDataUser
{
    /**
     * Handle an incoming request.
     * For User:
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    private $detailSupportRepo;

    public function __construct(
        DetailSupportRepositries $detailSupportRepositries
    ) {
        $this->detailSupportRepo = $detailSupportRepositries;
    }

    public function handle($request, Closure $next)
    {
        view()->composer('Customer.block.left', function ($view) {
            //lấy ra những tin nhắn chưa được đọc của Admin này:
            $userId = Auth::user()->id;
            $doGetUnSeenSupportDetailOfAdmin
                = $this->detailSupportRepo->getUnseenDetailSupportOfOneUser($userId);
            if ($doGetUnSeenSupportDetailOfAdmin->messageCode == 1) {
                $countMessage
                    = $doGetUnSeenSupportDetailOfAdmin->numberOfResult;
            } else {
                $countMessage = null;
            }
            //
            $view->with(['countMessage' => $countMessage]);
        });

        return $next($request);
    }
}
