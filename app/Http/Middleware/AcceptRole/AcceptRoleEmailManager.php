<?php

namespace App\Http\Middleware\AcceptRole;

use Closure;

class AcceptRoleEmailManager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (sizeof(session('adminInfo')->role)
            && in_array('6', session('adminInfo')->role)
        ) {
            return $next($request);
        } else {
            return redirect()->action('Admin\DashboardController@getHome')
                ->with([
                    'level' => 'success',
                    'message' => 'Bạn chưa được cấp quyền'
                ]);
        }
    }
}
