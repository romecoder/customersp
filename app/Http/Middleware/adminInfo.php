<?php

namespace App\Http\Middleware;

use App\Repositories\AdminRepositories;
use App\Repositories\AdminRoleRepositories;
use Closure;
use Illuminate\Support\Facades\Auth;
use Session;

class adminInfo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    private $adminRepo ;
    private $adminRoleRepo ;
    public function __construct(AdminRepositories $adminRepositories,AdminRoleRepositories $adminRoleRepositories)
    {
        $this->adminRepo = $adminRepositories;
        $this->adminRoleRepo = $adminRoleRepositories;
    }

    public function handle($request, Closure $next)
    {
        $doGetAdminInfo = $this->adminRepo->getOneById(Auth::guard('admin')->user()->id);
        if ($doGetAdminInfo->messageCode == 1) {
            $thisAdmin = $doGetAdminInfo->result ;
            $arrayRole = [];
            $doGetAllAdminRole = $this->adminRoleRepo->getAllRoleByAdminId($thisAdmin->id);
            if ($doGetAllAdminRole->messageCode == 1){
                $listAdminRole = $doGetAllAdminRole->result;
            } else {
                $listAdminRole = [];
            }
            if($listAdminRole){
                foreach ($listAdminRole as $item) {
                    $arrayRole[] = $item->role_id;
                }
            }
            $thisAdmin->role = $arrayRole;
        } else {
            $thisAdmin = null ;
        }

        Session::put('adminInfo',$thisAdmin);
        return $next($request);
    }
}
