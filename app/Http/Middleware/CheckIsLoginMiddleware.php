<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckIsLoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('admin')->check() || Auth::check()) {
            return redirect()->back();
        }else {
            return $next($request); //đi tiếp
        }
    }
}
