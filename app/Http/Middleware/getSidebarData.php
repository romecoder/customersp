<?php

namespace App\Http\Middleware;

use App\Repositories\DetailSupportRepositries;
use App\Repositories\OrderRepositories;
use App\Repositories\UserRepositories;
use Closure;
use Illuminate\Support\Facades\Auth;

class getSidebarData
{
    /**
     * Handle an incoming request.
     * For Admin:
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    private $detailSupportRepo;
    private $orderRepo;
    private $userRepo;

    public function __construct(
        DetailSupportRepositries $detailSupportRepositries,
        OrderRepositories $orderRepositories,
        UserRepositories $userRepositories
    ) {
        $this->detailSupportRepo = $detailSupportRepositries;
        $this->orderRepo = $orderRepositories;
        $this->userRepo = $userRepositories;
    }

    public function handle($request, Closure $next)
    {
        view()->composer('Admin.block.left', function ($view) {
            //lấy ra những tin nhắn chưa được đọc của Admin này:
            $adminId = Auth::guard('admin')->user()->id;
            $doGetUnSeenSupportDetailOfAdmin
                = $this->detailSupportRepo->getUnseenDetailSupportOfOneAdmin($adminId);
            if ($doGetUnSeenSupportDetailOfAdmin->messageCode == 1) {
                $countMessage
                    = $doGetUnSeenSupportDetailOfAdmin->numberOfResult;
            } else {
                $countMessage = null;
            }

            //lấy lượng đơn hàng chưa dc xử lý:
            $doGetUnSeenOrderList = $this->orderRepo->getUnseenOrderList();
            if ($doGetUnSeenOrderList->messageCode == 1) {
                $countOrderUnseen = $doGetUnSeenOrderList->numberOfResult;
            } else {
                $countOrderUnseen = null;
            }
            //lấy lượng người dùng chưa dc kích hoạt:
            $doGetUnSeenUserList = $this->userRepo->getUnSeenUserList();
            if ($doGetUnSeenUserList ->messageCode == 1) {
                $countUserUnseen = $doGetUnSeenUserList->numberOfResult;
            } else {
                $countUserUnseen = null;
            }
            //
            $view->with([
                'countMessage' => $countMessage,
                'countOrderUnseen' => $countOrderUnseen,
                'countUserUnseen' => $countUserUnseen
            ]);
        });

        return $next($request);
    }
}
