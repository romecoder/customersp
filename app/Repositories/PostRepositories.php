<?php

namespace App\Repositories;


use App\Models\Post;
use App\Objects\PostObject;
use App\Objects\ResultObject;
use League\Flysystem\Exception;

class PostRepositories
{
    private $postModel;

    public function __construct(Post $postModel)
    {
        $this->postModel = $postModel;
    }

    public function getListPost($limit = 0,$status = null)
    {
        $res = new ResultObject();
        try {
            if($limit == 0){
                if($status === null){
                    $doGetPostList = $this->postModel->orderBy('created_at', 'DESC')->get();
                } else {
                    $doGetPostList = $this->postModel->orderBy('created_at', 'DESC')->where('status',$status)->get();
                }
            } else {
                if($status === null){
                    $doGetPostList = $this->postModel->orderBy('created_at', 'DESC')->paginate($limit);
                } else {
                    $doGetPostList = $this->postModel->orderBy('created_at', 'DESC')->where('status',$status)->paginate($limit);
                }
            }
            if(sizeof($doGetPostList)){
                $res->messageCode = 1;
                $res->message = 'Thành công';
                $res->result = $doGetPostList;
            } else {
                $res->messageCode = 0;
                $res->message = 'không có dữ liệu';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }

        return $res;
    }


    public function getOne($id)
    {
        $res = new ResultObject();
        try {
            $doGetPostList = $this->postModel->find($id);
            if($doGetPostList){
                $res->messageCode = 1;
                $res->message = 'Thành công';
                $res->result = $doGetPostList;
            } else {
                $res->messageCode = 0;
                $res->message = 'Thất bại';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }

        return $res;
    }

    public function addPost(PostObject $postObj)
    {
        $res = new ResultObject;
        $newPost = new Post();
        $newPost->slug = $postObj->slug;
        $newPost->tittle = $postObj->tittle;
        $newPost->content = $postObj->content;
        $newPost->status = $postObj->status;
        $newPost->created_at = $postObj->createdAt;
        $newPost->updated_at = $postObj->updatedAt;
        $newPost->category_id = $postObj->categoryId;
        $newPost->author_id= $postObj->authorId;
        $newPost->image= $postObj->image;

        try{
            if($newPost->save()){
                $res->messageCode = 1;
                $res->message = 'thành công';
                $res->result = $newPost;
            } else {
                $res->messageCode = 0;
                $res->message = 'Thất bại';
            }
        } catch (\Exception $exception){
            $res->messageCode = 0;
            $res->message =  $exception->getMessage();
        }
        return $res;
    }

    public function updatePost($postObj)
    {
        $res = new ResultObject;
        $thisPost = $this->postModel->find($postObj->id);
        if($thisPost){
            if($postObj->slug){
                $thisPost->slug = $postObj->slug;
            }
            if($postObj->tittle){
                $thisPost->tittle = $postObj->tittle;
            }
            if($postObj->content){
                $thisPost->content = $postObj->content;
            }
            if(isset($postObj->status)){
                $thisPost->status = $postObj->status;
            }
            if($postObj->createdAt){
                $thisPost->created_at = $postObj->createdAt;
            }
            if($postObj->updatedAt){
                $thisPost->updated_at = $postObj->updatedAt;
            }
            if($postObj->categoryId){
                $thisPost->category_id = $postObj->categoryId;
            }
            if($postObj->authorId){
                $thisPost->author_id = $postObj->authorId;
            }
            if($postObj->image){
                $thisPost->image = $postObj->image;
            }
        } else {
            $res->messageCode = 0;
            $res->message = 'không tìm thấy bản ghi';
        }
        try{
            if($thisPost->save()){
                $res->messageCode = 1;
                $res->message = 'thành công';
                $res->result = $thisPost;
            } else {
                $res->messageCode = 0;
                $res->message = 'Thất bại';
            }
        } catch (\Exception $exception){
            $res->messageCode = 0;
            $res->message =  $exception->getMessage();
        }
        return $res;
    }

    public function deletePost($id)
    {
        $res = new ResultObject();
        try {
            $doDeletePost = $this->postModel->destroy($id);
            if ($doDeletePost) {
                $res->messageCode = 1;
                $res->message = 'Xóa thành công';
                $res->result = $doDeletePost;
            } else {
                $res->messageCode = 0;
                $res->message = 'Xóa không thành công';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }

        return $res;
    }

}