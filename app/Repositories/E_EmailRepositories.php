<?php


namespace App\Repositories;


use App\Models\E_Email;
use App\Models\E_EmailGroup;
use App\Objects\E_EmailObject;
use App\Objects\ResultObject;

class E_EmailRepositories
{
    private $model;
    private $emailGroupModel;
    public function __construct()
    {
        $this->model = new E_Email();
        $this->emailGroupModel = new E_EmailGroup();
    }

    public function getListEmail(
        $order = 'updated_at',
        $by = 'DESC',
        $limit  = null,
        $offset = null
    )
    {
        $res = new ResultObject();
        try{
            if($limit){
                $doGetList = $this->model->orderBy($order,$by)->paginate($limit);
            } else {
                $doGetList = $this->model->orderBy($order,$by)->get();
            }
            if(sizeof($doGetList)){
                $res->messageCode = 1;
                $res->message = 'success';
                $res->result = $doGetList;
            } else {
                $res->messageCode = 0;
                $res->message = 'fail';
            }
        } catch (\Exception $exception){
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }
        return $res;
    }


    public function getEmailById($id)
    {
        $res = new ResultObject();
        try{
            $doGetEmailById = $this->model->find($id);
            if($doGetEmailById){
                $res->messageCode = 1;
                $res->message = 'success';
                $res->result = $this->model->convertToObject($doGetEmailById);
            } else {
                $res->messageCode = 0;
                $res->message = 'fail';
            }
        } catch (\Exception $exception){
            $res->messageCode = 0 ;
            $res->message = $exception->getMessage();
        }
        return $res;
    }

    public function addNewEmail(E_EmailObject $emailObj)
    {
        $res = new ResultObject();
        $newEmail = new E_Email();

        $newEmail->id = $emailObj->id;
        $newEmail->email = $emailObj->email;
        $newEmail->name = $emailObj->name;
        $newEmail->status = $emailObj->status;
        $newEmail->created_at = $emailObj->createdAt;
        $newEmail->updated_at = $emailObj->updateAt;
        $newEmail->type = $emailObj->type;
        try{
            if($newEmail->save()){
                /*Them vao bang email_group*/
                if(sizeof($emailObj->groupId)){
                    foreach ($emailObj->groupId as $groupId){
                        $newRecord = new E_EmailGroup();
                        $newRecord->email_id = $newEmail->id;
                        $newRecord->group_id = $groupId;
                        $newRecord->save();
                    }
                }
                $res->messageCode = 1;
                $res->message = 'success';
                $res->result = $this->model->convertToObject($newEmail);
            } else {
                $res->messageCode = 0;
                $res->message = 'fail';
            }
        } catch (\Exception $exception){
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }
        return $res;
    }

    public function updateEmail(E_EmailObject $emailObj)
    {
        $res = new ResultObject();
        $thisEmail = $this->model->find($emailObj->id);

        if($thisEmail){
            if($emailObj->id){
                $thisEmail->id = $emailObj->id;
            }
            if($emailObj->email){
                $thisEmail->email = $emailObj->email;
            }
            if($emailObj->name){
                $thisEmail->name = $emailObj->name;
            }
            if($emailObj->createdAt){
                $thisEmail->created_at = $emailObj->createdAt;
            }
            if($emailObj->updateAt){
                $thisEmail->updated_at = $emailObj->updateAt;
            }
            if($emailObj->type){
                $thisEmail->type = $emailObj->type;
            }
            if($emailObj->status){
                $thisEmail->status = $emailObj->status;
            }
            try{
                if($thisEmail->save()){
                    if(sizeof($emailObj->groupId)){
                        /*Xóa hết record ở bảng email_group:*/
                        $deleteRecord = E_EmailGroup::where('email_id',$emailObj->id)->delete();
                            foreach ($emailObj->groupId as $groupId){
                                $newRecord = new E_EmailGroup();
                                $newRecord->email_id = $thisEmail->id;
                                $newRecord->group_id = $groupId;
                                $newRecord->save();
                            }
                    }
                    $res->messageCode = 1;
                    $res->message = 'Thanh cong';
                    $res->result = $this->model->convertToObject($thisEmail);
                } else {
                    $res->messageCode = 0;
                    $res->message = 'fail';
                }
            } catch (\Exception $exception){
                $res->messageCode = 0;
                $res->message = $exception->getMessage();
            }
        } else {
            $res->messageCode = 0;
            $res->message = 'data not found';
        }

        return $res;
    }

    public function deleteEmail($id)
    {
        $res = new ResultObject();
        try{
            $doDeleteEmail = $this->model->destroy($id);
            if($doDeleteEmail){
                $res->messageCode = 1;
                $res->message = 'success';
            } else {
                $res->messageCode = 0;
                $res->message = 'fail';
            }
        } catch (\Exception $exception){
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }
        return $res;
    }
    public function getEmailOfGroup($groupId)
    {
        $res = new ResultObject();
        try{
            $doGetEmailOfGroup = $this->emailGroupModel->where('group_id',$groupId)->get();
            if($doGetEmailOfGroup ){
                $res->messageCode = 1;
                $res->message = 'success';
                $res->result = $this->emailGroupModel->convertToArrayObject($doGetEmailOfGroup);
                $res->numberOfResult = count($doGetEmailOfGroup);
            } else {
                $res->messageCode = 0;
                $res->message = 'fail';
            }
        } catch (\Exception $exception){
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }
        return $res;
    }
}