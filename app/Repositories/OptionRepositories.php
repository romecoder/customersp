<?php


namespace App\Repositories;


use App\Models\Option;
use App\Objects\OptionObject;
use App\Objects\ResultObject;

class OptionRepositories
{
    private $optionModel;

    public function __construct(Option $option)
    {
        $this->optionModel = $option;
    }

    public function getList($type = 0, $limit = 0)
    {
        $res = new ResultObject();
        try {
            if ($type = 0) {
                $doGetList = $this->optionModel
                    ->orderBy('created_at','DESC')
                    ->all();
            } else {
                $doGetList = $this->optionModel
                    ->where('type', $type)
                    ->orderBy('created_at','DESC')
                    ->paginate($limit);
            }
            if (sizeof($doGetList)) {
                $res->message = 'Thanh cong';
                $res->result = $doGetList;
                $res->messageCode = 1;
            } else {
                $res->message = 'khong thanh cong';
                $res->messageCode = 0;
            }
        } catch (\Exception $exception) {
            $res->message = $exception->getMessage();
            $res->messageCode = 0;
        }

        return $res;
    }

    public function getListByKeyAndId($type, $elementId)
    {
        $res = new ResultObject();
        try {
            $doGetList = $this->optionModel->where('type', $type)
                ->where('element_id', $elementId)->get();
            if (sizeof($doGetList)) {
                $res->message = 'Thanh cong';
                $res->result = $doGetList;
                $res->messageCode = 1;
            } else {
                $res->message = 'khong co du lieu';
                $res->messageCode = 0;
            }
        } catch (\Exception $exception) {
            $res->message = $exception->getMessage();
            $res->messageCode = 0;
        }

        return $res;
    }

    public function store($optionObj)
    {
        $res = new ResultObject();
        $newOptionObj = new Option();
        $newOptionObj->type = $optionObj->type;
        $newOptionObj->element_id = $optionObj->elementId;
        $newOptionObj->key = $optionObj->key;
        $newOptionObj->value = $optionObj->value;
        $newOptionObj->created_at = $optionObj->createdAt;
        try {
            if ($newOptionObj->save()) {
                $res->messageCode = 1;
                $res->message = 'Thanh cong';
                $res->result = $newOptionObj;
            } else {

                $res->messageCode = 0;
                $res->message = 'That bai';
            }
        } catch (\Exception $exception) {

            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }

        return $res;
    }

    public function update($optionObj)
    {
        $res = new ResultObject();
        $thisOption = $this->optionModel->find($optionObj->id);
        if ($thisOption) {
            if ($optionObj->type) {
                $thisOption->type = $optionObj->type;
            }
            if ($optionObj->elementId) {
                $thisOption->element_id = $optionObj->elementId;
            }
            if ($optionObj->key) {
                $thisOption->key = $optionObj->key;
            }
            if ($optionObj->value) {
                $thisOption->value = $optionObj->value;
            }
            if ($optionObj->createdAt) {
                $thisOption->created_at = $optionObj->createdAt;
            }
        }
        try {
            if ($thisOption->save()) {
                $res->messageCode = 1;
                $res->message = 'Thanh cong';
                $res->result = $thisOption;
            } else {

                $res->messageCode = 0;
                $res->message = 'That bai';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }

        return $res;
    }

    public function updateByKey($key,$value = 0)
    {
        $res = new ResultObject();
        $doGetThisOption = $this->optionModel->where('key',$key)->first();
        if ($doGetThisOption) {
            $thisOption = $doGetThisOption;
            if ($value) {
                $thisOption->value = $value;
            }
        }
        try {
            if ($thisOption->save()) {
                $res->messageCode = 1;
                $res->message = 'Thanh cong';
                $res->result = $thisOption;
            } else {

                $res->messageCode = 0;
                $res->message = 'That bai';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }

        return $res;
    }


    public function getOne($id)
    {
        $res = new ResultObject();
        try {
            $doGetOne = $this->optionModel->find($id);
            if ($doGetOne) {
                $res->message = 'Thanh cong';
                $res->result = $doGetOne;
                $res->messageCode = 1;
            } else {
                $res->message = 'khong thanh cong';
                $res->messageCode = 0;
            }
        } catch (\Exception $exception) {
            $res->message = $exception->getMessage();
            $res->messageCode = 0;
        }

        return $res;
    }

    public function delete($id)
    {
        $res = new ResultObject();
        try {
            $doDelete = $this->optionModel->destroy($id);
            if ($doDelete) {
                $res->message = 'Thanh cong';
                $res->messageCode = 1;
            } else {
                $res->message = 'khong thanh cong';
                $res->messageCode = 0;
            }
        } catch (\Exception $exception) {
            $res->message = $exception->getMessage();
            $res->messageCode = 0;
        }

        return $res;
    }

}