<?php
/**
 * Created by PhpStorm.
 * User: ROME
 * Date: 5/7/2017
 * Time: 3:50 PM
 */

namespace App\Repositories;


use App\Models\Admin_Role;
use App\Models\Role;
use App\Objects\AdminObject;
use App\Objects\AdminRoleObject;
use App\Objects\ResultObject;
use App\Models\Admin;

class AdminRepositories
{
    private $adminRoleRepo ;

    public function getList($limit = 0)
    {
        $result = new ResultObject();
        try {
            if ($limit == 0) {
                $data = Admin::orderBy('created_at','DESC')->get();
            } else {
                $data = Admin::orderBy('created_at','DESC')->paginate($limit);
            }
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'get list Admins successfully!';
                $result->numberOfResult = count($data);
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'Fail to list Admins';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function add($admin)
    {
        $newAdmin = new Admin();

        if (isset($admin->id) && $admin->id) {
            $newAdmin->id = $admin->id;
        } else {
            $newAdmin->id = null;
        }
        if (isset($admin->name) && $admin->name) {
            $newAdmin->name = $admin->name;
        } else {
            $newAdmin->name = null;
        }
        if (isset($admin->password) && $admin->password) {
            $newAdmin->password = $admin->password;
        } else {
            $newAdmin->password = null;
        }
        if (isset($admin->avatar) && $admin->avatar) {
            $newAdmin->avatar = $admin->avatar;
        } else {
            $newAdmin->avatar = null;
        }
        if (isset($admin->gender) && $admin->gender) {
            $newAdmin->gender = $admin->gender;
        } else {
            $newAdmin->gender = null;
        }
        if (isset($admin->email) && $admin->email) {
            $newAdmin->email = $admin->email;
        } else {
            $newAdmin->email = null;
        }
        if (isset($admin->phone) && $admin->phone) {
            $newAdmin->phone = $admin->phone;
        } else {
            $newAdmin->phone = null;
        }
        if (isset($admin->address) && $admin->address) {
            $newAdmin->address = $admin->address;
        } else {
            $newAdmin->address = null;
        }
        if (isset($admin->status) && $admin->status) {
            $newAdmin->status = 1;
        } else {
            $newAdmin->status = 0;
        }
        if (isset($admin->role) && $admin->role) {
            $newAdmin->role = 1;
        } else {
            $newAdmin->role = 0;
        }
        if (isset($admin->content) && $admin->content) {
            $newAdmin->content = $admin->content;
        } else {
            $newAdmin->content = null;
        }
        if (isset($admin->updated_at) && $admin->updated_at) {
            $newAdmin->updated_at = $admin->updated_at;
        } else {
            $newAdmin->updated_at = null;
        }
        if (isset($admin->created_at) && $admin->created_at) {
            $newAdmin->created_at = $admin->created_at;
        } else {
            $newAdmin->created_at = null;
        }
        if (isset($admin->fullName) && $admin->fullName) {
            $newAdmin->full_name = $admin->fullName;
        } else {
            $newAdmin->full_name = null;
        }
        $result = new ResultObject();
        try {
            $data = $newAdmin->save();
            if ($data) {
                /*Lưu role vào bảng Admin_role*/
                if($admin->role){
                    foreach ($admin->role as $item){
                        $doAddNewRecord = new Admin_Role();
                        $doAddNewRecord->admin_id = $newAdmin->id;
                        $doAddNewRecord->role_id = $item;
                        $doAddNewRecord->save();
                    }
                }
                $result->messageCode = 1;
                $result->message = 'Add new Admins successfully!';
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'Fail to add new Admins';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function update(AdminObject $admin)
    {
        $thisAdmin = Admin::find($admin->id);

        if (isset($admin->name) && $admin->name) {
            $thisAdmin->name = $admin->name;
        }
        if (isset($admin->password) && $admin->password) {
            $thisAdmin->password = $admin->password;
        }
        if (isset($admin->avatar) && $admin->avatar) {
            $thisAdmin->avatar = $admin->avatar;
        }
        if (isset($admin->gender) && $admin->gender) {
            $thisAdmin->gender = $admin->gender;
        }
        if (isset($admin->email) && $admin->email) {
            $thisAdmin->email = $admin->email;
        }
        if (isset($admin->phone) && $admin->phone) {
            $thisAdmin->phone = $admin->phone;
        }
        if (isset($admin->address) && $admin->address) {
            $thisAdmin->address = $admin->address;
        }
        if (isset($admin->content) && $admin->content) {
            $thisAdmin->content = $admin->content;
        }
        if (isset($admin->status) && $admin->status) {
            $thisAdmin->status = 1;
        } else {
            $thisAdmin->status = 0;
        }
        if (isset($admin->updated_at) && $admin->updated_at) {
            $thisAdmin->updated_at = $admin->updated_at;
        }
        if (isset($admin->created_at) && $admin->created_at) {
            $thisAdmin->created_at = $admin->created_at;
        }
        if (isset($admin->fullName) && $admin->fullName) {
            $thisAdmin->full_name= $admin->fullName;
        }
        $result = new ResultObject();
        try {
            if ($thisAdmin->save()) {
                /*Lưu vào bảng admin_role*/
                if ($admin->role) {
                    /*Xóa hết quyền cũ của Admin này đi:*/
                    $doDeleteAdminRole = Admin_Role::where('admin_id',$admin->id)->delete();
                    if($doDeleteAdminRole !== false){
                        /*Thêm quyền mới của Admin này vào:*/
                        foreach ($admin->role as $item){
                            $newAdminRole = new Admin_Role();
                            $newAdminRole->admin_id = $admin->id ;
                            $newAdminRole->role_id = $item;
                            $newAdminRole->save();
                        }
                        $result->messageCode = 1;
                        $result->message = 'Cập nhật thành công';
                        $result->result = $thisAdmin;
                    } else {
                        $result->messageCode = 1;
                        $result->message = 'Cập nhật thành công Admin chưa có quyền nào!';
                    }
                }
                $result->messageCode = 1;
                $result->message = 'Cập nhật thành công';
                $result->result = $thisAdmin;
            } else {
                $result->messageCode = 0;
                $result->message = 'Cập nhật không thành công';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage().''.$exception->getFile().''.$exception->getLine();
        }

        return $result;
    }

    public function getOneById($id)
    {
        $result = new ResultObject();
        try {
            $data = Admin::findOrFail($id);
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'get list Admins successfully!';
//                $result->numberOfResult = count($data);
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'Fail to list Admins';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function getOneFullInfo($admin)
    {
        $result = new ResultObject();
        try {
            $data = Admin::findOrFail($admin->id);
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'get list Admins successfully!';
                $result->numberOfResult = count($data);
            } else {
                $result->messageCode = 0;
                $result->message = 'Fail to list Admins';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function delete($admin)
    {
        $result = new ResultObject();
        try {
            $data = Admin::destroy($admin->id);
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'Delete Admins successfully!';
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'Fail to delete Admins';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function checkEmail($email)
    {
        $result = new ResultObject();
        try {
            $data = Admin::where('email', $email)->first();
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'find email successfully!';
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'Fail to find Email';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function changeStatus($adminObj)
    {
        $res = new ResultObject();
        try {
            $thisAdmin = Admin::find($adminObj->id);
            if ($thisAdmin) {
                $thisAdmin->status = !$thisAdmin->status;
                if ($thisAdmin->save()) {
                    $res->messageCode = 1;
                    $res->message = 'find email successfully!';
                } else {
                    $res->messageCode = 0;
                    $res->message = 'Lưu không thành công';
                }
            } else {
                $res->messageCode = 0;
                $res->message = 'không tìm thấy dữ liệu phù hợp';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }

        return $res;
    }

    /*Lấy danh sách các quyền*/
    public function getAllRole()
    {
        $res = new ResultObject();
        try {
            $listAllRole = Role::all();
            if (sizeof($listAllRole)) {
                $res->messageCode = 1;
                $res->message = 'find data successfully!';
                $res->result = $listAllRole;
            } else {
                $res->messageCode = 0;
                $res->message = 'không tìm thấy dữ liệu phù hợp';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }
        return $res;
    }
    /*Lấy danh sách các quyền của Admin đó */


}