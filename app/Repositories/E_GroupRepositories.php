<?php


namespace App\Repositories;


use App\Models\E_EmailGroup;
use App\Models\E_Group;
use App\Objects\E_ContentObject;
use App\Objects\E_GroupObject;
use App\Objects\ResultObject;

class E_GroupRepositories
{
    private $model;
    public function __construct()
    {
        $this->model = new E_Group();
    }

    public function getListGroup(
        $order = 'updated_at',
        $by = 'DESC',
        $limit  = null,
        $offset = null
    )
    {
        $res = new ResultObject();
        try{
            $doGetList = $this->model->orderBy($order,$by)->paginate($limit);
            if(sizeof($doGetList)){
                $res->messageCode = 1;
                $res->message = 'success';
                $res->result = $doGetList;
            } else {
                $res->messageCode = 0;
                $res->message = 'fail';
            }
        } catch (\Exception $exception){
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }
        return $res;
    }


    public function getGroupById($id)
    {
        $res = new ResultObject();
        try{
            $doGetGroupById = $this->model->find($id);
            if($doGetGroupById){
                $res->messageCode = 1;
                $res->message = 'success';
                $res->result = $this->model->convertToObject($doGetGroupById);
            } else {
                $res->messageCode = 0;
                $res->message = 'fail';
            }
        } catch (\Exception $exception){
            $res->messageCode = 0 ;
            $res->message = $exception->getMessage();
        }
        return $res;
    }

    public function addNewGroup(E_GroupObject $groupObj)
    {
        $res = new ResultObject();
        $newGroup = new E_Group();

        $newGroup->id = $groupObj->id;
        $newGroup->name = $groupObj->name;
        $newGroup->status = $groupObj->status;
        $newGroup->created_at = $groupObj->createdAt;
        $newGroup->updated_at = $groupObj->updateAt;
        try{
            if($newGroup->save()){
                $res->messageCode = 1;
                $res->message = 'success';
                $res->result = $this->model->convertToObject($newGroup);
            } else {
                $res->messageCode = 0;
                $res->message = 'fail';
            }
        } catch (\Exception $exception){
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }
        return $res;
    }

    public function updateGroup(E_GroupObject $groupObj)
    {
        $res = new ResultObject();
        $thisGroup = $this->model->find($groupObj->id);

        if($thisGroup){
            if($groupObj->id){
                $thisGroup->id = $groupObj->id;
            }
            if($groupObj->createdAt){
                $thisGroup->created_at = $groupObj->createdAt;
            }
            if($groupObj->updateAt){
                $thisGroup->updated_at = $groupObj->updateAt;
            }
            if($groupObj->name){
                $thisGroup->name = $groupObj->name;
            }
            if($groupObj->status){
                $thisGroup->status = $groupObj->status;
            }
            try{
                if($thisGroup->save()){
                    $res->messageCode = 1;
                    $res->message = 'success';
                    $res->result = $this->model->convertToObject($thisGroup);
                } else {
                    $res->messageCode = 0;
                    $res->message = 'fail';
                }
            } catch (\Exception $exception){
                $res->messageCode = 0;
                $res->message = $exception->getMessage();
            }
        } else {
            $res->messageCode = 0;
            $res->message = 'data not found';
        }

        return $res;
    }

    public function deleteGroup($id)
    {
        $res = new ResultObject();
        try{
            $doDeleteEmail = $this->model->destroy($id);
            if($doDeleteEmail){
                $res->messageCode = 1;
                $res->message = 'success';
            } else {
                $res->messageCode = 0;
                $res->message = 'fail';
            }
        } catch (\Exception $exception){
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }
        return $res;
    }

}