<?php


namespace App\Repositories;


use App\Models\E_Campaign;
use App\Models\E_Content;
use App\Objects\E_CampaignObject;
use App\Objects\E_ContentObject;
use App\Objects\E_EmailObject;
use App\Objects\ResultObject;

class E_CampaignRepositories
{
    private $model;

    public function __construct()
    {
        $this->model = new E_Campaign();
    }

    public function getListCampaign(
        $order = 'updated_at',
        $by = 'DESC',
        $limit = null,
        $offset = null
    ) {
        $res = new ResultObject();
        try {
            $doGetList = $this->model->orderBy($order, $by)->paginate();
            if (sizeof($doGetList)) {
                $res->messageCode = 1;
                $res->message = 'success';
                $res->result = $doGetList;
            } else {
                $res->messageCode = 0;
                $res->message = 'fail';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }

        return $res;
    }


    public function getCampaignById($id)
    {
        $res = new ResultObject();
        try {
            $doGetContentById = $this->model->find($id);
            if ($doGetContentById) {
                $res->messageCode = 1;
                $res->message = 'success';
                $res->result = $this->model->convertToObject($doGetContentById);
            } else {
                $res->messageCode = 0;
                $res->message = 'fail';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = $exception->getMessage().$exception->getFile().$exception->getLine();
        }

        return $res;
    }

    public function addNewCampaign(E_CampaignObject $campaignObject)
    {
        $res = new ResultObject();
        $newCampaign = new E_Campaign();

        $newCampaign->name = $campaignObject->name;
        $newCampaign->group_id = $campaignObject->groupId;
        $newCampaign->content = $campaignObject->content;
        if($campaignObject->status){
            $newCampaign->status = 1;
        } else {
            $newCampaign->status = 0;
        }
        $newCampaign->created_at = $campaignObject->createdAt;
        $newCampaign->updated_at = $campaignObject->updatedAt;
        try {
            if ($newCampaign->save()) {
                $res->messageCode = 1;
                $res->message = 'success';
                $res->result = $this->model->convertToObject($newCampaign);
            } else {
                $res->messageCode = 0;
                $res->message = 'fail';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }

        return $res;
    }

    public function updateCampaign(E_CampaignObject $campaignObject)
    {
        $res = new ResultObject();
        $thisCampaign = $this->model->find($campaignObject->id);

        if ($thisCampaign) {
            if ($campaignObject->id) {
                $thisCampaign->id = $campaignObject->id;
            }
            if ($campaignObject->groupId) {
                $thisCampaign->group_id = $campaignObject->groupId;
            }
            if ($campaignObject->name) {
                $thisCampaign->name = $campaignObject->name;
            }
            if ($campaignObject->content) {
                $thisCampaign->content = $campaignObject->content;
            }
            if ($campaignObject->updatedAt) {
                $thisCampaign->updated_at = $campaignObject->updatedAt;
            }
            if ($campaignObject->status) {
                $thisCampaign->status = $campaignObject->status;
            }
            try {
                if ($thisCampaign->save()) {
                    $res->messageCode = 1;
                    $res->message = 'success';
                    $res->result = $this->model->convertToObject($thisCampaign);
                } else {
                    $res->messageCode = 0;
                    $res->message = 'fail';
                }
            } catch (\Exception $exception) {
                $res->messageCode = 0;
                $res->message = $exception->getMessage();
            }
        } else {
            $res->messageCode = 0;
            $res->message = 'data not found';
        }

        return $res;
    }

    public function deleteCampaign($id)
    {
        $res = new ResultObject();
        try {
            $doDeleteCampaign = $this->model->destroy($id);
            if ($doDeleteCampaign) {
                $res->messageCode = 1;
                $res->message = 'success';
            } else {
                $res->messageCode = 0;
                $res->message = 'fail';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }

        return $res;
    }
}