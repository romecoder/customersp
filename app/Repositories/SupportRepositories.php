<?php

namespace App\Repositories;

use App\Objects\ResultObject;
use App\Models\Support;
use Carbon\Carbon;

class SupportRepositories
{
    public function getList($limit = 0)
    {
        $result = new ResultObject();
        try {
            if($limit = 0){
                $data = Support::where('display', 1)->orderBy('created_at', 'DESC')
                    ->all();
            } else {
                $data = Support::where('display', 1)->orderBy('created_at', 'DESC')
                    ->paginate($limit);
            }

            if ($data) {
                $result->messageCode = 1;
                $result->message = 'Get list support successfully!';
                $result->numberOfResult = count($data);
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'Fail to Get list support';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function add($support)
    {
        $newSupport = [];

        if (isset($support->name) && $support->name) {
            $newSupport['name'] = $support->name;
        } else {
            $newSupport['name'] = null;
        }
        if (isset($support->userId) && $support->userId) {
            $newSupport['user_id'] = $support->userId;
        } else {
            $newSupport['user_id'] = null;
        }
        if (isset($support->adminId) && $support->adminId) {
            $newSupport['admin_id'] = $support->adminId;
        } else {
            $newSupport['admin_id'] = null;
        }
        if (isset($support->serviceId) && $support->serviceId) {
            $newSupport['service_id'] = $support->serviceId;
        } else {
            $newSupport['service_id'] = null;
        }
        if (isset($support->updatedAt) && $support->updatedAt) {
            $newSupport['updated_at'] = $support->updatedAt;
        } else {
            $newSupport['updated_at'] = null;
        }
        if (isset($support->createdAt) && $support->createdAt) {
            $newSupport['created_at'] = $support->createdAt;
        } else {
            $newSupport['created_at'] = null;
        }
        if (isset($support->status) && $support->status) {
            $newSupport['status'] = 1;
        } else {
            $newSupport['status'] = 0;
        }
        $result = new ResultObject();
        try {
            $data = Support::create($newSupport);
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'Thêm hỗ trợ thành công!';
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'Chưa thể thêm hỗ trợ';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function update($support)
    {
        $thisSupport = Support::find($support->id);

        if (isset($support->id) && $support->id) {
            $thisSupport->id = $support->id;
        }
        if (isset($support->name) && $support->name) {
            $thisSupport->name = $support->name;
        }
        if (isset($support->userId) && $support->userId) {
            $thisSupport->user_id = $support->userId;
        }
        if (isset($support->adminId) && $support->adminId) {
            $thisSupport->admin_id = $support->adminId;
        }
        if (isset($support->serviceId) && $support->serviceId) {
            $thisSupport->service_id = $support->serviceId;
        } else {
            $thisSupport->service_id = null;
        }
        if (isset($support->updatedAt) && $support->updatedAt) {
            $thisSupport->updated_at = $support->updatedAt;
        }
        if (isset($support->createdAt) && $support->createdAt) {
            $thisSupport->created_at = $support->createdAt;
        }
        if (isset($support->status) && $support->status) {
            $thisSupport->status = 1;
        } else {
            $thisSupport->status = 0;
        }

        $result = new ResultObject();
        try {
            $data = $thisSupport->save();
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'Cập nhật hỗ trợ thành công!';
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'Chưa thể cập nhật hỗ trợ';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function getOneById($support)
    {
        $result = new ResultObject();
        try {
            $data = Support::where('display', 1)->orderBy('created_at', 'DESC')
                ->findOrFail($support->id);
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'Tìm thấy hỗ trợ';
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'chưa thấy hỗ trợ cần tìm';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function getOneFullInfo($support)
    {
        $result = new ResultObject();
        try {
            $data = Support::where('display', 1)->orderBy('created_at', 'DESC')
                ->findOrFail($support->id);
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'Get this support successfully!';
                $result->numberOfResult = count($data);
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'Fail to get this support';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function delete($support)
    {
        $result = new ResultObject();
        try {
            $data = Support::destroy($support->id);
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'Get this support successfully!';
                $result->numberOfResult = count($data);
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'Fail to get this support';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function remove($support)
    {
        $result = new ResultObject();
        $thisSupport = Support::find($support->id);
        $thisSupport->display = $support->display;

        $result = new ResultObject();
        try {
            $data = $thisSupport->save();
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'Xóa hỗ trợ thành công!';
                $result->numberOfResult = count($data);
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'Chưa thể xóa hỗ trợ';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function changeStatus($support)
    {
        $result = new ResultObject();
        try {
            $thisSupport = Support::find($support->id);
            if ($thisSupport) {
                $thisSupport->status = !$thisSupport->status;
                if ($thisSupport->save()) {
                    $result->messageCode = 1;
                    $result->message = 'Change status successfully';
                    $result->result = $thisSupport;
                } else {
                    $result->messageCode = 0;
                    $result->message
                        = 'Thay đổi trạng thái Hõ trợ thành công';
                }
            } else {
                $result->messageCode = 0;
                $result->message
                    = 'Thay đổi trạng thái Hõ trợ không thành công';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function getAllSupportOfUser($supportObj, $limit = 0)
    {
        $res = new ResultObject();
        try {
            if($limit == 0){
                $query = Support::where('display', 1)
                    ->where('user_id', $supportObj->userId)
                    ->orderBy('created_at', 'DESC')->get();
            } else {
                $query = Support::where('display', 1)
                    ->where('user_id', $supportObj->userId)
                    ->orderBy('created_at', 'DESC')->paginate($limit);
            }
            if ($query) {
                $res->messageCode = 1;
                $res->message = 'Lấy dữ liệu thành công';
                $res->result = $query;
            } else {
                $res->messageCode = 0;
                $res->message = 'Lấy dữ liệu không thành công';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = 'Lỗi hệ thống';
        }

        return $res;
    }
    public function getNewSupportInMonth($startDate = null, $endDate = null)
    {
        $now = Carbon::now();
        $month = $now->month;

        $res = new ResultObject();
        try{
            if($startDate!== null && $endDate !== null){
                $doGetNewSupportInMonth = Support::whereBetween('created_at',[$startDate, $endDate])->get();
            } else {
                $doGetNewSupportInMonth = Support::whereMonth('created_at',$month)->get();
            }

            if(sizeof($doGetNewSupportInMonth)){
                $res->messageCode = 1;
                $res->message = 'thành công';
                $res->result = $doGetNewSupportInMonth;
                $res->numberOfResult = count($doGetNewSupportInMonth);
            } else {
                $res->messageCode = 0;
                $res->message = 'That bai';
            }
        } catch (\Exception $exception){
            $res->messageCode = 1;
            $res->message = $exception->getMessage();
        }
        return $res;
    }
}