<?php
namespace App\Repositories;

use App\Models\User_Service;
use App\Objects\ResultObject;
use App\Models\Service;
use App\Objects\ServiceObject;
use DB;

class ServiceRepositories
{
    public function getList($condition = null,$limit = 0)
    {
        $result = new ResultObject();
        try {
            if($condition == null){
                if($limit == 0){
                    $data = Service::orderBy('created_at','DESC')->get();
                } else {
                    $data = Service::orderBy('created_at','DESC')->paginate($limit);
                }
            }elseif ($condition == 'status'){
                if($limit == 0){
                    $data = Service::orderBy('created_at','DESC')->where('status',1)->get();
                } else {
                    $data = Service::orderBy('created_at','DESC')->where('status',1)->paginate($limit);
                }
            }
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'success';
                $result->numberOfResult = count($data);
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'Fail';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage().$exception->getFile();
        }

        return $result;
    }

    public function getAllSerViceIsUsingByUserId($userId,$tatus,$limit = 0)
    {
        $res = new ResultObject();
        try {
            if($limit == 0){
                $query = DB::table('user')
                    ->select([
                        'order.*',
                    ])
                    ->join('order', 'user.id', '=', 'order.user_id')
                    ->where('order.user_id', $userId)
                    ->where('order.status', $tatus)
                    ->get();
            } else {
                $query = DB::table('user')
                    ->select([
                        'order.*',
                    ])
                    ->join('order', 'user.id', '=', 'order.user_id')
                    ->where('order.user_id', $userId)
                    ->where('order.status', $tatus)
                    ->paginate($limit);
            }

            if (sizeOf($query)) {
                $res->messageCode = 1;
                $res->message = 'Lấy dữ liệu thành công';
                $res->result = $query;
            } else {
                $res->message = 'không chứa dữ liệu';
                $res->messageCode = 0;
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }

        return $res;
    }

    public function getAllServiceNotUsingByUserId($userId,$array)
    {
        $result = new ResultObject();
        try {
//            $query = DB::select(DB::raw("SELECT *
//                                FROM `service`
//                                WHERE id NOT IN (
//                                SELECT s.`id`
//                                FROM `service` AS s
//                                INNER JOIN `order` AS od ON s.id = od.service_id
//                                INNER JOIN `user` AS u ON u.id = od.user_id
//                                WHERE u.id = $userId )"));
            $query = Service::whereNotIn('id', $array)->where('status',1)->get();
            if (sizeof($query)) {
                $result->messageCode = 1;
                $result->message = 'lấy dữ liệu thành công';
                $result->numberOfResult = count($query);
                $result->result = $query;
            } else {
                $result->messageCode = 0;
                $result->message = 'không thành công';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function userAddNewService($userServiceObj)
    {
        $userServiceArray = [];

        $res = new ResultObject();
        try {
            foreach ($userServiceObj->service_id as $item) {
                $doAdd = DB::table('user_service')->insertGetId([
                    'user_id' => $userServiceObj->user_id,
                    'service_id' => $item,
                    'created_at' => $userServiceObj->created_at,
                    'status' => $userServiceObj->status,
                    'startdate' => $userServiceObj->startdate,
                    'startdate' => $userServiceObj->startdate,
                    'enddate' => $userServiceObj->enddate,
                ]);
            }
            if ($doAdd) {
                $res->messageCode = 1;
                $res->message = 'Thêm dịch vụ mới thành công.';
            } else {
                $res->messageCode = 0;
                $res->message = 'Thêm dịch vụ mới thất bại.';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 1;
            $res->message = 'Thêm dịch vụ mới thât bại, lỗi hệ thống.';
        }

        return $res;
    }

    public function userDeleteService($userServiceObj)
    {
        $res = new ResultObject();
        try {
            $deleteService = User_Service::where('user_id',
                $userServiceObj->user_id)
                ->where('service_id', $userServiceObj->service_id)->delete();
            if ($deleteService) {
                $res->messageCode = 1;
                $res->message = 'xóa dịch vụ mới thành công.';
            } else {
                $res->messageCode = 0;
                $res->message = 'xóa dịch vụ mới thất bại.';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 1;
            $res->message = 'xóa dịch vụ mới thât bại, lỗi hệ thống.';
        }

        return $res;
    }

    public function userChangeServiceStatus($userServiceObj)
    {

        $res = new ResultObject();
        $thisService = User_Service::find($userServiceObj->id)->first();
        try {
            $doChangeStatus = User_Service::find($userServiceObj->id)
                ->update(['status' => !$thisService->status]);
            if ($doChangeStatus) {
                $res->messageCode = 1;
                $res->message
                    = 'Thay đổi trạng thái dịch vụ mới thành công.';
            } else {
                $res->messageCode = 0;
                $res->message
                    = 'Thay đổi trạng thái dịch vụ mới thất bại.';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 1;
            $res->message
                = 'Thay đổi trạng thái thât bại, lỗi hệ thống.';
        }

        return $res;
    }

    public function add(ServiceObject $service)
    {
        $newService = new Service();

        if (isset($service->id) && $service->id) {
            $newService->id = $service->id;
        } else {
            $newService->id = null;
        }
        if (isset($service->name) && $service->name) {
            $newService->name = $service->name;
        } else {
            $newService->name = null;
        }
        if (isset($service->status) && $service->status) {
            $newService->status = 1;
        } else {
            $newService->status = 0;
        }
        if (isset($service->updated_at) && $service->updated_at) {
            $newService->updated_at = $service->updated_at;
        } else {
            $newService->updated_at = null;
        }
        if (isset($service->typeId) && $service->typeId) {
            $newService->type_id = $service->typeId;
        } else {
            $newService->type_id = null;
        }
        if (isset($service->thumbnail) && $service->thumbnail) {
            $newService->thumbnail = $service->thumbnail;
        } else {
            $newService->thumbnail = null;
        }
        if (isset($service->content) && $service->content) {
            $newService->content = $service->content;
        } else {
            $newService->content = null;
        }
        if (isset($service->price) && $service->price) {
            $newService->price = $service->price;
        } else {
            $newService->price = null;
        }
        if (isset($service->created_at) && $service->created_at) {
            $newService->created_at = $service->created_at;
        } else {
            $newService->created_at = null;
        }
        $result = new ResultObject();
        try {
            $data = $newService->save();
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'Thêm mới dịch vụ thành công';
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'Thêm mới dịch vụ thất bại';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function update($service)
    {
        $thisService = Service::findOrFail($service->id);

        if (isset($service->id) && $service->id) {
            $thisService->id = $service->id;
        } else {
            $thisService->id = null;
        }
        if (isset($service->name) && $service->name) {
            $thisService->name = $service->name;
        } else {
            $thisService->name = null;
        }
        if (isset($service->status) && $service->status) {
            $thisService->status = 1;
        } else {
            $thisService->status = 0;
        }
        if (isset($service->updated_at) && $service->updated_at) {
            $thisService->updated_at = $service->updated_at;
        } else {
            $thisService->updated_at = null;
        }
        if (isset($service->typeId) && $service->typeId) {
            $thisService->type_id = $service->typeId;
        } else {
            $thisService->type_id = null;
        }
        if (isset($service->thumbnail) && $service->thumbnail) {
            $thisService->thumbnail = $service->thumbnail;
        }
        if (isset($service->content) && $service->content) {
            $thisService->content = $service->content;
        } else {
            $thisService->content = null;
        }
        if (isset($service->price) && $service->price) {
            $thisService->price = $service->price;
        } else {
            $thisService->price = null;
        }
        if (isset($service->created_at) && $service->created_at) {
            $thisService->created_at = $service->created_at;
        } else {
            $thisService->created_at = null;
        }
        $result = new ResultObject();
        try {
            $data = $thisService->save();
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'Cập nhật dịch vụ thành công';
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'Cập nhật dịch vụ thất bại';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function getOneById($id)
    {
        $result = new ResultObject();
        try {
            $data = Service::findOrFail($id);
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'Lấy dịch vụ thành công';
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'Lấy dịch vụ thất bại';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function getOneFullInfo($service)
    {
        $result = new ResultObject();
        try {
            $data = Service::findOrFail($service->id);
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'Get this Service successfully!';
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'Fail to Get this Service';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function delete($service)
    {
        $result = new ResultObject();
        try {
            $data = Service::destroy($service->id);
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'Delete this Service successfully!';
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'Fail to Get this Service';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function changeStatus($serviceObj)
    {
        $res = new ResultObject();
        try {
            $thisService = Service::find($serviceObj->id);
            if($thisService){
                $thisService->status = !$thisService->status;
                if($thisService->save()){
                    $res->messageCode = 1;
                    $res->message = 'Thay đổi trạng thái thành công';
                } else {
                    $res->messageCode = 0;
                    $res->message = 'chưa thay đổi được trạng thái';
                }
            } else {
                $res->messageCode = 0;
                $res->message = 'không tìm thấy dữ liệu phù hợp';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }

        return $res;
    }

}