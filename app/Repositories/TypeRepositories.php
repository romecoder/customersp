<?php


namespace App\Repositories;


use App\Models\TypeService;
use App\Objects\ResultObject;
use App\Objects\ServiceTypeObject;
use App\Objects\TypeObject;

class TypeRepositories
{
    public function listServiceType($limit = 0)
    {
        $res = new ResultObject();
        try {
            if($limit == 0){
                $doGetListServiceType = TypeService::all();
            } else {
                $doGetListServiceType = TypeService::paginate($limit);
            }
            if (sizeof($doGetListServiceType)) {
                $res->message = 'success';
                $res->messageCode = 1;
                $res->result = $doGetListServiceType;
            } else {
                $res->message = 'fail';
                $res->messageCode = 0;
            }
        } catch (\Exception $exception) {
            $res->message = 'warning';
            $res->messageCode = 0;
        }
        return $res;
    }

    public function addServiceType(TypeObject $typeObj)
    {
        $res = new ResultObject();
        $serviceTypeObject = new TypeService();

        $serviceTypeObject->name = $typeObj->name;
        $serviceTypeObject->time = $typeObj->time;
        if($typeObj->status == 'on'){
            $serviceTypeObject->status = 1;
        } else {
            $serviceTypeObject->status = 0;
        }
        $serviceTypeObject->updated_at = $typeObj->updatedAt;
        $serviceTypeObject->created_at  = $typeObj->createAt;
        try {
            if ( $serviceTypeObject->save()) {
                $res->message = 'success';
                $res->messageCode = 1;
                $res->result = $serviceTypeObject;
            } else {
                $res->message = 'fail';
                $res->messageCode = 0;
            }
        } catch (\Exception $exception) {
            $res->message = 'warning'.$exception->getMessage();
            $res->messageCode = 0;
        }

        return $res;
    }

    public function findById($typeObj)
    {
        $res = new ResultObject();
        try {
            $doFindServiceType = TypeService::find($typeObj->id);
            if ($doFindServiceType) {
                $res->message = 'success';
                $res->messageCode = 1;
                $res->result = $doFindServiceType;
            } else {
                $res->message = 'fail';
                $res->messageCode = 0;
            }
        } catch (\Exception $exception) {
            $res->message = 'warning';
            $res->messageCode = 0;
        }
        return $res;
    }

    public function updateServiceType(TypeObject $typeObj)
    {
        $res = new ResultObject();
        try {
            $doFindServiceType = $this->findById($typeObj);
            if($doFindServiceType->messageCode == 1){
                $thisTypeService = $doFindServiceType->result;
                $thisTypeService->name = $typeObj->name;
                $thisTypeService->time = $typeObj->time;
                if ($typeObj->status == 'on'){
                    $thisTypeService->status = 1;
                } elseif ($typeObj->status == null)
                {
                    $thisTypeService->status = 0;
                }
                $thisTypeService->updated_at = $typeObj->updatedAt;
                if($thisTypeService->save()){
                    $res->message = 'success';
                    $res->messageCode = 1;
                    $res->result = $thisTypeService;
                } else {
                    $res->message = 'fail';
                    $res->messageCode = 0;
                }
            } else {
                $res->message = 'khong tim thay';
                $res->messageCode = 0;
            }
        } catch (\Exception $exception) {
            $res->message = 'warning'.$exception->getMessage();
            $res->messageCode = 0;
        }

        return $res;
    }

    public function deleteServiceType($typeObj)
    {
        $res = new ResultObject();
        try {
            $doDelete = TypeService::destroy($typeObj->id);
            if ($doDelete) {
                $res->message = 'success';
                $res->messageCode = 1;
                $res->result = $doDelete ;
            } else {
                $res->message = 'fail';
                $res->messageCode = 0;
            }
        } catch (\Exception $exception) {
            $res->message = 'warning';
            $res->messageCode = 0;
        }
        return $res;
    }
}