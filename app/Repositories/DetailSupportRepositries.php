<?php


namespace App\Repositories;


use App\Models\DetailSupport;
use App\Objects\ResultObject;
use DB;

class DetailSupportRepositries
{
    private $detailSupportModel;

    public function __construct(DetailSupport $detailSupport)
    {
        $this->detailSupportModel = $detailSupport;
    }

    public function getAllBySupportId($id, $seen = null)
    {
        $res = new ResultObject();
        try {
            if ($seen === 0 || $seen === 1) {
                $doGetAllSupportDetailById = DetailSupport::where('support_id',
                    $id)
                    ->where('seen', $seen)->orderBy('create_at', 'ASC')->get();
            } else {
                $doGetAllSupportDetailById = DetailSupport::where('support_id',
                    $id)
                    ->orderBy('create_at', 'ASC')->get();
            }
            if (sizeof($doGetAllSupportDetailById) >= 1) {
                $res->messageCode = 1;
                $res->message = 'Lấy dữ liệu thành công';
                $res->result = $doGetAllSupportDetailById;
            } else {
                $res->messageCode = 0;
                $res->message = 'Không có dữ liệu';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = 'Lỗi hệ thống';
        }

        return $res;
    }

    public function addNewRecord($detailSupportObj)
    {
        $res = new ResultObject();
        $doGetAddNewRecord = new DetailSupport();
        $doGetAddNewRecord->support_id = $detailSupportObj->supportId;
        $doGetAddNewRecord->create_at = $detailSupportObj->createAt;
        $doGetAddNewRecord->content = $detailSupportObj->content;
        $doGetAddNewRecord->author = $detailSupportObj->author;
        $doGetAddNewRecord->seen = $detailSupportObj->seen;
        try {
            if ($doGetAddNewRecord->save()) {
                $res->messageCode = 1;
                $res->message = 'Thanh cong';
                $res->result = $doGetAddNewRecord;

            } else {
                $res->messageCode = 0;
                $res->message = 'That bai';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }

        return $res;
    }

    public function getUnseenDetailSupportOfOneUser($userId)
    {
        $res = new ResultObject();
        $doGetRecord = DB::table('user')
            ->join('support', 'user.id', 'support.user_id')
            ->join('detail_support', 'detail_support.support_id', 'support.id')
            ->select('detail_support.*')
            ->where('user.id', $userId)
            ->where('detail_support.seen', 0)
            ->where('detail_support.author', 1)
            ->get();
        if (sizeof($doGetRecord)) {
            $res->messageCode = 1;
            $res->message = 'success';
            $res->result = $doGetRecord;
            $res->numberOfResult = count($doGetRecord);
        } else {
            $res->messageCode = 0;
            $res->message = 'warning';
        }

        return $res;
    }

    public function getUnseenDetailSupportOfOneAdmin($adminId)
    {
        $res = new ResultObject();
        $doGetRecord = DB::table('admin')
            ->join('support', 'admin.id', 'support.admin_id')
            ->join('detail_support', 'detail_support.support_id', 'support.id')
            ->select('detail_support.*')
            ->where('admin.id', $adminId)
            ->where('detail_support.seen', 0)
            ->where('detail_support.author', 0)
            ->get();
        if (sizeof($doGetRecord)) {
            $res->messageCode = 1;
            $res->message = 'success';
            $res->result = $doGetRecord;
            $res->numberOfResult = count($doGetRecord);
        } else {
            $res->messageCode = 0;
            $res->message = 'warning';
        }

        return $res;
    }

    public function updateDetailSupportToSeen($supportId, $author = null)
    {
        $res = new ResultObject();
        if ($author === 1 || $author === 0) {
            $doUpdate = DB::table('detail_support')
                ->where('support_id', $supportId)
                ->where('author', $author)
                ->update(['seen' => 1]);
        } else {
            $doUpdate = DB::table('detail_support')
                ->where('support_id', $supportId)
                ->update(['seen' => 1]);
        }
        if (($doUpdate)) {
            $res->messageCode = 1;
            $res->message = 'success';
            $res->result = $doUpdate;
            $res->numberOfResult = count($doUpdate);
        } else {
            $res->messageCode = 0;
            $res->message = 'warning';
        }

        return $res;
    }
}