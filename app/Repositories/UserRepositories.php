<?php

namespace App\Repositories;

use App\Objects\CustomerObject;
use App\Objects\ResultObject;
use App\Models\User;
use Carbon\Carbon;


class UserRepositories
{
    public function getList($limit = 0)
    {
        $result = new ResultObject();

        try {
            if ($limit == 0) {
                $data = User::orderBy('created_at','DESC')->get();
            } else {
                $data = User::orderBy('created_at','DESC')->paginate($limit);
            }

            if ($data) {
                $result->messageCode = 1;
                $result->message = 'get list Admins successfully!';
                $result->numberOfResult = count($data);
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'Fail to list Admins';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function add($user)
    {
        $newUser = new User;

        if (isset($user->id) && $user->id) {
            $newUser->id = $user->id;
        } else {
            $newUser->id = null;
        }
        if (isset($user->agreeTerm) && $user->agreeTerm) {
            $newUser->agree_term = $user->agreeTerm;
        } else {
            $newUser->agree_term = 0;
        }
        if (isset($user->name) && $user->name) {
            $newUser->name = $user->name;
        } else {
            $newUser->name = null;
        }
        if (isset($user->password) && $user->password) {
            $newUser->password = $user->password;
        } else {
            $newUser->password = null;
        }
        if (isset($user->avatar) && $user->avatar) {
            $newUser->avatar = $user->avatar;
        } else {
            $newUser->avatar = null;
        }
        if (isset($user->gender) && $user->gender) {
            $newUser->gender = $user->gender;
        } else {
            $newUser->gender = null;
        }
        if (isset($user->content) && $user->content) {
            $newUser->content = $user->content;
        } else {
            $newUser->content = null;
        }
        if (isset($user->email) && $user->email) {
            $newUser->email = $user->email;
        } else {
            $newUser->email = null;
        }
        if (isset($user->phone) && $user->phone) {
            $newUser->phone = $user->phone;
        } else {
            $newUser->phone = null;
        }
        if (isset($user->address) && $user->address) {
            $newUser->address = $user->address;
        } else {
            $newUser->address = null;
        }
        if (isset($user->status) && $user->status) {
            $newUser->status = 1;
        } else {
            $newUser->status = 0;
        }
        if (isset($user->active) && $user->active) {
            $newUser->active = $user->active;
        } else {
            $newUser->active = null;
        }
        if (isset($user->updated_at) && $user->updated_at) {
            $newUser->updated_at = $user->updated_at;
        } else {
            $newUser->updated_at = null;
        }
        if (isset($user->created_at) && $user->created_at) {
            $newUser->created_at = $user->created_at;
        } else {
            $newUser->created_at = null;
        }
        if (isset($user->birthday) && $user->birthday) {
            $newUser->birthday = $user->birthday;
        } else {
            $newUser->birthday = null;
        }
        if (isset($user->fullName) && $user->fullName) {
            $newUser->full_name = $user->fullName;
        } else {
            $newUser->full_name = null;
        }
        $result = new ResultObject();
        try {
            $data = $newUser->save();
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'Thêm khách hàng thành công';
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'Thêm khách hàng không thành công';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function update(CustomerObject $user)
    {
        $thisUser = User::findOrFail($user->id);

        if (isset($user->id) && $user->id) {
            $thisUser->id = $user->id;
        }
        if (isset($user->agreeTerm) && $user->agreeTerm) {
            $thisUser->agree_term = $user->agreeTerm;
        }
        if (isset($user->name) && $user->name) {
            $thisUser->name = $user->name;
        }
        if (isset($user->password) && $user->password) {
            $thisUser->password = $user->password;
        }
        if (isset($user->avatar) && $user->avatar) {
            $thisUser->avatar = $user->avatar;
        }
        if (isset($user->gender) && $user->gender) {
            $thisUser->gender = $user->gender;
        }
        if (isset($user->content) && $user->content) {
            $thisUser->content = $user->content;
        }
        if (isset($user->email) && $user->email) {
            $thisUser->email = $user->email;
        }
        if (isset($user->phone) && $user->phone) {
            $thisUser->phone = $user->phone;
        }
        if (isset($user->address) && $user->address) {
            $thisUser->address = $user->address;
        }
        if (isset($user->status) && $user->status) {
            $thisUser->status = 1;
        } else {
            $thisUser->status = 0;
        }
        if (isset($user->updated_at) && $user->updated_at) {
            $thisUser->updated_at = $user->updated_at;
        }
        if (isset($user->created_at) && $user->created_at) {
            $thisUser->created_at = $user->created_at;
        }
        if (isset($user->seen) && $user->seen) {
            $thisUser->seen = $user->seen;
        }
        if (isset($user->birthday) && $user->birthday) {
            $thisUser->birthday = $user->birthday;
        }
        if (isset($user->fullName) && $user->fullName) {
            $thisUser->full_name = $user->fullName;
        }
        $result = new ResultObject();
        try {
            $data = $thisUser->save();
            if ($data) {
                $result->messageCode = 1;
                $result->message
                    = 'Cập nhật thông tin khách hàng thành công';
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message
                    = 'Cập nhật thông tin khách hàng không thành công';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function getOneById($id)
    {
        $result = new ResultObject();
        try {
            $data = User::findOrFail($id);
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'Lấy thông tin khách hàng thành công';
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message
                    = 'Lấy thông tin khách hàng không thành công';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function getOneByEmail($email)
    {
        $result = new ResultObject();
        try {
            $data = User::where('email', $email)->get();
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'Lấy thông tin khách hàng thành công';
                $result->numberOfResult = count($data);
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message
                    = 'Lấy thông tin khách hàng không thành công';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function getOneFullInfo($user)
    {
        $result = new ResultObject();
        try {
            $data = User::findOrFail($user->id);
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'Lấy thông tin khách hàng thành công';
                $result->numberOfResult = count($data);
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message
                    = 'Lấy thông tin khách hàng không thành công';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function delete($user)
    {
        $result = new ResultObject();
        try {
            $data = User::destroy($user->id);
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'Xóa dữ liệu Khách hàng thành công';
                $result->numberOfResult = count($data);
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message
                    = 'Xóa dữ liệu khách hàng không thành công';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function checkEmail($email)
    {
        $result = new ResultObject();
        try {
            $data = User::where('email', $email)->first();
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'Tìm thấy email';
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'Không tìm thấy email';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function changeStatus($customerObj)
    {
        $res = new ResultObject();
        try {
            $thisUser = User::find($customerObj->id);
            if ($thisUser) {
                $thisUser->status = !$thisUser->status;
                if ($thisUser->save()) {
                    $res->messageCode = 1;
                    $res->message = 'Thay đổi trạng thái thành công';
                } else {
                    $res->messageCode = 0;
                    $res->message = 'Thay đổi trạng thái thành công';
                }
            } else {
                $res->messageCode = 0;
                $res->message = 'không tìm thấy dữ liệu phù hợp';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }

        return $res;
    }

    public function getNewCustomerInMonth($startDate = null, $endDate = null)
    {
        $now = Carbon::now();
        $month = $now->month;
        $res = new ResultObject();

        try {
            if($startDate!== null && $endDate !== null){
                $doGetNewCustomerInMonth = User::whereBetween('created_at',[$startDate,$endDate])->get();
            } else {
                $doGetNewCustomerInMonth = User::whereMonth('created_at', $month)->get();
            }

            if (sizeof($doGetNewCustomerInMonth)) {
                $res->messageCode = 1;
                $res->message = 'thành công';
                $res->result = $doGetNewCustomerInMonth;
                $res->numberOfResult = count($doGetNewCustomerInMonth);
            } else {
                $res->messageCode = 0;
                $res->message = 'That bai';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 1;
            $res->message = $exception->getMessage();
        }

        return $res;
    }

    public function getUnSeenUserList()
    {
        $res = new ResultObject;
        try {
            $doGetUnseenUserList = User::where('seen',0)->get();
            if (sizeof($doGetUnseenUserList)) {
                $res->result = $doGetUnseenUserList;
                $res->message = "thành công";
                $res->messageCode = 1;
                $res->numberOfResult = count($doGetUnseenUserList);
            } else {
                $res->message = "không có";
                $res->messageCode = 0;
            }
        } catch (\Exception $exception) {
            $res->message = "Lỗi code";
            $res->messageCode = 0;
        }

        return $res;
    }
    /**
     * Ham: lấy ra danh sách những user có sinh nhật là ngày hôm nay:
     */
    public function getListUserHaveBirthDayIsToDay(){
        $result = new ResultObject();
        $toDay = date('y-m-d');
        try{
            $doGetListUserHaveBirthDayIsToDay = User::whereDate('birthday',$toDay)->get();
            if(sizeof($doGetListUserHaveBirthDayIsToDay)){
                $result->message = "Danh sach chuc mung sinh nhat";
                $result->messageCode = 1;
                $result->result = $doGetListUserHaveBirthDayIsToDay;
            } else {
                $result->message = "Khong co ai trong danh sach";
                $result->messageCode = 0;
            }
        } catch (\Exception $exception){
            $result->message = "Co loi xay ra";
            $result->messageCode = 0;
        }
        return $result;
    }

}














