<?php


namespace App\Repositories;


use App\Models\E_Content;
use App\Objects\E_ContentObject;
use App\Objects\E_EmailObject;
use App\Objects\ResultObject;

class E_ContentRepositories
{
    private $model;

    public function __construct()
    {
        $this->model = new E_Content();
    }

    public function getListContent(
        $order = 'updated_at',
        $by = 'DESC',
        $limit = null,
        $offset = null
    ) {
        $res = new ResultObject();
        try {
            $doGetList = $this->model->orderBy($order, $by)->get();
            if (sizeof($doGetList)) {
                $res->messageCode = 1;
                $res->message = 'success';
                $res->result = $this->model->convertToArrayObject($doGetList);
            } else {
                $res->messageCode = 0;
                $res->message = 'fail';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }

        return $res;
    }


    public function getContentById($id)
    {
        $res = new ResultObject();
        try {
            $doGetContentById = $this->model->find($id);
            if ($doGetContentById) {
                $res->messageCode = 1;
                $res->message = 'success';
                $res->result = $this->model->convertToObject($doGetContentById);
            } else {
                $res->messageCode = 0;
                $res->message = 'fail';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }

        return $res;
    }

    public function addNewEmail(E_ContentObject $contentObject)
    {
        $res = new ResultObject();
        $newContent = new E_Content();

        $newContent->id = $contentObject->id;
        $newContent->title = $contentObject->title;
        $newContent->subject = $contentObject->subject;
        $newContent->content = $contentObject->content;
        $newContent->status = $contentObject->status;
        $newContent->from_email = $contentObject->fromEmail;
        $newContent->from_name = $contentObject->fromName;
        $newContent->reply_email = $contentObject->replyEmail;
        $newContent->created_at = $contentObject->createdAt;
        $newContent->updated_at = $contentObject->updateAt;
        try {
            if ($newContent->save()) {
                $res->messageCode = 1;
                $res->message = 'success';
                $res->result = $this->model->convertToObject($newContent);
            } else {
                $res->messageCode = 0;
                $res->message = 'fail';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }

        return $res;
    }

    public function updateEmail(E_ContentObject $contentObject)
    {
        $res = new ResultObject();
        $thisContent = $this->model->find($contentObject->id);

        if ($thisContent) {
            if ($contentObject->id) {
                $thisContent->id = $contentObject->id;
            }
            if ($contentObject->title) {
                $thisContent->title = $contentObject->title;
            }
            if ($contentObject->subject) {
                $thisContent->subject = $contentObject->subject;
            }
            if ($contentObject->createdAt) {
                $thisContent->created_at = $contentObject->createdAt;
            }
            if ($contentObject->updateAt) {
                $thisContent->updated_at = $contentObject->updateAt;
            }
            if ($contentObject->content) {
                $thisContent->content = $contentObject->content;
            }
            if ($contentObject->status) {
                $thisContent->status = $contentObject->status;
            }
            if ($contentObject->fromEmail) {
                $thisContent->from_email = $contentObject->fromEmail;
            }
            if ($contentObject->fromName) {
                $thisContent->from_name = $contentObject->fromName;
            }
            if ($contentObject->replyEmail) {
                $thisContent->reply_email = $contentObject->replyEmail;
            }
            try {
                if ($thisContent->save()) {
                    $res->messageCode = 1;
                    $res->message = 'success';
                    $res->result = $this->model->convertToObject($thisContent);
                } else {
                    $res->messageCode = 0;
                    $res->message = 'fail';
                }
            } catch (\Exception $exception) {
                $res->messageCode = 0;
                $res->message = $exception->getMessage();
            }
        } else {
            $res->messageCode = 0;
            $res->message = 'data not found';
        }

        return $res;
    }

    public function deleteContent($id)
    {
        $res = new ResultObject();
        try {
            $doDeleteContent = $this->model->destroy($id);
            if ($doDeleteContent) {
                $res->messageCode = 1;
                $res->message = 'success';
            } else {
                $res->messageCode = 0;
                $res->message = 'fail';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }

        return $res;
    }
}