<?php

namespace App\Repositories;

use App\Models\Service;
use App\Objects\OrderObject;
use App\Objects\ResultObject;
use App\Models\Order;
use Carbon\Carbon;

class OrderRepositories
{
    public function getList($limit = 0)
    {
        $result = new ResultObject();
        try {

            if ($limit == 0) {
                $data = Order::orderBy('created_at', 'DESC')->all();
            } else {
                $data = Order::orderBy('created_at', 'DESC')->paginate($limit);
            }
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'Get list Order successfully!';
//                $result->numberOfResult = count($data);
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'Fail to Get list Order';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function add(OrderObject $order)
    {
        $newOrder = new Order();

        if (isset($order->id) && $order->id) {
            $newOrder->id = $order->id;
        } else {
            $newOrder->id = null;
        }
        if (isset($order->user_id) && $order->user_id) {
            $newOrder->user_id = $order->user_id;
        } else {
            $newOrder->user_id = null;
        }
        if (isset($order->service_id) && $order->service_id) {
            $newOrder->service_id = $order->service_id;
        } else {
            $newOrder->service_id = null;
        }
        if (isset($order->note) && $order->note) {
            $newOrder->note = $order->note;
        } else {
            $newOrder->note = null;
        }
        if (isset($order->username) && $order->username) {
            $newOrder->username = $order->username;
        } else {
            $newOrder->username = null;
        }
        if (isset($order->phone) && $order->phone) {
            $newOrder->phone = $order->phone;
        } else {
            $newOrder->phone = null;
        }
        if (isset($order->email) && $order->email) {
            $newOrder->email = $order->email;
        } else {
            $newOrder->email = null;
        }
        if (isset($order->address) && $order->address) {
            $newOrder->address = $order->address;
        } else {
            $newOrder->address = null;
        }
        if (isset($order->status) && $order->status) {
            $newOrder->status = 1;
        } else {
            $newOrder->status = 0;
        }

        $newOrder->total_price = $order->totalPrice;
        $newOrder->paid_money = $order->paidMoney;
        $newOrder->missing_money = $order->missingMoney;
        $newOrder->seen = $order->seen;
        $newOrder->updated_at = $order->updated_at;
        $newOrder->created_at = $order->created_at;

        $result = new ResultObject();
        try {
            $data = $newOrder->save();
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'Add new Order successfully!';
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'Fail to add new Order';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function update($order)
    {
        $thisOder = Order::findOrFail($order->id);

        if (isset($order->id) && $order->id) {
            $thisOder->id = $order->id;
        }
        if (isset($order->user_id) && $order->user_id) {
            $thisOder->user_id = $order->user_id;
        }
        if (isset($order->service_id) && $order->service_id) {
            $thisOder->service_id = $order->service_id;
        }
        if (isset($order->note) && $order->note) {
            $thisOder->note = $order->note;
        }
        if (isset($order->phone) && $order->phone) {
            $thisOder->phone = $order->phone;
        }
        if (isset($order->email) && $order->email) {
            $thisOder->email = $order->email;
        }
        if (isset($order->address) && $order->address) {
            $thisOder->address = $order->address;
        }
        if (isset($order->status) && $order->status) {
            $thisOder->status = 1;
        }
        if (isset($order->updated_at) && $order->updated_at) {
            $thisOder->updated_at = $order->updated_at;
        }
        if (isset($order->created_at) && $order->created_at) {
            $thisOder->created_at = $order->created_at;
        }
        if (isset($order->startDate) && $order->startDate) {
            $thisOder->start_date = $order->startDate;
        }
        if (isset($order->endDate) && $order->endDate) {
            $thisOder->end_date = $order->endDate;
        }
        if (isset($order->seen) && $order->seen) {
            $thisOder->seen = $order->seen;
        }
        if (isset($order->paidMoney) && $order->paidMoney) {
            $thisOder->paid_money = $order->paidMoney;
        }
        if (isset($order->missingMoney) && $order->missingMoney  !== null) {
            $thisOder->missing_money = $order->missingMoney;
        }
        if (isset($order->totalPrice) && $order->totalPrice) {
            $thisOder->total_price = $order->totalPrice;
        }
        if (isset($order->seen) && $order->seen) {
            $thisOder->seen = $order->seen;
        }
        $result = new ResultObject();
        try {
            $data = $thisOder->save();
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'Update Order successfully!';
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'Fail to update Order';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function getOneById($id)
    {
        $result = new ResultObject();
        try {
            $data = Order::findOrFail($id);
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'Get this Order successfully!';
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'Fail to Get this Order';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage().'-'
                .$exception->getFile().'-'.$exception->getline();
        }

        return $result;
    }

    public function getOneFullInfo($order)
    {
        $result = new ResultObject();
        try {
            $data = Order::findOrFail($order->id);
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'lấy dữ liệu thành công';
                $result->numberOfResult = count($data);
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'lấy dữ liệu không thành công';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function delete($order)
    {
        $result = new ResultObject();
        try {
            $data = Order::destroy($order->id);
            if ($data) {
                $result->messageCode = 1;
                $result->message = 'Xóa thành công';
                $result->numberOfResult = count($data);
                $result->result = $data;
            } else {
                $result->messageCode = 0;
                $result->message = 'Xóa không thành công';
            }
        } catch (\Exception $exception) {
            $result->messageCode = 0;
            $result->message = $exception->getMessage();
        }

        return $result;
    }

    public function getAllOrderOfUser($userId, $limit = 0)
    {
        $res = new ResultObject();
        try {
            if ($limit == 0) {
                $getAllOrderOfUser = Order::where('user_id', $userId)->get();
            } else {
                $getAllOrderOfUser = Order::where('user_id', $userId)
                    ->paginate($limit);
            }
            if (sizeof($getAllOrderOfUser)) {
                $res->messageCode = 1;
                $res->message = 'Lấy dữ liệu thành công';
                $res->result = $getAllOrderOfUser;
            } else {
                $res->messageCode = 0;
                $res->message = 'Lấy dữ liệu không thành công';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }

        return $res;
    }

    public function getOneOrderOfUser($orderObj)
    {
        $res = new ResultObject();
        try {
            $getOneOrderOfUser = Order::where('user_id', $orderObj->userId)
                ->where('id', $orderObj->id)->first();
            if (sizeof(getOneOrderOfUser)) {
                $res->messageCode = 1;
                $res->message = 'Lấy dữ liệu thành công';
                $res->result = $getOneOrderOfUser;
            } else {
                $res->messageCode = 0;
                $res->message = 'Lấy dữ liệu không thành công';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }

        return $res;
    }

    public function deleteOrderOfUser($orderObj)
    {
        $res = new ResultObject();
        try {
            $thisOrder = Order::where('id', $orderObj->id)->first();
            if ($thisOrder) {
                $doDeleteOrder = Order::where('user_id', $orderObj->userId)
                    ->first();
                if ($doDeleteOrder) {
                    $res->messageCode = 1;
                    $res->message = 'Lấy dữ liệu thành công';
                    $res->result = $doDeleteOrder;
                } else {
                    $res->messageCode = 0;
                    $res->message = 'Lấy dữ liệu không thành công';
                }
            } else {
                $res->messageCode = 0;
                $res->message = 'lỗi hệ thống';
            }

        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }

        return $res;
    }

    public function changeStatus($orderObj)
    {
        $res = new ResultObject();
        try {
            $thisOrrder = Order::find($orderObj->id);
            if ($thisOrrder) {
                $thisOrrder->status = !$thisOrrder->status;
                $thisOrrder->end_date = $thisOrrder->end_date;
                $thisOrrder->start_date = $thisOrrder->start_date;

                if ($thisOrrder->save()) {
                    $res->messageCode = 1;
                    $res->result = $thisOrrder;
                    $res->message = 'Thay đổi trạng thái thành công';
                } else {
                    $res->messageCode = 0;
                    $res->message = 'chưa thay đổi được trạng thái';
                }
            } else {
                $res->messageCode = 0;
                $res->message = 'không tìm thấy dữ liệu phù hợp';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }

        return $res;
    }

    /*Lấy ra toàn bộ giao dịch trong 1 tháng:*/
    public function getAllOrderInMonth($startDate = null, $endDate = null)
    {
        $now = Carbon::now();
        $month = $now->month;

        $res = new ResultObject();
        try {
            if($startDate!== null && $endDate !== null){
                $doGetAllOrderInMonth = Order::whereBetween('created_at',[$startDate,$endDate])->get();
            } else {
                $doGetAllOrderInMonth = Order::whereMonth('created_at', $month)->get();
            }

            if (sizeof($doGetAllOrderInMonth)) {
                foreach ($doGetAllOrderInMonth as $order) {
                    /*lấy ra giá sản phẩm , khi biết service_id*/
                    $thisService = Service::find($order->service_id);
                    if ($thisService) {
                        $order->price = $thisService->price;
                    }
                }
                $res->messageCode = 1;
                $res->message = 'Lấy dữ liệu thành công';
                $res->result = $doGetAllOrderInMonth;
            } else {
                $res->messageCode = 0;
                $res->message = 'Lấy dữ liệu không thành công';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }

        return $res;
    }

    public function getAllOrderOfService($serviceId, $startDate = null, $endDate = null)
    {
        $now = Carbon::now();
        $month = $now->month;

        $res = new ResultObject();
        try {
            if($startDate!== null && $endDate !== null){
                $doGetAllOrderOfAService = Order::where('service_id','like', '%'.$serviceId.'%')
                    ->whereBetween('created_at',[$startDate,$endDate])
                    ->get();
            } else {
                $doGetAllOrderOfAService = Order::where('service_id','like', '%'.$serviceId.'%')
                    ->whereMonth('created_at', $month)
                    ->get();
            }

            if (sizeof($doGetAllOrderOfAService)) {
                $res->messageCode = 1;
                $res->message = 'Lấy dữ liệu thành công';
                $res->result = $doGetAllOrderOfAService;
            } else {
                $res->messageCode = 0;
                $res->message = 'không có dữ liệu';
                $res->result = $doGetAllOrderOfAService;
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = $exception->getMessage();
        }

        return $res;
    }

    /*Lấy danh sách các đơn hàng, chuẩn bị hêt hạn(chỉ còn 3 ngày nữa là hết hạn)*/
    public function getIsAboutToExpireOrderList()
    {
        $res = new ResultObject;
        $afferToDayThreeDay = date('Y-m-d', strtotime(date('Y-m-d')) + 259200);
        try {
            $doGetIsAboutToExpireOrderList = Order::whereDate('end_date',
                $afferToDayThreeDay)->get();
            if (sizeof($doGetIsAboutToExpireOrderList)) {
                $res->result = $doGetIsAboutToExpireOrderList;
                $res->message = "thành công";
                $res->messageCode = 1;
            } else {
                $res->message = "không có";
                $res->messageCode = 0;
            }
        } catch (\Exception $exception) {
            $res->message = "Lỗi code";
            $res->messageCode = 0;
        }

        return $res;
    }

    /*Lấy những đơn hàng trạng thái chưa dc xem*/
    public function getUnseenOrderList()
    {
        $res = new ResultObject;
        try {
            $doGetUnseenOrderList = Order::where('seen',0)->get();
            if (sizeof($doGetUnseenOrderList)) {
                $res->result = $doGetUnseenOrderList;
                $res->message = "thành công";
                $res->messageCode = 1;
                $res->numberOfResult = count($doGetUnseenOrderList);
            } else {
                $res->message = "không có";
                $res->messageCode = 0;
            }
        } catch (\Exception $exception) {
            $res->message = "Lỗi code";
            $res->messageCode = 0;
        }

        return $res;
    }
}