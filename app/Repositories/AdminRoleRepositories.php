<?php


namespace App\Repositories;


use App\Models\Admin_Role;
use App\Objects\AdminRoleObject;
use App\Objects\ResultObject;

class AdminRoleRepositories
{
    private $adminRoleModel;

    public function __construct(Admin_Role $adminRole)
    {
        $this->adminRoleModel = $adminRole;
    }

    public function getAllRoleByAdminId($adminId)
    {
        $res = new ResultObject();
        try {
            $doGetAllRole = $this->adminRoleModel->where('admin_id', $adminId)
                ->get();
            if (sizeof($doGetAllRole)) {
                $res->messageCode = 1;
                $res->message = 'Success';
                $res->result = $doGetAllRole;
            } else {
                $res->messageCode = 0;
                $res->message = 'none';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = 'Exception';
        }

        return $res;
    }

    public function addNewAdminRole(AdminRoleObject $adminRoleObj)
    {
        $res = new ResultObject();
        $newAdminRole = new Admin_Role();
        $newAdminRole->admin_id = $adminRoleObj->adminId;
        $newAdminRole->role_id = $adminRoleObj->roleId;
        try {
            if ($newAdminRole->save()) {
                $res->messageCode = 1;
                $res->message = 'Success';
                $res->result = $newAdminRole;
            } else {
                $res->messageCode = 0;
                $res->message = 'none';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = 'Exception';
        }

        return $res;
    }
    public function deleteAllAdminRole(AdminRoleObject $adminRoleObj)
    {
        $res = new ResultObject();
        try {
            $doDeleteAdminRole = $this->adminRoleModel->where('admin_id',$adminRoleObj->adminId)->delete();
            if ($doDeleteAdminRole) {
                $res->messageCode = 1;
                $res->message = 'Success';
            } else {
                $res->messageCode = 0;
                $res->message = 'none';
            }
        } catch (\Exception $exception) {
            $res->messageCode = 0;
            $res->message = 'Exception';
        }

        return $res;
    }
}