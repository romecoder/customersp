<?php


namespace App\Objects;


class DetailSupportObject
{
    public $id;
    public $supportId;
    public $createAt;
    public $content;
    public $author;
    public $seen;
}