<?php

namespace App\Objects;


class E_ContentObject
{
    public $id;

    public $title;

    public $subject;

    public $content;

    public $status;

    public $fromEmail;

    public $fromName;

    public $replyEmail;

    public $createdAt;

    public $updateAt;
}