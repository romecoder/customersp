<?php


namespace App\Objects;


class PostObject
{
    public $id;

    public $tittle;

    public $content;

    public $status;

    public $createdAt;

    public $updatedAt;

    public $categoryId;

    public $slug;

    public $authorId;

    public $image;
}