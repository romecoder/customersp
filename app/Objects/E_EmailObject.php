<?php

namespace App\Objects;


class E_EmailObject
{
    public $id;

    public $email;

    public $groupId;

    public $createdAt;

    public $updateAt;

    public $status;

    public $type;

    public $name;
}