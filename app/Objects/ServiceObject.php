<?php
/**
 * Created by PhpStorm.
 * User: ROME
 * Date: 5/7/2017
 * Time: 10:20 AM
 */

namespace App\Objects;


class ServiceObject
{
    public $id;
    public $name;
    public $status;
    public $created_at;
    public $updated_at;
    public $typeId;
    public $content;
    public $thumbnail;
    public $price;
}