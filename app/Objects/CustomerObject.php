<?php

namespace App\Objects;


class CustomerObject
{
    public $id;

    public $name;

    public $password;

    public $avatar;

    public $gender;

    public $role;

    public $email;

    public $phone;

    public $address;

    public $status;

    public $updated_at;

    public $created_at;

    public $content;

    public $agreeTerm;

    public $seen;

    public $birthday;

    public $fullName;
}