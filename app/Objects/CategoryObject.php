<?php


namespace App\Objects;


class CategoryObject
{
    public $id;

    public $name;

    public $createdAt;

    public $updatedAt;

    public $status;
}