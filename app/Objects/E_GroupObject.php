<?php

namespace App\Objects;


class E_GroupObject
{
    public $id;

    public $name;

    public $status;

    public $emailList;

    public $numberEmail;

    public $createdAt;

    public $updateAt;
    
}