<?php
/**
 * Created by PhpStorm.
 * User: ROME
 * Date: 5/7/2017
 * Time: 10:19 AM
 */

namespace App\Objects;


class AdminObject
{
    public $id;
    public $name;
    public $password;
    public $avatar;
    public $gender;
    public $role;
    public $email;
    public $phone;
    public $address;
    public $status;
    public $content;
    public $updated_at;
    public $created_at;
    public $fullName;
}