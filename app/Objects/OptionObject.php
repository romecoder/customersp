<?php


namespace App\Objects;


class OptionObject
{
    public $id;

    public $type;

    public $elementId;

    public $key;

    public $value;

    public $createdAt;

    public $user;
}