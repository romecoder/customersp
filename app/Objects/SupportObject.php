<?php
/**
 * Created by PhpStorm.
 * User: ROME
 * Date: 5/7/2017
 * Time: 10:20 AM
 */

namespace App\Objects;


class SupportObject
{
    public $id;
    public $name;
    public $status;
    public $createdAt;
    public $updatedAt;
    public $userId;
    public $adminId;
    public $serviceId;
    public $display;

}