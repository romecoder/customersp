<?php


namespace App\Objects;


class SearchResult
{
    public $title;

    public $link;

    public $img;

    public $type;

    public $date;

}