<?php


namespace App\Objects;


class TypeObject
{
    public $id;

    public $name;

    public $status;

    public $createAt;

    public $updatedAt;

    public $time;
}