<?php

namespace App\Objects;


class OrderObject
{
    public $id;

    public $user_id;

    public $status;

    public $created_at;

    public $updated_at;

    public $service_id;

    public $note;

    public $phone;

    public $email;

    public $address;

    public $username;

    public $startDate;

    public $endDate ;

    public $price;

    public $seen;

    public $totalPrice;

    public $missingMoney;

    public $paidMoney;

}