<?php


namespace App\Objects;


class E_CampaignObject
{
    public $id;

    public $groupId;

    public $content;

    public $name;

    public $numberEmail;

    public $emailList;

    public $status;

    public $createdAt;

    public $updatedAt;


}