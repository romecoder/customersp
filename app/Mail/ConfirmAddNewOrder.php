<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConfirmAddNewOrder extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $name;
    private $service;

    public function __construct($name, $service)
    {
        $this->name = $name;
        $this->service = $service;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Email.addService')->with([
            'name' => $this->name,
            'service' => $this->service
        ])
            ->subject('Đăng ký dịch vụ thành công','Hệ thống Pveser');
    }
}
