<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConfirmAddNewOrderToAdmin extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $name;
    private $service;

    public function __construct($name, $service)
    {
        $this->name = $name;
        $this->service = $service;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Email.addServiceSendToAdmin')->with([
            'name' => $this->name,
            'service' => $this->service
        ])->subject('Khách hàng đăng ký sử dụng dịch vụ','Hệ thống Pveser');
    }
}
