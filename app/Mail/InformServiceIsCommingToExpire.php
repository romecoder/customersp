<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InformServiceIsCommingToExpire extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $name;
    private $service;
    private $endDate;

    public function __construct($name, $service,$endDate)
    {
        $this->name = $name;
        $this->endDate= $endDate;
        $this->service = $service;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Email.InformServiceIscommingToExpire')->with([
            'name' => $this->name,
            'service' => $this->service,
            'endDate' => $this->endDate
        ])
            ->subject('Dịch vụ '.$this->service.' sắp hết hạn','Hệ thống Pveser');
    }
}
