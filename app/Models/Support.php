<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Support extends Model
{
    //
    protected $table = 'support';

    protected $fillable
        = [
            'id',
            'name',
            'status',
            'created_at',
            'updated_at',
            'user_id',
            'admin_id',
            'service_id',
            'content_id',
        ];
    public $timestamps = false;
    public function admin()
    {
        return $this->belongsTo('App\Models\Admin');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function service()
    {
        return $this->belongsTo('App\Models\Service');
    }


}
