<?php

namespace App\Models;

use App\Objects\E_EmailObject;
use Illuminate\Database\Eloquent\Model;

class E_Email extends Model
{
    protected $table = 'e_email';
    public $timestamps = false;

    public function convertToObject($databaseObject = null)
    {
        $emailObject = new E_EmailObject();
        if($databaseObject){
            $emailObject->id = $databaseObject->id;
            $emailObject->email = $databaseObject->email;
            $emailObject->status = $databaseObject->status;
            $emailObject->groupId = $databaseObject->group_id;
            $emailObject->type = $databaseObject->type;
            $emailObject->createdAt = $databaseObject->created_at;
            $emailObject->updateAt = $databaseObject->update_at ;
            $emailObject->name = $databaseObject->name;
        } else {
            $emailObject->id = $this->id;
            $emailObject->email = $this->email;
            $emailObject->status = $this->status;
            $emailObject->groupId = $this->group_id;
            $emailObject->type = $this->type;
            $emailObject->createdAt = $this->created_at;
            $emailObject->updateAt = $this->update_at ;
            $emailObject->name = $this->name ;
        }
        return $emailObject;
    }

    public function convertToArrayObject($arrayDatabaseObject)
    {
        $returnArray = [];
        foreach ($arrayDatabaseObject as $databaseObj)
        {
            $standardObj = $this->convertToObject($databaseObj);
            $returnArray[] = $standardObj ;
        }

        return $returnArray;
    }

}
