<?php

namespace App\Models;

use App\Objects\E_GroupObject;
use Illuminate\Database\Eloquent\Model;

class E_Group extends Model
{
    protected $table = 'e_group';
    public $timestamps = false;

    public function convertToObject($databaseObject = null)
    {
        $group = new E_GroupObject();
        if($databaseObject){
            $group->id = $databaseObject->id;
            $group->status = $databaseObject->status;
            $group->createdAt = $databaseObject->created_at ;
            $group->updateAt = $databaseObject->update_at ;
            $group->name = $databaseObject->name;
        } else {
            $group->id = $this->id;
            $group->status = $this->status;
            $group->createdAt = $this->created_at ;
            $group->updateAt = $this->update_at ;
            $group->name = $this->name;
        }
        return $group;
    }

    public function convertToArrayObject($arrayDatabaseObject)
    {
        $returnArray = [];
        foreach ($arrayDatabaseObject as $databaseObj)
        {
            $standardObj = $this->convertToObject($databaseObj);
            $returnArray[] = $standardObj ;
        }

        return $returnArray;
    }
}
