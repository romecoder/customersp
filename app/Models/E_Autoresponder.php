<?php

namespace App\Models;

use App\Objects\E_AutoResponderObject;
use Illuminate\Database\Eloquent\Model;

class E_Autoresponder extends Model
{
    protected $table = 'e_autoresponder';
    public $timestamps = false;

    public function convertToObject($databaseObject = null)
    {
        $autoResponderObject = new E_AutoResponderObject();
        if ($databaseObject) {
            $autoResponderObject->id = $databaseObject->id;
        } else {
            $autoResponderObject->id = $this->id;
        }

        return $autoResponderObject;
    }

    public function convertToArrayObject($arrayDatabaseObject)
    {
        $returnArray = [];
        foreach ($arrayDatabaseObject as $databaseObj)
        {
            $standardObj = $this->convertToObject($databaseObj);
            $returnArray[] = $standardObj ;
        }

        return $returnArray;
    }
}
