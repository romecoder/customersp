<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class E_EmailGroup extends Model
{
    protected $table = 'e_email_group' ;

    public $timestamps = false;

    public function convertToObject($databaseObject = null)
    {
        $emailObject = new E_Email();
        if($databaseObject){
            $emailObject->id = $databaseObject->id;
            $emailObject->groupId = $databaseObject->group_id;
            $emailObject->emailId = $databaseObject->email_id;
        } else {
            $emailObject->id = $this->id;
            $emailObject->groupId = $this->group_id;
            $emailObject->emailId= $this->email_id;
        }
        return $emailObject;
    }

    public function convertToArrayObject($arrayDatabaseObject)
    {
        $returnArray = [];
        foreach ($arrayDatabaseObject as $databaseObj)
        {
            $standardObj = $this->convertToObject($databaseObj);
            $returnArray[] = $standardObj ;
        }

        return $returnArray;
    }
}
