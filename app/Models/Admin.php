<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    protected $table = 'admin';
    protected $fillable
        = [
            'id',
            'name',
            'password',
            'avatar',
            'role',
            'email',
            'phone',
            'address',
            'gender',
            'updated_at',
            'created_at',
            'status',
            'active',
            'content'
        ];
    public $timestamps = false;

    public function support()
    {
        return $this->belongsToMany('App\Models\User','support');
    }
}
