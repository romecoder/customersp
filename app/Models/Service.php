<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    //
    protected $table = 'service';

    protected $fillable
        = [
            'id',
            'name',
            'status',
            'created_at',
            'updated_at',
            'type',
            'thumbnail',
            'content',
            'price',
        ];
    public $timestamps = false;

    public function order()
    {
        return $this->hasMany('App\Models\order');
    }
    public function support()
    {
        return $this->hasMany('App\Models\support');
    }
    public function service()
    {
        return $this->belongsToMany('App\Models\User','User_Service');
    }

}
