<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'user';
    protected $fillable
        = [
            'id',
            'name',
            'email',
            'password',
            'phone',
            'address',
            'avatar',
            'created_at',
            'updated_at',
            'gender',
            'content',
            'status',
            'active',
            'full_name'
        ];
    public $timestamps = false;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden
        = [
            'password',
            'remember_token',
        ];

    public function order()
    {
        return $this->hasMany('App\Models\Order');
    }
    public function support()
    {
        return $this->belongsToMany('App\Models\Admin','support');
    }
    public function service()
    {
        return $this->hasMany('App\Models\Service','User_Service');
    }
}
