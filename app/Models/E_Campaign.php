<?php

namespace App\Models;

use App\Objects\E_CampaignObject;
use Illuminate\Database\Eloquent\Model;

class E_Campaign extends Model
{
    protected $table = 'e_campaign';
    public $timestamps = false;

    public function convertToObject($databaseObject = null)
    {
        $campaignObj = new E_CampaignObject();
        if ($databaseObject) {
            $campaignObj->id = $databaseObject->id;
            $campaignObj->groupId = $databaseObject->group_id;
            $campaignObj->content= $databaseObject->content;
            $campaignObj->name= $databaseObject->name;
            $campaignObj->status= $databaseObject->status;
            $campaignObj->createdAt = $databaseObject->created_at ;
            $campaignObj->updatedAt = $databaseObject->updated_at;
        } else {
            $campaignObj->id = $this->id;
            $campaignObj->groupId = $this->group_id;
            $campaignObj->content= $this->content;
            $campaignObj->name= $this->name;
            $campaignObj->status= $this->status;
            $campaignObj->createdAt = $this->created_at ;
            $campaignObj->updatedAt = $this->updated_at;
        }

        return $campaignObj;
    }

    public function convertToArrayObject($arrayDatabaseObject)
    {
        $returnArray = [];
        foreach ($arrayDatabaseObject as $databaseObj)
        {
            $standardObj = $this->convertToObject($databaseObj);
            $returnArray[] = $standardObj ;
        }

        return $returnArray;
    }
}
