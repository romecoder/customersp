<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailSupport extends Model
{
    protected $table = 'detail_support';
    public $timestamps = false;

    public function support()
    {
        return $this->belongsToMany('App\Models\Support', 'support');
    }
}
