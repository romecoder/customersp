<?php

namespace App\Models;

use App\Objects\E_ContentObject;
use Illuminate\Database\Eloquent\Model;

class E_Content extends Model
{
    protected $table = 'e_content';
    public $timestamps = false;

    public function converToObject($databaseObject = null)
    {
        $content = new E_ContentObject();
        if($databaseObject){
            $content ->id = $databaseObject->id;
            $content ->title = $databaseObject->title;
            $content ->subject = $databaseObject->subject ;
            $content ->content = $databaseObject->content;
            $content ->createdAt = $databaseObject->created_at;
            $content ->updateAt = $databaseObject->update_at ;
            $content ->status = $databaseObject->status;
            $content ->fromEmail = $databaseObject->from_email;
            $content ->fromName = $databaseObject->from_name ;
            $content ->replyMail = $databaseObject->reply_email;
        } else {
            $content ->id = $this->id;
            $content ->title = $this->title;
            $content ->subject = $this->subject ;
            $content ->content = $this->content;
            $content ->createdAt = $this->created_at;
            $content ->updateAt = $this->update_at ;
            $content ->status = $this->status;
            $content ->fromEmail = $this->from_email;
            $content ->fromName = $this->from_name ;
            $content ->replyMail = $this->reply_email;
        }
        return $content;
    }
    public function convertToArrayObject($arrayDatabaseObject)
    {
        $returnArray = [];
        foreach ($arrayDatabaseObject as $databaseObj)
        {
            $standardObj = $this->convertToObject($databaseObj);
            $returnArray[] = $standardObj ;
        }

        return $returnArray;
    }
}
