<?php

namespace App\Console\Commands;

use App\Mail\ConfirmCreateAccountSuccess;
use Illuminate\Console\Command;
use App\Mail\InformServiceIsCommingToExpire;
use App\Repositories\OrderRepositories;
use App\Repositories\ServiceRepositories;
use App\Repositories\UserRepositories;
use Illuminate\Support\Facades\Mail;

class InformServiceIsCommingToExpireCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'informServiceExpire:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email inform service is comming to expire';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    private $orderRepo;
    private $userRepo;
    private $serviceRepo;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->orderRepo = new OrderRepositories();
        $listOrders = $this->orderRepo->getIsAboutToExpireOrderList();

        if ($listOrders->messageCode == 1) {
            $emailList = [];
            foreach ($listOrders->result as $order) {
                array_push($emailList,$order->email);
            }
            /*lấy ra list email:*/
            foreach ($listOrders->result as $order) {
                /*Lấy thông tin dịch vụ:*/
                $this->serviceRepo = new ServiceRepositories();
                $doGetServiceInfo
                    = $this->serviceRepo->getOneById($order->service_id);
                /*lấy thông tin người dùng*/
                $this->userRepo = new UserRepositories();
                $doGetUserInfo = $this->userRepo->getOneById($order->user_id);
                if ($doGetUserInfo->messageCode == 1) {
                    /*Gửi luôn Email tới người sắp hết hạn sử dụng dv*/
                    try {
                        $data = [
                            'name' => $doGetUserInfo->result->name,
                            'endDate' => $order->end_date,
                            'service' => $doGetServiceInfo->result->name
                        ];

                        Mail::send('Email.InformServiceIscommingToExpire',
                            $data, function ($msg) use ($emailList) {
                                $msg->from('notification@mailgun.yournews.romecody.com',
                                    'Hệ thống Pveser thông báo');
                                $msg->to($emailList)
                                    ->subject('Dịch vụ sắp hết hạn.');
                            });
                    } catch (\Exception $exception) {
                        Mail::send('Email.InformServiceIscommingToExpire',
                            $data, function ($msg) use ($emailList) {
                                $msg->from('notification@mailgun.yournews.romecody.com',
                                    'Hệ thống Pveser thông báo');
                                $msg->to('romecoder@gmail.com')
                                    ->subject('Gửi email Dịch vụ sắp hết hạn lỗi.');
                            });
                    }
                }
            }
        } else {
            dd('Cannot find any order is about to expire');
        }
    }
}
