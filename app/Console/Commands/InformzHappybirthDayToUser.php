<?php

namespace App\Console\Commands;

use App\Repositories\UserRepositories;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class InformzHappybirthDayToUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'informHappyBirthday:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'tu dong gui thu chuc mung sinh nhat';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    private $userRepo;
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /**
         * Ham: lấy ra danh sách những user có sinh nhật là ngày hôm nay:
         */
        $this->userRepo = new UserRepositories();
        $doGetListHappyBirthday = $this->userRepo->getListUserHaveBirthDayIsToDay();
        if($doGetListHappyBirthday->messageCode == 1){
            $emailList = [];
            foreach ($doGetListHappyBirthday->result as $user){
                $emailList[] = $user->email;
            }
            foreach ($doGetListHappyBirthday->result as $user) {
                try{
                    $data = [
                        'name' => $user->name,
                    ];
                    Mail::send('Email.InformHappyBirthDay',
                        $data, function ($msg) use ($emailList) {
                            $msg->from('notification@mailgun.yournews.romecody.com',
                                'Pveser Happy birthday to you!!');
                            $msg->to($emailList)
                                ->subject('Chúc Mừng Sinh Nhật.');
                        });
                }catch (\Exception $exception){
                    $data = [
                        'name' => $user->name,
                    ];
                    Mail::send('Email.InformHappyBirthDay',
                        $data, function ($msg) use ($emailList) {
                            $msg->from('notification@mailgun.yournews.romecody.com',
                                'Hệ thống Pveser thông báo');
                            $msg->to('romecoder@gmail.com')
                                ->subject('Lỗi phần gửi mail tự động chúc mừng sinh nhật');
                        });
                }
            }
        } else {
            dd("khong co nguoi dung nao co sinh nhat ngay hom nay");
        }
    }
}
