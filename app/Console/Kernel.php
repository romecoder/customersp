<?php

namespace App\Console;


use App\Console\Commands\InformServiceIsCommingToExpireCommand;
use App\Console\Commands\InformzHappybirthDayToUser;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands
        = [
            InformServiceIsCommingToExpireCommand::class,
            InformzHappybirthDayToUser::class,
        ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command(InformServiceIsCommingToExpireCommand::class)->dailyAt('09:00');
        $schedule->command(InformzHappybirthDayToUser::class)->dailyAt('06:00');
//        $schedule->call(function () {
//            Log::info("test schedule");
//        })->everyMinute();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
